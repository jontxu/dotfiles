# Run the system-wide support stuff
. $GLOBALAUTOSTART

# Programs to launch at startup
nitrogen --restore &

# Programs that will run after Openbox has started
(sleep 2 && nitrogen --restore) &
(sleep 2 && tint2) &
(sleep 3 && conky -q) &
dropbox &
