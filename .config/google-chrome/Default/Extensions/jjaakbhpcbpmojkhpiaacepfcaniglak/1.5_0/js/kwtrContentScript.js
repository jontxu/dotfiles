(function($, window, undefined) {
    var kloutIcon16URL = chrome.extension.getURL("icons/klout-16.png");
    var kloutIcon18URL = chrome.extension.getURL("icons/klout-18.png");

    var scoreTemplate = _.template(
        '<a class="klout-link" href="http://klout.com/<%= username %>">' +
        '<span class="klout-score-span">'                                +
        '<span class="klout-icon-span">'                                 +
        '<img class="klout-icon" alt="Tiny Klout Flag" src="' + '<%= kloutIconURL %>' + '" />' +
        '</span>'                                                        +
        '<%= score %>'                                                   +
        '</span>'                                                        +
        '</a>'
    );

    var fetchScores = function(screenNames, fn) {
        chrome.extension.sendRequest({ action: 'fetchScore', screenNames: screenNames }, fn);
    };

    var extractScreenName = {
        // @deprecated '.user-details': function(idx, el) { return $('.user-screen-name', el); },
        // @depreated '.user-content-rest': function(idx, el) { return $('a', el); },
        '.js-stream-tweet':   function(idx, el) {
            var old = $('a.tweet-screen-name', el);
            if (old && old.length > 0) {
                // "Old" Twitter selector.
                return old;
            } else {
                // New-new Twitter "Fly" selector.
                var fly = $('.fullname', el);
                // Remap function of the node which has the text value we want (Screen-name.)
                fly.text = function() { return $('.username b', el).text() };
                return fly;
            }
        }
    };

    var visibleScreenNames = function() {
        return _.uniq(_.flatten(_.map(_.keys(extractScreenName), function(selector) {
            return _.toArray($(selector).map(function (idx, el) {
                return extractScreenName[selector](idx, el).text();
            }));
        })));
    };

    var foreachVisibleScreenName = function(fn) {
        _.each(_.keys(extractScreenName), function(selector) {
            $(selector).each(function(idx, el) {
                fn(extractScreenName[selector](idx, el), el);
            });
        });
    };

    var makeScoreDiv = function(username, score, parentEl) {
        return $(scoreTemplate({
            username:       username,
            score:          Math.round(score),
            kloutIconURL:   $(parentEl).hasClass('simple-tweet') ? kloutIcon16URL : kloutIcon18URL
        }));
    };

    var scores = {};

    var fetchMissingScores = function() {
        var screenNames = visibleScreenNames().filter(function(screenName) {
            return undefined === scores[screenName.toLowerCase()];
        });

        if (0 !== screenNames.length) {
            screenNames = screenNames.slice(0, 4);
            _.each(screenNames, function(user) {
                scores[user.toLowerCase()] = null;
            });
            fetchScores(screenNames.join(','), function(response) {
                _.each(response, function(scoreData, userName) {
                    scores[userName.toLowerCase()] = scoreData.score;
                });

                fetchMissingScores();
                checkTweets();
            });
        }
    };

    var checkTweets = function() {
        foreachVisibleScreenName(function(el, parentEl) {
            var screenName = el.text();
            var score = scores[screenName.toLowerCase()];
            if (null != score && 0 === $('.klout-score-span', parentEl).length) {
                var scoreDiv = makeScoreDiv(screenName, score, parentEl);
                scoreDiv.click(function(e) {
                    window.open('http://klout.com/user/' + screenName);
                    e.preventDefault();
                    e.stopPropagation();
                });
                $(el).before(scoreDiv);
            }
        });
    }

    var intervals = [ 500, 1000, 5000, 10000 ];
    var check = function() {
        _.each(intervals, function(interval) {
            _.delay(function() { fetchMissingScores(); checkTweets(); }, interval);
        });
    };

    $(window).scroll(_.debounce(function() {
        fetchMissingScores();
        checkTweets();
    }, 500));

    $('body').live('mouseup', check);
    $(check);
})(jQuery, window);
