# preferences for the calibre GUI

### Begin group: DEFAULT
 
# send to storage card by default
# Lehenetsita: bidali fitxategia memoria-txartelara trepetaren memoria nagusira bidali beharrean
send_to_storage_card_by_default = False
 
# confirm delete
# Baieztatu ezabatu baino lehen
confirm_delete = False
 
# main window geometry
# Leiho nagusiaren geometria
main_window_geometry = cPickle.loads('\x80\x02csip\n_unpickle_type\nq\x01U\x0cPyQt4.QtCoreq\x02U\nQByteArrayU.\x01\xd9\xd0\xcb\x00\x01\x00\x00\x00\x00\x01\\\x00\x00\x00\x0c\x00\x00\x05U\x00\x00\x02\xe6\x00\x00\x01_\x00\x00\x00$\x00\x00\x05R\x00\x00\x02\xe3\x00\x00\x00\x00\x00\x00\x85\x87Rq\x03.')
 
# new version notification
# Oharra bidali bertsio berri bat eskuragarri dagoen bakoitzean
new_version_notification = True
 
# use roman numerals for series number
# Erabili zenbaki erromatarrak zenbaki segidetarako
use_roman_numerals_for_series_number = True
 
# sort tags by
# Sailkatu etiketa zerrendak, izenen arabera, ospearen arabera edo balorazioen arabera
sort_tags_by = 'name'
 
# match tags type
# Etiketen egokitasuna, korrespondentzia egiten du baten arabera edo guztien arabera.
match_tags_type = 'any'
 
# cover flow queue length
# Erakutsiko den liburu-azal kopurua, liburu-azal-arakatzaile moduan
cover_flow_queue_length = 6
 
# LRF conversion defaults
# Lehenetsitako balioak LRF formatura bihurtzeko
LRF_conversion_defaults = cPickle.loads('\x80\x02]q\x01.')
 
# LRF ebook viewer options
# Aukerak LRF liburu-e irakurgailuarentzat
LRF_ebook_viewer_options = None
 
# internally viewed formats
# Barneko irakurtzeko sistema erabilita ikus daitezkeen formatuak
internally_viewed_formats = cPickle.loads('\x80\x02]q\x01(U\x03LRFq\x02U\x04EPUBq\x03U\x03LITq\x04U\x04MOBIq\x05U\x03PRCq\x06U\x04POBIq\x07U\x03AZWq\x08U\x04AZW3q\tU\x04HTMLq\nU\x03FB2q\x0bU\x03PDBq\x0cU\x02RBq\rU\x03SNBq\x0eU\x05HTMLZq\x0fe.')
 
# column map
# Liburu zerrenda zenbat zutabetan erakutsiko
column_map = cPickle.loads('\x80\x02]q\x01(U\x05titleq\x02U\x08ondeviceq\x03U\x07authorsq\x04U\x04sizeq\x05U\ttimestampq\x06U\x06ratingq\x07U\tpublisherq\x08U\x04tagsq\tU\x06seriesq\nU\x07pubdateq\x0be.')
 
# autolaunch server
# Automatikoki abiaraziko du zerbitzariko edukia hasi aplikazioan
autolaunch_server = False
 
# oldest news
# Albiste zaharragoak datu basean gordeta
oldest_news = 60
 
# systray icon
# Erakutsi sistemako erretiluaren ikonoa
systray_icon = False
 
# upload news to device
# Karga itzazu irakurgailuan deskargaturiko albisteak
upload_news_to_device = True
 
# delete news from library on upload
# Liburutegitik liburu berriak ezabatu gailura igo eta gero
delete_news_from_library_on_upload = False
 
# separate cover flow
# Erakutsi Cover Flow, (liburu-azal nabigazioa), berariazko leiho batean eta ez calibreren leiho nagusian
separate_cover_flow = False
 
# disable tray notification
# Desgaitu abisuak sistemaren erretilu ikonotik
disable_tray_notification = False
 
# default send to device action
# Lehenetsitako egiteko ekintza klik egiten duzunean "bidali irakurgailura" botoian
default_send_to_device_action = 'DeviceAction:main::False:False'
 
# asked library thing password
# Asked library thing password at least once.
asked_library_thing_password = False
 
# search as you type
# Start searching as you type. If this is disabled then search will only take place when the Enter or Return key is pressed.
search_as_you_type = False
 
# highlight search matches
# When searching, show all books with search results highlighted instead of showing only the matches. You can use the N or F3 keys to go to the next match.
highlight_search_matches = False
 
# save to disk template history
# Previously used Save to Disk templates
save_to_disk_template_history = cPickle.loads('\x80\x02]q\x01.')
 
# send to device template history
# Previously used Send to Device templates
send_to_device_template_history = cPickle.loads('\x80\x02]q\x01.')
 
# main search history
# Search history for the main GUI
main_search_history = cPickle.loads('\x80\x02]q\x01.')
 
# viewer search history
# Search history for the ebook viewer
viewer_search_history = cPickle.loads('\x80\x02]q\x01.')
 
# lrf viewer search history
# Search history for the LRF viewer
lrf_viewer_search_history = cPickle.loads('\x80\x02]q\x01.')
 
# scheduler search history
# Search history for the recipe scheduler
scheduler_search_history = cPickle.loads('\x80\x02]q\x01.')
 
# plugin search history
# Search history for the plugin preferences
plugin_search_history = cPickle.loads('\x80\x02]q\x01.')
 
# shortcuts search history
# Search history for the keyboard preferences
shortcuts_search_history = cPickle.loads('\x80\x02]q\x01.')
 
# jobs search history
# Search history for the keyboard preferences
jobs_search_history = cPickle.loads('\x80\x02]q\x01.')
 
# tweaks search history
# Search history for tweaks
tweaks_search_history = cPickle.loads('\x80\x02]q\x01.')
 
# worker limit
# Maximum number of simultaneous conversion/news download jobs. This number is twice the actual value for historical reasons.
worker_limit = 6
 
# get social metadata
# Deskargatu gizarte mailako metadatuak (etiketak/balorazioak/eta abar.)
get_social_metadata = True
 
# overwrite author title metadata
# Gainetik idatzi egilearen izena eta testuaren izenburua metadatu berriekin
overwrite_author_title_metadata = True
 
# auto download cover
# Modu automatikoan deskargatu liburu-azala, eskura baldin badago.
auto_download_cover = False
 
# enforce cpu limit
# Murriztu itzazu aldi bereko gehienezko egitekoen kopurua dauden CPU-en arabera
enforce_cpu_limit = True
 
# gui layout
# The layout of the user interface. Wide has the book details panel on the right and narrow has it at the bottom.
gui_layout = 'wide'
 
# show avg rating
# Erakutsi batez besteko balorazioa kontu bakoitzeko etiketen arakatzailean
show_avg_rating = True
 
# disable animations
# Desgaitu EI (erabiltzailearen interfazearen) animazioak
disable_animations = False
 
# tag browser hidden categories
# etiketatu arakatzailearen kategoriak ez erakusteko moduan
tag_browser_hidden_categories = cPickle.loads('\x80\x02c__builtin__\nset\nq\x01]\x85Rq\x02.')
 


