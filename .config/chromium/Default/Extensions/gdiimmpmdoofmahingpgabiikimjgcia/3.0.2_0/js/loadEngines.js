'use strict';

function dummpy(s) {
    return '';
}

var ConsoleData = function() {
    var attributes = {};
    var object = {};
    var observers = [];

    if (localStorage.getItem('ConsoleData')) {
        attributes = JSON.parse(localStorage.getItem('ConsoleData'));
    }

    if (typeof(attributes.langLogs) !== 'object') {
        attributes.langLogs = {};
    }
    if (typeof(attributes.isKeyupOutput) !== 'boolean') {
        attributes.isKeyupOutput = true;
    }
    attributes.currentLang = attributes.currentLang || 'Python';

    observers.push(function(o) {
        localStorage.setItem('ConsoleData', JSON.stringify(attributes));
    });

    var replEngines = [];

    ['python', 'ruby', 'kaffeine', 'javascript'].forEach(function(lang) {//
        var jsrepl = new JSREPL({
            input: dummpy
        });
        jsrepl.loadLanguage(lang, function() {
            if (!(jsrepl.lang.name in attributes.langLogs)) {
                attributes.langLogs[jsrepl.lang.name] = [];
            }else if(attributes.langLogs[jsrepl.lang.name].length > 1 && attributes.langLogs[jsrepl.lang.name][attributes.langLogs[jsrepl.lang.name].length - 1][0] !== ' ---\n'){
                attributes.langLogs[jsrepl.lang.name].push([' ---\n', 'output']);
            }
            replEngines.push(jsrepl);
            if (attributes.currentLang === null || attributes.currentLang === jsrepl.lang.name) {
                attributes.currentLang = jsrepl.lang.name;
            }
            callObservers();
        });
    });

    var callObservers = function() {
        observers.forEach(function(o) {
            o(Object.freeze(object));
        });
    };

    object.getLanguages = function() {
        return replEngines;
    };
    object.getLanguageNames = function() {
        return replEngines.map(function(e) {return e.lang.name;});
    };
    object.addObserver = function(o) {
        observers.push(o);
        o(Object.freeze(object));
    };
    object.getCurrentLang = function() {
        return attributes.currentLang;
    };
    object.getCurrentRelp = function() {
        var currentRelp = null;
        replEngines.forEach(function(e) {
            if (e.lang.name === attributes.currentLang) {
                currentRelp = e;
            }
        });

        return currentRelp;
    };
    object.getLog = function() {
        return attributes.langLogs[attributes.currentLang];
    };
    object.addLog = function(msg, className) {
        attributes.langLogs[attributes.currentLang].push([msg, className]);
        callObservers();
    };
    object.clearLog = function() {
        attributes.langLogs[attributes.currentLang] = [];
        callObservers();
    };
    object.setCurrentLang = function(currentLang) {
        attributes.currentLang = currentLang;
        callObservers();
    };
    object.setIsKeyupOutput = function(isKeyupOutput) {
        attributes.isKeyupOutput = isKeyupOutput;
        callObservers();
    };
    object.isKeyupOutput = function() {
        return attributes.isKeyupOutput;
    };

    return Object.freeze(object);
};

window['consoleData'] = ConsoleData();
