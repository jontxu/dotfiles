// ==ClosureCompiler==
// @output_file_name default.js
// @compilation_level WHITESPACE_ONLY
// @code_url http://closure-compiler.googlecode.com/svn/trunk/contrib/externs/jquery-1.7.js
// @code_url http://closure-compiler.googlecode.com/svn/trunk/contrib/externs/underscore-1.3.1.js
// @code_url http://closure-compiler.googlecode.com/svn/trunk/contrib/externs/chrome_extensions.js
// @code_url http://closure-compiler.googlecode.com/svn/trunk/contrib/externs/webkit_console.js
// ==/ClosureCompiler==
'use strict';

var backgroundDOMWindow = chrome.extension.getBackgroundPage();
window['consoleData'] = backgroundDOMWindow.window['consoleData'];

//~ (function ($, JSREPL){
    // Creating the console.

    var langTemplate = function() {
        var s = '';
        consoleData.getLanguages().forEach(function(e, i) {
            s += '<option';
            if (e.lang.name === consoleData.getCurrentLang()) {
                s += ' selected';
            }
            s += ' value="' + e.lang.name + '">' + e.lang.name + '</option>';
        });

        return s;
    };

    consoleData = window['consoleData'];
    $('#options').click(function() {$('body').toggleClass('options');});
    $('#keyupResults').click(function() {consoleData.setIsKeyupOutput(this.checked !== 1);});

    consoleData.addObserver(function(o) {
        $('#results').toggle(consoleData.isKeyupOutput());
        $('#keyupResults').prop('checked', consoleData.isKeyupOutput());
        $('#languages').html(langTemplate());
    });

    $('#console').click(function() {
        $('body').removeClass('options');
    });

    $('#languages').change(function() {
        console.log('setCurrentLang ', this.value);
        consoleData.setCurrentLang(this.value);
        loadRepl(consoleData.getCurrentRelp());
        //jqconsole.Reset();
    });

    $('#openPopup').click(function() {
        var day = new Date();
        var id = day.getTime();
        window.open('index.html', id, 'toolbar=0,scrollbars=0,statusbar=0,menubar=0,width=460,height=600');
    });

    var header = '';
    window.jqconsole = $('#console').jqconsole(header, ' ');

    jqconsole.RegisterMatching('{', '}', 'brace');
    jqconsole.RegisterMatching('(', ')', 'paran');
    jqconsole.RegisterMatching('[', ']', 'bracket');

    document.ready = function() {
        //jqconsole.Focus();
        document.querySelector('#print_hotkeys').onclick = function() {
            jqconsole.Write(
                'Up: Previous history item.\n\n' +
                'Down: Next history item.\n\n' +
                'Shift+Up, Ctrl+Up: Move cursor to the line above the current one.\n\n' +
                'Shift+Down, Ctrl+Down: Move cursor to the line below the current one.\n\n' +
                'Ctrl+Z: Abort prompt.\n\n' +
                'Ctrl+R: Clear console.\n\n' +
                'Shift+Enter: New line.\n'
            , 'jqconsole-old-prompt');
        }
    };


    function loadRepl(jsrepl) {
        $('#results').text('');
        // Abort prompt on Ctrl+Z.
        jqconsole.RegisterShortcut('Z', function() {
            jqconsole.AbortPrompt();
            handler();
        });

        function clearConsole() {
            jqconsole.Reset();
            handler();
            consoleData.clearLog();
        }


        // Ctrl+R: resets the console.
        jqconsole.RegisterShortcut('R', clearConsole);
        document.querySelector('#clear_console').onclick = clearConsole;

        var compilerResult = function(s) {
            if (s) {
                jqconsole.Write('  ' + s + '\n', 'output');
                consoleData.addLog('  ' + s + '\n', 'output');
            }
        };

        var compilerError = function(s) {
            if (s) {
                //s = s.split('\n').slice(3).join('\n');
               jqconsole.Write('  ' + s + '\n', 'error');
               consoleData.addLog('  ' + s + '\n', 'error');
            }
        };


        var handler = function(command) {
            if (command) {
                jsrepl.off('error');
                jsrepl.off(['result', 'output']);
                jsrepl.on(['result', 'output'], compilerResult);
                jsrepl.on('error', compilerError);

                consoleData.addLog(' ' + command + '\n', 'jqconsole-old-prompt');
                jsrepl.eval(command);
                $('#results').text(' ');
                $('.jqconsole-prompt, #results').removeClass('error');

               //~ try {
                    //~ jqconsole.Write('  ' + eval(command) + '\n', 'output');
                //~ } catch (e) {
                    //~ jqconsole.Write('  ERROR: ' + e.message + '\n', 'error');
                //~ }
            }
            jqconsole.Prompt(true, handler, function(command) {
                return false;
            });
        };

        var previewResult = function(s) {
           if (s !== null) {
                $('#results').text('  ' + s);
            }
        };

        // $(document).keyup(function (e) {
        //     if(consoleData.getCurrentLang() === 'Python' && consoleData.isKeyupOutput()){
        //         var currentCommand = jqconsole.GetPromptText();
        //         if(currentCommand){
        //             //jsrepl.off('error');
        //             jsrepl.off(['result', 'output']);
        //             jsrepl.on(['result', 'output'], previewResult);
        //             //jsrepl.on('error', previewError);

        //             if( consoleData.getCurrentLang() === 'Python' ){
        //                 jsrepl.eval('eval("' + currentCommand +'")');
        //             }

        //         }else{
        //             $('#results').text(' ');
        //         }
        //     }
        // });

        jqconsole.Reset();
        handler();

        jqconsole._HistoryNext();
        jqconsole._HistoryPrevious();
        consoleData.getLog().forEach(function(l) {
            if (l[1] === 'jqconsole-old-prompt') {
                jqconsole._HistoryNext();
            }
        });

        setTimeout(function() {
            jqconsole.Focus();
        },400);

        consoleData.getLog().forEach(function(l) {
            jqconsole.Write(l[0], l[1]);
            if (l[1] === 'jqconsole-old-prompt') {
                jqconsole.AddHistory(l[0].substring(1, l[0].length - 1));
            }
        });

        //$('.jqconsole').append('<span id="results"> </span>');
        $('#results').toggle(consoleData.isKeyupOutput());

        //jqconsole.Write('---', 'output');
        //consoleData.addLog('---\n', 'output');
    }


    function checkFromReplLoad() {
        if (consoleData.getCurrentRelp()) {
            loadRepl(consoleData.getCurrentRelp());
        }else {
            console.log('repleNotLoaded');
            setTimeout(checkFromReplLoad, 1000);
        }
    }


    jqconsole.Focus();
    checkFromReplLoad();

//~ })(backgroundDOMWindow.jQuery, backgroundDOMWindow.JSREPL);

