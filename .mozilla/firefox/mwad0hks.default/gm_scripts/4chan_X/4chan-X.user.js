// ==UserScript==
// @name         4chan X
// @version      3.12.0
// @minGMVer     1.12
// @minFFVer     22
// @namespace    4chan-X
// @description  Cross-browser extension for productive lurking on 4chan.
// @license      MIT; https://github.com/MayhemYDG/4chan-x/blob/v3/LICENSE
// @match        *://api.4chan.org/*
// @match        *://boards.4chan.org/*
// @match        *://images.4chan.org/*
// @match        *://sys.4chan.org/*
// @grant        GM_getValue
// @grant        GM_setValue
// @grant        GM_deleteValue
// @grant        GM_openInTab
// @run-at       document-start
// @updateURL    https://4chan-x.just-believe.in/builds/4chan-X.meta.js
// @downloadURL  https://4chan-x.just-believe.in/builds/4chan-X.user.js
// @icon         data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADAAAAAwCAMAAABg3Am1AAAACVBMVEUAAGcAAABmzDNZt9VtAAAAAXRSTlMAQObYZgAAAF5JREFUeNrtkTESABAQxPD/R6tsE2dUGYUtFJvLDKf93KevHJAjpBorAQWSBIKqFASC4G0pCAkm4GfaEvgYXl0T6HBaE97f0vmnfYHbZOMLZCx9ISdKWwjOWZSC8GYm4SUGwfYgqI4AAAAASUVORK5CYII=
// ==/UserScript==

/* 4chan X - Version 3.12.0 - 2013-11-04
 * https://4chan-x.just-believe.in/
 *
 * Copyrights and License: https://github.com/MayhemYDG/4chan-x/blob/v3/LICENSE
 *
 * Contributors:
 * https://github.com/MayhemYDG/4chan-x/graphs/contributors
 * Non-GitHub contributors:
 * ferongr, xat-, Ongpot, thisisanon and Anonymous - favicon contributions
 * e000 - cooldown sanity check
 * Seiba - chrome quick reply focusing
 * herpaderpderp - recaptcha fixes
 * WakiMiko - recaptcha tab order http://userscripts.org/scripts/show/82657
 *
 * All the people who've taken the time to write bug reports.
 *
 * Thank you.
 */

'use strict';

(function() {
  var $, $$, Anonymize, ArchiveLink, AutoGIF, Board, Build, Clone, Conf, Config, CustomCSS, DataBoard, DeleteLink, Dice, DownloadLink, ExpandThread, Favicon, FileInfo, Filter, Fourchan, Get, Header, IDColor, ImageExpand, ImageHover, Index, Keybinds, Linkify, Main, Menu, Nav, Notice, PSAHiding, Polyfill, Post, PostHiding, QR, QuoteBacklink, QuoteCT, QuoteInline, QuoteOP, QuotePreview, QuoteStrikeThrough, QuoteYou, Quotify, Recursive, Redirect, RelativeDates, Report, ReportLink, RevealSpoilers, Sauce, Settings, Thread, ThreadExcerpt, ThreadHiding, ThreadStats, ThreadUpdater, ThreadWatcher, Time, UI, Unread, c, d, doc, g,
    __slice = [].slice,
    __bind = function(fn, me){ return function(){ return fn.apply(me, arguments); }; },
    __indexOf = [].indexOf || function(item) { for (var i = 0, l = this.length; i < l; i++) { if (i in this && this[i] === item) return i; } return -1; },
    __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  Config = {
    main: {
      'Miscellaneous': {
        'Enable 4chan\'s Extension': [false, 'Compatibility between 4chan X and 4chan\'s inline extension is NOT guaranteed.'],
        'Desktop Notifications': [true, 'Enables desktop notifications across various 4chan X features.'],
        'Announcement Hiding': [true, 'Add button to hide 4chan announcements.'],
        '404 Redirect': [true, 'Redirect dead threads and images.'],
        'Keybinds': [true, 'Bind actions to keyboard shortcuts.'],
        'Time Formatting': [true, 'Localize and format timestamps.'],
        'Relative Post Dates': [false, 'Display dates like "3 minutes ago". Tooltip shows the timestamp.'],
        'File Info Formatting': [true, 'Reformat the file information.'],
        'Thread Expansion': [true, 'Add buttons to expand threads.'],
        'Index Navigation': [false, 'Add buttons to navigate between threads.'],
        'Reply Navigation': [false, 'Add buttons to navigate to top / bottom of thread.'],
        'Show Dice Roll': [true, 'Show dice that were entered into the email field.']
      },
      'Filtering': {
        'Anonymize': [false, 'Make everyone Anonymous.'],
        'Filter': [true, 'Self-moderation placebo.'],
        'Recursive Hiding': [true, 'Hide replies of hidden posts, recursively.'],
        'Thread Hiding': [true, 'Add buttons to hide entire threads.'],
        'Reply Hiding': [true, 'Add buttons to hide single replies.'],
        'Stubs': [true, 'Show stubs of hidden threads / replies.']
      },
      'Images': {
        'Auto-GIF': [false, 'Animate GIF thumbnails (disabled on /gif/, /wsg/).'],
        'Image Expansion': [true, 'Expand images inline.'],
        'Image Hover': [false, 'Show a floating expanded image on hover.'],
        'Sauce': [true, 'Add sauce links to images.'],
        'Reveal Spoilers': [false, 'Reveal spoiler thumbnails.']
      },
      'Linkification': {
        'Linkify': [true, 'Convert text links into hyperlinks.'],
        'Clean Links': [true, 'Remove spoiler and code tags commonly used to bypass blocked links.']
      },
      'Menu': {
        'Menu': [true, 'Add a drop-down menu to posts.'],
        'Report Link': [true, 'Add a report link to the menu.'],
        'Thread Hiding Link': [true, 'Add a link to hide entire threads.'],
        'Reply Hiding Link': [true, 'Add a link to hide single replies.'],
        'Delete Link': [true, 'Add post and image deletion links to the menu.'],
        'Archive Link': [true, 'Add an archive link to the menu.']
      },
      'Monitoring': {
        'Thread Updater': [true, 'Fetch and insert new replies. Has more options in its own dialog.'],
        'Unread Count': [true, 'Show the unread posts count in the tab title.'],
        'Hide Unread Count at (0)': [false, 'Hide the unread posts count when it reaches 0.'],
        'Unread Tab Icon': [true, 'Show a different favicon when there are unread posts.'],
        'Unread Line': [true, 'Show a line to distinguish read posts from unread ones.'],
        'Scroll to Last Read Post': [true, 'Scroll back to the last read post when reopening a thread.'],
        'Thread Excerpt': [true, 'Show an excerpt of the thread in the tab title.'],
        'Thread Stats': [true, 'Display reply, image, and page count.'],
        'Thread Watcher': [true, 'Bookmark threads.'],
        'Color User IDs': [true, 'Assign unique colors to user IDs on boards that use them.']
      },
      'Posting': {
        'Quick Reply': [true, 'All-in-one form to reply, create threads, automate dumping and more.'],
        'Persistent QR': [false, 'The Quick reply won\'t disappear after posting.'],
        'Auto-Hide QR': [false, 'Automatically hide the quick reply when posting.'],
        'Open Post in New Tab': [true, 'Open new threads or replies to a thread from the index in a new tab.'],
        'Remember QR Size': [false, 'Remember the size of the Quick reply.'],
        'Remember Subject': [false, 'Remember the subject field, instead of resetting after posting.'],
        'Remember Spoiler': [false, 'Remember the spoiler state, instead of resetting after posting.'],
        'Hide Original Post Form': [true, 'Hide the normal post form.'],
        'Cooldown': [true, 'Indicate the remaining time before posting again.'],
        'Cooldown Prediction': [true, 'Decrease the cooldown time by taking into account upload speed. Disable it if it\'s inaccurate for you.']
      },
      'Quote Links': {
        'Quote Backlinks': [true, 'Add quote backlinks.'],
        'OP Backlinks': [true, 'Add backlinks to the OP.'],
        'Quote Inlining': [true, 'Inline quoted post on click.'],
        'Forward Hiding': [true, 'Hide original posts of inlined backlinks.'],
        'Quote Previewing': [true, 'Show quoted post on hover.'],
        'Quote Highlighting': [true, 'Highlight the previewed post.'],
        'Resurrect Quotes': [true, 'Link dead quotes to the archives.'],
        'Mark Quotes of You': [true, 'Add \'(You)\' to quotes linking to your posts.'],
        'Mark OP Quotes': [true, 'Add \'(OP)\' to OP quotes.'],
        'Mark Cross-thread Quotes': [true, 'Add \'(Cross-thread)\' to cross-threads quotes.']
      }
    },
    imageExpansion: {
      'Fit width': [true, ''],
      'Fit height': [false, ''],
      'Expand spoilers': [false, 'Expand all images along with spoilers.'],
      'Expand from here': [true, 'Expand all images only from current position to thread end.']
    },
    threadWatcher: {
      'Current Board': [false, 'Only show watched threads from the current board.'],
      'Auto Watch': [true, 'Automatically watch threads you start.'],
      'Auto Watch Reply': [false, 'Automatically watch threads you reply to.'],
      'Auto Prune': [false, 'Automatically prune 404\'d threads.']
    },
    filter: {
      name: "# Filter any namefags:\n#/^(?!Anonymous$)/",
      uniqueID: "# Filter a specific ID:\n#/Txhvk1Tl/",
      tripcode: "# Filter any tripfag\n#/^!/",
      capcode: "# Set a custom class for mods:\n#/Mod$/;highlight:mod;op:yes\n# Set a custom class for moot:\n#/Admin$/;highlight:moot;op:yes",
      email: "",
      subject: "# Filter Generals on /v/:\n#/general/i;boards:v;op:only",
      comment: "# Filter Stallman copypasta on /g/:\n#/what you're refer+ing to as linux/i;boards:g",
      flag: "",
      filename: "",
      dimensions: "# Highlight potential wallpapers:\n#/1920x1080/;op:yes;highlight;top:no;boards:w,wg",
      filesize: "",
      MD5: ""
    },
    sauces: "https://www.google.com/searchbyimage?image_url=%TURL\nhttp://iqdb.org/?url=%TURL\n#//tineye.com/search?url=%TURL\n#http://saucenao.com/search.php?url=%TURL\n#http://3d.iqdb.org/?url=%TURL\n#http://regex.info/exif.cgi?imgurl=%URL\n# uploaders:\n#http://imgur.com/upload?url=%URL;text:Upload to imgur\n#http://ompldr.org/upload?url1=%URL;text:Upload to ompldr\n# \"View Same\" in archives:\n#//archive.foolz.us/_/search/image/%MD5/;text:View same on foolz\n#//archive.foolz.us/%board/search/image/%MD5/;text:View same on foolz /%board/\n#//archive.installgentoo.net/%board/image/%MD5;text:View same on installgentoo /%board/",
    'Custom CSS': false,
    Index: {
      'Index Mode': 'paged',
      'Index Sort': 'bump'
    },
    Header: {
      'Header auto-hide': false,
      'Bottom header': false,
      'Header catalog links': false,
      'Top Board List': false,
      'Bottom Board List': false,
      'Custom Board Navigation': true
    },
    QR: {
      'QR.personas': "#email:\"sage\";boards:jp;always\nemail:\"sage\""
    },
    boardnav: '[current-title / toggle-all]',
    time: '%m/%d/%y(%a)%H:%M:%S',
    backlink: '>>%id',
    fileInfo: '%l (%p%s, %r)',
    favicon: 'ferongr',
    usercss: '',
    hotkeys: {
      'Toggle board list': ['Ctrl+b', 'Toggle the full board list.'],
      'Open empty QR': ['q', 'Open QR without post number inserted.'],
      'Open QR': ['Shift+q', 'Open QR with post number inserted.'],
      'Open settings': ['Alt+o', 'Open Settings.'],
      'Close': ['Esc', 'Close Settings, Notifications or QR.'],
      'Spoiler tags': ['Ctrl+s', 'Insert spoiler tags.'],
      'Code tags': ['Alt+c', 'Insert code tags.'],
      'Eqn tags': ['Alt+e', 'Insert eqn tags.'],
      'Math tags': ['Alt+m', 'Insert math tags.'],
      'Submit QR': ['Alt+s', 'Submit post.'],
      'Update': ['r', 'Refresh the index/thread.'],
      'Watch': ['w', 'Watch thread.'],
      'Expand image': ['Shift+e', 'Expand selected image.'],
      'Expand images': ['e', 'Expand all images.'],
      'Front page': ['0', 'Jump to page 0.'],
      'Open front page': ['Shift+0', 'Open page 0 in a new tab.'],
      'Next page': ['Right', 'Jump to the next page.'],
      'Previous page': ['Left', 'Jump to the previous page.'],
      'Search form': ['Ctrl+Alt+s', 'Open the search field on the board index.'],
      'Next thread': ['Down', 'See next thread.'],
      'Previous thread': ['Up', 'See previous thread.'],
      'Expand thread': ['Ctrl+e', 'Expand thread.'],
      'Open thread': ['o', 'Open thread in current tab.'],
      'Open thread tab': ['Shift+o', 'Open thread in new tab.'],
      'Next reply': ['j', 'Select next reply.'],
      'Previous reply': ['k', 'Select previous reply.'],
      'Deselect reply': ['Shift+d', 'Deselect reply.'],
      'Hide': ['x', 'Hide thread.']
    },
    updater: {
      checkbox: {
        'Beep': [false, 'Beep on new post to completely read thread.'],
        'Auto Scroll': [false, 'Scroll updated posts into view. Only enabled at bottom of page.'],
        'Bottom Scroll': [false, 'Always scroll to the bottom, not the first new post. Useful for event threads.'],
        'Scroll BG': [false, 'Auto-scroll background tabs.'],
        'Auto Update': [true, 'Automatically fetch new posts.']
      },
      'Interval': 30
    }
  };

  Conf = {};

  c = console;

  d = document;

  doc = d.documentElement;

  g = {
    VERSION: '3.12.0',
    NAMESPACE: '4chan X.',
    boards: {},
    threads: {},
    posts: {}
  };

  $ = function(selector, root) {
    if (root == null) {
      root = d.body;
    }
    return root.querySelector(selector);
  };

  $$ = function(selector, root) {
    if (root == null) {
      root = d.body;
    }
    return __slice.call(root.querySelectorAll(selector));
  };

  $.SECOND = 1000;

  $.MINUTE = 1000 * 60;

  $.HOUR = 1000 * 60 * 60;

  $.DAY = 1000 * 60 * 60 * 24;

  $.id = function(id) {
    return d.getElementById(id);
  };

  $.ready = function(fc) {
    var cb;
    if (d.readyState !== 'loading') {
      $.queueTask(fc);
      return;
    }
    cb = function() {
      $.off(d, 'DOMContentLoaded', cb);
      return fc();
    };
    return $.on(d, 'DOMContentLoaded', cb);
  };

  $.formData = function(form) {
    var fd, key, val;
    if (form instanceof HTMLFormElement) {
      return new FormData(form);
    }
    fd = new FormData();
    for (key in form) {
      val = form[key];
      if (val) {
        if (typeof val === 'object' && 'newName' in val) {
          fd.append(key, val, val.newName);
        } else {
          fd.append(key, val);
        }
      }
    }
    return fd;
  };

  $.extend = function(object, properties) {
    var key, val;
    for (key in properties) {
      val = properties[key];
      object[key] = val;
    }
  };

  $.ajax = (function() {
    var lastModified;
    lastModified = {};
    return function(url, options, extra) {
      var form, r, sync, type, upCallbacks, whenModified;
      if (extra == null) {
        extra = {};
      }
      type = extra.type, whenModified = extra.whenModified, upCallbacks = extra.upCallbacks, form = extra.form, sync = extra.sync;
      r = new XMLHttpRequest();
      type || (type = form && 'post' || 'get');
      r.open(type, url, !sync);
      if (whenModified) {
        if (url in lastModified) {
          r.setRequestHeader('If-Modified-Since', lastModified[url]);
        }
        $.on(r, 'load', function() {
          return lastModified[url] = r.getResponseHeader('Last-Modified');
        });
      }
      $.extend(r, options);
      $.extend(r.upload, upCallbacks);
      r.send(form);
      return r;
    };
  })();

  $.cache = (function() {
    var reqs;
    reqs = {};
    return function(url, cb, options) {
      var req, rm;
      if (req = reqs[url]) {
        if (req.readyState === 4) {
          cb.call(req, req.evt);
        } else {
          req.callbacks.push(cb);
        }
        return;
      }
      rm = function() {
        return delete reqs[url];
      };
      req = $.ajax(url, options);
      $.on(req, 'load', function(e) {
        var _i, _len, _ref;
        _ref = this.callbacks;
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
          cb = _ref[_i];
          cb.call(this, e);
        }
        this.evt = e;
        return delete this.callbacks;
      });
      $.on(req, 'abort', rm);
      $.on(req, 'error', rm);
      req.callbacks = [cb];
      return reqs[url] = req;
    };
  })();

  $.cb = {
    checked: function() {
      $.set(this.name, this.checked);
      return Conf[this.name] = this.checked;
    },
    value: function() {
      $.set(this.name, this.value.trim());
      return Conf[this.name] = this.value;
    }
  };

  $.asap = function(test, cb) {
    if (test()) {
      return cb();
    } else {
      return setTimeout($.asap, 25, test, cb);
    }
  };

  $.addStyle = function(css) {
    var style;
    style = $.el('style', {
      textContent: css
    });
    $.asap((function() {
      return d.head;
    }), function() {
      return $.add(d.head, style);
    });
    return style;
  };

  $.x = function(path, root) {
    if (root == null) {
      root = d.body;
    }
    return d.evaluate(path, root, null, 8, null).singleNodeValue;
  };

  $.addClass = function(el, className) {
    return el.classList.add(className);
  };

  $.rmClass = function(el, className) {
    return el.classList.remove(className);
  };

  $.hasClass = function(el, className) {
    return el.classList.contains(className);
  };

  $.rm = (function() {
    if ('remove' in Element.prototype) {
      return function(el) {
        return el.remove();
      };
    } else {
      return function(el) {
        var _ref;
        return (_ref = el.parentNode) != null ? _ref.removeChild(el) : void 0;
      };
    }
  })();

  $.rmAll = function(root) {
    var node, _i, _len, _ref;
    _ref = __slice.call(root.childNodes);
    for (_i = 0, _len = _ref.length; _i < _len; _i++) {
      node = _ref[_i];
      root.removeChild(node);
    }
  };

  $.tn = function(s) {
    return d.createTextNode(s);
  };

  $.nodes = function(nodes) {
    var frag, node, _i, _len;
    if (!(nodes instanceof Array)) {
      return nodes;
    }
    frag = d.createDocumentFragment();
    for (_i = 0, _len = nodes.length; _i < _len; _i++) {
      node = nodes[_i];
      frag.appendChild(node);
    }
    return frag;
  };

  $.add = function(parent, el) {
    return parent.appendChild($.nodes(el));
  };

  $.prepend = function(parent, el) {
    return parent.insertBefore($.nodes(el), parent.firstChild);
  };

  $.after = function(root, el) {
    return root.parentNode.insertBefore($.nodes(el), root.nextSibling);
  };

  $.before = function(root, el) {
    return root.parentNode.insertBefore($.nodes(el), root);
  };

  $.replace = function(root, el) {
    return root.parentNode.replaceChild($.nodes(el), root);
  };

  $.el = function(tag, properties) {
    var el;
    el = d.createElement(tag);
    if (properties) {
      $.extend(el, properties);
    }
    return el;
  };

  $.on = function(el, events, handler) {
    var event, _i, _len, _ref;
    _ref = events.split(' ');
    for (_i = 0, _len = _ref.length; _i < _len; _i++) {
      event = _ref[_i];
      el.addEventListener(event, handler, false);
    }
  };

  $.off = function(el, events, handler) {
    var event, _i, _len, _ref;
    _ref = events.split(' ');
    for (_i = 0, _len = _ref.length; _i < _len; _i++) {
      event = _ref[_i];
      el.removeEventListener(event, handler, false);
    }
  };

  $.event = function(event, detail, root) {
    if (root == null) {
      root = d;
    }
    return root.dispatchEvent(new CustomEvent(event, {
      bubbles: true,
      detail: detail
    }));
  };

  $.open = GM_openInTab;

  $.debounce = function(wait, fn) {
    var args, exec, lastCall, that, timeout;
    lastCall = 0;
    timeout = null;
    that = null;
    args = null;
    exec = function() {
      lastCall = Date.now();
      return fn.apply(that, args);
    };
    return function() {
      args = arguments;
      that = this;
      if (lastCall < Date.now() - wait) {
        return exec();
      }
      clearTimeout(timeout);
      return timeout = setTimeout(exec, wait);
    };
  };

  $.queueTask = (function() {
    var execTask, taskChannel, taskQueue;
    taskQueue = [];
    execTask = function() {
      var args, func, task;
      task = taskQueue.shift();
      func = task[0];
      args = Array.prototype.slice.call(task, 1);
      return func.apply(func, args);
    };
    if (window.MessageChannel) {
      taskChannel = new MessageChannel();
      taskChannel.port1.onmessage = execTask;
      return function() {
        taskQueue.push(arguments);
        return taskChannel.port2.postMessage(null);
      };
    } else {
      return function() {
        taskQueue.push(arguments);
        return setTimeout(execTask, 0);
      };
    }
  })();

  $.globalEval = function(code) {
    var script;
    script = $.el('script', {
      textContent: code
    });
    $.add(d.head || doc, script);
    return $.rm(script);
  };

  $.bytesToString = function(size) {
    var unit;
    unit = 0;
    while (size >= 1024) {
      size /= 1024;
      unit++;
    }
    size = unit > 1 ? Math.round(size * 100) / 100 : Math.round(size);
    return "" + size + " " + ['B', 'KB', 'MB', 'GB'][unit];
  };

  $.item = function(key, val) {
    var item;
    item = {};
    item[key] = val;
    return item;
  };

  $.syncing = {};

  $.sync = (function() {
    $.on(window, 'storage', function(_arg) {
      var cb, key, newValue;
      key = _arg.key, newValue = _arg.newValue;
      if (cb = $.syncing[key]) {
        return cb(JSON.parse(newValue), key);
      }
    });
    return function(key, cb) {
      return $.syncing[g.NAMESPACE + key] = cb;
    };
  })();

  $["delete"] = function(keys) {
    var key, _i, _len;
    if (!(keys instanceof Array)) {
      keys = [keys];
    }
    for (_i = 0, _len = keys.length; _i < _len; _i++) {
      key = keys[_i];
      key = g.NAMESPACE + key;
      localStorage.removeItem(key);
      GM_deleteValue(key);
    }
  };

  $.get = function(key, val, cb) {
    var items;
    if (typeof cb === 'function') {
      items = $.item(key, val);
    } else {
      items = key;
      cb = val;
    }
    return $.queueTask(function() {
      for (key in items) {
        if (val = GM_getValue(g.NAMESPACE + key)) {
          items[key] = JSON.parse(val);
        }
      }
      return cb(items);
    });
  };

  $.set = (function() {
    var set;
    set = function(key, val) {
      key = g.NAMESPACE + key;
      val = JSON.stringify(val);
      if (key in $.syncing) {
        localStorage.setItem(key, val);
      }
      return GM_setValue(key, val);
    };
    return function(keys, val) {
      var key;
      if (typeof keys === 'string') {
        set(keys, val);
        return;
      }
      for (key in keys) {
        val = keys[key];
        set(key, val);
      }
    };
  })();

  Polyfill = {
    init: function() {},
    notificationPermission: function() {
      if (!window.Notification || 'permission' in Notification) {
        return;
      }
      return Object.defineProperty(Notification, 'permission', {
        get: function() {
          switch (webkitNotifications.checkPermission()) {
            case 0:
              return 'granted';
            case 1:
              return 'default';
            case 2:
              return 'denied';
          }
        }
      });
    },
    toBlob: function() {
      var _base;
      return (_base = HTMLCanvasElement.prototype).toBlob || (_base.toBlob = function(cb) {
        var data, i, l, ui8a, _i;
        data = atob(this.toDataURL().slice(22));
        l = data.length;
        ui8a = new Uint8Array(l);
        for (i = _i = 0; _i < l; i = _i += 1) {
          ui8a[i] = data.charCodeAt(i);
        }
        return cb(new Blob([ui8a], {
          type: 'image/png'
        }));
      });
    },
    visibility: function() {
      if (!('webkitHidden' in document)) {
        return;
      }
      Object.defineProperties(HTMLDocument.prototype, {
        visibilityState: {
          get: function() {
            return this.webkitVisibilityState;
          }
        },
        hidden: {
          get: function() {
            return this.webkitHidden;
          }
        }
      });
      return $.on(d, 'webkitvisibilitychange', function() {
        return $.event('visibilitychange');
      });
    }
  };

  UI = (function() {
    var Menu, dialog, drag, dragend, dragstart, hover, hoverend, hoverstart, touchend, touchmove;
    dialog = function(id, position, html) {
      var el;
      el = $.el('div', {
        className: 'dialog',
        innerHTML: html,
        id: id
      });
      el.style.cssText = position;
      $.get("" + id + ".position", position, function(item) {
        return el.style.cssText = item["" + id + ".position"];
      });
      $.on($('.move', el), 'touchstart mousedown', dragstart);
      return el;
    };
    Menu = (function() {
      var currentMenu, lastToggledButton;

      currentMenu = null;

      lastToggledButton = null;

      function Menu(type) {
        this.type = type;
        this.addEntry = __bind(this.addEntry, this);
        this.keybinds = __bind(this.keybinds, this);
        this.close = __bind(this.close, this);
        $.on(d, 'AddMenuEntry', this.addEntry);
        this.entries = [];
      }

      Menu.prototype.makeMenu = function() {
        var menu;
        menu = $.el('div', {
          className: 'dialog',
          id: 'menu',
          tabIndex: 0
        });
        $.on(menu, 'click', function(e) {
          return e.stopPropagation();
        });
        $.on(menu, 'keydown', this.keybinds);
        return menu;
      };

      Menu.prototype.toggle = function(e, button, data) {
        var previousButton;
        e.preventDefault();
        e.stopPropagation();
        if (currentMenu) {
          previousButton = lastToggledButton;
          this.close();
          if (previousButton === button) {
            return;
          }
        }
        if (!this.entries.length) {
          return;
        }
        return this.open(button, data);
      };

      Menu.prototype.open = function(button, data) {
        var bLeft, bRect, bTop, cHeight, cWidth, entry, mRect, menu, prevEntry, _i, _len, _ref;
        menu = this.makeMenu();
        currentMenu = menu;
        lastToggledButton = button;
        _ref = this.entries;
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
          entry = _ref[_i];
          this.insertEntry(entry, menu, data);
        }
        entry = $('.entry', menu);
        while (prevEntry = this.findNextEntry(entry, -1)) {
          entry = prevEntry;
        }
        this.focus(entry);
        $.on(d, 'click', this.close);
        $.on(d, 'CloseMenu', this.close);
        $.add(button, menu);
        mRect = menu.getBoundingClientRect();
        bRect = button.getBoundingClientRect();
        bTop = window.scrollY + bRect.top;
        bLeft = window.scrollX + bRect.left;
        cHeight = doc.clientHeight;
        cWidth = doc.clientWidth;
        if (bRect.top + bRect.height + mRect.height < cHeight) {
          $.addClass(menu, 'top');
          $.rmClass(menu, 'bottom');
        } else {
          $.addClass(menu, 'bottom');
          $.rmClass(menu, 'top');
        }
        if (bRect.left + mRect.width < cWidth) {
          $.addClass(menu, 'left');
          $.rmClass(menu, 'right');
        } else {
          $.addClass(menu, 'right');
          $.rmClass(menu, 'left');
        }
        return menu.focus();
      };

      Menu.prototype.insertEntry = function(entry, parent, data) {
        var subEntry, submenu, _i, _len, _ref;
        if (typeof entry.open === 'function') {
          if (!entry.open(data)) {
            return;
          }
        }
        $.add(parent, entry.el);
        if (!entry.subEntries) {
          return;
        }
        if (submenu = $('.submenu', entry.el)) {
          $.rm(submenu);
        }
        submenu = $.el('div', {
          className: 'dialog submenu'
        });
        _ref = entry.subEntries;
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
          subEntry = _ref[_i];
          this.insertEntry(subEntry, submenu, data);
        }
        $.add(entry.el, submenu);
      };

      Menu.prototype.close = function() {
        $.rm(currentMenu);
        currentMenu = null;
        lastToggledButton = null;
        return $.off(d, 'click CloseMenu', this.close);
      };

      Menu.prototype.findNextEntry = function(entry, direction) {
        var entries;
        entries = __slice.call(entry.parentNode.children);
        entries.sort(function(first, second) {
          return first.style.order - second.style.order;
        });
        return entries[entries.indexOf(entry) + direction];
      };

      Menu.prototype.keybinds = function(e) {
        var entry, next, nextPrev, subEntry, submenu;
        entry = $('.focused', currentMenu);
        while (subEntry = $('.focused', entry)) {
          entry = subEntry;
        }
        switch (e.keyCode) {
          case 27:
            lastToggledButton.focus();
            this.close();
            break;
          case 13:
          case 32:
            entry.click();
            break;
          case 38:
            if (next = this.findNextEntry(entry, -1)) {
              this.focus(next);
            }
            break;
          case 40:
            if (next = this.findNextEntry(entry, +1)) {
              this.focus(next);
            }
            break;
          case 39:
            if ((submenu = $('.submenu', entry)) && (next = submenu.firstElementChild)) {
              while (nextPrev = this.findNextEntry(next, -1)) {
                next = nextPrev;
              }
              this.focus(next);
            }
            break;
          case 37:
            if (next = $.x('parent::*[contains(@class,"submenu")]/parent::*', entry)) {
              this.focus(next);
            }
            break;
          default:
            return;
        }
        e.preventDefault();
        return e.stopPropagation();
      };

      Menu.prototype.focus = function(entry) {
        var cHeight, cWidth, eRect, focused, sRect, submenu, _i, _len, _ref;
        while (focused = $.x('parent::*/child::*[contains(@class,"focused")]', entry)) {
          $.rmClass(focused, 'focused');
        }
        _ref = $$('.focused', entry);
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
          focused = _ref[_i];
          $.rmClass(focused, 'focused');
        }
        $.addClass(entry, 'focused');
        if (!(submenu = $('.submenu', entry))) {
          return;
        }
        sRect = submenu.getBoundingClientRect();
        eRect = entry.getBoundingClientRect();
        cHeight = doc.clientHeight;
        cWidth = doc.clientWidth;
        if (eRect.top + sRect.height < cHeight) {
          $.addClass(submenu, 'top');
          $.rmClass(submenu, 'bottom');
        } else {
          $.addClass(submenu, 'bottom');
          $.rmClass(submenu, 'top');
        }
        if (eRect.right + sRect.width < cWidth) {
          $.addClass(submenu, 'left');
          return $.rmClass(submenu, 'right');
        } else {
          $.addClass(submenu, 'right');
          return $.rmClass(submenu, 'left');
        }
      };

      Menu.prototype.addEntry = function(e) {
        var entry;
        entry = e.detail;
        if (entry.type !== this.type) {
          return;
        }
        this.parseEntry(entry);
        return this.entries.push(entry);
      };

      Menu.prototype.parseEntry = function(entry) {
        var el, subEntries, subEntry, _i, _len;
        el = entry.el, subEntries = entry.subEntries;
        $.addClass(el, 'entry');
        $.on(el, 'focus mouseover', (function(e) {
          e.stopPropagation();
          return this.focus(el);
        }).bind(this));
        el.style.order = entry.order || 100;
        if (!subEntries) {
          return;
        }
        $.addClass(el, 'has-submenu');
        for (_i = 0, _len = subEntries.length; _i < _len; _i++) {
          subEntry = subEntries[_i];
          this.parseEntry(subEntry);
        }
      };

      return Menu;

    })();
    dragstart = function(e) {
      var el, isTouching, o, rect, screenHeight, screenWidth;
      if (e.type === 'mousedown' && e.button !== 0) {
        return;
      }
      e.preventDefault();
      if (isTouching = e.type === 'touchstart') {
        e = e.changedTouches[e.changedTouches.length - 1];
      }
      el = $.x('ancestor::div[contains(@class,"dialog")][1]', this);
      rect = el.getBoundingClientRect();
      screenHeight = doc.clientHeight;
      screenWidth = doc.clientWidth;
      o = {
        id: el.id,
        style: el.style,
        dx: e.clientX - rect.left,
        dy: e.clientY - rect.top,
        height: screenHeight - rect.height,
        width: screenWidth - rect.width,
        screenHeight: screenHeight,
        screenWidth: screenWidth,
        isTouching: isTouching
      };
      if (isTouching) {
        o.identifier = e.identifier;
        o.move = touchmove.bind(o);
        o.up = touchend.bind(o);
        $.on(d, 'touchmove', o.move);
        return $.on(d, 'touchend touchcancel', o.up);
      } else {
        o.move = drag.bind(o);
        o.up = dragend.bind(o);
        $.on(d, 'mousemove', o.move);
        return $.on(d, 'mouseup', o.up);
      }
    };
    touchmove = function(e) {
      var touch, _i, _len, _ref;
      _ref = e.changedTouches;
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        touch = _ref[_i];
        if (touch.identifier === this.identifier) {
          drag.call(this, touch);
          return;
        }
      }
    };
    drag = function(e) {
      var bottom, clientX, clientY, left, right, style, top;
      clientX = e.clientX, clientY = e.clientY;
      left = clientX - this.dx;
      left = left < 10 ? 0 : this.width - left < 10 ? null : left / this.screenWidth * 100 + '%';
      top = clientY - this.dy;
      top = top < 10 ? 0 : this.height - top < 10 ? null : top / this.screenHeight * 100 + '%';
      right = left === null ? 0 : null;
      bottom = top === null ? 0 : null;
      style = this.style;
      style.left = left;
      style.right = right;
      style.top = top;
      return style.bottom = bottom;
    };
    touchend = function(e) {
      var touch, _i, _len, _ref;
      _ref = e.changedTouches;
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        touch = _ref[_i];
        if (touch.identifier === this.identifier) {
          dragend.call(this);
          return;
        }
      }
    };
    dragend = function() {
      if (this.isTouching) {
        $.off(d, 'touchmove', this.move);
        $.off(d, 'touchend touchcancel', this.up);
      } else {
        $.off(d, 'mousemove', this.move);
        $.off(d, 'mouseup', this.up);
      }
      return $.set("" + this.id + ".position", this.style.cssText);
    };
    hoverstart = function(_arg) {
      var asapTest, cb, el, endEvents, latestEvent, o, root;
      root = _arg.root, el = _arg.el, latestEvent = _arg.latestEvent, endEvents = _arg.endEvents, asapTest = _arg.asapTest, cb = _arg.cb;
      o = {
        root: root,
        el: el,
        style: el.style,
        cb: cb,
        endEvents: endEvents,
        latestEvent: latestEvent,
        clientHeight: doc.clientHeight,
        clientWidth: doc.clientWidth
      };
      o.hover = hover.bind(o);
      o.hoverend = hoverend.bind(o);
      $.asap(function() {
        return !el.parentNode || asapTest();
      }, function() {
        if (el.parentNode) {
          return o.hover(o.latestEvent);
        }
      });
      $.on(root, endEvents, o.hoverend);
      $.on(root, 'mousemove', o.hover);
      o.workaround = function(e) {
        if (!root.contains(e.target)) {
          return o.hoverend();
        }
      };
      return $.on(doc, 'mousemove', o.workaround);
    };
    hover = function(e) {
      var clientX, clientY, height, left, right, style, top, _ref;
      this.latestEvent = e;
      height = this.el.offsetHeight;
      clientX = e.clientX, clientY = e.clientY;
      top = clientY - 120;
      top = this.clientHeight <= height || top <= 0 ? 0 : top + height >= this.clientHeight ? this.clientHeight - height : top;
      _ref = clientX <= this.clientWidth - 400 ? [clientX + 45 + 'px', null] : [null, this.clientWidth - clientX + 45 + 'px'], left = _ref[0], right = _ref[1];
      style = this.style;
      style.top = top + 'px';
      style.left = left;
      return style.right = right;
    };
    hoverend = function() {
      $.rm(this.el);
      $.off(this.root, this.endEvents, this.hoverend);
      $.off(this.root, 'mousemove', this.hover);
      $.off(doc, 'mousemove', this.workaround);
      if (this.cb) {
        return this.cb.call(this);
      }
    };
    return {
      dialog: dialog,
      Menu: Menu,
      hover: hoverstart
    };
  })();

  Header = {
    init: function() {
      var barPositionToggler, botBoardToggler, catalogToggler, customNavToggler, editCustomNav, headerEl, headerToggler, menuButton, topBoardToggler;
      headerEl = $.el('div', {
        id: 'header',
        innerHTML: "<div id=\"header-bar\" class=\"dialog\"><span id=\"shortcuts\" class=\"brackets-wrap\"></span><span id=\"board-list\"><span id=\"custom-board-list\"></span><span id=\"full-board-list\" hidden></span></span><div id=\"toggle-header-bar\" title=\"Toggle the header auto-hiding.\"></div></div><div id=\"notifications\"></div>"
      });
      this.bar = $('#header-bar', headerEl);
      this.toggle = $('#toggle-header-bar', this.bar);
      this.noticesRoot = $('#notifications', headerEl);
      this.menu = new UI.Menu('header');
      menuButton = $.el('a', {
        className: 'menu-button',
        innerHTML: '<i class="fa fa-reorder"></i>',
        href: 'javascript:;'
      });
      $.on(menuButton, 'click', this.menuToggle);
      this.addShortcut(menuButton, 0);
      $.on(this.toggle, 'mousedown', this.toggleBarVisibility);
      $.on(window, 'load hashchange', Header.hashScroll);
      $.on(d, 'CreateNotification', this.createNotification);
      headerToggler = $.el('label', {
        innerHTML: '<input type=checkbox name="Header auto-hide"> Auto-hide header'
      });
      barPositionToggler = $.el('label', {
        innerHTML: '<input type=checkbox name="Bottom header"> Bottom header'
      });
      catalogToggler = $.el('label', {
        innerHTML: '<input type=checkbox name="Header catalog links"> Use catalog board links'
      });
      topBoardToggler = $.el('label', {
        innerHTML: '<input type=checkbox name="Top Board List"> Top original board list'
      });
      botBoardToggler = $.el('label', {
        innerHTML: '<input type=checkbox name="Bottom Board List"> Bottom original board list'
      });
      customNavToggler = $.el('label', {
        innerHTML: '<input type=checkbox name="Custom Board Navigation"> Custom board navigation'
      });
      editCustomNav = $.el('a', {
        textContent: 'Edit custom board navigation',
        href: 'javascript:;'
      });
      this.headerToggler = headerToggler.firstElementChild;
      this.barPositionToggler = barPositionToggler.firstElementChild;
      this.catalogToggler = catalogToggler.firstElementChild;
      this.topBoardToggler = topBoardToggler.firstElementChild;
      this.botBoardToggler = botBoardToggler.firstElementChild;
      this.customNavToggler = customNavToggler.firstElementChild;
      $.on(this.headerToggler, 'change', this.toggleBarVisibility);
      $.on(this.barPositionToggler, 'change', this.toggleBarPosition);
      $.on(this.catalogToggler, 'change', this.toggleCatalogLinks);
      $.on(this.topBoardToggler, 'change', this.toggleOriginalBoardList);
      $.on(this.botBoardToggler, 'change', this.toggleOriginalBoardList);
      $.on(this.customNavToggler, 'change', this.toggleCustomNav);
      $.on(editCustomNav, 'click', this.editCustomNav);
      this.setBarVisibility(Conf['Header auto-hide']);
      this.setBarPosition(Conf['Bottom header']);
      this.setTopBoardList(Conf['Top Board List']);
      this.setBotBoardList(Conf['Bottom Board List']);
      $.sync('Header auto-hide', this.setBarVisibility);
      $.sync('Bottom header', this.setBarPosition);
      $.sync('Top Board List', this.setTopBoardList);
      $.sync('Bottom Board List', this.setBotBoardList);
      $.event('AddMenuEntry', {
        type: 'header',
        el: $.el('span', {
          textContent: 'Header'
        }),
        order: 105,
        subEntries: [
          {
            el: headerToggler
          }, {
            el: barPositionToggler
          }, {
            el: catalogToggler
          }, {
            el: topBoardToggler
          }, {
            el: botBoardToggler
          }, {
            el: customNavToggler
          }, {
            el: editCustomNav
          }
        ]
      });
      $.asap((function() {
        return d.body;
      }), function() {
        if (!Main.isThisPageLegit()) {
          return;
        }
        $.asap((function() {
          return $.id('boardNavMobile') || d.readyState !== 'loading';
        }), Header.setBoardList);
        return $.prepend(d.body, headerEl);
      });
      $.ready(function() {
        var a;
        if (a = $("a[href*='/" + g.BOARD + "/']", $.id('boardNavDesktopFoot'))) {
          a.className = 'current';
        }
        Header.setCatalogLinks(Conf['Header catalog links']);
        return $.sync('Header catalog links', Header.setCatalogLinks);
      });
      return this.enableDesktopNotifications();
    },
    setBoardList: function() {
      var a, btn, fullBoardList, nav;
      nav = $.id('boardNavDesktop');
      if (a = $("a[href*='/" + g.BOARD + "/']", nav)) {
        a.className = 'current';
      }
      fullBoardList = $('#full-board-list', Header.bar);
      fullBoardList.innerHTML = nav.innerHTML;
      $.rm($('#navtopright', fullBoardList));
      btn = $.el('span', {
        className: 'hide-board-list-button brackets-wrap',
        innerHTML: '<a href=javascript:;> - </a>'
      });
      $.on(btn, 'click', Header.toggleBoardList);
      $.add(fullBoardList, btn);
      Header.setCustomNav(Conf['Custom Board Navigation']);
      Header.generateBoardList(Conf['boardnav']);
      $.sync('Custom Board Navigation', Header.setCustomNav);
      return $.sync('boardnav', Header.generateBoardList);
    },
    generateBoardList: function(text) {
      var as, list, nodes;
      list = $('#custom-board-list', Header.bar);
      $.rmAll(list);
      if (!text) {
        return;
      }
      as = $$('#full-board-list a[title]', Header.bar);
      nodes = text.match(/[\w@]+(-(all|title|replace|full|index|catalog|text:"[^"]+"))*|[^\w@]+/g).map(function(t) {
        var a, board, m, _i, _len;
        if (/^[^\w@]/.test(t)) {
          return $.tn(t);
        }
        if (/^toggle-all/.test(t)) {
          a = $.el('a', {
            className: 'show-board-list-button',
            textContent: (t.match(/-text:"(.+)"/) || [null, '+'])[1],
            href: 'javascript:;'
          });
          $.on(a, 'click', Header.toggleBoardList);
          return a;
        }
        board = /^current/.test(t) ? g.BOARD.ID : t.match(/^[^-]+/)[0];
        for (_i = 0, _len = as.length; _i < _len; _i++) {
          a = as[_i];
          if (a.textContent === board) {
            a = a.cloneNode(true);
            a.textContent = /-title/.test(t) || /-replace/.test(t) && $.hasClass(a, 'current') ? a.title : /-full/.test(t) ? "/" + board + "/ - " + a.title : (m = t.match(/-text:"(.+)"/)) ? m[1] : a.textContent;
            if (m = t.match(/-(index|catalog)/)) {
              a.dataset.only = m[1];
              a.href = "//boards.4chan.org/" + board + "/";
              if (m[1] === 'catalog') {
                a.href += 'catalog';
              }
            }
            if (board === '@') {
              $.addClass(a, 'navSmall');
            }
            return a;
          }
        }
        return $.tn(t);
      });
      return $.add(list, nodes);
    },
    toggleBoardList: function() {
      var bar, custom, full, showBoardList;
      bar = Header.bar;
      custom = $('#custom-board-list', bar);
      full = $('#full-board-list', bar);
      showBoardList = !full.hidden;
      custom.hidden = !showBoardList;
      return full.hidden = showBoardList;
    },
    setBarVisibility: function(hide) {
      Header.headerToggler.checked = hide;
      $.event('CloseMenu');
      return (hide ? $.addClass : $.rmClass)(Header.bar, 'autohide');
    },
    toggleBarVisibility: function(e) {
      var hide, message;
      if (e.type === 'mousedown' && e.button !== 0) {
        return;
      }
      hide = this.nodeName === 'INPUT' ? this.checked : !$.hasClass(Header.bar, 'autohide');
      Conf['Header auto-hide'] = hide;
      $.set('Header auto-hide', hide);
      Header.setBarVisibility(hide);
      message = hide ? 'The header bar will automatically hide itself.' : 'The header bar will remain visible.';
      return new Notice('info', message, 2);
    },
    setBarPosition: function(bottom) {
      Header.barPositionToggler.checked = bottom;
      $.event('CloseMenu');
      if (bottom) {
        $.addClass(doc, 'bottom-header');
        $.rmClass(doc, 'top-header');
        return Header.bar.parentNode.className = 'bottom';
      } else {
        $.addClass(doc, 'top-header');
        $.rmClass(doc, 'bottom-header');
        return Header.bar.parentNode.className = 'top';
      }
    },
    toggleBarPosition: function() {
      $.cb.checked.call(this);
      return Header.setBarPosition(this.checked);
    },
    setCatalogLinks: function(useCatalog) {
      var a, as, path, _i, _len;
      Header.catalogToggler.checked = useCatalog;
      as = $$(['#board-list a[href*="boards.4chan.org"]', '#boardNavDesktop a[href*="boards.4chan.org"]', '#boardNavDesktopFoot a[href*="boards.4chan.org"]'].join(', '));
      path = useCatalog ? 'catalog' : '';
      for (_i = 0, _len = as.length; _i < _len; _i++) {
        a = as[_i];
        if (!a.dataset.only) {
          a.pathname = "/" + (a.pathname.split('/')[1]) + "/" + path;
        }
      }
    },
    toggleCatalogLinks: function() {
      $.cb.checked.call(this);
      return Header.setCatalogLinks(this.checked);
    },
    setTopBoardList: function(show) {
      Header.topBoardToggler.checked = show;
      if (show) {
        return $.addClass(doc, 'show-original-top-board-list');
      } else {
        return $.rmClass(doc, 'show-original-top-board-list');
      }
    },
    setBotBoardList: function(show) {
      Header.botBoardToggler.checked = show;
      if (show) {
        return $.addClass(doc, 'show-original-bot-board-list');
      } else {
        return $.rmClass(doc, 'show-original-bot-board-list');
      }
    },
    toggleOriginalBoardList: function() {
      $.cb.checked.call(this);
      return (this.name === 'Top Board List' ? Header.setTopBoardList : Header.setBotBoardList)(this.checked);
    },
    setCustomNav: function(show) {
      var btn, cust, full, _ref;
      Header.customNavToggler.checked = show;
      cust = $('#custom-board-list', Header.bar);
      full = $('#full-board-list', Header.bar);
      btn = $('.hide-board-list-button', full);
      return _ref = show ? [false, true, false] : [true, false, true], cust.hidden = _ref[0], full.hidden = _ref[1], btn.hidden = _ref[2], _ref;
    },
    toggleCustomNav: function() {
      $.cb.checked.call(this);
      return Header.setCustomNav(this.checked);
    },
    editCustomNav: function() {
      var settings;
      Settings.open('Rice');
      settings = $.id('fourchanx-settings');
      return $('input[name=boardnav]', settings).focus();
    },
    hashScroll: function() {
      var hash, post;
      hash = this.location.hash.slice(1);
      if (!(/^p\d+$/.test(hash) && (post = $.id(hash)))) {
        return;
      }
      if ((Get.postFromRoot(post)).isHidden) {
        return;
      }
      return Header.scrollTo(post);
    },
    scrollTo: function(root, down, needed) {
      var x;
      if (down) {
        x = Header.getBottomOf(root);
        if (!(needed && x >= 0)) {
          return window.scrollBy(0, -x);
        }
      } else {
        x = Header.getTopOf(root);
        if (!(needed && x >= 0)) {
          return window.scrollBy(0, x);
        }
      }
    },
    scrollToIfNeeded: function(root, down) {
      return Header.scrollTo(root, down, true);
    },
    getTopOf: function(root) {
      var headRect, top;
      top = root.getBoundingClientRect().top;
      if (!Conf['Bottom header']) {
        headRect = Header.toggle.getBoundingClientRect();
        top -= headRect.top + headRect.height;
      }
      return top;
    },
    getBottomOf: function(root) {
      var bottom, clientHeight, headRect;
      clientHeight = doc.clientHeight;
      bottom = clientHeight - root.getBoundingClientRect().bottom;
      if (Conf['Bottom header']) {
        headRect = Header.toggle.getBoundingClientRect();
        bottom -= clientHeight - headRect.bottom + headRect.height;
      }
      return bottom;
    },
    addShortcut: function(el, index) {
      var shortcut, shortcuts;
      shortcut = $.el('span', {
        className: 'shortcut'
      });
      shortcut.dataset.index = index;
      $.add(shortcut, el);
      shortcuts = $('#shortcuts', Header.bar);
      return $.add(shortcuts, __slice.call(shortcuts.childNodes).concat(shortcut).sort(function(a, b) {
        return a.dataset.index - b.dataset.index;
      }));
    },
    menuToggle: function(e) {
      return Header.menu.toggle(e, this, g);
    },
    createNotification: function(e) {
      var cb, content, lifetime, notice, type, _ref;
      _ref = e.detail, type = _ref.type, content = _ref.content, lifetime = _ref.lifetime, cb = _ref.cb;
      notice = new Notice(type, content, lifetime);
      if (cb) {
        return cb(notice);
      }
    },
    areNotificationsEnabled: false,
    enableDesktopNotifications: function() {
      var authorize, disable, el, notice, _ref;
      if (!(window.Notification && Conf['Desktop Notifications'])) {
        return;
      }
      switch (Notification.permission) {
        case 'granted':
          Header.areNotificationsEnabled = true;
          return;
        case 'denied':
          return;
      }
      el = $.el('span', {
        innerHTML: "Desktop notification permissions are not granted.\n[<a href='https://github.com/MayhemYDG/4chan-x/wiki/FAQ#desktop-notifications' target=_blank>FAQ</a>]<br>\n<button>Authorize</button> or <button>Disable</button>"
      });
      _ref = $$('button', el), authorize = _ref[0], disable = _ref[1];
      $.on(authorize, 'click', function() {
        return Notification.requestPermission(function(status) {
          Header.areNotificationsEnabled = status === 'granted';
          if (status === 'default') {
            return;
          }
          return notice.close();
        });
      });
      $.on(disable, 'click', function() {
        $.set('Desktop Notifications', false);
        return notice.close();
      });
      return notice = new Notice('info', el);
    }
  };

  Notice = (function() {
    function Notice(type, content, timeout) {
      this.timeout = timeout;
      this.close = __bind(this.close, this);
      this.add = __bind(this.add, this);
      this.el = $.el('div', {
        innerHTML: '<a href=javascript:; class=close title=Close>×</a><div class=message></div>'
      });
      this.el.style.opacity = 0;
      this.setType(type);
      $.on(this.el.firstElementChild, 'click', this.close);
      if (typeof content === 'string') {
        content = $.tn(content);
      }
      $.add(this.el.lastElementChild, content);
      $.ready(this.add);
    }

    Notice.prototype.setType = function(type) {
      return this.el.className = "notification " + type;
    };

    Notice.prototype.add = function() {
      if (d.hidden) {
        $.on(d, 'visibilitychange', this.add);
        return;
      }
      $.off(d, 'visibilitychange', this.add);
      $.add(Header.noticesRoot, this.el);
      this.el.clientHeight;
      this.el.style.opacity = 1;
      if (this.timeout) {
        return setTimeout(this.close, this.timeout * $.SECOND);
      }
    };

    Notice.prototype.close = function() {
      $.off(d, 'visibilitychange', this.add);
      return $.rm(this.el);
    };

    return Notice;

  })();

  Settings = {
    init: function() {
      var link, settings;
      link = $.el('a', {
        className: 'settings-link',
        textContent: '4chan X Settings',
        href: 'javascript:;'
      });
      $.on(link, 'click', Settings.open);
      $.event('AddMenuEntry', {
        type: 'header',
        el: link,
        order: 111
      });
      link = $.el('a', {
        className: 'fourchan-settings-link',
        textContent: '4chan Settings',
        href: 'javascript:;'
      });
      $.on(link, 'click', function() {
        return $.id('settingsWindowLink').click();
      });
      $.event('AddMenuEntry', {
        type: 'header',
        el: link,
        order: 110,
        open: function() {
          return Conf['Enable 4chan\'s Extension'];
        }
      });
      $.get('previousversion', null, function(item) {
        var changelog, el, now, previous;
        if (previous = item['previousversion']) {
          if (previous === g.VERSION) {
            return;
          }
          changelog = 'https://github.com/MayhemYDG/4chan-x/blob/v3/CHANGELOG.md';
          el = $.el('span', {
            innerHTML: "4chan X has been updated to <a href='" + changelog + "' target=_blank>version " + g.VERSION + "</a>."
          });
          new Notice('info', el, 30);
        } else {
          $.on(d, '4chanXInitFinished', Settings.open);
        }
        Conf['archives'] = Redirect.archives;
        now = Date.now();
        return $.set({
          archives: Conf['archives'],
          lastarchivecheck: now,
          previousversion: g.VERSION
        });
      });
      Settings.addSection('Main', Settings.main);
      Settings.addSection('Filter', Settings.filter);
      Settings.addSection('QR', Settings.qr);
      Settings.addSection('Sauce', Settings.sauce);
      Settings.addSection('Rice', Settings.rice);
      Settings.addSection('Archives', Settings.archives);
      Settings.addSection('Keybinds', Settings.keybinds);
      $.on(d, 'AddSettingsSection', Settings.addSection);
      $.on(d, 'OpenSettings', function(e) {
        return Settings.open(e.detail);
      });
      if (Conf['Enable 4chan\'s Extension']) {
        return;
      }
      settings = JSON.parse(localStorage.getItem('4chan-settings')) || {};
      if (settings.disableAll) {
        return;
      }
      settings.disableAll = true;
      return localStorage.setItem('4chan-settings', JSON.stringify(settings));
    },
    open: function(openSection) {
      var html, link, links, overlay, section, sectionToOpen, _i, _len, _ref;
      $.off(d, '4chanXInitFinished', Settings.open);
      if (Settings.dialog) {
        return;
      }
      $.event('CloseMenu');
      html = "<div id=\"fourchanx-settings\" class=\"dialog\"><nav><div class=\"sections-list\"></div><div class=\"credits\"><a href=\"https://4chan-x.just-believe.in/\" target=\"_blank\">4chan X</a> |<a href=\"https://github.com/MayhemYDG/4chan-x/blob/v3/CHANGELOG.md\" target=\"_blank\">" + g.VERSION + "</a> |<a href=\"https://github.com/MayhemYDG/4chan-x/blob/v3/CONTRIBUTING.md#reporting-bugs-and-suggestions\" target=\"_blank\">Issues</a> |<a href=\"javascript:;\" class=\"close\" title=\"Close\">×</a></div></nav><hr><div class=\"section-container\"><section></section></div></div>";
      Settings.dialog = overlay = $.el('div', {
        id: 'overlay',
        innerHTML: html
      });
      links = [];
      _ref = Settings.sections;
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        section = _ref[_i];
        link = $.el('a', {
          className: "tab-" + section.hyphenatedTitle,
          textContent: section.title,
          href: 'javascript:;'
        });
        $.on(link, 'click', Settings.openSection.bind(section));
        links.push(link, $.tn(' | '));
        if (section.title === openSection) {
          sectionToOpen = link;
        }
      }
      links.pop();
      $.add($('.sections-list', overlay), links);
      (sectionToOpen ? sectionToOpen : links[0]).click();
      $.on($('.close', overlay), 'click', Settings.close);
      $.on(overlay, 'click', Settings.close);
      $.on(overlay.firstElementChild, 'click', function(e) {
        return e.stopPropagation();
      });
      d.body.style.width = "" + d.body.clientWidth + "px";
      $.addClass(d.body, 'unscroll');
      return $.add(d.body, overlay);
    },
    close: function() {
      if (!Settings.dialog) {
        return;
      }
      d.body.style.removeProperty('width');
      $.rmClass(d.body, 'unscroll');
      $.rm(Settings.dialog);
      return delete Settings.dialog;
    },
    sections: [],
    addSection: function(title, open) {
      var hyphenatedTitle, _ref;
      if (typeof title !== 'string') {
        _ref = title.detail, title = _ref.title, open = _ref.open;
      }
      hyphenatedTitle = title.toLowerCase().replace(/\s+/g, '-');
      return Settings.sections.push({
        title: title,
        hyphenatedTitle: hyphenatedTitle,
        open: open
      });
    },
    openSection: function() {
      var section, selected;
      if (selected = $('.tab-selected', Settings.dialog)) {
        $.rmClass(selected, 'tab-selected');
      }
      $.addClass($(".tab-" + this.hyphenatedTitle, Settings.dialog), 'tab-selected');
      section = $('section', Settings.dialog);
      $.rmAll(section);
      section.className = "section-" + this.hyphenatedTitle;
      this.open(section, g);
      return section.scrollTop = 0;
    },
    main: function(section) {
      var arr, button, description, div, fs, hiddenNum, input, inputs, items, key, obj, _ref;
      section.innerHTML = "<div class=\"imp-exp\"><button class=\"export\">Export Settings</button><button class=\"import\">Import Settings</button><input type=\"file\" style=\"visibility: hidden;\"></div><p class=\"imp-exp-result\"></p>";
      $.on($('.export', section), 'click', Settings["export"]);
      $.on($('.import', section), 'click', Settings["import"]);
      $.on($('input', section), 'change', Settings.onImport);
      items = {};
      inputs = {};
      _ref = Config.main;
      for (key in _ref) {
        obj = _ref[key];
        fs = $.el('fieldset', {
          innerHTML: "<legend>" + key + "</legend>"
        });
        for (key in obj) {
          arr = obj[key];
          description = arr[1];
          div = $.el('div', {
            innerHTML: "<label><input type=checkbox name=\"" + key + "\">" + key + "</label><span class=description>: " + description + "</span>"
          });
          input = $('input', div);
          $.on(input, 'change', $.cb.checked);
          items[key] = Conf[key];
          inputs[key] = input;
          $.add(fs, div);
        }
        $.add(section, fs);
      }
      $.get(items, function(items) {
        var val;
        for (key in items) {
          val = items[key];
          inputs[key].checked = val;
        }
      });
      div = $.el('div', {
        innerHTML: "<button></button><span class=description>: Clear manually-hidden threads and posts on all boards. Reload the page to apply."
      });
      button = $('button', div);
      hiddenNum = 0;
      $.get('hiddenThreads', {
        boards: {}
      }, function(item) {
        var ID, board, thread, _ref1;
        _ref1 = item.hiddenThreads.boards;
        for (ID in _ref1) {
          board = _ref1[ID];
          for (ID in board) {
            thread = board[ID];
            hiddenNum++;
          }
        }
        return button.textContent = "Hidden: " + hiddenNum;
      });
      $.get('hiddenPosts', {
        boards: {}
      }, function(item) {
        var ID, board, post, thread, _ref1;
        _ref1 = item.hiddenPosts.boards;
        for (ID in _ref1) {
          board = _ref1[ID];
          for (ID in board) {
            thread = board[ID];
            for (ID in thread) {
              post = thread[ID];
              hiddenNum++;
            }
          }
        }
        return button.textContent = "Hidden: " + hiddenNum;
      });
      $.on(button, 'click', function() {
        this.textContent = 'Hidden: 0';
        return $.get('hiddenThreads', {
          boards: {}
        }, function(item) {
          var boardID;
          for (boardID in item.hiddenThreads.boards) {
            localStorage.removeItem("4chan-hide-t-" + boardID);
          }
          return $["delete"](['hiddenThreads', 'hiddenPosts']);
        });
      });
      return $.after($('input[name="Stubs"]', section).parentNode.parentNode, div);
    },
    "export": function(now, data) {
      var a, db, p, _i, _len, _ref;
      if (typeof now !== 'number') {
        now = Date.now();
        data = {
          version: g.VERSION,
          date: now
        };
        _ref = DataBoard.keys;
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
          db = _ref[_i];
          Conf[db] = {
            boards: {}
          };
        }
        $.get(Conf, function(Conf) {
          delete Conf['archives'];
          data.Conf = Conf;
          return Settings["export"](now, data);
        });
        return;
      }
      a = $.el('a', {
        className: 'warning',
        textContent: 'Save me!',
        download: "4chan X v" + g.VERSION + "-" + now + ".json",
        href: "data:application/json;base64," + (btoa(unescape(encodeURIComponent(JSON.stringify(data, null, 2))))),
        target: '_blank'
      });
      p = $('.imp-exp-result', Settings.dialog);
      $.rmAll(p);
      return $.add(p, a);
    },
    "import": function() {
      return this.nextElementSibling.click();
    },
    onImport: function() {
      var file, output, reader;
      if (!(file = this.files[0])) {
        return;
      }
      output = this.parentNode.nextElementSibling;
      if (!confirm('Your current settings will be entirely overwritten, are you sure?')) {
        output.textContent = 'Import aborted.';
        return;
      }
      reader = new FileReader();
      reader.onload = function(e) {
        var data, err;
        try {
          data = JSON.parse(e.target.result);
          Settings.loadSettings(data);
          if (confirm('Import successful. Reload now?')) {
            return window.location.reload();
          }
        } catch (_error) {
          err = _error;
          output.textContent = 'Import failed due to an error.';
          return c.error(err.stack);
        }
      };
      return reader.readAsText(file);
    },
    loadSettings: function(data) {
      var key, val, version, _ref;
      version = data.version.split('.');
      if (version[0] === '2') {
        data = Settings.convertSettings(data, {
          'Disable 4chan\'s extension': '',
          'Catalog Links': '',
          'Reply Navigation': '',
          'Show Stubs': 'Stubs',
          'Image Auto-Gif': 'Auto-GIF',
          'Expand From Current': '',
          'Unread Favicon': 'Unread Tab Icon',
          'Post in Title': 'Thread Excerpt',
          'Auto Hide QR': '',
          'Open Reply in New Tab': '',
          'Remember QR size': '',
          'Quote Inline': 'Quote Inlining',
          'Quote Preview': 'Quote Previewing',
          'Indicate OP quote': 'Mark OP Quotes',
          'Indicate Cross-thread Quotes': 'Mark Cross-thread Quotes',
          'uniqueid': 'uniqueID',
          'mod': 'capcode',
          'country': 'flag',
          'md5': 'MD5',
          'openEmptyQR': 'Open empty QR',
          'openQR': 'Open QR',
          'openOptions': 'Open settings',
          'close': 'Close',
          'spoiler': 'Spoiler tags',
          'code': 'Code tags',
          'submit': 'Submit QR',
          'watch': 'Watch',
          'update': 'Update',
          'unreadCountTo0': '',
          'expandAllImages': 'Expand images',
          'expandImage': 'Expand image',
          'zero': 'Front page',
          'nextPage': 'Next page',
          'previousPage': 'Previous page',
          'nextThread': 'Next thread',
          'previousThread': 'Previous thread',
          'expandThread': 'Expand thread',
          'openThreadTab': 'Open thread',
          'openThread': 'Open thread tab',
          'nextReply': 'Next reply',
          'previousReply': 'Previous reply',
          'hide': 'Hide',
          'Scrolling': 'Auto Scroll',
          'Verbose': ''
        });
        data.Conf.sauces = data.Conf.sauces.replace(/\$\d/g, function(c) {
          switch (c) {
            case '$1':
              return '%TURL';
            case '$2':
              return '%URL';
            case '$3':
              return '%MD5';
            case '$4':
              return '%board';
            default:
              return c;
          }
        });
        _ref = Config.hotkeys;
        for (key in _ref) {
          val = _ref[key];
          if (key in data.Conf) {
            data.Conf[key] = data.Conf[key].replace(/ctrl|alt|meta/g, function(s) {
              return "" + (s[0].toUpperCase()) + s.slice(1);
            }).replace(/(^|.+\+)[A-Z]$/g, function(s) {
              return "Shift+" + s.slice(0, -1) + (s.slice(-1).toLowerCase());
            });
          }
        }
        data.Conf['WatchedThreads'] = data.WatchedThreads;
      }
      if (data.Conf['WatchedThreads']) {
        data.Conf['watchedThreads'] = {
          boards: ThreadWatcher.convert(data.Conf['WatchedThreads'])
        };
        delete data.Conf['WatchedThreads'];
      }
      return $.set(data.Conf);
    },
    convertSettings: function(data, map) {
      var newKey, prevKey;
      for (prevKey in map) {
        newKey = map[prevKey];
        if (newKey) {
          data.Conf[newKey] = data.Conf[prevKey];
        }
        delete data.Conf[prevKey];
      }
      return data;
    },
    filter: function(section) {
      var select;
      section.innerHTML = "<select name=\"filter\"><option value=\"guide\">Guide</option><option value=\"name\">Name</option><option value=\"uniqueID\">Unique ID</option><option value=\"tripcode\">Tripcode</option><option value=\"capcode\">Capcode</option><option value=\"email\">E-mail</option><option value=\"subject\">Subject</option><option value=\"comment\">Comment</option><option value=\"flag\">Flag</option><option value=\"filename\">Filename</option><option value=\"dimensions\">Image dimensions</option><option value=\"filesize\">Filesize</option><option value=\"MD5\">Image MD5</option></select><div></div>";
      select = $('select', section);
      $.on(select, 'change', Settings.selectFilter);
      return Settings.selectFilter.call(select);
    },
    selectFilter: function() {
      var div, name, ta;
      div = this.nextElementSibling;
      if ((name = this.value) !== 'guide') {
        $.rmAll(div);
        ta = $.el('textarea', {
          name: name,
          className: 'field',
          spellcheck: false
        });
        $.get(name, Conf[name], function(item) {
          return ta.value = item[name];
        });
        $.on(ta, 'change', $.cb.value);
        $.add(div, ta);
        return;
      }
      return div.innerHTML = "<div class=\"warning\" " + (Conf['Filter'] ? 'hidden' : '') + "><code>Filter</code> is disabled.</div><p>Use <a href=\"https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Regular_Expressions\">regular expressions</a>, one per line.<br>Lines starting with a <code>#</code> will be ignored.<br>For example, <code>/weeaboo/i</code> will filter posts containing the string `<code>weeaboo</code>`, case-insensitive.<br>MD5 filtering uses exact string matching, not regular expressions.</p><ul>You can use these settings with each regular expression, separate them with semicolons:<li>Per boards, separate them with commas. It is global if not specified.<br>For example: <code>boards:a,jp;</code>.</li><li>Filter OPs only along with their threads (`only`), replies only (`no`), or both (`yes`, this is default).<br>For example: <code>op:only;</code>, <code>op:no;</code> or <code>op:yes;</code>.</li><li>Overrule the `Show Stubs` setting if specified: create a stub (`yes`) or not (`no`).<br>For example: <code>stub:yes;</code> or <code>stub:no;</code>.</li><li>Highlight instead of hiding. You can specify a class name to use with a userstyle.<br>For example: <code>highlight;</code> or <code>highlight:wallpaper;</code>.</li><li>Highlighted OPs will have their threads put on top of the board index by default.<br>For example: <code>top:yes;</code> or <code>top:no;</code>.</li></ul>";
    },
    qr: function(section) {
      var ta;
      section.innerHTML = "<fieldset><legend>Quick Reply Personas <span class=\"warning\" " + (Conf['Quick Reply'] ? 'hidden' : '') + ">is disabled.</span></legend><textarea name=\"QR.personas\" class=\"field\" spellcheck=\"false\"></textarea><p>One item per line.<br>Items will be added in the relevant input's auto-completion list.<br>Password items will always be used, since there is no password input.<br>Lines starting with a <code>#</code> will be ignored.</p><ul>You can use these settings with each item, separate them with semicolons:<li>Possible items are: <code>name</code>, <code>email</code>, <code>subject</code> and <code>password</code>.</li><li>Wrap values of items with quotes, like this: <code>email:\"sage\"</code>.</li><li>Force values as defaults with the <code>always</code> keyword, for example: <code>email:\"sage\";always</code>.</li><li>Select specific boards for an item, separated with commas, for example: <code>email:\"sage\";boards:jp;always</code>.</li></ul></fieldset>";
      ta = $('textarea', section);
      $.get('QR.personas', Conf['QR.personas'], function(item) {
        return ta.value = item['QR.personas'];
      });
      return $.on(ta, 'change', $.cb.value);
    },
    sauce: function(section) {
      var ta;
      section.innerHTML = "<div class=\"warning\" " + (Conf['Sauce'] ? 'hidden' : '') + "><code>Sauce</code> is disabled.</div><div>Lines starting with a <code>#</code> will be ignored.</div><div>You can specify a display text by appending <code>;text:[text]</code> to the URL.</div><ul>These parameters will be replaced by their corresponding values:<li><code>%TURL</code>: Thumbnail URL.</li><li><code>%URL</code>: Full image URL.</li><li><code>%MD5</code>: MD5 hash.</li><li><code>%board</code>: Current board.</li></ul><textarea name=\"sauces\" class=\"field\" spellcheck=\"false\"></textarea>";
      ta = $('textarea', section);
      $.get('sauces', Conf['sauces'], function(item) {
        return ta.value = item['sauces'];
      });
      return $.on(ta, 'change', $.cb.value);
    },
    rice: function(section) {
      var input, inputs, items, name, _i, _len, _ref;
      section.innerHTML = "<fieldset><legend>Custom Board Navigation <span class=\"warning\" " + (Conf['Custom Board Navigation'] ? 'hidden' : '') + ">is disabled.</span></legend><div><input name=\"boardnav\" class=\"field\" spellcheck=\"false\"></div><div>In the following, <code>board</code> can translate to a board ID (<code>a</code>, <code>b</code>, etc...), the current board (<code>current</code>), or the Twitter link (<code>@</code>).</div><div>Board link: <code>board</code></div><div>Title link: <code>board-title</code></div><div>Board link (Replace with title when on that board): <code>board-replace</code></div><div>Full text link: <code>board-full</code></div><div>Custom text link: <code>board-text:\"VIP Board\"</code></div><div>Index-only link: <code>board-index</code></div><div>Catalog-only link: <code>board-catalog</code></div><div>Combinations are possible: <code>board-index-text:\"VIP Index\"</code></div><div>Full board list toggle: <code>toggle-all</code></div></fieldset><fieldset><legend>Time Formatting <span class=\"warning\" " + (Conf['Time Formatting'] ? 'hidden' : '') + ">is disabled.</span></legend><div><input name=\"time\" class=\"field\" spellcheck=\"false\">: <span class=\"time-preview\"></span></div><div>Supported <a href=\"//en.wikipedia.org/wiki/Date_%28Unix%29#Formatting\">format specifiers</a>:</div><div>Day: <code>%a</code>, <code>%A</code>, <code>%d</code>, <code>%e</code></div><div>Month: <code>%m</code>, <code>%b</code>, <code>%B</code></div><div>Year: <code>%y</code>, <code>20%y</code></div><div>Hour: <code>%k</code>, <code>%H</code>, <code>%l</code>, <code>%I</code>, <code>%p</code>, <code>%P</code></div><div>Minute: <code>%M</code></div><div>Second: <code>%S</code></div></fieldset><fieldset><legend>Quote Backlinks formatting <span class=\"warning\" " + (Conf['Quote Backlinks'] ? 'hidden' : '') + ">is disabled.</span></legend><div><input name=\"backlink\" class=\"field\" spellcheck=\"false\">: <span class=\"backlink-preview\"></span></div></fieldset><fieldset><legend>File Info Formatting <span class=\"warning\" " + (Conf['File Info Formatting'] ? 'hidden' : '') + ">is disabled.</span></legend><div><input name=\"fileInfo\" class=\"field\" spellcheck=\"false\">: <span class=\"fileText file-info-preview\"></span></div><div>Link: <code>%l</code> (truncated), <code>%L</code> (untruncated), <code>%T</code> (Unix timestamp)</div><div>Original file name: <code>%n</code> (truncated), <code>%N</code> (untruncated), <code>%t</code> (Unix timestamp)</div><div>Spoiler indicator: <code>%p</code></div><div>Size: <code>%B</code> (Bytes), <code>%K</code> (KB), <code>%M</code> (MB), <code>%s</code> (4chan default)</div><div>Resolution: <code>%r</code> (Displays 'PDF' for PDF files)</div></fieldset><fieldset><legend>Unread Tab Icon <span class=\"warning\" " + (Conf['Unread Tab Icon'] ? 'hidden' : '') + ">is disabled.</span></legend><select name=\"favicon\"><option value=\"ferongr\">ferongr</option><option value=\"xat-\">xat-</option><option value=\"Mayhem\">Mayhem</option><option value=\"Original\">Original</option></select><span class=\"favicon-preview\"></span></fieldset><fieldset><legend><label><input type=\"checkbox\" name=\"Custom CSS\" " + (Conf['Custom CSS'] ? 'checked' : '') + "> Custom CSS</label></legend><button id=\"apply-css\">Apply CSS</button><textarea name=\"usercss\" class=\"field\" spellcheck=\"false\" " + (Conf['Custom CSS'] ? '' : 'disabled') + "></textarea></fieldset>";
      items = {};
      inputs = {};
      _ref = ['boardnav', 'time', 'backlink', 'fileInfo', 'favicon', 'usercss'];
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        name = _ref[_i];
        input = $("[name=" + name + "]", section);
        items[name] = Conf[name];
        inputs[name] = input;
        $.on(input, 'change', $.cb.value);
      }
      $.get(items, function(items) {
        var event, key, val;
        for (key in items) {
          val = items[key];
          input = inputs[key];
          input.value = val;
          if (key === 'usercss') {
            continue;
          }
          event = key === 'favicon' || key === 'usercss' ? 'change' : 'input';
          $.on(input, event, Settings[key]);
          Settings[key].call(input);
        }
      });
      $.on($('input[name="Custom CSS"]', section), 'change', Settings.togglecss);
      return $.on($('#apply-css', section), 'click', Settings.usercss);
    },
    boardnav: function() {
      return Header.generateBoardList(this.value);
    },
    time: function() {
      var funk;
      funk = Time.createFunc(this.value);
      return this.nextElementSibling.textContent = funk(Time, new Date());
    },
    backlink: function() {
      return this.nextElementSibling.textContent = this.value.replace(/%id/, '123456789');
    },
    fileInfo: function() {
      var data, funk;
      data = {
        isReply: true,
        file: {
          URL: '//images.4chan.org/g/src/1334437723720.jpg',
          name: 'd9bb2efc98dd0df141a94399ff5880b7.jpg',
          size: '276 KB',
          sizeInBytes: 276 * 1024,
          dimensions: '1280x720',
          isImage: true,
          isSpoiler: true
        }
      };
      funk = FileInfo.createFunc(this.value);
      return this.nextElementSibling.innerHTML = funk(FileInfo, data);
    },
    favicon: function() {
      Favicon["switch"]();
      if (g.VIEW === 'thread' && Conf['Unread Tab Icon']) {
        Unread.update();
      }
      return this.nextElementSibling.innerHTML = "<img src=" + Favicon["default"] + ">\n<img src=" + Favicon.unreadSFW + ">\n<img src=" + Favicon.unreadNSFW + ">\n<img src=" + Favicon.unreadDead + ">";
    },
    togglecss: function() {
      if ($('textarea[name=usercss]', $.x('ancestor::fieldset[1]', this)).disabled = !this.checked) {
        CustomCSS.rmStyle();
      } else {
        CustomCSS.addStyle();
      }
      return $.cb.checked.call(this);
    },
    usercss: function() {
      return CustomCSS.update();
    },
    archives: function(section) {
      var archive, boardID, boards, button, data, row, rows, showLastUpdateTime, _i, _j, _k, _len, _len1, _len2, _ref, _ref1, _ref2;
      section.innerHTML = "<div class=\"warning\" " + (Conf['404 Redirect'] ? 'hidden' : '') + "><code>404 Redirect</code> is disabled.</div><p>Disabled selections indicate that only one archive is available for that board and redirection type.</p><p><button>Update now</button> Last updated: <time></time></p><table><caption>Archived boards</caption><thead><th>Board</th><th>Thread redirection</th><th>Post fetching</th><th>File redirection</th></thead><tbody></tbody></table>";
      showLastUpdateTime = function(time) {
        return $('time', section).textContent = new Date(time).toLocaleString();
      };
      button = $('button', section);
      $.on(button, 'click', function() {
        $["delete"]('lastarchivecheck');
        button.textContent = '...';
        button.disabled = true;
        return Redirect.update(function(time) {
          button.textContent = 'Updated';
          return showLastUpdateTime(time);
        });
      });
      $.get('lastarchivecheck', 0, function(_arg) {
        var lastarchivecheck;
        lastarchivecheck = _arg.lastarchivecheck;
        return showLastUpdateTime(lastarchivecheck);
      });
      boards = {};
      _ref = Conf['archives'];
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        archive = _ref[_i];
        _ref1 = archive.boards;
        for (_j = 0, _len1 = _ref1.length; _j < _len1; _j++) {
          boardID = _ref1[_j];
          data = boards[boardID] || (boards[boardID] = {
            thread: [],
            post: [],
            file: []
          });
          data.thread.push(archive);
          if (archive.software === 'foolfuuka') {
            data.post.push(archive);
          }
          if (__indexOf.call(archive.files, boardID) >= 0) {
            data.file.push(archive);
          }
        }
      }
      rows = [];
      _ref2 = Object.keys(boards).sort();
      for (_k = 0, _len2 = _ref2.length; _k < _len2; _k++) {
        boardID = _ref2[_k];
        row = $.el('tr');
        rows.push(row);
        $.add(row, $.el('th', {
          textContent: "/" + boardID + "/",
          className: boardID === g.BOARD.ID ? 'warning' : ''
        }));
        data = boards[boardID];
        Settings.addArchiveCell(row, boardID, data, 'thread');
        Settings.addArchiveCell(row, boardID, data, 'post');
        Settings.addArchiveCell(row, boardID, data, 'file');
      }
      $.add($('tbody', section), rows);
      return $.get('selectedArchives', Conf['selectedArchives'], function(_arg) {
        var option, selectedArchives, type, uid;
        selectedArchives = _arg.selectedArchives;
        for (boardID in selectedArchives) {
          data = selectedArchives[boardID];
          for (type in data) {
            uid = data[type];
            if (option = $("select[data-board-i-d='" + boardID + "'][data-type='" + type + "'] > option[value='" + uid + "']", section)) {
              option.selected = true;
            }
          }
        }
      });
    },
    addArchiveCell: function(row, boardID, data, type) {
      var archive, length, options, select, td, _i, _len, _ref;
      options = [];
      _ref = data[type];
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        archive = _ref[_i];
        options.push($.el('option', {
          textContent: archive.name,
          value: archive.uid
        }));
      }
      td = $.el('td');
      length = options.length;
      if (length) {
        td.innerHTML = '<select></select>';
        select = td.firstElementChild;
        if (!(select.disabled = length === 1)) {
          $.extend(select.dataset, {
            boardID: boardID,
            type: type
          });
          $.on(select, 'change', Settings.saveSelectedArchive);
        }
        $.add(select, options);
      } else {
        td.textContent = 'N/A';
      }
      return $.add(row, td);
    },
    saveSelectedArchive: function() {
      var _this = this;
      return $.get('selectedArchives', Conf['selectedArchives'], function(_arg) {
        var selectedArchives, _name;
        selectedArchives = _arg.selectedArchives;
        (selectedArchives[_name = _this.dataset.boardID] || (selectedArchives[_name] = {}))[_this.dataset.type] = +_this.value;
        return $.set('selectedArchives', selectedArchives);
      });
    },
    keybinds: function(section) {
      var arr, input, inputs, items, key, tbody, tr, _ref;
      section.innerHTML = "<div class=\"warning\" " + (Conf['Keybinds'] ? 'hidden' : '') + "><code>Keybinds</code> are disabled.</div><div>Allowed keys: <kbd>a-z</kbd>, <kbd>0-9</kbd>, <kbd>Ctrl</kbd>, <kbd>Shift</kbd>, <kbd>Alt</kbd>, <kbd>Meta</kbd>, <kbd>Enter</kbd>, <kbd>Esc</kbd>, <kbd>Up</kbd>, <kbd>Down</kbd>, <kbd>Right</kbd>, <kbd>Left</kbd>.</div><div>Press <kbd>Backspace</kbd> to disable a keybind.</div><table><tbody><tr><th>Actions</th><th>Keybinds</th></tr></tbody></table>";
      tbody = $('tbody', section);
      items = {};
      inputs = {};
      _ref = Config.hotkeys;
      for (key in _ref) {
        arr = _ref[key];
        tr = $.el('tr', {
          innerHTML: "<td>" + arr[1] + "</td><td><input class=field></td>"
        });
        input = $('input', tr);
        input.name = key;
        input.spellcheck = false;
        items[key] = Conf[key];
        inputs[key] = input;
        $.on(input, 'keydown', Settings.keybind);
        $.add(tbody, tr);
      }
      return $.get(items, function(items) {
        var val;
        for (key in items) {
          val = items[key];
          inputs[key].value = val;
        }
      });
    },
    keybind: function(e) {
      var key;
      if (e.keyCode === 9) {
        return;
      }
      e.preventDefault();
      e.stopPropagation();
      if ((key = Keybinds.keyCode(e)) == null) {
        return;
      }
      this.value = key;
      return $.cb.value.call(this);
    }
  };

  Index = {
    init: function() {
      var input, label, modeEntry, sortEntry, _i, _j, _len, _len1, _ref, _ref1;
      if (g.VIEW !== 'index' || g.BOARD.ID === 'f') {
        return;
      }
      this.button = $.el('a', {
        className: 'index-refresh-shortcut fa fa-refresh',
        title: 'Refresh Index',
        href: 'javascript:;'
      });
      $.on(this.button, 'click', this.update);
      Header.addShortcut(this.button, 1);
      modeEntry = {
        el: $.el('span', {
          textContent: 'Index mode'
        }),
        subEntries: [
          {
            el: $.el('label', {
              innerHTML: '<input type=radio name="Index Mode" value="paged"> Paged'
            })
          }, {
            el: $.el('label', {
              innerHTML: '<input type=radio name="Index Mode" value="all pages"> All threads'
            })
          }
        ]
      };
      _ref = modeEntry.subEntries;
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        label = _ref[_i];
        input = label.el.firstChild;
        input.checked = Conf['Index Mode'] === input.value;
        $.on(input, 'change', $.cb.value);
        $.on(input, 'change', this.cb.mode);
      }
      sortEntry = {
        el: $.el('span', {
          textContent: 'Sort by'
        }),
        subEntries: [
          {
            el: $.el('label', {
              innerHTML: '<input type=radio name="Index Sort" value="bump"> Bump order'
            })
          }, {
            el: $.el('label', {
              innerHTML: '<input type=radio name="Index Sort" value="lastreply"> Last reply'
            })
          }, {
            el: $.el('label', {
              innerHTML: '<input type=radio name="Index Sort" value="birth"> Creation date'
            })
          }, {
            el: $.el('label', {
              innerHTML: '<input type=radio name="Index Sort" value="replycount"> Reply count'
            })
          }, {
            el: $.el('label', {
              innerHTML: '<input type=radio name="Index Sort" value="filecount"> File count'
            })
          }
        ]
      };
      _ref1 = sortEntry.subEntries;
      for (_j = 0, _len1 = _ref1.length; _j < _len1; _j++) {
        label = _ref1[_j];
        input = label.el.firstChild;
        input.checked = Conf['Index Sort'] === input.value;
        $.on(input, 'change', $.cb.value);
        $.on(input, 'change', this.cb.sort);
      }
      $.event('AddMenuEntry', {
        type: 'header',
        el: $.el('span', {
          textContent: 'Index Navigation'
        }),
        order: 90,
        subEntries: [modeEntry, sortEntry]
      });
      $.addClass(doc, 'index-loading');
      this.update();
      this.root = $.el('div', {
        className: 'board'
      });
      this.pagelist = $.el('div', {
        className: 'pagelist',
        hidden: true,
        innerHTML: "<div class=\"prev\"><a><button disabled>Previous</button></a></div><div class=\"pages\"></div><div class=\"next\"><a><button disabled>Next</button></a></div>"
      });
      this.currentPage = this.getCurrentPage();
      $.on(window, 'popstate', this.cb.popstate);
      $.on(this.pagelist, 'click', this.cb.pageNav);
      return $.asap((function() {
        return $('.pagelist', doc) || d.readyState !== 'loading';
      }), function() {
        $.replace($('.board'), Index.root);
        $.replace($('.pagelist'), Index.pagelist);
        return $.rmClass(doc, 'index-loading');
      });
    },
    cb: {
      mode: function() {
        Index.togglePagelist();
        return Index.buildIndex();
      },
      sort: function() {
        Index.sort();
        return Index.buildIndex();
      },
      popstate: function(e) {
        var pageNum;
        pageNum = Index.getCurrentPage();
        if (Index.currentPage !== pageNum) {
          return Index.pageLoad(pageNum);
        }
      },
      pageNav: function(e) {
        var a;
        if (e.shiftKey || e.altKey || e.ctrlKey || e.metaKey || e.button !== 0) {
          return;
        }
        switch (e.target.nodeName) {
          case 'BUTTON':
            a = e.target.parentNode;
            break;
          case 'A':
            a = e.target;
            break;
          default:
            return;
        }
        e.preventDefault();
        return Index.pageNav(+a.pathname.split('/')[2]);
      }
    },
    scrollToIndex: function() {
      return Header.scrollToIfNeeded(Index.root);
    },
    getCurrentPage: function() {
      return +window.location.pathname.split('/')[2];
    },
    pageNav: function(pageNum) {
      if (Index.currentPage === pageNum) {
        return;
      }
      history.pushState(null, '', pageNum === 0 ? './' : pageNum);
      return Index.pageLoad(pageNum);
    },
    pageLoad: function(pageNum) {
      Index.currentPage = pageNum;
      if (Conf['Index Mode'] !== 'paged') {
        return;
      }
      Index.buildIndex();
      Index.setPage();
      return Index.scrollToIndex();
    },
    togglePagelist: function() {
      return Index.pagelist.hidden = Conf['Index Mode'] !== 'paged';
    },
    buildPagelist: function() {
      var a, i, nodes, pagesRoot, _i, _ref;
      pagesRoot = $('.pages', Index.pagelist);
      if (pagesRoot.childElementCount !== Index.pagesNum) {
        nodes = [];
        for (i = _i = 0, _ref = Index.pagesNum - 1; 0 <= _ref ? _i <= _ref : _i >= _ref; i = 0 <= _ref ? ++_i : --_i) {
          a = $.el('a', {
            textContent: i,
            href: i ? i : './'
          });
          nodes.push($.tn('['), a, $.tn('] '));
        }
        $.rmAll(pagesRoot);
        $.add(pagesRoot, nodes);
      }
      Index.setPage();
      return Index.togglePagelist();
    },
    setPage: function() {
      var a, href, next, pageNum, pagesRoot, prev, strong;
      pageNum = Index.getCurrentPage();
      pagesRoot = $('.pages', Index.pagelist);
      prev = pagesRoot.previousSibling.firstChild;
      next = pagesRoot.nextSibling.firstChild;
      href = Math.max(pageNum - 1, 0);
      prev.href = href === 0 ? './' : href;
      prev.firstChild.disabled = href === pageNum;
      href = Math.min(pageNum + 1, Index.pagesNum - 1);
      next.href = href === 0 ? './' : href;
      next.firstChild.disabled = href === pageNum;
      if (strong = $('strong', pagesRoot)) {
        if (+strong.textContent === pageNum) {
          return;
        }
        $.replace(strong, strong.firstChild);
      } else {
        strong = $.el('strong');
      }
      a = pagesRoot.children[pageNum];
      $.before(a, strong);
      return $.add(strong, a);
    },
    update: function() {
      var _ref, _ref1;
      if (!navigator.onLine) {
        return;
      }
      if ((_ref = Index.req) != null) {
        _ref.abort();
      }
      if ((_ref1 = Index.notice) != null) {
        _ref1.close();
      }
      Index.notice = new Notice('info', 'Refreshing index...');
      Index.req = $.ajax("//api.4chan.org/" + g.BOARD + "/catalog.json", {
        onabort: Index.load,
        onloadend: Index.load
      }, {
        whenModified: true
      });
      return $.addClass(Index.button, 'fa-spin');
    },
    load: function(e) {
      var err, notice, req;
      $.rmClass(Index.button, 'fa-spin');
      req = Index.req, notice = Index.notice;
      delete Index.req;
      delete Index.notice;
      if (e.type === 'abort') {
        req.onloadend = null;
        notice.close();
        return;
      }
      try {
        if (req.status === 200) {
          Index.parse(JSON.parse(req.response));
        }
      } catch (_error) {
        err = _error;
        c.error('Index failure:', err.stack);
        notice.setType('error');
        notice.el.lastElementChild.textContent = 'Index refresh failed.';
        setTimeout(notice.close, 2 * $.SECOND);
        return;
      }
      notice.setType('success');
      notice.el.lastElementChild.textContent = 'Index refreshed!';
      setTimeout(notice.close, $.SECOND);
      return Index.scrollToIndex();
    },
    parse: function(pages) {
      Index.parseThreadList(pages);
      Index.buildThreads();
      Index.sort();
      Index.buildIndex();
      return Index.buildPagelist();
    },
    parseThreadList: function(pages) {
      var thread, threadID, _ref, _ref1;
      Index.pagesNum = pages.length;
      Index.threadsNumPerPage = pages[0].threads.length;
      Index.liveThreadData = pages.reduce((function(arr, next) {
        return arr.concat(next.threads);
      }), []);
      Index.liveThreadIDs = Index.liveThreadData.map(function(data) {
        return data.no;
      });
      _ref = g.BOARD.threads;
      for (threadID in _ref) {
        thread = _ref[threadID];
        if (_ref1 = thread.ID, __indexOf.call(Index.liveThreadIDs, _ref1) < 0) {
          thread.collect();
        }
      }
    },
    buildThreads: function() {
      var OPRoot, err, posts, thread, threadData, threadRoot, threads, _i, _len, _ref;
      Index.nodes = [];
      threads = [];
      posts = [];
      _ref = Index.liveThreadData;
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        threadData = _ref[_i];
        threadRoot = Build.thread(g.BOARD, threadData);
        Index.nodes.push(threadRoot, $.el('hr'));
        if (thread = g.BOARD.threads[threadData.no]) {
          thread.setStatus('Sticky', !!threadData.sticky);
          thread.setStatus('Closed', !!threadData.closed);
        } else {
          thread = new Thread(threadData.no, g.BOARD);
          threads.push(thread);
        }
        OPRoot = $('.opContainer', threadRoot);
        if (OPRoot.id.match(/\d+/)[0] in thread.posts) {
          continue;
        }
        try {
          posts.push(new Post(OPRoot, thread, g.BOARD));
        } catch (_error) {
          err = _error;
          Main.handleErrors({
            message: "Parsing of Post No." + (postRoot.id.match(/\d+/)) + " failed. Post will be skipped.",
            error: err
          });
        }
      }
      $.nodes(Index.nodes);
      Main.callbackNodes(Thread, threads);
      return Main.callbackNodes(Post, posts);
    },
    buildReplies: function(threadRoots) {
      var data, err, errors, i, lastReplies, node, nodes, post, posts, thread, threadRoot, _i, _j, _len, _len1;
      posts = [];
      for (_i = 0, _len = threadRoots.length; _i < _len; _i += 2) {
        threadRoot = threadRoots[_i];
        thread = Get.threadFromRoot(threadRoot);
        i = Index.liveThreadIDs.indexOf(thread.ID);
        if (!(lastReplies = Index.liveThreadData[i].last_replies)) {
          continue;
        }
        nodes = [];
        for (_j = 0, _len1 = lastReplies.length; _j < _len1; _j++) {
          data = lastReplies[_j];
          if (post = thread.posts[data.no]) {
            nodes.push(post.nodes.root);
            continue;
          }
          nodes.push(node = Build.postFromObject(data, thread.board.ID));
          try {
            posts.push(new Post(node, thread, thread.board));
          } catch (_error) {
            err = _error;
            if (!errors) {
              errors = [];
            }
            errors.push({
              message: "Parsing of Post No." + (postRoot.id.match(/\d+/)) + " failed. Post will be skipped.",
              error: err
            });
          }
        }
        $.add(threadRoot, nodes);
      }
      if (errors) {
        Main.handleErrors(errors);
      }
      return Main.callbackNodes(Post, posts);
    },
    sort: function() {
      var i, offset, sortedThreadIDs, threadID, threadRoot, _i, _j, _k, _len, _len1, _len2, _ref, _ref1, _ref2, _ref3;
      switch (Conf['Index Sort']) {
        case 'bump':
          sortedThreadIDs = Index.liveThreadIDs;
          break;
        case 'lastreply':
          sortedThreadIDs = __slice.call(Index.liveThreadData).sort(function(a, b) {
            if ('last_replies' in a) {
              a = a.last_replies[a.last_replies.length - 1];
            }
            if ('last_replies' in b) {
              b = b.last_replies[b.last_replies.length - 1];
            }
            return b.no - a.no;
          }).map(function(data) {
            return data.no;
          });
          break;
        case 'birth':
          sortedThreadIDs = __slice.call(Index.liveThreadIDs).sort(function(a, b) {
            return b - a;
          });
          break;
        case 'replycount':
          sortedThreadIDs = __slice.call(Index.liveThreadData).sort(function(a, b) {
            return b.replies - a.replies;
          }).map(function(data) {
            return data.no;
          });
          break;
        case 'filecount':
          sortedThreadIDs = __slice.call(Index.liveThreadData).sort(function(a, b) {
            return b.images - a.images;
          }).map(function(data) {
            return data.no;
          });
      }
      Index.sortedNodes = [];
      for (_i = 0, _len = sortedThreadIDs.length; _i < _len; _i++) {
        threadID = sortedThreadIDs[_i];
        i = Index.liveThreadIDs.indexOf(threadID) * 2;
        Index.sortedNodes.push(Index.nodes[i], Index.nodes[i + 1]);
      }
      offset = 0;
      _ref = Index.sortedNodes;
      for (i = _j = 0, _len1 = _ref.length; _j < _len1; i = _j += 2) {
        threadRoot = _ref[i];
        if (Get.threadFromRoot(threadRoot).isSticky) {
          (_ref1 = Index.sortedNodes).splice.apply(_ref1, [offset++ * 2, 0].concat(__slice.call(Index.sortedNodes.splice(i, 2))));
        }
      }
      if (!Conf['Filter']) {
        return;
      }
      offset = 0;
      _ref2 = Index.sortedNodes;
      for (i = _k = 0, _len2 = _ref2.length; _k < _len2; i = _k += 2) {
        threadRoot = _ref2[i];
        if (Get.threadFromRoot(threadRoot).isOnTop) {
          (_ref3 = Index.sortedNodes).splice.apply(_ref3, [offset++ * 2, 0].concat(__slice.call(Index.sortedNodes.splice(i, 2))));
        }
      }
    },
    buildIndex: function() {
      var nodes, nodesPerPage, pageNum;
      if (Conf['Index Mode'] === 'paged') {
        pageNum = Index.getCurrentPage();
        nodesPerPage = Index.threadsNumPerPage * 2;
        nodes = Index.sortedNodes.slice(nodesPerPage * pageNum, nodesPerPage * (pageNum + 1));
      } else {
        nodes = Index.sortedNodes;
      }
      $.rmAll(Index.root);
      Index.buildReplies(nodes);
      $.event('IndexRefresh');
      return $.add(Index.root, nodes);
    }
  };

  Get = {
    threadExcerpt: function(thread) {
      var OP, excerpt, _ref;
      OP = thread.OP;
      excerpt = ((_ref = OP.info.subject) != null ? _ref.trim() : void 0) || OP.info.comment.replace(/\n+/g, ' // ') || Conf['Anonymize'] && 'Anonymous' || $('.nameBlock', OP.nodes.info).textContent.trim();
      if (excerpt.length > 70) {
        excerpt = "" + excerpt.slice(0, 67) + "...";
      }
      return "/" + thread.board + "/ - " + excerpt;
    },
    threadFromRoot: function(root) {
      return g.threads["" + g.BOARD + "." + root.id.slice(1)];
    },
    threadFromNode: function(node) {
      return Get.threadFromRoot($.x('ancestor::div[@class="thread"]', node));
    },
    postFromRoot: function(root) {
      var boardID, index, link, post, postID;
      link = $('a[title="Highlight this post"]', root);
      boardID = link.pathname.split('/')[1];
      postID = link.hash.slice(2);
      index = root.dataset.clone;
      post = g.posts["" + boardID + "." + postID];
      if (index) {
        return post.clones[index];
      } else {
        return post;
      }
    },
    postFromNode: function(node) {
      return Get.postFromRoot($.x('ancestor::div[contains(@class,"postContainer")][1]', node));
    },
    contextFromNode: function(node) {
      return Get.postFromRoot($.x('ancestor::div[parent::div[@class="thread"]][1]', node));
    },
    postDataFromLink: function(link) {
      var boardID, path, postID, threadID, _ref;
      if (link.hostname === 'boards.4chan.org') {
        path = link.pathname.split('/');
        boardID = path[1];
        threadID = path[3];
        postID = link.hash.slice(2);
      } else {
        _ref = link.dataset, boardID = _ref.boardID, threadID = _ref.threadID, postID = _ref.postID;
        threadID || (threadID = 0);
      }
      return {
        boardID: boardID,
        threadID: +threadID,
        postID: +postID
      };
    },
    allQuotelinksLinkingTo: function(post) {
      var ID, quote, quotedPost, quotelinks, quoterPost, _i, _j, _k, _len, _len1, _len2, _ref, _ref1, _ref2, _ref3, _ref4;
      quotelinks = [];
      _ref = g.posts;
      for (ID in _ref) {
        quoterPost = _ref[ID];
        if (_ref1 = post.fullID, __indexOf.call(quoterPost.quotes, _ref1) >= 0) {
          _ref2 = [quoterPost].concat(quoterPost.clones);
          for (_i = 0, _len = _ref2.length; _i < _len; _i++) {
            quoterPost = _ref2[_i];
            quotelinks.push.apply(quotelinks, quoterPost.nodes.quotelinks);
          }
        }
      }
      if (Conf['Quote Backlinks']) {
        _ref3 = post.quotes;
        for (_j = 0, _len1 = _ref3.length; _j < _len1; _j++) {
          quote = _ref3[_j];
          if (!(quotedPost = g.posts[quote])) {
            continue;
          }
          _ref4 = [quotedPost].concat(quotedPost.clones);
          for (_k = 0, _len2 = _ref4.length; _k < _len2; _k++) {
            quotedPost = _ref4[_k];
            quotelinks.push.apply(quotelinks, __slice.call(quotedPost.nodes.backlinks));
          }
        }
      }
      return quotelinks.filter(function(quotelink) {
        var boardID, postID, _ref5;
        _ref5 = Get.postDataFromLink(quotelink), boardID = _ref5.boardID, postID = _ref5.postID;
        return boardID === post.board.ID && postID === post.ID;
      });
    },
    postClone: function(boardID, threadID, postID, root, context) {
      var post, url;
      if (post = g.posts["" + boardID + "." + postID]) {
        Get.insert(post, root, context);
        return;
      }
      root.textContent = "Loading post No." + postID + "...";
      if (threadID) {
        return $.cache("//api.4chan.org/" + boardID + "/res/" + threadID + ".json", function() {
          return Get.fetchedPost(this, boardID, threadID, postID, root, context);
        });
      } else if (url = Redirect.to('post', {
        boardID: boardID,
        postID: postID
      })) {
        return $.cache(url, function() {
          return Get.archivedPost(this, boardID, postID, root, context);
        }, {
          withCredentials: url.archive.withCredentials
        });
      }
    },
    insert: function(post, root, context) {
      var clone, nodes;
      if (!root.parentNode) {
        return;
      }
      clone = post.addClone(context);
      Main.callbackNodes(Clone, [clone]);
      nodes = clone.nodes;
      $.rmAll(nodes.root);
      $.add(nodes.root, nodes.post);
      $.rmAll(root);
      return $.add(root, nodes.root);
    },
    fetchedPost: function(req, boardID, threadID, postID, root, context) {
      var board, post, posts, status, thread, url, _i, _len;
      if (post = g.posts["" + boardID + "." + postID]) {
        Get.insert(post, root, context);
        return;
      }
      status = req.status;
      if (status !== 200 && status !== 304) {
        if (url = Redirect.to('post', {
          boardID: boardID,
          postID: postID
        })) {
          $.cache(url, function() {
            return Get.archivedPost(this, boardID, postID, root, context);
          }, {
            withCredentials: url.archive.withCredentials
          });
        } else {
          $.addClass(root, 'warning');
          root.textContent = status === 404 ? "Thread No." + threadID + " 404'd." : "Error " + req.statusText + " (" + req.status + ").";
        }
        return;
      }
      posts = JSON.parse(req.response).posts;
      Build.spoilerRange[boardID] = posts[0].custom_spoiler;
      for (_i = 0, _len = posts.length; _i < _len; _i++) {
        post = posts[_i];
        if (post.no === postID) {
          break;
        }
      }
      if (post.no !== postID) {
        if (url = Redirect.to('post', {
          boardID: boardID,
          postID: postID
        })) {
          $.cache(url, function() {
            return Get.archivedPost(this, boardID, postID, root, context);
          }, {
            withCredentials: url.archive.withCredentials
          });
        } else {
          $.addClass(root, 'warning');
          root.textContent = "Post No." + postID + " was not found.";
        }
        return;
      }
      board = g.boards[boardID] || new Board(boardID);
      thread = g.threads["" + boardID + "." + threadID] || new Thread(threadID, board);
      post = new Post(Build.postFromObject(post, boardID), thread, board);
      Main.callbackNodes(Post, [post]);
      return Get.insert(post, root, context);
    },
    archivedPost: function(req, boardID, postID, root, context) {
      var board, bq, comment, data, o, post, thread, threadID, _ref;
      if (post = g.posts["" + boardID + "." + postID]) {
        Get.insert(post, root, context);
        return;
      }
      data = JSON.parse(req.response);
      if (data.error) {
        $.addClass(root, 'warning');
        root.textContent = data.error;
        return;
      }
      bq = $.el('blockquote', {
        textContent: data.comment
      });
      bq.innerHTML = bq.innerHTML.replace(/\n|\[\/?[a-z]+(:lit)?\]/g, function(text) {
        switch (text) {
          case '\n':
            return '<br>';
          case '[b]':
            return '<b>';
          case '[/b]':
            return '</b>';
          case '[spoiler]':
            return '<s>';
          case '[/spoiler]':
            return '</s>';
          case '[code]':
            return '<pre class=prettyprint>';
          case '[/code]':
            return '</pre>';
          case '[moot]':
            return '<div style="padding:5px;margin-left:.5em;border-color:#faa;border:2px dashed rgba(255,0,0,.1);border-radius:2px">';
          case '[/moot]':
            return '</div>';
          case '[banned]':
            return '<strong style="color: red;">';
          case '[/banned]':
            return '</strong>';
          default:
            return text.replace(':lit', '');
        }
      });
      comment = bq.innerHTML.replace(/(^|>)(&gt;[^<$]*)(<|$)/g, '$1<span class=quote>$2</span>$3').replace(/((&gt;){2}(&gt;\/[a-z\d]+\/)?\d+)/g, '<span class=deadlink>$1</span>');
      threadID = +data.thread_num;
      o = {
        postID: "" + postID,
        threadID: "" + threadID,
        boardID: boardID,
        name: data.name_processed,
        capcode: (function() {
          switch (data.capcode) {
            case 'M':
              return 'mod';
            case 'A':
              return 'admin';
            case 'D':
              return 'developer';
          }
        })(),
        tripcode: data.trip,
        uniqueID: data.poster_hash,
        email: data.email ? encodeURI(data.email) : '',
        subject: data.title_processed,
        flagCode: data.poster_country,
        flagName: data.poster_country_name_processed,
        date: data.fourchan_date,
        dateUTC: data.timestamp,
        comment: comment
      };
      if ((_ref = data.media) != null ? _ref.media_filename : void 0) {
        o.file = {
          name: data.media.media_filename_processed,
          timestamp: data.media.media_orig,
          url: data.media.media_link || data.media.remote_media_link,
          height: data.media.media_h,
          width: data.media.media_w,
          MD5: data.media.media_hash,
          size: data.media.media_size,
          turl: data.media.thumb_link || ("//thumbs.4chan.org/" + boardID + "/thumb/" + data.media.preview_orig),
          theight: data.media.preview_h,
          twidth: data.media.preview_w,
          isSpoiler: data.media.spoiler === '1'
        };
      }
      board = g.boards[boardID] || new Board(boardID);
      thread = g.threads["" + boardID + "." + threadID] || new Thread(threadID, board);
      post = new Post(Build.post(o, true), thread, board, {
        isArchived: true
      });
      Main.callbackNodes(Post, [post]);
      return Get.insert(post, root, context);
    }
  };

  Build = {
    spoilerRange: {},
    shortFilename: function(filename, isReply) {
      var threshold;
      threshold = isReply ? 30 : 40;
      if (filename.length - 4 > threshold) {
        return "" + filename.slice(0, threshold - 5) + "(...)." + filename.slice(-3);
      } else {
        return filename;
      }
    },
    postFromObject: function(data, boardID) {
      var o;
      o = {
        postID: data.no,
        threadID: data.resto || data.no,
        boardID: boardID,
        name: data.name,
        capcode: data.capcode,
        tripcode: data.trip,
        uniqueID: data.id,
        email: data.email ? encodeURI(data.email.replace(/&quot;/g, '"')) : '',
        subject: data.sub,
        flagCode: data.country,
        flagName: data.country_name,
        date: data.now,
        dateUTC: data.time,
        comment: data.com,
        isSticky: !!data.sticky,
        isClosed: !!data.closed
      };
      if (data.ext || data.filedeleted) {
        o.file = {
          name: data.filename + data.ext,
          timestamp: "" + data.tim + data.ext,
          url: boardID === 'f' ? "//images.4channel.org/" + boardID + "/src/" + data.filename + data.ext : "//images.4chan.org/" + boardID + "/src/" + data.tim + data.ext,
          height: data.h,
          width: data.w,
          MD5: data.md5,
          size: data.fsize,
          turl: "//thumbs.4chan.org/" + boardID + "/thumb/" + data.tim + "s.jpg",
          theight: data.tn_h,
          twidth: data.tn_w,
          isSpoiler: !!data.spoiler,
          isDeleted: !!data.filedeleted
        };
      }
      return Build.post(o);
    },
    post: function(o, isArchived) {
      var a, boardID, capcode, capcodeClass, capcodeStart, closed, comment, container, date, dateUTC, email, emailEnd, emailStart, ext, file, fileDims, fileHTML, fileInfo, fileSize, fileThumb, filename, flag, flagCode, flagName, gifIcon, href, imgSrc, isClosed, isOP, isSticky, name, postID, quote, replyLink, shortFilename, spoilerRange, staticPath, sticky, subject, threadID, tripcode, uniqueID, userID, _i, _len, _ref;
      postID = o.postID, threadID = o.threadID, boardID = o.boardID, name = o.name, capcode = o.capcode, tripcode = o.tripcode, uniqueID = o.uniqueID, email = o.email, subject = o.subject, flagCode = o.flagCode, flagName = o.flagName, date = o.date, dateUTC = o.dateUTC, isSticky = o.isSticky, isClosed = o.isClosed, comment = o.comment, file = o.file;
      isOP = postID === threadID;
      staticPath = '//static.4chan.org/image/';
      gifIcon = window.devicePixelRatio >= 2 ? '@2x.gif' : '.gif';
      if (email) {
        emailStart = '<a href="mailto:' + email + '" class="useremail">';
        emailEnd = '</a>';
      } else {
        emailStart = '';
        emailEnd = '';
      }
      subject = "<span class=subject>" + (subject || '') + "</span>";
      userID = !capcode && uniqueID ? (" <span class='posteruid id_" + uniqueID + "'>(ID: ") + ("<span class=hand title='Highlight posts by this ID'>" + uniqueID + "</span>)</span> ") : '';
      switch (capcode) {
        case 'admin':
        case 'admin_highlight':
          capcodeClass = " capcodeAdmin";
          capcodeStart = " <strong class='capcode hand id_admin'" + "title='Highlight posts by the Administrator'>## Admin</strong>";
          capcode = (" <img src='" + staticPath + "adminicon" + gifIcon + "' ") + "alt='This user is the 4chan Administrator.' " + "title='This user is the 4chan Administrator.' class=identityIcon>";
          break;
        case 'mod':
          capcodeClass = " capcodeMod";
          capcodeStart = " <strong class='capcode hand id_mod' " + "title='Highlight posts by Moderators'>## Mod</strong>";
          capcode = (" <img src='" + staticPath + "modicon" + gifIcon + "' ") + "alt='This user is a 4chan Moderator.' " + "title='This user is a 4chan Moderator.' class=identityIcon>";
          break;
        case 'developer':
          capcodeClass = " capcodeDeveloper";
          capcodeStart = " <strong class='capcode hand id_developer' " + "title='Highlight posts by Developers'>## Developer</strong>";
          capcode = (" <img src='" + staticPath + "developericon" + gifIcon + "' ") + "alt='This user is a 4chan Developer.' " + "title='This user is a 4chan Developer.' class=identityIcon>";
          break;
        default:
          capcodeClass = '';
          capcodeStart = '';
          capcode = '';
      }
      flag = !flagCode ? '' : boardID === 'pol' ? " <img src='" + staticPath + "country/troll/" + (flagCode.toLowerCase()) + ".gif' alt=" + flagCode + " title='" + flagName + "' class=countryFlag>" : " <span title='" + flagName + "' class='flag flag-" + (flagCode.toLowerCase()) + "'></span>";
      if (file != null ? file.isDeleted : void 0) {
        fileHTML = isOP ? ("<div class=file id=f" + postID + "><div class=fileInfo></div><span class=fileThumb>") + ("<img src='" + staticPath + "filedeleted" + gifIcon + "' alt='File deleted.' class=fileDeletedRes>") + "</span></div>" : ("<div class=file id=f" + postID + "><span class=fileThumb>") + ("<img src='" + staticPath + "filedeleted-res" + gifIcon + "' alt='File deleted.' class=fileDeletedRes>") + "</span></div>";
      } else if (file) {
        ext = file.name.slice(-3);
        if (!file.twidth && !file.theight && ext === 'gif') {
          file.twidth = file.width;
          file.theight = file.height;
        }
        fileSize = $.bytesToString(file.size);
        fileThumb = file.turl;
        if (file.isSpoiler) {
          fileSize = "Spoiler Image, " + fileSize;
          if (!isArchived) {
            fileThumb = "" + staticPath + "spoiler";
            if (spoilerRange = Build.spoilerRange[boardID]) {
              fileThumb += ("-" + boardID) + Math.floor(1 + spoilerRange * Math.random());
            }
            fileThumb += '.png';
            file.twidth = file.theight = 100;
          }
        }
        imgSrc = boardID === 'f' ? '' : ("<a class='fileThumb" + (file.isSpoiler ? ' imgspoiler' : '') + "' href='" + file.url + "' target=_blank>") + ("<img src='" + fileThumb + "' alt='" + fileSize + "' data-md5=" + file.MD5 + " style='height: " + file.theight + "px; width: " + file.twidth + "px;'>") + "</a>";
        a = $.el('a', {
          innerHTML: file.name
        });
        filename = a.textContent.replace(/%22/g, '"');
        a.textContent = Build.shortFilename(filename);
        shortFilename = a.innerHTML;
        a.textContent = filename;
        filename = a.innerHTML.replace(/'/g, '&apos;');
        fileDims = ext === 'pdf' ? 'PDF' : "" + file.width + "x" + file.height;
        fileInfo = ("<span class=fileText id=fT" + postID + (file.isSpoiler ? " title='" + filename + "'" : '') + ">File: <a href='" + file.url + "' target=_blank>" + file.timestamp + "</a>") + ("-(" + fileSize + ", " + fileDims + (file.isSpoiler ? '' : ", <span title='" + filename + "'>" + shortFilename + "</span>")) + ")</span>";
        fileHTML = "<div id=f" + postID + " class=file><div class=fileInfo>" + fileInfo + "</div>" + imgSrc + "</div>";
      } else {
        fileHTML = '';
      }
      tripcode = tripcode ? " <span class=postertrip>" + tripcode + "</span>" : '';
      sticky = isSticky ? " <img src=" + staticPath + "sticky" + gifIcon + " alt=Sticky title=Sticky class=stickyIcon>" : '';
      closed = isClosed ? " <img src=" + staticPath + "closed" + gifIcon + " alt=Closed title=Closed class=closedIcon>" : '';
      replyLink = isOP && g.VIEW === 'index' ? " &nbsp; <span>[<a href='/" + boardID + "/res/" + threadID + "' class=replylink>Reply</a>]</span>" : '';
      container = $.el('div', {
        id: "pc" + postID,
        className: "postContainer " + (isOP ? 'op' : 'reply') + "Container",
        innerHTML: (isOP ? '' : "<div class=sideArrows id=sa" + postID + ">&gt;&gt;</div>") + ("<div id=p" + postID + " class='post " + (isOP ? 'op' : 'reply') + (capcode === 'admin_highlight' ? ' highlightPost' : '') + "'>") + ("<div class='postInfoM mobile' id=pim" + postID + ">") + ("<span class='nameBlock" + capcodeClass + "'>") + ("<span class=name>" + (name || '') + "</span>") + tripcode + capcodeStart + capcode + userID + flag + sticky + closed + ("<br>" + subject) + ("</span><span class='dateTime postNum' data-utc=" + dateUTC + ">" + date) + ("<a href=" + ("/" + boardID + "/res/" + threadID + "#p" + postID) + ">No.</a>") + ("<a href='" + (g.VIEW === 'thread' && g.THREADID === +threadID ? "javascript:quote(" + postID + ")" : "/" + boardID + "/res/" + threadID + "#q" + postID) + "'>" + postID + "</a>") + '</span>' + '</div>' + (isOP ? fileHTML : '') + ("<div class='postInfo desktop' id=pi" + postID + ">") + ("<input type=checkbox name=" + postID + " value=delete> ") + ("" + subject + " ") + ("<span class='nameBlock" + capcodeClass + "'>") + emailStart + ("<span class=name>" + (name || '') + "</span>") + tripcode + capcodeStart + emailEnd + capcode + userID + flag + ' </span> ' + ("<span class=dateTime data-utc=" + dateUTC + ">" + date + "</span> ") + "<span class='postNum desktop'>" + ("<a href=" + ("/" + boardID + "/res/" + threadID + "#p" + postID) + " title='Highlight this post'>No.</a>") + ("<a href='" + (g.VIEW === 'thread' && g.THREADID === +threadID ? "javascript:quote(" + postID + ")" : "/" + boardID + "/res/" + threadID + "#q" + postID) + "' title='Quote this post'>" + postID + "</a>") + sticky + closed + replyLink + '</span>' + '</div>' + (isOP ? '' : fileHTML) + ("<blockquote class=postMessage id=m" + postID + ">" + (comment || '') + "</blockquote> ") + '</div>'
      });
      _ref = $$('.quotelink', container);
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        quote = _ref[_i];
        href = quote.getAttribute('href');
        if (href[0] === '/') {
          continue;
        }
        quote.href = "/" + boardID + "/res/" + href;
      }
      return container;
    },
    summary: function(boardID, threadID, posts, files) {
      var text;
      text = [];
      text.push("" + posts + " post" + (posts > 1 ? 's' : ''));
      if (files) {
        text.push("and " + files + " image repl" + (files > 1 ? 'ies' : 'y'));
      }
      text.push('omitted.');
      return $.el('a', {
        className: 'summary',
        textContent: text.join(' '),
        href: "/" + boardID + "/res/" + threadID
      });
    },
    thread: function(board, data) {
      var OP, nodes, root;
      Build.spoilerRange[board] = data.custom_spoiler;
      if ((OP = board.posts[data.no]) && (root = OP.nodes.root.parentNode)) {
        $.rmAll(root);
      } else {
        root = $.el('div', {
          className: 'thread',
          id: "t" + data.no
        });
      }
      nodes = [OP ? OP.nodes.root : Build.postFromObject(data, board.ID)];
      if (data.omitted_posts) {
        nodes.push(Build.summary(board.ID, data.no, data.omitted_posts, data.omitted_images));
      }
      $.add(root, nodes);
      return root;
    }
  };

  Filter = {
    filters: {},
    init: function() {
      var boards, err, filter, hl, key, op, regexp, stub, top, _i, _len, _ref, _ref1, _ref2, _ref3, _ref4, _ref5;
      if (g.VIEW === 'catalog' || !Conf['Filter']) {
        return;
      }
      for (key in Config.filter) {
        this.filters[key] = [];
        _ref = Conf[key].split('\n');
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
          filter = _ref[_i];
          if (filter[0] === '#') {
            continue;
          }
          if (!(regexp = filter.match(/\/(.+)\/(\w*)/))) {
            continue;
          }
          filter = filter.replace(regexp[0], '');
          boards = ((_ref1 = filter.match(/boards:([^;]+)/)) != null ? _ref1[1].toLowerCase() : void 0) || 'global';
          if (boards !== 'global' && !(_ref2 = g.BOARD.ID, __indexOf.call(boards.split(','), _ref2) >= 0)) {
            continue;
          }
          if (key === 'uniqueID' || key === 'MD5') {
            regexp = regexp[1];
          } else {
            try {
              regexp = RegExp(regexp[1], regexp[2]);
            } catch (_error) {
              err = _error;
              new Notice('warning', err.message, 60);
              continue;
            }
          }
          op = ((_ref3 = filter.match(/[^t]op:(yes|no|only)/)) != null ? _ref3[1] : void 0) || 'yes';
          stub = (function() {
            var _ref4;
            switch ((_ref4 = filter.match(/stub:(yes|no)/)) != null ? _ref4[1] : void 0) {
              case 'yes':
                return true;
              case 'no':
                return false;
              default:
                return Conf['Stubs'];
            }
          })();
          if (hl = /highlight/.test(filter)) {
            hl = ((_ref4 = filter.match(/highlight:(\w+)/)) != null ? _ref4[1] : void 0) || 'filter-highlight';
            top = ((_ref5 = filter.match(/top:(yes|no)/)) != null ? _ref5[1] : void 0) || 'yes';
            top = top === 'yes';
          }
          this.filters[key].push(this.createFilter(regexp, op, stub, hl, top));
        }
        if (!this.filters[key].length) {
          delete this.filters[key];
        }
      }
      if (!Object.keys(this.filters).length) {
        return;
      }
      return Post.callbacks.push({
        name: 'Filter',
        cb: this.node
      });
    },
    createFilter: function(regexp, op, stub, hl, top) {
      var settings, test;
      test = typeof regexp === 'string' ? function(value) {
        return regexp === value;
      } : function(value) {
        return regexp.test(value);
      };
      settings = {
        hide: !hl,
        stub: stub,
        "class": hl,
        top: top
      };
      return function(value, isReply) {
        if (isReply && op === 'only' || !isReply && op === 'no') {
          return false;
        }
        if (!test(value)) {
          return false;
        }
        return settings;
      };
    },
    node: function() {
      var filter, key, result, value, _i, _len, _ref;
      if (this.isClone) {
        return;
      }
      for (key in Filter.filters) {
        value = Filter[key](this);
        if (value === false) {
          continue;
        }
        _ref = Filter.filters[key];
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
          filter = _ref[_i];
          if (!(result = filter(value, this.isReply))) {
            continue;
          }
          if (result.hide) {
            if (this.isReply) {
              PostHiding.hide(this, result.stub);
            } else if (g.VIEW === 'index') {
              ThreadHiding.hide(this.thread, result.stub);
            } else {
              continue;
            }
            return;
          }
          $.addClass(this.nodes.root, result["class"]);
          if (!this.isReply && result.top) {
            this.thread.isOnTop = true;
          }
        }
      }
    },
    name: function(post) {
      if ('name' in post.info) {
        return post.info.name;
      }
      return false;
    },
    uniqueID: function(post) {
      if ('uniqueID' in post.info) {
        return post.info.uniqueID;
      }
      return false;
    },
    tripcode: function(post) {
      if ('tripcode' in post.info) {
        return post.info.tripcode;
      }
      return false;
    },
    capcode: function(post) {
      if ('capcode' in post.info) {
        return post.info.capcode;
      }
      return false;
    },
    email: function(post) {
      if ('email' in post.info) {
        return post.info.email;
      }
      return false;
    },
    subject: function(post) {
      if ('subject' in post.info) {
        return post.info.subject || false;
      }
      return false;
    },
    comment: function(post) {
      if ('comment' in post.info) {
        return post.info.comment;
      }
      return false;
    },
    flag: function(post) {
      if ('flag' in post.info) {
        return post.info.flag;
      }
      return false;
    },
    filename: function(post) {
      if (post.file) {
        return post.file.name;
      }
      return false;
    },
    dimensions: function(post) {
      if (post.file && post.file.isImage) {
        return post.file.dimensions;
      }
      return false;
    },
    filesize: function(post) {
      if (post.file) {
        return post.file.size;
      }
      return false;
    },
    MD5: function(post) {
      if (post.file) {
        return post.file.MD5;
      }
      return false;
    },
    menu: {
      init: function() {
        var div, entry, type, _i, _len, _ref;
        if (g.VIEW === 'catalog' || !Conf['Menu'] || !Conf['Filter']) {
          return;
        }
        div = $.el('div', {
          textContent: 'Filter'
        });
        entry = {
          type: 'post',
          el: div,
          order: 50,
          open: function(post) {
            Filter.menu.post = post;
            return true;
          },
          subEntries: []
        };
        _ref = [['Name', 'name'], ['Unique ID', 'uniqueID'], ['Tripcode', 'tripcode'], ['Capcode', 'capcode'], ['E-mail', 'email'], ['Subject', 'subject'], ['Comment', 'comment'], ['Flag', 'flag'], ['Filename', 'filename'], ['Image dimensions', 'dimensions'], ['Filesize', 'filesize'], ['Image MD5', 'MD5']];
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
          type = _ref[_i];
          entry.subEntries.push(Filter.menu.createSubEntry(type[0], type[1]));
        }
        return $.event('AddMenuEntry', entry);
      },
      createSubEntry: function(text, type) {
        var el;
        el = $.el('a', {
          href: 'javascript:;',
          textContent: text
        });
        el.dataset.type = type;
        $.on(el, 'click', Filter.menu.makeFilter);
        return {
          el: el,
          open: function(post) {
            var value;
            value = Filter[type](post);
            return value !== false;
          }
        };
      },
      makeFilter: function() {
        var re, type, value;
        type = this.dataset.type;
        value = Filter[type](Filter.menu.post);
        re = type === 'uniqueID' || type === 'MD5' ? value : value.replace(/\/|\\|\^|\$|\n|\.|\(|\)|\{|\}|\[|\]|\?|\*|\+|\|/g, function(c) {
          if (c === '\n') {
            return '\\n';
          } else if (c === '\\') {
            return '\\\\';
          } else {
            return "\\" + c;
          }
        });
        re = type === 'uniqueID' || type === 'MD5' ? "/" + re + "/" : "/^" + re + "$/";
        return $.get(type, Conf[type], function(item) {
          var save, section, select, ta, tl;
          save = item[type];
          save = save ? "" + save + "\n" + re : re;
          $.set(type, save);
          Settings.open('Filter');
          section = $('.section-container');
          select = $('select[name=filter]', section);
          select.value = type;
          Settings.selectFilter.call(select);
          ta = $('textarea', section);
          tl = ta.textLength;
          ta.setSelectionRange(tl, tl);
          return ta.focus();
        });
      }
    }
  };

  PostHiding = {
    init: function() {
      if (g.VIEW === 'catalog' || !Conf['Reply Hiding'] && !Conf['Reply Hiding Link']) {
        return;
      }
      this.db = new DataBoard('hiddenPosts');
      return Post.callbacks.push({
        name: 'Reply Hiding',
        cb: this.node
      });
    },
    node: function() {
      var data;
      if (!this.isReply || this.isClone) {
        return;
      }
      if (data = PostHiding.db.get({
        boardID: this.board.ID,
        threadID: this.thread.ID,
        postID: this.ID
      })) {
        if (data.thisPost) {
          PostHiding.hide(this, data.makeStub, data.hideRecursively);
        } else {
          Recursive.apply(PostHiding.hide, this, data.makeStub, true);
          Recursive.add(PostHiding.hide, this, data.makeStub, true);
        }
      }
      if (!Conf['Reply Hiding']) {
        return;
      }
      return $.replace($('.sideArrows', this.nodes.root), PostHiding.makeButton(this, 'hide'));
    },
    menu: {
      init: function() {
        var apply, div, makeStub, replies, thisPost;
        if (g.VIEW === 'catalog' || !Conf['Menu'] || !Conf['Reply Hiding Link']) {
          return;
        }
        div = $.el('div', {
          className: 'hide-reply-link',
          textContent: 'Hide reply'
        });
        apply = $.el('a', {
          textContent: 'Apply',
          href: 'javascript:;'
        });
        $.on(apply, 'click', PostHiding.menu.hide);
        thisPost = $.el('label', {
          innerHTML: '<input type=checkbox name=thisPost checked> This post'
        });
        replies = $.el('label', {
          innerHTML: "<input type=checkbox name=replies  checked=" + Conf['Recursive Hiding'] + "> Hide replies"
        });
        makeStub = $.el('label', {
          innerHTML: "<input type=checkbox name=makeStub checked=" + Conf['Stubs'] + "> Make stub"
        });
        $.event('AddMenuEntry', {
          type: 'post',
          el: div,
          order: 20,
          open: function(post) {
            if (!post.isReply || post.isClone || post.isHidden) {
              return false;
            }
            PostHiding.menu.post = post;
            return true;
          },
          subEntries: [
            {
              el: apply
            }, {
              el: thisPost
            }, {
              el: replies
            }, {
              el: makeStub
            }
          ]
        });
        div = $.el('div', {
          className: 'show-reply-link',
          textContent: 'Show reply'
        });
        apply = $.el('a', {
          textContent: 'Apply',
          href: 'javascript:;'
        });
        $.on(apply, 'click', PostHiding.menu.show);
        thisPost = $.el('label', {
          innerHTML: '<input type=checkbox name=thisPost> This post'
        });
        replies = $.el('label', {
          innerHTML: "<input type=checkbox name=replies> Show replies"
        });
        return $.event('AddMenuEntry', {
          type: 'post',
          el: div,
          order: 20,
          open: function(post) {
            var data;
            if (!post.isReply || post.isClone || !post.isHidden) {
              return false;
            }
            if (!(data = PostHiding.db.get({
              boardID: post.board.ID,
              threadID: post.thread.ID,
              postID: post.ID
            }))) {
              return false;
            }
            PostHiding.menu.post = post;
            thisPost.firstChild.checked = post.isHidden;
            replies.firstChild.checked = (data != null ? data.hideRecursively : void 0) != null ? data.hideRecursively : Conf['Recursive Hiding'];
            return true;
          },
          subEntries: [
            {
              el: apply
            }, {
              el: thisPost
            }, {
              el: replies
            }
          ]
        });
      },
      hide: function() {
        var makeStub, parent, post, replies, thisPost;
        parent = this.parentNode;
        thisPost = $('input[name=thisPost]', parent).checked;
        replies = $('input[name=replies]', parent).checked;
        makeStub = $('input[name=makeStub]', parent).checked;
        post = PostHiding.menu.post;
        if (thisPost) {
          PostHiding.hide(post, makeStub, replies);
        } else if (replies) {
          Recursive.apply(PostHiding.hide, post, makeStub, true);
          Recursive.add(PostHiding.hide, post, makeStub, true);
        } else {
          return;
        }
        PostHiding.saveHiddenState(post, true, thisPost, makeStub, replies);
        return $.event('CloseMenu');
      },
      show: function() {
        var data, parent, post, replies, thisPost;
        parent = this.parentNode;
        thisPost = $('input[name=thisPost]', parent).checked;
        replies = $('input[name=replies]', parent).checked;
        post = PostHiding.menu.post;
        if (thisPost) {
          PostHiding.show(post, replies);
        } else if (replies) {
          Recursive.apply(PostHiding.show, post, true);
          Recursive.rm(PostHiding.hide, post, true);
        } else {
          return;
        }
        if (data = PostHiding.db.get({
          boardID: post.board.ID,
          threadID: post.thread.ID,
          postID: post.ID
        })) {
          PostHiding.saveHiddenState(post, !(thisPost && replies), !thisPost, data.makeStub, !replies);
        }
        return $.event('CloseMenu');
      }
    },
    makeButton: function(post, type) {
      var a;
      a = $.el('a', {
        className: "" + type + "-reply-button",
        innerHTML: "<span>[&nbsp;" + (type === 'hide' ? '-' : '+') + "&nbsp;]</span>",
        href: 'javascript:;'
      });
      $.on(a, 'click', PostHiding.toggle);
      return a;
    },
    saveHiddenState: function(post, isHiding, thisPost, makeStub, hideRecursively) {
      var data;
      data = {
        boardID: post.board.ID,
        threadID: post.thread.ID,
        postID: post.ID
      };
      if (isHiding) {
        data.val = {
          thisPost: thisPost !== false,
          makeStub: makeStub,
          hideRecursively: hideRecursively
        };
        return PostHiding.db.set(data);
      } else {
        return PostHiding.db["delete"](data);
      }
    },
    toggle: function() {
      var post;
      post = Get.postFromNode(this);
      if (post.isHidden) {
        PostHiding.show(post);
      } else {
        PostHiding.hide(post);
      }
      return PostHiding.saveHiddenState(post, post.isHidden);
    },
    hide: function(post, makeStub, hideRecursively) {
      var a, postInfo, quotelink, _i, _len, _ref;
      if (makeStub == null) {
        makeStub = Conf['Stubs'];
      }
      if (hideRecursively == null) {
        hideRecursively = Conf['Recursive Hiding'];
      }
      if (post.isHidden) {
        return;
      }
      post.isHidden = true;
      if (hideRecursively) {
        Recursive.apply(PostHiding.hide, post, makeStub, true);
        Recursive.add(PostHiding.hide, post, makeStub, true);
      }
      _ref = Get.allQuotelinksLinkingTo(post);
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        quotelink = _ref[_i];
        $.addClass(quotelink, 'filtered');
      }
      if (!makeStub) {
        post.nodes.root.hidden = true;
        return;
      }
      a = PostHiding.makeButton(post, 'show');
      postInfo = Conf['Anonymize'] ? 'Anonymous' : $('.nameBlock', post.nodes.info).textContent;
      $.add(a, $.tn(" " + postInfo));
      post.nodes.stub = $.el('div', {
        className: 'stub'
      });
      $.add(post.nodes.stub, a);
      if (Conf['Menu']) {
        $.add(post.nodes.stub, [$.tn(' '), Menu.makeButton()]);
      }
      return $.prepend(post.nodes.root, post.nodes.stub);
    },
    show: function(post, showRecursively) {
      var quotelink, _i, _len, _ref;
      if (showRecursively == null) {
        showRecursively = Conf['Recursive Hiding'];
      }
      if (post.nodes.stub) {
        $.rm(post.nodes.stub);
        delete post.nodes.stub;
      } else {
        post.nodes.root.hidden = false;
      }
      post.isHidden = false;
      if (showRecursively) {
        Recursive.apply(PostHiding.show, post, true);
        Recursive.rm(PostHiding.hide, post);
      }
      _ref = Get.allQuotelinksLinkingTo(post);
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        quotelink = _ref[_i];
        $.rmClass(quotelink, 'filtered');
      }
    }
  };

  Recursive = {
    recursives: {},
    init: function() {
      if (g.VIEW === 'catalog') {
        return;
      }
      return Post.callbacks.push({
        name: 'Recursive',
        cb: this.node
      });
    },
    node: function() {
      var i, obj, quote, recursive, _i, _j, _len, _len1, _ref, _ref1;
      if (this.isClone) {
        return;
      }
      _ref = this.quotes;
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        quote = _ref[_i];
        if (obj = Recursive.recursives[quote]) {
          _ref1 = obj.recursives;
          for (i = _j = 0, _len1 = _ref1.length; _j < _len1; i = ++_j) {
            recursive = _ref1[i];
            recursive.apply(null, [this].concat(__slice.call(obj.args[i])));
          }
        }
      }
    },
    add: function() {
      var args, obj, post, recursive, _base, _name;
      recursive = arguments[0], post = arguments[1], args = 3 <= arguments.length ? __slice.call(arguments, 2) : [];
      obj = (_base = Recursive.recursives)[_name = post.fullID] || (_base[_name] = {
        recursives: [],
        args: []
      });
      obj.recursives.push(recursive);
      return obj.args.push(args);
    },
    rm: function(recursive, post) {
      var i, obj, rec, _i, _len, _ref;
      if (!(obj = Recursive.recursives[post.fullID])) {
        return;
      }
      _ref = obj.recursives;
      for (i = _i = 0, _len = _ref.length; _i < _len; i = ++_i) {
        rec = _ref[i];
        if (rec === recursive) {
          obj.recursives.splice(i, 1);
          obj.args.splice(i, 1);
        }
      }
    },
    apply: function() {
      var ID, args, fullID, post, recursive, _ref;
      recursive = arguments[0], post = arguments[1], args = 3 <= arguments.length ? __slice.call(arguments, 2) : [];
      fullID = post.fullID;
      _ref = g.posts;
      for (ID in _ref) {
        post = _ref[ID];
        if (__indexOf.call(post.quotes, fullID) >= 0) {
          recursive.apply(null, [post].concat(__slice.call(args)));
        }
      }
    }
  };

  ThreadHiding = {
    init: function() {
      if (g.VIEW !== 'index' || !Conf['Thread Hiding'] && !Conf['Thread Hiding Link']) {
        return;
      }
      this.db = new DataBoard('hiddenThreads');
      this.syncCatalog();
      $.on(d, 'IndexRefresh', this.onrefresh);
      return Thread.callbacks.push({
        name: 'Thread Hiding',
        cb: this.node
      });
    },
    node: function() {
      var data;
      if (data = ThreadHiding.db.get({
        boardID: this.board.ID,
        threadID: this.ID
      })) {
        ThreadHiding.hide(this, data.makeStub);
      }
      if (!Conf['Thread Hiding']) {
        return;
      }
      return $.prepend(this.OP.nodes.root, ThreadHiding.makeButton(this, 'hide'));
    },
    onrefresh: function() {
      var root, thread, threadID, _ref;
      _ref = g.BOARD.threads;
      for (threadID in _ref) {
        thread = _ref[threadID];
        if (!thread.isHidden) {
          continue;
        }
        root = thread.OP.nodes.root.parentNode;
        if (thread.stub) {
          $.prepend(root, thread.stub);
        } else {
          threadRoot.nextElementSibling.hidden = true;
        }
      }
    },
    syncCatalog: function() {
      var hiddenThreads, hiddenThreadsOnCatalog, threadID;
      hiddenThreads = ThreadHiding.db.get({
        boardID: g.BOARD.ID,
        defaultValue: {}
      });
      hiddenThreadsOnCatalog = JSON.parse(localStorage.getItem("4chan-hide-t-" + g.BOARD)) || {};
      for (threadID in hiddenThreadsOnCatalog) {
        if (!(threadID in hiddenThreads)) {
          hiddenThreads[threadID] = {};
        }
      }
      for (threadID in hiddenThreads) {
        if (!(threadID in hiddenThreadsOnCatalog)) {
          delete hiddenThreads[threadID];
        }
      }
      if ((ThreadHiding.db.data.lastChecked || 0) > Date.now() - $.MINUTE) {
        ThreadHiding.cleanCatalog(hiddenThreadsOnCatalog);
      }
      if (!Object.keys(hiddenThreads).length) {
        ThreadHiding.db["delete"]({
          boardID: g.BOARD.ID
        });
        return;
      }
      return ThreadHiding.db.set({
        boardID: g.BOARD.ID,
        val: hiddenThreads
      });
    },
    cleanCatalog: function(hiddenThreadsOnCatalog) {
      return $.cache("//api.4chan.org/" + g.BOARD + "/threads.json", function() {
        var page, thread, threads, _i, _j, _len, _len1, _ref, _ref1;
        if (this.status !== 200) {
          return;
        }
        threads = {};
        _ref = JSON.parse(this.response);
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
          page = _ref[_i];
          _ref1 = page.threads;
          for (_j = 0, _len1 = _ref1.length; _j < _len1; _j++) {
            thread = _ref1[_j];
            if (thread.no in hiddenThreadsOnCatalog) {
              threads[thread.no] = hiddenThreadsOnCatalog[thread.no];
            }
          }
        }
        if (Object.keys(threads).length) {
          return localStorage.setItem("4chan-hide-t-" + g.BOARD, JSON.stringify(threads));
        } else {
          return localStorage.removeItem("4chan-hide-t-" + g.BOARD);
        }
      });
    },
    menu: {
      init: function() {
        var apply, div, makeStub;
        if (g.VIEW !== 'index' || !Conf['Menu'] || !Conf['Thread Hiding Link']) {
          return;
        }
        div = $.el('div', {
          className: 'hide-thread-link',
          textContent: 'Hide thread'
        });
        apply = $.el('a', {
          textContent: 'Apply',
          href: 'javascript:;'
        });
        $.on(apply, 'click', ThreadHiding.menu.hide);
        makeStub = $.el('label', {
          innerHTML: "<input type=checkbox checked=" + Conf['Stubs'] + "> Make stub"
        });
        return $.event('AddMenuEntry', {
          type: 'post',
          el: div,
          order: 20,
          open: function(_arg) {
            var isReply, thread;
            thread = _arg.thread, isReply = _arg.isReply;
            if (isReply || thread.isHidden) {
              return false;
            }
            ThreadHiding.menu.thread = thread;
            return true;
          },
          subEntries: [
            {
              el: apply
            }, {
              el: makeStub
            }
          ]
        });
      },
      hide: function() {
        var makeStub, thread;
        makeStub = $('input', this.parentNode).checked;
        thread = ThreadHiding.menu.thread;
        ThreadHiding.hide(thread, makeStub);
        ThreadHiding.saveHiddenState(thread, makeStub);
        return $.event('CloseMenu');
      }
    },
    makeButton: function(thread, type) {
      var a;
      a = $.el('a', {
        className: "" + type + "-thread-button",
        innerHTML: "<span>[&nbsp;" + (type === 'hide' ? '-' : '+') + "&nbsp;]</span>",
        href: 'javascript:;'
      });
      a.dataset.fullID = thread.fullID;
      $.on(a, 'click', ThreadHiding.toggle);
      return a;
    },
    saveHiddenState: function(thread, makeStub) {
      var hiddenThreadsOnCatalog;
      hiddenThreadsOnCatalog = JSON.parse(localStorage.getItem("4chan-hide-t-" + g.BOARD)) || {};
      if (thread.isHidden) {
        ThreadHiding.db.set({
          boardID: thread.board.ID,
          threadID: thread.ID,
          val: {
            makeStub: makeStub
          }
        });
        hiddenThreadsOnCatalog[thread] = true;
      } else {
        ThreadHiding.db["delete"]({
          boardID: thread.board.ID,
          threadID: thread.ID
        });
        delete hiddenThreadsOnCatalog[thread];
      }
      return localStorage.setItem("4chan-hide-t-" + g.BOARD, JSON.stringify(hiddenThreadsOnCatalog));
    },
    toggle: function(thread) {
      if (!(thread instanceof Thread)) {
        thread = g.threads[this.dataset.fullID];
      }
      if (thread.isHidden) {
        ThreadHiding.show(thread);
      } else {
        ThreadHiding.hide(thread);
      }
      return ThreadHiding.saveHiddenState(thread);
    },
    hide: function(thread, makeStub) {
      var OP, a, numReplies, opInfo, span, threadRoot;
      if (makeStub == null) {
        makeStub = Conf['Stubs'];
      }
      if (thread.isHidden) {
        return;
      }
      OP = thread.OP;
      threadRoot = OP.nodes.root.parentNode;
      thread.isHidden = true;
      if (!makeStub) {
        threadRoot.hidden = threadRoot.nextElementSibling.hidden = true;
        return;
      }
      numReplies = 0;
      if (span = $('.summary', threadRoot)) {
        numReplies = +span.textContent.match(/\d+/);
      }
      numReplies += $$('.opContainer ~ .replyContainer', threadRoot).length;
      numReplies = numReplies === 1 ? '1 reply' : "" + numReplies + " replies";
      opInfo = Conf['Anonymize'] ? 'Anonymous' : $('.nameBlock', OP.nodes.info).textContent;
      a = ThreadHiding.makeButton(thread, 'show');
      $.add(a, $.tn(" " + opInfo + " (" + numReplies + ")"));
      thread.stub = $.el('div', {
        className: 'stub'
      });
      $.add(thread.stub, a);
      if (Conf['Menu']) {
        $.add(thread.stub, [$.tn(' '), Menu.makeButton()]);
      }
      return $.prepend(threadRoot, thread.stub);
    },
    show: function(thread) {
      var threadRoot;
      if (thread.stub) {
        $.rm(thread.stub);
        delete thread.stub;
      }
      threadRoot = thread.OP.nodes.root.parentNode;
      return threadRoot.nextElementSibling.hidden = threadRoot.hidden = thread.isHidden = false;
    }
  };

  QuoteBacklink = {
    init: function() {
      var format;
      if (g.VIEW === 'catalog' || !Conf['Quote Backlinks']) {
        return;
      }
      format = Conf['backlink'].replace(/%id/g, "' + id + '");
      this.funk = Function('id', "return '" + format + "'");
      this.containers = {};
      Post.callbacks.push({
        name: 'Quote Backlinking Part 1',
        cb: this.firstNode
      });
      return Post.callbacks.push({
        name: 'Quote Backlinking Part 2',
        cb: this.secondNode
      });
    },
    firstNode: function() {
      var a, clone, container, containers, link, post, quote, _i, _j, _k, _len, _len1, _len2, _ref, _ref1;
      if (this.isClone || !this.quotes.length) {
        return;
      }
      a = $.el('a', {
        href: "/" + this.board + "/res/" + this.thread + "#p" + this,
        className: this.isHidden ? 'filtered backlink' : 'backlink',
        textContent: QuoteBacklink.funk(this.ID)
      });
      _ref = this.quotes;
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        quote = _ref[_i];
        containers = [QuoteBacklink.getContainer(quote)];
        if ((post = g.posts[quote]) && post.nodes.backlinkContainer) {
          _ref1 = post.clones;
          for (_j = 0, _len1 = _ref1.length; _j < _len1; _j++) {
            clone = _ref1[_j];
            containers.push(clone.nodes.backlinkContainer);
          }
        }
        for (_k = 0, _len2 = containers.length; _k < _len2; _k++) {
          container = containers[_k];
          link = a.cloneNode(true);
          if (Conf['Quote Previewing']) {
            $.on(link, 'mouseover', QuotePreview.mouseover);
          }
          if (Conf['Quote Inlining']) {
            $.on(link, 'click', QuoteInline.toggle);
          }
          $.add(container, [$.tn(' '), link]);
        }
      }
    },
    secondNode: function() {
      var container;
      if (this.isClone && (this.origin.isReply || Conf['OP Backlinks'])) {
        this.nodes.backlinkContainer = $('.container', this.nodes.info);
        return;
      }
      if (!(this.isReply || Conf['OP Backlinks'])) {
        return;
      }
      container = QuoteBacklink.getContainer(this.fullID);
      this.nodes.backlinkContainer = container;
      return $.add(this.nodes.info, container);
    },
    getContainer: function(id) {
      var _base;
      return (_base = this.containers)[id] || (_base[id] = $.el('span', {
        className: 'container'
      }));
    }
  };

  QuoteCT = {
    init: function() {
      if (g.VIEW === 'catalog' || !Conf['Mark Cross-thread Quotes']) {
        return;
      }
      this.text = '\u00A0(Cross-thread)';
      return Post.callbacks.push({
        name: 'Mark Cross-thread Quotes',
        cb: this.node
      });
    },
    node: function() {
      var board, boardID, quotelink, thread, threadID, _i, _len, _ref, _ref1, _ref2;
      if (this.isClone && this.thread === this.context.thread) {
        return;
      }
      _ref = this.isClone ? this.context : this, board = _ref.board, thread = _ref.thread;
      _ref1 = this.nodes.quotelinks;
      for (_i = 0, _len = _ref1.length; _i < _len; _i++) {
        quotelink = _ref1[_i];
        _ref2 = Get.postDataFromLink(quotelink), boardID = _ref2.boardID, threadID = _ref2.threadID;
        if (!threadID) {
          continue;
        }
        if (this.isClone) {
          quotelink.textContent = quotelink.textContent.replace(QuoteCT.text, '');
        }
        if (boardID === board.ID && threadID !== thread.ID) {
          $.add(quotelink, $.tn(QuoteCT.text));
        }
      }
    }
  };

  QuoteInline = {
    init: function() {
      if (g.VIEW === 'catalog' || !Conf['Quote Inlining']) {
        return;
      }
      return Post.callbacks.push({
        name: 'Quote Inlining',
        cb: this.node
      });
    },
    node: function() {
      var link, _i, _len, _ref;
      _ref = this.nodes.quotelinks.concat(__slice.call(this.nodes.backlinks));
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        link = _ref[_i];
        $.on(link, 'click', QuoteInline.toggle);
      }
    },
    toggle: function(e) {
      var boardID, context, postID, threadID, _ref;
      if (e.shiftKey || e.altKey || e.ctrlKey || e.metaKey || e.button !== 0) {
        return;
      }
      e.preventDefault();
      _ref = Get.postDataFromLink(this), boardID = _ref.boardID, threadID = _ref.threadID, postID = _ref.postID;
      context = Get.contextFromNode(this);
      if ($.hasClass(this, 'inlined')) {
        QuoteInline.rm(this, boardID, threadID, postID, context);
      } else {
        if ($.x("ancestor::div[@id='p" + postID + "']", this)) {
          return;
        }
        QuoteInline.add(this, boardID, threadID, postID, context);
      }
      return this.classList.toggle('inlined');
    },
    findRoot: function(quotelink, isBacklink) {
      if (isBacklink) {
        return quotelink.parentNode.parentNode;
      } else {
        return $.x('ancestor-or-self::*[parent::blockquote][1]', quotelink);
      }
    },
    add: function(quotelink, boardID, threadID, postID, context) {
      var inline, isBacklink, post;
      isBacklink = $.hasClass(quotelink, 'backlink');
      inline = $.el('div', {
        id: "i" + postID,
        className: 'inline'
      });
      $.after(QuoteInline.findRoot(quotelink, isBacklink), inline);
      Get.postClone(boardID, threadID, postID, inline, context);
      if (!((post = g.posts["" + boardID + "." + postID]) && context.thread === post.thread)) {
        return;
      }
      if (isBacklink && Conf['Forward Hiding']) {
        $.addClass(post.nodes.root, 'forwarded');
        post.forwarded++ || (post.forwarded = 1);
      }
      if (!Unread.posts) {
        return;
      }
      return Unread.readSinglePost(post);
    },
    rm: function(quotelink, boardID, threadID, postID, context) {
      var el, inlined, isBacklink, post, root, _ref;
      isBacklink = $.hasClass(quotelink, 'backlink');
      root = QuoteInline.findRoot(quotelink, isBacklink);
      root = $.x("following-sibling::div[@id='i" + postID + "'][1]", root);
      $.rm(root);
      if (!(el = root.firstElementChild)) {
        return;
      }
      post = g.posts["" + boardID + "." + postID];
      post.rmClone(el.dataset.clone);
      if (Conf['Forward Hiding'] && isBacklink && context.thread === g.threads["" + boardID + "." + threadID] && !--post.forwarded) {
        delete post.forwarded;
        $.rmClass(post.nodes.root, 'forwarded');
      }
      while (inlined = $('.inlined', el)) {
        _ref = Get.postDataFromLink(inlined), boardID = _ref.boardID, threadID = _ref.threadID, postID = _ref.postID;
        QuoteInline.rm(inlined, boardID, threadID, postID, context);
        $.rmClass(inlined, 'inlined');
      }
    }
  };

  QuoteOP = {
    init: function() {
      if (g.VIEW === 'catalog' || !Conf['Mark OP Quotes']) {
        return;
      }
      this.text = '\u00A0(OP)';
      return Post.callbacks.push({
        name: 'Mark OP Quotes',
        cb: this.node
      });
    },
    node: function() {
      var boardID, fullID, postID, quotelink, quotelinks, quotes, _i, _j, _len, _len1, _ref, _ref1;
      if (this.isClone && this.thread === this.context.thread) {
        return;
      }
      if (!(quotes = this.quotes).length) {
        return;
      }
      quotelinks = this.nodes.quotelinks;
      if (this.isClone && (_ref = this.thread.fullID, __indexOf.call(quotes, _ref) >= 0)) {
        for (_i = 0, _len = quotelinks.length; _i < _len; _i++) {
          quotelink = quotelinks[_i];
          quotelink.textContent = quotelink.textContent.replace(QuoteOP.text, '');
        }
      }
      fullID = (this.isClone ? this.context : this).thread.fullID;
      if (__indexOf.call(quotes, fullID) < 0) {
        return;
      }
      for (_j = 0, _len1 = quotelinks.length; _j < _len1; _j++) {
        quotelink = quotelinks[_j];
        _ref1 = Get.postDataFromLink(quotelink), boardID = _ref1.boardID, postID = _ref1.postID;
        if (("" + boardID + "." + postID) === fullID) {
          $.add(quotelink, $.tn(QuoteOP.text));
        }
      }
    }
  };

  QuotePreview = {
    init: function() {
      if (g.VIEW === 'catalog' || !Conf['Quote Previewing']) {
        return;
      }
      return Post.callbacks.push({
        name: 'Quote Previewing',
        cb: this.node
      });
    },
    node: function() {
      var link, _i, _len, _ref;
      _ref = this.nodes.quotelinks.concat(__slice.call(this.nodes.backlinks));
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        link = _ref[_i];
        $.on(link, 'mouseover', QuotePreview.mouseover);
      }
    },
    mouseover: function(e) {
      var boardID, clone, origin, post, postID, posts, qp, quote, quoterID, threadID, _i, _j, _len, _len1, _ref, _ref1;
      if ($.hasClass(this, 'inlined')) {
        return;
      }
      _ref = Get.postDataFromLink(this), boardID = _ref.boardID, threadID = _ref.threadID, postID = _ref.postID;
      qp = $.el('div', {
        id: 'qp',
        className: 'dialog'
      });
      $.add(d.body, qp);
      Get.postClone(boardID, threadID, postID, qp, Get.contextFromNode(this));
      UI.hover({
        root: this,
        el: qp,
        latestEvent: e,
        endEvents: 'mouseout click',
        cb: QuotePreview.mouseout,
        asapTest: function() {
          return qp.firstElementChild;
        }
      });
      if (!(origin = g.posts["" + boardID + "." + postID])) {
        return;
      }
      if (Conf['Quote Highlighting']) {
        posts = [origin].concat(origin.clones);
        posts.pop();
        for (_i = 0, _len = posts.length; _i < _len; _i++) {
          post = posts[_i];
          $.addClass(post.nodes.post, 'qphl');
        }
      }
      quoterID = $.x('ancestor::*[@id][1]', this).id.match(/\d+$/)[0];
      clone = Get.postFromRoot(qp.firstChild);
      _ref1 = clone.nodes.quotelinks.concat(__slice.call(clone.nodes.backlinks));
      for (_j = 0, _len1 = _ref1.length; _j < _len1; _j++) {
        quote = _ref1[_j];
        if (quote.hash.slice(2) === quoterID) {
          $.addClass(quote, 'forwardlink');
        }
      }
    },
    mouseout: function() {
      var clone, post, root, _i, _len, _ref;
      if (!(root = this.el.firstElementChild)) {
        return;
      }
      clone = Get.postFromRoot(root);
      post = clone.origin;
      post.rmClone(root.dataset.clone);
      if (!Conf['Quote Highlighting']) {
        return;
      }
      _ref = [post].concat(post.clones);
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        post = _ref[_i];
        $.rmClass(post.nodes.post, 'qphl');
      }
    }
  };

  QuoteStrikeThrough = {
    init: function() {
      if (g.VIEW === 'catalog' || !Conf['Reply Hiding'] && !Conf['Reply Hiding Link'] && !Conf['Filter']) {
        return;
      }
      return Post.callbacks.push({
        name: 'Strike-through Quotes',
        cb: this.node
      });
    },
    node: function() {
      var boardID, postID, quotelink, _i, _len, _ref, _ref1, _ref2;
      if (this.isClone) {
        return;
      }
      _ref = this.nodes.quotelinks;
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        quotelink = _ref[_i];
        _ref1 = Get.postDataFromLink(quotelink), boardID = _ref1.boardID, postID = _ref1.postID;
        if ((_ref2 = g.posts["" + boardID + "." + postID]) != null ? _ref2.isHidden : void 0) {
          $.addClass(quotelink, 'filtered');
        }
      }
    }
  };

  QuoteYou = {
    init: function() {
      if (g.VIEW === 'catalog' || !Conf['Mark Quotes of You'] || !Conf['Quick Reply']) {
        return;
      }
      this.text = '\u00A0(You)';
      return Post.callbacks.push({
        name: 'Mark Quotes of You',
        cb: this.node
      });
    },
    node: function() {
      var quotelink, _i, _len, _ref;
      if (this.isClone) {
        return;
      }
      _ref = this.nodes.quotelinks;
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        quotelink = _ref[_i];
        if (QR.db.get(Get.postDataFromLink(quotelink))) {
          $.add(quotelink, $.tn(QuoteYou.text));
        }
      }
    }
  };

  Quotify = {
    init: function() {
      if (g.VIEW === 'catalog' || !Conf['Resurrect Quotes']) {
        return;
      }
      return Post.callbacks.push({
        name: 'Resurrect Quotes',
        cb: this.node
      });
    },
    node: function() {
      var deadlink, _i, _len, _ref;
      _ref = $$('.deadlink', this.nodes.comment);
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        deadlink = _ref[_i];
        if (this.isClone) {
          if ($.hasClass(deadlink, 'quotelink')) {
            this.nodes.quotelinks.push(deadlink);
          }
        } else {
          Quotify.parseDeadlink.call(this, deadlink);
        }
      }
    },
    parseDeadlink: function(deadlink) {
      var a, boardID, m, post, postID, quote, quoteID, redirect, _ref;
      if ($.hasClass(deadlink.parentNode, 'prettyprint')) {
        Quotify.fixDeadlink(deadlink);
        return;
      }
      quote = deadlink.textContent;
      if (!(postID = (_ref = quote.match(/\d+$/)) != null ? _ref[0] : void 0)) {
        return;
      }
      if (postID[0] === '0') {
        Quotify.fixDeadlink(deadlink);
        return;
      }
      boardID = (m = quote.match(/^>>>\/([a-z\d]+)/)) ? m[1] : this.board.ID;
      quoteID = "" + boardID + "." + postID;
      if (post = g.posts[quoteID]) {
        if (!post.isDead) {
          a = $.el('a', {
            href: "/" + boardID + "/res/" + post.thread + "#p" + postID,
            className: 'quotelink',
            textContent: quote
          });
        } else {
          a = $.el('a', {
            href: "/" + boardID + "/res/" + post.thread + "#p" + postID,
            className: 'quotelink deadlink',
            target: '_blank',
            textContent: "" + quote + "\u00A0(Dead)"
          });
          $.extend(a.dataset, {
            boardID: boardID,
            threadID: post.thread.ID,
            postID: postID
          });
        }
      } else if (redirect = Redirect.to('thread', {
        boardID: boardID,
        threadID: 0,
        postID: postID
      })) {
        a = $.el('a', {
          href: redirect,
          className: 'deadlink',
          target: '_blank',
          textContent: "" + quote + "\u00A0(Dead)"
        });
        if (Redirect.to('post', {
          boardID: boardID,
          postID: postID
        })) {
          $.addClass(a, 'quotelink');
          $.extend(a.dataset, {
            boardID: boardID,
            postID: postID
          });
        }
      }
      if (__indexOf.call(this.quotes, quoteID) < 0) {
        this.quotes.push(quoteID);
      }
      if (!a) {
        deadlink.textContent = "" + quote + "\u00A0(Dead)";
        return;
      }
      $.replace(deadlink, a);
      if ($.hasClass(a, 'quotelink')) {
        return this.nodes.quotelinks.push(a);
      }
    },
    fixDeadlink: function(deadlink) {
      var el, green;
      if (!(el = deadlink.previousSibling) || el.nodeName === 'BR') {
        green = $.el('span', {
          className: 'quote'
        });
        $.before(deadlink, green);
        $.add(green, deadlink);
      }
      return $.replace(deadlink, __slice.call(deadlink.childNodes));
    }
  };

  QR = {
    init: function() {
      if (!Conf['Quick Reply']) {
        return;
      }
      this.db = new DataBoard('yourPosts');
      if (Conf['Hide Original Post Form']) {
        $.addClass(doc, 'hide-original-post-form');
      }
      $.on(d, '4chanXInitFinished', this.initReady);
      return Post.callbacks.push({
        name: 'Quick Reply',
        cb: this.node
      });
    },
    initReady: function() {
      var sc;
      $.off(d, '4chanXInitFinished', QR.initReady);
      QR.postingIsEnabled = !!$.id('postForm');
      if (!QR.postingIsEnabled) {
        return;
      }
      sc = $.el('a', {
        className: 'qr-shortcut fa fa-comment-o',
        title: 'Quick Reply',
        href: 'javascript:;'
      });
      $.on(sc, 'click', function() {
        $.event('CloseMenu');
        QR.open();
        return QR.nodes.com.focus();
      });
      Header.addShortcut(sc, 2);
      $.on(d, 'QRGetSelectedPost', function(_arg) {
        var cb;
        cb = _arg.detail;
        return cb(QR.selected);
      });
      $.on(d, 'QRAddPreSubmitHook', function(_arg) {
        var cb;
        cb = _arg.detail;
        return QR.preSubmitHooks.push(cb);
      });
      $.on(d, 'dragover', QR.dragOver);
      $.on(d, 'drop', QR.dropFile);
      $.on(d, 'dragstart dragend', QR.drag);
      switch (g.VIEW) {
        case 'index':
          $.on(d, 'IndexRefresh', QR.generatePostableThreadsList);
          break;
        case 'thread':
          $.on(d, 'ThreadUpdate', function() {
            if (g.DEAD) {
              return QR.abort();
            } else {
              return QR.status();
            }
          });
      }
      if (Conf['Persistent QR']) {
        return QR.persist();
      }
    },
    node: function() {
      return $.on($('a[title="Quote this post"]', this.nodes.info), 'click', QR.quote);
    },
    persist: function() {
      QR.open();
      if (Conf['Auto-Hide QR'] || g.VIEW === 'catalog') {
        return QR.hide();
      }
    },
    open: function() {
      var err;
      if (QR.nodes) {
        QR.nodes.el.hidden = false;
        QR.unhide();
        return;
      }
      try {
        return QR.dialog();
      } catch (_error) {
        err = _error;
        delete QR.nodes;
        return Main.handleErrors({
          message: 'Quick Reply dialog creation crashed.',
          error: err
        });
      }
    },
    close: function() {
      var post, _i, _len, _ref;
      if (QR.req) {
        QR.abort();
        return;
      }
      QR.nodes.el.hidden = true;
      QR.cleanNotifications();
      d.activeElement.blur();
      $.rmClass(QR.nodes.el, 'dump');
      new QR.post(true);
      _ref = QR.posts.splice(0, QR.posts.length - 1);
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        post = _ref[_i];
        post["delete"]();
      }
      QR.cooldown.auto = false;
      return QR.status();
    },
    focusin: function() {
      return $.addClass(QR.nodes.el, 'has-focus');
    },
    focusout: function() {
      return $.queueTask(function() {
        if ($.x('ancestor::div[@id="qr"]', d.activeElement)) {
          return;
        }
        return $.rmClass(QR.nodes.el, 'has-focus');
      });
    },
    hide: function() {
      d.activeElement.blur();
      $.addClass(QR.nodes.el, 'autohide');
      return QR.nodes.autohide.checked = true;
    },
    unhide: function() {
      $.rmClass(QR.nodes.el, 'autohide');
      return QR.nodes.autohide.checked = false;
    },
    toggleHide: function() {
      if (this.checked) {
        return QR.hide();
      } else {
        return QR.unhide();
      }
    },
    error: function(err) {
      var el, notice, notif;
      QR.open();
      if (typeof err === 'string') {
        el = $.tn(err);
      } else {
        el = err;
        el.removeAttribute('style');
      }
      if (QR.captcha.isEnabled && /captcha|verification/i.test(el.textContent)) {
        QR.captcha.nodes.input.focus();
      }
      notice = new Notice('warning', el);
      QR.notifications.push(notice);
      if (!Header.areNotificationsEnabled) {
        return;
      }
      notif = new Notification('Quick reply warning', {
        body: el.textContent,
        icon: Favicon.logo
      });
      return notif.onclick = function() {
        return window.focus();
      };
    },
    notifications: [],
    cleanNotifications: function() {
      var notification, _i, _len, _ref;
      _ref = QR.notifications;
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        notification = _ref[_i];
        notification.close();
      }
      return QR.notifications = [];
    },
    status: function() {
      var disabled, status, thread, value;
      if (!QR.nodes) {
        return;
      }
      thread = QR.posts[0].thread;
      if (thread !== 'new' && g.threads["" + g.BOARD + "." + thread].isDead) {
        value = 404;
        disabled = true;
        QR.cooldown.auto = false;
      }
      value = QR.req ? QR.req.progress : QR.cooldown.seconds || value;
      status = QR.nodes.status;
      status.value = !value ? 'Submit' : QR.cooldown.auto ? "Auto " + value : value;
      return status.disabled = disabled || false;
    },
    persona: {
      pwd: '',
      always: {},
      init: function() {
        QR.persona.getPassword();
        return $.get('QR.personas', Conf['QR.personas'], function(_arg) {
          var arr, item, personas, type, types, _i, _len, _ref;
          personas = _arg['QR.personas'];
          types = {
            name: [],
            email: [],
            sub: []
          };
          _ref = personas.split('\n');
          for (_i = 0, _len = _ref.length; _i < _len; _i++) {
            item = _ref[_i];
            QR.persona.parseItem(item.trim(), types);
          }
          for (type in types) {
            arr = types[type];
            QR.persona.loadPersonas(type, arr);
          }
        });
      },
      parseItem: function(item, types) {
        var boards, match, type, val, _ref, _ref1, _ref2;
        if (item[0] === '#') {
          return;
        }
        if (!(match = item.match(/(name|email|subject|password):"(.*)"/i))) {
          return;
        }
        _ref = match, match = _ref[0], type = _ref[1], val = _ref[2];
        item = item.replace(match, '');
        boards = ((_ref1 = item.match(/boards:([^;]+)/i)) != null ? _ref1[1].toLowerCase() : void 0) || 'global';
        if (boards !== 'global' && !(_ref2 = g.BOARD.ID, __indexOf.call(boards.split(','), _ref2) >= 0)) {
          return;
        }
        if (type === 'password') {
          QR.persona.pwd = val;
          return;
        }
        if (type === 'subject') {
          type = 'sub';
        }
        if (/always/i.test(item)) {
          QR.persona.always[type] = val;
        }
        if (__indexOf.call(types[type], val) < 0) {
          return types[type].push(val);
        }
      },
      loadPersonas: function(type, arr) {
        var list, val, _i, _len;
        list = $("#list-" + type, QR.nodes.el);
        for (_i = 0, _len = arr.length; _i < _len; _i++) {
          val = arr[_i];
          if (val) {
            $.add(list, $.el('option', {
              textContent: val
            }));
          }
        }
      },
      getPassword: function() {
        var input, m;
        if (!QR.persona.pwd) {
          QR.persona.pwd = (m = d.cookie.match(/4chan_pass=([^;]+)/)) ? decodeURIComponent(m[1]) : (input = $.id('postPassword')) ? input.value : $.id('delPassword').value;
        }
        return QR.persona.pwd;
      },
      get: function(cb) {
        return $.get('QR.persona', {}, function(_arg) {
          var persona;
          persona = _arg['QR.persona'];
          return cb(persona);
        });
      },
      set: function(post) {
        return $.get('QR.persona', {}, function(_arg) {
          var persona;
          persona = _arg['QR.persona'];
          persona = {
            name: post.name,
            email: /^sage$/.test(post.email) ? persona.email : post.email,
            sub: Conf['Remember Subject'] ? post.sub : void 0,
            flag: post.flag
          };
          return $.set('QR.persona', persona);
        });
      }
    },
    cooldown: {
      init: function() {
        var key, setTimers, type, _base,
          _this = this;
        if (!Conf['Cooldown']) {
          return;
        }
        setTimers = function(e) {
          return QR.cooldown.types = e.detail;
        };
        $.on(window, 'cooldown:timers', setTimers);
        $.globalEval('window.dispatchEvent(new CustomEvent("cooldown:timers", {detail: cooldowns}))');
        (_base = QR.cooldown).types || (_base.types = {});
        $.off(window, 'cooldown:timers', setTimers);
        for (type in QR.cooldown.types) {
          QR.cooldown.types[type] = +QR.cooldown.types[type];
        }
        QR.cooldown.upSpd = 0;
        QR.cooldown.upSpdAccuracy = .5;
        key = "cooldown." + g.BOARD;
        $.get(key, {}, function(item) {
          QR.cooldown.cooldowns = item[key];
          return QR.cooldown.start();
        });
        return $.sync(key, QR.cooldown.sync);
      },
      start: function() {
        if (!Conf['Cooldown']) {
          return;
        }
        if (QR.cooldown.isCounting) {
          return;
        }
        QR.cooldown.isCounting = true;
        return QR.cooldown.count();
      },
      sync: function(cooldowns) {
        var id;
        for (id in cooldowns) {
          QR.cooldown.cooldowns[id] = cooldowns[id];
        }
        return QR.cooldown.start();
      },
      set: function(data) {
        var cooldown, delay, hasFile, isReply, post, req, start, threadID, upSpd;
        if (!Conf['Cooldown']) {
          return;
        }
        req = data.req, post = data.post, isReply = data.isReply, threadID = data.threadID, delay = data.delay;
        start = req ? req.uploadEndTime : Date.now();
        if (delay) {
          cooldown = {
            delay: delay
          };
        } else {
          if (hasFile = !!post.file) {
            upSpd = post.file.size / ((start - req.uploadStartTime) / $.SECOND);
            QR.cooldown.upSpdAccuracy = ((upSpd > QR.cooldown.upSpd * .9) + QR.cooldown.upSpdAccuracy) / 2;
            QR.cooldown.upSpd = upSpd;
          }
          cooldown = {
            isReply: isReply,
            hasFile: hasFile,
            threadID: threadID
          };
        }
        QR.cooldown.cooldowns[start] = cooldown;
        $.set("cooldown." + g.BOARD, QR.cooldown.cooldowns);
        return QR.cooldown.start();
      },
      unset: function(id) {
        delete QR.cooldown.cooldowns[id];
        if (Object.keys(QR.cooldown.cooldowns).length) {
          return $.set("cooldown." + g.BOARD, QR.cooldown.cooldowns);
        } else {
          return $["delete"]("cooldown." + g.BOARD);
        }
      },
      count: function() {
        var cooldown, cooldowns, elapsed, hasFile, isReply, maxTimer, now, post, seconds, start, type, types, upSpd, upSpdAccuracy, update, _ref;
        if (!Object.keys(QR.cooldown.cooldowns).length) {
          $["delete"]("" + g.BOARD + ".cooldown");
          delete QR.cooldown.isCounting;
          delete QR.cooldown.seconds;
          QR.status();
          return;
        }
        clearTimeout(QR.cooldown.timeout);
        QR.cooldown.timeout = setTimeout(QR.cooldown.count, $.SECOND);
        now = Date.now();
        post = QR.posts[0];
        isReply = post.thread !== 'new';
        hasFile = !!post.file;
        seconds = null;
        _ref = QR.cooldown, types = _ref.types, cooldowns = _ref.cooldowns, upSpd = _ref.upSpd, upSpdAccuracy = _ref.upSpdAccuracy;
        for (start in cooldowns) {
          cooldown = cooldowns[start];
          if ('delay' in cooldown) {
            if (cooldown.delay) {
              seconds = Math.max(seconds, cooldown.delay--);
            } else {
              seconds = Math.max(seconds, 0);
              QR.cooldown.unset(start);
            }
            continue;
          }
          if ('timeout' in cooldown) {
            QR.cooldown.unset(start);
            continue;
          }
          if (isReply === cooldown.isReply) {
            elapsed = Math.floor((now - start) / $.SECOND);
            if (elapsed < 0) {
              continue;
            }
            if (!isReply) {
              type = 'thread';
            } else if (hasFile) {
              if (!cooldown.hasFile) {
                seconds = Math.max(seconds, 0);
                continue;
              }
              type = 'image';
            } else {
              type = 'reply';
            }
            maxTimer = Math.max(types[type] || 0, types[type + '_intra'] || 0);
            if (!((start <= now && now <= start + maxTimer * $.SECOND))) {
              QR.cooldown.unset(start);
            }
            if (isReply && +post.thread === cooldown.threadID) {
              type += '_intra';
            }
            seconds = Math.max(seconds, types[type] - elapsed);
          }
        }
        if (seconds && Conf['Cooldown Prediction'] && hasFile && upSpd) {
          seconds -= Math.floor(post.file.size / upSpd * upSpdAccuracy);
          seconds = Math.max(seconds, 0);
        }
        update = seconds !== null || !!QR.cooldown.seconds;
        QR.cooldown.seconds = seconds;
        if (update) {
          QR.status();
        }
        if (seconds === 0 && QR.cooldown.auto && !QR.req) {
          return QR.submit();
        }
      }
    },
    quote: function(e) {
      var caretPos, com, index, post, range, s, sel, text, thread, _ref;
      if (e != null) {
        e.preventDefault();
      }
      if (!QR.postingIsEnabled) {
        return;
      }
      sel = d.getSelection();
      post = Get.postFromNode(this);
      text = ">>" + post + "\n";
      if ((s = sel.toString().trim()) && post === Get.postFromNode(sel.anchorNode)) {
        s = s.replace(/\n/g, '\n>');
        text += ">" + s + "\n";
      }
      QR.open();
      if (QR.selected.isLocked) {
        index = QR.posts.indexOf(QR.selected);
        (QR.posts[index + 1] || new QR.post()).select();
        $.addClass(QR.nodes.el, 'dump');
        QR.cooldown.auto = true;
      }
      _ref = QR.nodes, com = _ref.com, thread = _ref.thread;
      if (!com.value) {
        thread.value = Get.threadFromNode(this);
      }
      caretPos = com.selectionStart;
      com.value = com.value.slice(0, caretPos) + text + com.value.slice(com.selectionEnd);
      range = caretPos + text.length;
      com.setSelectionRange(range, range);
      com.focus();
      QR.selected.save(com);
      return QR.selected.save(thread);
    },
    characterCount: function() {
      var count, counter;
      counter = QR.nodes.charCount;
      count = QR.nodes.com.textLength;
      counter.textContent = count;
      counter.hidden = count < 1000;
      return (count > 1500 ? $.addClass : $.rmClass)(counter, 'warning');
    },
    drag: function(e) {
      var toggle;
      toggle = e.type === 'dragstart' ? $.off : $.on;
      toggle(d, 'dragover', QR.dragOver);
      return toggle(d, 'drop', QR.dropFile);
    },
    dragOver: function(e) {
      e.preventDefault();
      return e.dataTransfer.dropEffect = 'copy';
    },
    dropFile: function(e) {
      if (!e.dataTransfer.files.length) {
        return;
      }
      e.preventDefault();
      QR.open();
      return QR.handleFiles(e.dataTransfer.files);
    },
    paste: function(e) {
      var blob, files, item, _i, _len, _ref;
      files = [];
      _ref = e.clipboardData.items;
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        item = _ref[_i];
        if (!(item.kind === 'file')) {
          continue;
        }
        blob = item.getAsFile();
        blob.name = 'file';
        if (blob.type) {
          blob.name += '.' + blob.type.split('/')[1];
        }
        files.push(blob);
      }
      if (!files.length) {
        return;
      }
      QR.open();
      QR.handleFiles(files);
      return $.addClass(QR.nodes.el, 'dump');
    },
    handleFiles: function(files) {
      var file, isSingle, max, _i, _len;
      if (this !== QR) {
        files = __slice.call(this.files);
        this.value = null;
      }
      if (!files.length) {
        return;
      }
      max = QR.nodes.fileInput.max;
      isSingle = files.length === 1;
      QR.cleanNotifications();
      for (_i = 0, _len = files.length; _i < _len; _i++) {
        file = files[_i];
        QR.handleFile(file, isSingle, max);
      }
      if (!isSingle) {
        return $.addClass(QR.nodes.el, 'dump');
      }
    },
    handleFile: function(file, isSingle, max) {
      var post, _ref;
      if (file.size > max) {
        QR.error("" + file.name + ": File too large (file: " + ($.bytesToString(file.size)) + ", max: " + ($.bytesToString(max)) + ").");
        return;
      }
      if (_ref = file.type, __indexOf.call(QR.mimeTypes, _ref) < 0) {
        if (!/^text/.test(file.type)) {
          QR.error("" + file.name + ": Unsupported file type.");
          return;
        }
        if (isSingle) {
          post = QR.selected;
        } else if ((post = QR.posts[QR.posts.length - 1]).com) {
          post = new QR.post();
        }
        post.pasteText(file);
        return;
      }
      if (isSingle) {
        post = QR.selected;
      } else if ((post = QR.posts[QR.posts.length - 1]).file) {
        post = new QR.post();
      }
      return post.setFile(file);
    },
    openFileInput: function() {
      return QR.nodes.fileInput.click();
    },
    posts: [],
    post: (function() {
      function _Class(select) {
        this.select = __bind(this.select, this);
        var el, elm, event, prev, _i, _j, _len, _len1, _ref, _ref1,
          _this = this;
        el = $.el('a', {
          className: 'qr-preview',
          draggable: true,
          href: 'javascript:;',
          innerHTML: '<a class=remove>×</a><label hidden><input type=checkbox> Spoiler</label><span></span>'
        });
        this.nodes = {
          el: el,
          rm: el.firstChild,
          label: $('label', el),
          spoiler: $('input', el),
          span: el.lastChild
        };
        _ref = $$('*', el);
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
          elm = _ref[_i];
          $.on(elm, 'blur', QR.focusout);
          $.on(elm, 'focus', QR.focusin);
        }
        $.on(el, 'click', this.select);
        $.on(this.nodes.rm, 'click', function(e) {
          e.stopPropagation();
          return _this.rm();
        });
        $.on(this.nodes.label, 'click', function(e) {
          return e.stopPropagation();
        });
        $.on(this.nodes.spoiler, 'change', function(e) {
          _this.spoiler = e.target.checked;
          if (_this === QR.selected) {
            return QR.nodes.spoiler.checked = _this.spoiler;
          }
        });
        $.add(QR.nodes.dumpList, el);
        _ref1 = ['dragStart', 'dragEnter', 'dragLeave', 'dragOver', 'dragEnd', 'drop'];
        for (_j = 0, _len1 = _ref1.length; _j < _len1; _j++) {
          event = _ref1[_j];
          $.on(el, event.toLowerCase(), this[event]);
        }
        this.thread = g.VIEW === 'thread' ? g.THREADID : 'new';
        prev = QR.posts[QR.posts.length - 1];
        QR.posts.push(this);
        this.nodes.spoiler.checked = this.spoiler = prev && Conf['Remember Spoiler'] ? prev.spoiler : false;
        QR.persona.get(function(persona) {
          _this.name = 'name' in QR.persona.always ? QR.persona.always.name : prev ? prev.name : persona.name;
          _this.email = 'email' in QR.persona.always ? QR.persona.always.email : prev && !/^sage$/.test(prev.email) ? prev.email : persona.email;
          _this.sub = 'sub' in QR.persona.always ? QR.persona.always.sub : Conf['Remember Subject'] ? prev ? prev.sub : persona.sub : '';
          if (QR.nodes.flag) {
            _this.flag = prev ? prev.flag : persona.flag;
          }
          if (QR.selected === _this) {
            return _this.load();
          }
        });
        if (select) {
          this.select();
        }
        this.unlock();
      }

      _Class.prototype.rm = function() {
        var index;
        this["delete"]();
        index = QR.posts.indexOf(this);
        if (QR.posts.length === 1) {
          new QR.post(true);
        } else if (this === QR.selected) {
          (QR.posts[index - 1] || QR.posts[index + 1]).select();
        }
        QR.posts.splice(index, 1);
        return QR.status();
      };

      _Class.prototype["delete"] = function() {
        $.rm(this.nodes.el);
        return URL.revokeObjectURL(this.URL);
      };

      _Class.prototype.lock = function(lock) {
        var name, node, _i, _len, _ref;
        if (lock == null) {
          lock = true;
        }
        this.isLocked = lock;
        if (this !== QR.selected) {
          return;
        }
        _ref = ['thread', 'name', 'email', 'sub', 'com', 'fileButton', 'filename', 'spoiler', 'flag'];
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
          name = _ref[_i];
          if (!(node = QR.nodes[name])) {
            continue;
          }
          node.disabled = lock;
        }
        this.nodes.rm.style.visibility = lock ? 'hidden' : '';
        (lock ? $.off : $.on)(QR.nodes.filename.previousElementSibling, 'click', QR.openFileInput);
        this.nodes.spoiler.disabled = lock;
        return this.nodes.el.draggable = !lock;
      };

      _Class.prototype.unlock = function() {
        return this.lock(false);
      };

      _Class.prototype.select = function() {
        var rectEl, rectList;
        if (QR.selected) {
          QR.selected.nodes.el.id = null;
          QR.selected.forceSave();
        }
        QR.selected = this;
        this.lock(this.isLocked);
        this.nodes.el.id = 'selected';
        rectEl = this.nodes.el.getBoundingClientRect();
        rectList = this.nodes.el.parentNode.getBoundingClientRect();
        this.nodes.el.parentNode.scrollLeft += rectEl.left + rectEl.width / 2 - rectList.left - rectList.width / 2;
        this.load();
        return $.event('QRPostSelection', this);
      };

      _Class.prototype.load = function() {
        var name, node, _i, _len, _ref;
        _ref = ['thread', 'name', 'email', 'sub', 'com', 'filename', 'flag'];
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
          name = _ref[_i];
          if (!(node = QR.nodes[name])) {
            continue;
          }
          node.value = this[name] || node.dataset["default"] || null;
        }
        this.showFileData();
        return QR.characterCount();
      };

      _Class.prototype.save = function(input) {
        var name, _ref;
        if (input.type === 'checkbox') {
          this.spoiler = input.checked;
          return;
        }
        name = input.dataset.name;
        this[name] = input.value || input.dataset["default"] || null;
        switch (name) {
          case 'thread':
            return QR.status();
          case 'com':
            this.nodes.span.textContent = this.com;
            QR.characterCount();
            if (QR.cooldown.auto && this === QR.posts[0] && (0 < (_ref = QR.cooldown.seconds) && _ref <= 5)) {
              return QR.cooldown.auto = false;
            }
            break;
          case 'filename':
            if (!this.file) {
              return;
            }
            this.file.newName = this.filename.replace(/[/\\]/g, '-');
            if (!/\.(jpe?g|png|gif|pdf|swf)$/i.test(this.filename)) {
              this.file.newName += '.jpg';
            }
            return this.updateFilename();
        }
      };

      _Class.prototype.forceSave = function() {
        var name, node, _i, _len, _ref;
        if (this !== QR.selected) {
          return;
        }
        _ref = ['thread', 'name', 'email', 'sub', 'com', 'filename', 'spoiler', 'flag'];
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
          name = _ref[_i];
          if (!(node = QR.nodes[name])) {
            continue;
          }
          this.save(node);
        }
      };

      _Class.prototype.setFile = function(file) {
        this.file = file;
        this.filename = file.name;
        this.filesize = $.bytesToString(file.size);
        if (QR.spoiler) {
          this.nodes.label.hidden = false;
        }
        URL.revokeObjectURL(this.URL);
        if (this === QR.selected) {
          this.showFileData();
        }
        if (!/^image/.test(file.type)) {
          this.nodes.el.style.backgroundImage = null;
          return;
        }
        return this.setThumbnail();
      };

      _Class.prototype.setThumbnail = function() {
        var fileURL, img,
          _this = this;
        img = $.el('img');
        img.onload = function() {
          var cv, height, s, width;
          s = 90 * 2;
          if (_this.file.type === 'image/gif') {
            s *= 3;
          }
          height = img.height, width = img.width;
          if (height < s || width < s) {
            _this.URL = fileURL;
            _this.nodes.el.style.backgroundImage = "url(" + _this.URL + ")";
            return;
          }
          if (height <= width) {
            width = s / height * width;
            height = s;
          } else {
            height = s / width * height;
            width = s;
          }
          cv = $.el('canvas');
          cv.height = img.height = height;
          cv.width = img.width = width;
          cv.getContext('2d').drawImage(img, 0, 0, width, height);
          URL.revokeObjectURL(fileURL);
          return cv.toBlob(function(blob) {
            _this.URL = URL.createObjectURL(blob);
            return _this.nodes.el.style.backgroundImage = "url(" + _this.URL + ")";
          });
        };
        fileURL = URL.createObjectURL(this.file);
        return img.src = fileURL;
      };

      _Class.prototype.rmFile = function() {
        if (this.isLocked) {
          return;
        }
        delete this.file;
        delete this.filename;
        delete this.filesize;
        this.nodes.el.title = null;
        this.nodes.el.style.backgroundImage = null;
        if (QR.spoiler) {
          this.nodes.label.hidden = true;
        }
        this.showFileData();
        return URL.revokeObjectURL(this.URL);
      };

      _Class.prototype.updateFilename = function() {
        var long;
        long = "" + this.filename + " (" + this.filesize + ")";
        this.nodes.el.title = long;
        if (this !== QR.selected) {
          return;
        }
        return QR.nodes.filename.title = long;
      };

      _Class.prototype.showFileData = function() {
        if (this.file) {
          this.updateFilename();
          QR.nodes.filename.value = this.filename;
          QR.nodes.filesize.textContent = this.filesize;
          QR.nodes.spoiler.checked = this.spoiler;
          return $.addClass(QR.nodes.fileSubmit, 'has-file');
        } else {
          return $.rmClass(QR.nodes.fileSubmit, 'has-file');
        }
      };

      _Class.prototype.pasteText = function(file) {
        var reader,
          _this = this;
        reader = new FileReader();
        reader.onload = function(e) {
          var text;
          text = e.target.result;
          if (_this.com) {
            _this.com += "\n" + text;
          } else {
            _this.com = text;
          }
          if (QR.selected === _this) {
            QR.nodes.com.value = _this.com;
          }
          return _this.nodes.span.textContent = _this.com;
        };
        return reader.readAsText(file);
      };

      _Class.prototype.dragStart = function(e) {
        e.dataTransfer.setDragImage(this, e.layerX, e.layerY);
        return $.addClass(this, 'drag');
      };

      _Class.prototype.dragEnd = function() {
        return $.rmClass(this, 'drag');
      };

      _Class.prototype.dragEnter = function() {
        return $.addClass(this, 'over');
      };

      _Class.prototype.dragLeave = function() {
        return $.rmClass(this, 'over');
      };

      _Class.prototype.dragOver = function(e) {
        e.preventDefault();
        return e.dataTransfer.dropEffect = 'move';
      };

      _Class.prototype.drop = function() {
        var el, index, newIndex, oldIndex, post;
        $.rmClass(this, 'over');
        if (!this.draggable) {
          return;
        }
        el = $('.drag', this.parentNode);
        index = function(el) {
          return __slice.call(el.parentNode.children).indexOf(el);
        };
        oldIndex = index(el);
        newIndex = index(this);
        (oldIndex < newIndex ? $.after : $.before)(this, el);
        post = QR.posts.splice(oldIndex, 1)[0];
        QR.posts.splice(newIndex, 0, post);
        return QR.status();
      };

      return _Class;

    })(),
    captcha: {
      init: function() {
        if (d.cookie.indexOf('pass_enabled=1') >= 0) {
          return;
        }
        if (!(this.isEnabled = !!$.id('captchaFormPart'))) {
          return;
        }
        return $.asap((function() {
          return $.id('recaptcha_challenge_field_holder');
        }), this.ready.bind(this));
      },
      ready: function() {
        var imgContainer, input, setLifetime,
          _this = this;
        setLifetime = function(e) {
          return _this.lifetime = e.detail;
        };
        $.on(window, 'captcha:timeout', setLifetime);
        $.globalEval('window.dispatchEvent(new CustomEvent("captcha:timeout", {detail: RecaptchaState.timeout}))');
        $.off(window, 'captcha:timeout', setLifetime);
        imgContainer = $.el('div', {
          className: 'captcha-img',
          title: 'Reload reCAPTCHA',
          innerHTML: '<img>'
        });
        input = $.el('input', {
          className: 'captcha-input field',
          title: 'Verification',
          autocomplete: 'off',
          spellcheck: false
        });
        this.nodes = {
          challenge: $.id('recaptcha_challenge_field_holder'),
          img: imgContainer.firstChild,
          input: input
        };
        new MutationObserver(this.load.bind(this)).observe(this.nodes.challenge, {
          childList: true
        });
        $.on(imgContainer, 'click', this.reload.bind(this));
        $.on(input, 'keydown', this.keydown.bind(this));
        $.get('captchas', [], function(_arg) {
          var captchas;
          captchas = _arg.captchas;
          return _this.sync(captchas);
        });
        $.sync('captchas', this.sync);
        this.reload();
        $.on(input, 'blur', QR.focusout);
        $.on(input, 'focus', QR.focusin);
        $.addClass(QR.nodes.el, 'has-captcha');
        return $.after(QR.nodes.com.parentNode, [imgContainer, input]);
      },
      sync: function(captchas) {
        QR.captcha.captchas = captchas;
        return QR.captcha.count();
      },
      getOne: function() {
        var captcha, challenge, response;
        this.clear();
        if (captcha = this.captchas.shift()) {
          challenge = captcha.challenge, response = captcha.response;
          this.count();
          $.set('captchas', this.captchas);
        } else {
          challenge = this.nodes.img.alt;
          if (response = this.nodes.input.value) {
            this.reload();
          }
        }
        if (response) {
          response = response.trim();
          if (!/\s/.test(response)) {
            response = "" + response + " " + response;
          }
        }
        return {
          challenge: challenge,
          response: response
        };
      },
      save: function() {
        var response;
        if (!(response = this.nodes.input.value.trim())) {
          return;
        }
        this.captchas.push({
          challenge: this.nodes.img.alt,
          response: response,
          timeout: this.timeout
        });
        this.count();
        this.reload();
        return $.set('captchas', this.captchas);
      },
      clear: function() {
        var captcha, i, now, _i, _len, _ref;
        now = Date.now();
        _ref = this.captchas;
        for (i = _i = 0, _len = _ref.length; _i < _len; i = ++_i) {
          captcha = _ref[i];
          if (captcha.timeout > now) {
            break;
          }
        }
        if (!i) {
          return;
        }
        this.captchas = this.captchas.slice(i);
        this.count();
        return $.set('captchas', this.captchas);
      },
      load: function() {
        var challenge;
        if (!this.nodes.challenge.firstChild) {
          return;
        }
        this.timeout = Date.now() + this.lifetime * $.SECOND - $.MINUTE;
        challenge = this.nodes.challenge.firstChild.value;
        this.nodes.img.alt = challenge;
        this.nodes.img.src = "//www.google.com/recaptcha/api/image?c=" + challenge;
        this.nodes.input.value = null;
        return this.clear();
      },
      count: function() {
        var count;
        count = this.captchas.length;
        this.nodes.input.placeholder = (function() {
          switch (count) {
            case 0:
              return 'Verification (Shift + Enter to cache)';
            case 1:
              return 'Verification (1 cached captcha)';
            default:
              return "Verification (" + count + " cached captchas)";
          }
        })();
        return this.nodes.input.alt = count;
      },
      reload: function(focus) {
        $.globalEval('Recaptcha.reload("t")');
        if (focus) {
          return this.nodes.input.focus();
        }
      },
      keydown: function(e) {
        if (e.keyCode === 8 && !this.nodes.input.value) {
          this.reload();
        } else if (e.keyCode === 13 && e.shiftKey) {
          this.save();
        } else {
          return;
        }
        return e.preventDefault();
      }
    },
    generatePostableThreadsList: function() {
      var list, options, thread, val;
      if (!QR.nodes) {
        return;
      }
      list = QR.nodes.thread;
      options = [list.firstChild];
      for (thread in g.BOARD.threads) {
        options.push($.el('option', {
          value: thread,
          textContent: "Thread No." + thread
        }));
      }
      val = list.value;
      $.rmAll(list);
      $.add(list, options);
      list.value = val;
      if (!list.value) {
        return;
      }
      return list.value = g.VIEW === 'thread' ? g.THREADID : 'new';
    },
    dialog: function() {
      var dialog, elm, event, flagSelector, mimeTypes, name, node, nodes, save, _i, _j, _len, _len1, _ref, _ref1;
      dialog = UI.dialog('qr', 'top:0;right:0;', "<div><input type=\"checkbox\" id=\"autohide\" title=\"Auto-hide\"><select data-name=\"thread\" title=\"Create a new thread / Reply\"><option value=\"new\">New thread</option></select><span class=\"move\"></span><a href=\"javascript:;\" class=\"close\" title=\"Close\">×</a></div><form><div class=\"persona\"><input type=\"button\" id=\"dump-button\" title=\"Dump list\" value=\"+\"><input data-name=\"name\"  list=\"list-name\"  placeholder=\"Name\"    class=\"field\" size=\"1\"><input data-name=\"email\" list=\"list-email\" placeholder=\"E-mail\"  class=\"field\" size=\"1\"><input data-name=\"sub\"   list=\"list-sub\"   placeholder=\"Subject\" class=\"field\" size=\"1\"></div><div id=\"dump-list-container\"><div id=\"dump-list\"></div><a id=\"add-post\" href=\"javascript:;\" title=\"Add a post\">+</a></div><div class=\"textarea\"><textarea data-name=\"com\" placeholder=\"Comment\" class=\"field\"></textarea><span id=\"char-count\"></span></div><div id=\"file-n-submit-container\"><input type=\"file\" multiple><div id=\"file-n-submit\"><input type=\"submit\"><input type=\"button\" id=\"qr-file-button\" value=\"Choose files\"><span id=\"qr-no-file\">No selected file</span><input id=\"qr-filename\" data-name=\"filename\" spellcheck=\"false\"><span id=\"qr-filesize\"></span><a id=\"qr-filerm\" href=\"javascript:;\" title=\"Remove file\">×</a><input type=\"checkbox\" id=\"qr-file-spoiler\" title=\"Spoiler image\"></div></div></form><datalist id=\"list-name\"></datalist><datalist id=\"list-email\"></datalist><datalist id=\"list-sub\"></datalist>");
      QR.nodes = nodes = {
        el: dialog,
        move: $('.move', dialog),
        autohide: $('#autohide', dialog),
        thread: $('select', dialog),
        close: $('.close', dialog),
        form: $('form', dialog),
        dumpButton: $('#dump-button', dialog),
        name: $('[data-name=name]', dialog),
        email: $('[data-name=email]', dialog),
        sub: $('[data-name=sub]', dialog),
        com: $('[data-name=com]', dialog),
        dumpList: $('#dump-list', dialog),
        addPost: $('#add-post', dialog),
        charCount: $('#char-count', dialog),
        fileSubmit: $('#file-n-submit', dialog),
        fileButton: $('#qr-file-button', dialog),
        filename: $('#qr-filename', dialog),
        filesize: $('#qr-filesize', dialog),
        fileRM: $('#qr-filerm', dialog),
        spoiler: $('#qr-file-spoiler', dialog),
        status: $('[type=submit]', dialog),
        fileInput: $('[type=file]', dialog)
      };
      if (Conf['Tab to Choose Files First']) {
        $.add(nodes.fileSubmit, nodes.status);
      }
      mimeTypes = $('ul.rules > li').textContent.trim().match(/: (.+)/)[1].toLowerCase().replace(/\w+/g, function(type) {
        switch (type) {
          case 'jpg':
            return 'image/jpeg';
          case 'pdf':
            return 'application/pdf';
          case 'swf':
            return 'application/x-shockwave-flash';
          default:
            return "image/" + type;
        }
      });
      QR.mimeTypes = mimeTypes.split(', ');
      QR.mimeTypes.push('');
      nodes.fileInput.max = $('input[name=MAX_FILE_SIZE]').value;
      QR.spoiler = !!$('input[name=spoiler]');
      nodes.spoiler.hidden = !QR.spoiler;
      if (g.BOARD.ID === 'f') {
        nodes.flashTag = $.el('select', {
          name: 'filetag',
          innerHTML: "<option value=0>Hentai</option>\n<option value=6>Porn</option>\n<option value=1>Japanese</option>\n<option value=2>Anime</option>\n<option value=3>Game</option>\n<option value=5>Loop</option>\n<option value=4 selected>Other</option>"
        });
        nodes.flashTag.dataset["default"] = '4';
        $.add(nodes.form, nodes.flashTag);
      }
      if (flagSelector = $('.flagSelector')) {
        nodes.flag = flagSelector.cloneNode(true);
        nodes.flag.dataset.name = 'flag';
        nodes.flag.dataset["default"] = '0';
        $.add(nodes.form, nodes.flag);
      }
      _ref = $$('*', QR.nodes.el);
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        elm = _ref[_i];
        $.on(elm, 'blur', QR.focusout);
        $.on(elm, 'focus', QR.focusin);
      }
      $.on(dialog, 'focusin', QR.focusin);
      $.on(dialog, 'focusout', QR.focusout);
      $.on(nodes.fileButton, 'click', QR.openFileInput);
      $.on(nodes.autohide, 'change', QR.toggleHide);
      $.on(nodes.close, 'click', QR.close);
      $.on(nodes.dumpButton, 'click', function() {
        return nodes.el.classList.toggle('dump');
      });
      $.on(nodes.addPost, 'click', function() {
        return new QR.post(true);
      });
      $.on(nodes.form, 'submit', QR.submit);
      $.on(nodes.fileRM, 'click', function() {
        return QR.selected.rmFile();
      });
      $.on(nodes.spoiler, 'change', function() {
        return QR.selected.nodes.spoiler.click();
      });
      $.on(nodes.fileInput, 'change', QR.handleFiles);
      save = function() {
        return QR.selected.save(this);
      };
      _ref1 = ['thread', 'name', 'email', 'sub', 'com', 'filename', 'flag'];
      for (_j = 0, _len1 = _ref1.length; _j < _len1; _j++) {
        name = _ref1[_j];
        if (!(node = nodes[name])) {
          continue;
        }
        event = node.nodeName === 'SELECT' ? 'change' : 'input';
        $.on(nodes[name], event, save);
      }
      if (Conf['Remember QR Size']) {
        $.get('QR Size', '', function(item) {
          return nodes.com.style.cssText = item['QR Size'];
        });
        $.on(nodes.com, 'mouseup', function(e) {
          if (e.button !== 0) {
            return;
          }
          return $.set('QR Size', this.style.cssText);
        });
      }
      QR.generatePostableThreadsList();
      QR.persona.init();
      new QR.post(true);
      QR.status();
      QR.cooldown.init();
      QR.captcha.init();
      $.add(d.body, dialog);
      return $.event('QRDialogCreation', null, dialog);
    },
    preSubmitHooks: [],
    submit: function(e) {
      var challenge, err, extra, filetag, formData, hook, options, post, response, textOnly, thread, threadID, _i, _len, _ref, _ref1;
      if (e != null) {
        e.preventDefault();
      }
      if (QR.req) {
        QR.abort();
        return;
      }
      if (QR.cooldown.seconds) {
        QR.cooldown.auto = !QR.cooldown.auto;
        QR.status();
        return;
      }
      post = QR.posts[0];
      post.forceSave();
      if (g.BOARD.ID === 'f') {
        filetag = QR.nodes.flashTag.value;
      }
      threadID = post.thread;
      thread = g.BOARD.threads[threadID];
      if (threadID === 'new') {
        threadID = null;
        if (g.BOARD.ID === 'vg' && !post.sub) {
          err = 'New threads require a subject.';
        } else if (!(post.file || (textOnly = !!$('input[name=textonly]', $.id('postForm'))))) {
          err = 'No file selected.';
        }
      } else if (g.BOARD.threads[threadID].isClosed) {
        err = 'You can\'t reply to this thread anymore.';
      } else if (!(post.com || post.file)) {
        err = 'No file selected.';
      } else if (post.file && thread.fileLimit) {
        err = 'Max limit of image replies has been reached.';
      } else {
        _ref = QR.preSubmitHooks;
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
          hook = _ref[_i];
          if (err = hook(post, thread)) {
            break;
          }
        }
      }
      if (QR.captcha.isEnabled && !err) {
        _ref1 = QR.captcha.getOne(), challenge = _ref1.challenge, response = _ref1.response;
        if (!response) {
          err = 'No valid captcha.';
        }
      }
      QR.cleanNotifications();
      if (err) {
        QR.cooldown.auto = false;
        QR.status();
        QR.error(err);
        return;
      }
      QR.cooldown.auto = QR.posts.length > 1;
      if (Conf['Auto-Hide QR'] && !QR.cooldown.auto) {
        QR.hide();
      }
      if (!QR.cooldown.auto && $.x('ancestor::div[@id="qr"]', d.activeElement)) {
        d.activeElement.blur();
      }
      post.lock();
      formData = {
        resto: threadID,
        name: post.name,
        email: post.email,
        sub: post.sub,
        com: post.com,
        upfile: post.file,
        filetag: filetag,
        spoiler: post.spoiler,
        flag: post.flag,
        textonly: textOnly,
        mode: 'regist',
        pwd: QR.persona.pwd,
        recaptcha_challenge_field: challenge,
        recaptcha_response_field: response
      };
      options = {
        responseType: 'document',
        withCredentials: true,
        onload: QR.response,
        onerror: function() {
          delete QR.req;
          post.unlock();
          QR.cooldown.auto = false;
          QR.status();
          return QR.error($.el('span', {
            innerHTML: "Connection error. You may have been <a href=//www.4chan.org/banned target=_blank>banned</a>.\n[<a href=\"https://github.com/MayhemYDG/4chan-x/wiki/FAQ#what-does-connection-error-you-may-have-been-banned-mean\" target=_blank>FAQ</a>]"
          }));
        }
      };
      extra = {
        form: $.formData(formData),
        upCallbacks: {
          onload: function() {
            QR.req.isUploadFinished = true;
            QR.req.uploadEndTime = Date.now();
            QR.req.progress = '...';
            return QR.status();
          },
          onprogress: function(e) {
            QR.req.progress = "" + (Math.round(e.loaded / e.total * 100)) + "%";
            return QR.status();
          }
        }
      };
      QR.req = $.ajax($.id('postForm').parentNode.action, options, extra);
      QR.req.uploadStartTime = Date.now();
      QR.req.progress = '...';
      return QR.status();
    },
    response: function() {
      var URL, ban, board, captchasCount, err, h1, isReply, m, notif, post, postID, postsCount, req, resDoc, threadID, _, _ref, _ref1;
      req = QR.req;
      delete QR.req;
      post = QR.posts[0];
      post.unlock();
      resDoc = req.response;
      if (ban = $('.banType', resDoc)) {
        board = $('.board', resDoc).innerHTML;
        err = $.el('span', {
          innerHTML: ban.textContent.toLowerCase() === 'banned' ? "You are banned on " + board + "! ;_;<br>\nClick <a href=//www.4chan.org/banned target=_blank>here</a> to see the reason." : "You were issued a warning on " + board + " as " + ($('.nameBlock', resDoc).innerHTML) + ".<br>\nReason: " + ($('.reason', resDoc).innerHTML)
        });
      } else if (err = resDoc.getElementById('errmsg')) {
        if ((_ref = $('a', err)) != null) {
          _ref.target = '_blank';
        }
      } else if (resDoc.title !== 'Post successful!') {
        err = 'Connection error with sys.4chan.org.';
      } else if (req.status !== 200) {
        err = "Error " + req.statusText + " (" + req.status + ")";
      }
      if (err) {
        if (/captcha|verification/i.test(err.textContent) || err === 'Connection error with sys.4chan.org.') {
          if (/mistyped/i.test(err.textContent)) {
            err = 'You seem to have mistyped the CAPTCHA.';
          }
          QR.cooldown.auto = QR.captcha.isEnabled ? !!QR.captcha.captchas.length : err === 'Connection error with sys.4chan.org.' ? true : false;
          QR.cooldown.set({
            delay: 2
          });
        } else if (err.textContent && (m = err.textContent.match(/wait\s(\d+)\ssecond/i))) {
          QR.cooldown.auto = QR.captcha.isEnabled ? !!QR.captcha.captchas.length : true;
          QR.cooldown.set({
            delay: m[1]
          });
        } else {
          QR.cooldown.auto = false;
        }
        QR.status();
        QR.error(err);
        return;
      }
      h1 = $('h1', resDoc);
      QR.cleanNotifications();
      QR.notifications.push(new Notice('success', h1.textContent, 5));
      QR.persona.set(post);
      _ref1 = h1.nextSibling.textContent.match(/thread:(\d+),no:(\d+)/), _ = _ref1[0], threadID = _ref1[1], postID = _ref1[2];
      postID = +postID;
      threadID = +threadID || postID;
      isReply = threadID !== postID;
      QR.db.set({
        boardID: g.BOARD.ID,
        threadID: threadID,
        postID: postID,
        val: true
      });
      $.event('QRPostSuccessful', {
        board: g.BOARD,
        threadID: threadID,
        postID: postID
      });
      $.event('QRPostSuccessful_', {
        threadID: threadID,
        postID: postID
      });
      postsCount = QR.posts.length - 1;
      QR.cooldown.auto = postsCount && isReply;
      if (QR.cooldown.auto && QR.captcha.isEnabled && (captchasCount = QR.captcha.captchas.length) < 3 && captchasCount < postsCount) {
        notif = new Notification('Quick reply warning', {
          body: "You are running low on cached captchas. Cache count: " + captchasCount + ".",
          icon: Favicon.logo
        });
        notif.onclick = function() {
          QR.open();
          QR.captcha.nodes.input.focus();
          return window.focus();
        };
        notif.onshow = function() {
          return setTimeout(function() {
            return notif.close();
          }, 7 * $.SECOND);
        };
      }
      if (!(Conf['Persistent QR'] || QR.cooldown.auto)) {
        QR.close();
      } else {
        post.rm();
      }
      QR.cooldown.set({
        req: req,
        post: post,
        isReply: isReply,
        threadID: threadID
      });
      URL = threadID === postID ? "/" + g.BOARD + "/res/" + threadID : g.VIEW === 'index' && !QR.cooldown.auto && Conf['Open Post in New Tab'] ? "/" + g.BOARD + "/res/" + threadID + "#p" + postID : void 0;
      if (URL) {
        if (Conf['Open Post in New Tab']) {
          $.open(URL);
        } else {
          window.location = URL;
        }
      }
      return QR.status();
    },
    abort: function() {
      if (QR.req && !QR.req.isUploadFinished) {
        QR.req.abort();
        delete QR.req;
        QR.posts[0].unlock();
        QR.cooldown.auto = false;
        QR.notifications.push(new Notice('info', 'QR upload aborted.', 5));
      }
      return QR.status();
    }
  };

  AutoGIF = {
    init: function() {
      var _ref;
      if (g.VIEW === 'catalog' || !Conf['Auto-GIF'] || ((_ref = g.BOARD.ID) === 'gif' || _ref === 'wsg')) {
        return;
      }
      return Post.callbacks.push({
        name: 'Auto-GIF',
        cb: this.node
      });
    },
    node: function() {
      var URL, gif, style, thumb, _ref, _ref1;
      if (this.isClone || this.isHidden || this.thread.isHidden || !((_ref = this.file) != null ? _ref.isImage : void 0)) {
        return;
      }
      _ref1 = this.file, thumb = _ref1.thumb, URL = _ref1.URL;
      if (!(/gif$/.test(URL) && !/spoiler/.test(thumb.src))) {
        return;
      }
      if (this.file.isSpoiler) {
        style = thumb.style;
        style.maxHeight = style.maxWidth = this.isReply ? '125px' : '250px';
      }
      gif = $.el('img');
      $.on(gif, 'load', function() {
        return thumb.src = URL;
      });
      return gif.src = URL;
    }
  };

  ImageExpand = {
    init: function() {
      if (g.VIEW === 'catalog' || !Conf['Image Expansion']) {
        return;
      }
      this.EAI = $.el('a', {
        className: 'expand-all-shortcut fa fa-resize-full',
        title: 'Expand All Images',
        href: 'javascript:;'
      });
      $.on(this.EAI, 'click', ImageExpand.cb.toggleAll);
      Header.addShortcut(this.EAI, 3);
      return Post.callbacks.push({
        name: 'Image Expansion',
        cb: this.node
      });
    },
    node: function() {
      var thumb, _ref;
      if (!((_ref = this.file) != null ? _ref.isImage : void 0)) {
        return;
      }
      thumb = this.file.thumb;
      $.on(thumb.parentNode, 'click', ImageExpand.cb.toggle);
      if (this.isClone && $.hasClass(thumb, 'expanding')) {
        ImageExpand.contract(this);
        ImageExpand.expand(this);
        return;
      }
      if (ImageExpand.on && !this.isHidden && (Conf['Expand spoilers'] || !this.file.isSpoiler)) {
        return ImageExpand.expand(this);
      }
    },
    cb: {
      toggle: function(e) {
        if (e.shiftKey || e.altKey || e.ctrlKey || e.metaKey || e.button !== 0) {
          return;
        }
        e.preventDefault();
        return ImageExpand.toggle(Get.postFromNode(this));
      },
      toggleAll: function() {
        var ID, file, func, post, _i, _len, _ref, _ref1;
        $.event('CloseMenu');
        if (ImageExpand.on = $.hasClass(ImageExpand.EAI, 'expand-all-shortcut')) {
          ImageExpand.EAI.className = 'contract-all-shortcut fa fa-resize-small';
          ImageExpand.EAI.title = 'Contract All Images';
          func = ImageExpand.expand;
        } else {
          ImageExpand.EAI.className = 'expand-all-shortcut fa fa-resize-full';
          ImageExpand.EAI.title = 'Expand All Images';
          func = ImageExpand.contract;
        }
        _ref = g.posts;
        for (ID in _ref) {
          post = _ref[ID];
          _ref1 = [post].concat(post.clones);
          for (_i = 0, _len = _ref1.length; _i < _len; _i++) {
            post = _ref1[_i];
            file = post.file;
            if (!(file && file.isImage && doc.contains(post.nodes.root))) {
              continue;
            }
            if (ImageExpand.on && (!Conf['Expand spoilers'] && file.isSpoiler || Conf['Expand from here'] && Header.getTopOf(file.thumb) < 0)) {
              continue;
            }
            $.queueTask(func, post);
          }
        }
      },
      setFitness: function() {
        return (this.checked ? $.addClass : $.rmClass)(doc, this.name.toLowerCase().replace(/\s+/g, '-'));
      }
    },
    toggle: function(post) {
      var thumb, top, x, y;
      thumb = post.file.thumb;
      if (!(post.file.isExpanded || $.hasClass(thumb, 'expanding'))) {
        ImageExpand.expand(post);
        return;
      }
      top = Header.getTopOf(post.nodes.root);
      if (top < 0) {
        y = top;
      }
      if (post.nodes.root.getBoundingClientRect().left < 0) {
        x = -window.scrollX;
      }
      if (x || y) {
        window.scrollBy(x, y);
      }
      return ImageExpand.contract(post);
    },
    contract: function(post) {
      $.rmClass(post.nodes.root, 'expanded-image');
      $.rmClass(post.file.thumb, 'expanding');
      return post.file.isExpanded = false;
    },
    expand: function(post, src) {
      var img, thumb;
      thumb = post.file.thumb;
      if (post.isHidden || post.file.isExpanded || $.hasClass(thumb, 'expanding')) {
        return;
      }
      $.addClass(thumb, 'expanding');
      if (post.file.fullImage) {
        $.asap((function() {
          return post.file.fullImage.naturalHeight;
        }), function() {
          return ImageExpand.completeExpand(post);
        });
        return;
      }
      post.file.fullImage = img = $.el('img', {
        className: 'full-image',
        src: src || post.file.URL
      });
      $.on(img, 'error', ImageExpand.error);
      $.asap((function() {
        return post.file.fullImage.naturalHeight;
      }), function() {
        return ImageExpand.completeExpand(post);
      });
      return $.after(thumb, img);
    },
    completeExpand: function(post) {
      var bottom, thumb;
      thumb = post.file.thumb;
      if (!$.hasClass(thumb, 'expanding')) {
        return;
      }
      post.file.isExpanded = true;
      if (!post.nodes.root.parentNode) {
        $.addClass(post.nodes.root, 'expanded-image');
        $.rmClass(post.file.thumb, 'expanding');
        return;
      }
      bottom = post.nodes.root.getBoundingClientRect().bottom;
      return $.queueTask(function() {
        $.addClass(post.nodes.root, 'expanded-image');
        $.rmClass(post.file.thumb, 'expanding');
        if (!(bottom <= 0)) {
          return;
        }
        return window.scrollBy(0, post.nodes.root.getBoundingClientRect().bottom - bottom);
      });
    },
    error: function() {
      var URL, post, src, timeoutID;
      post = Get.postFromNode(this);
      $.rm(this);
      delete post.file.fullImage;
      if (!($.hasClass(post.file.thumb, 'expanding') || $.hasClass(post.nodes.root, 'expanded-image'))) {
        return;
      }
      ImageExpand.contract(post);
      src = this.src.split('/');
      if (src[2] === 'images.4chan.org') {
        URL = Redirect.to('file', {
          boardID: src[3],
          filename: src[5].replace(/\?.+$/, '')
        });
        if (URL) {
          setTimeout(ImageExpand.expand, 10000, post, URL);
          return;
        }
        if (g.DEAD || post.isDead || post.file.isDead) {
          return;
        }
      }
      timeoutID = setTimeout(ImageExpand.expand, 10000, post);
      return $.ajax("//api.4chan.org/" + post.board + "/res/" + post.thread + ".json", {
        onload: function() {
          var postObj, _i, _len, _ref;
          if (this.status !== 200) {
            return;
          }
          _ref = JSON.parse(this.response).posts;
          for (_i = 0, _len = _ref.length; _i < _len; _i++) {
            postObj = _ref[_i];
            if (postObj.no === post.ID) {
              break;
            }
          }
          if (postObj.no !== post.ID) {
            clearTimeout(timeoutID);
            return post.kill();
          } else if (postObj.filedeleted) {
            clearTimeout(timeoutID);
            return post.kill(true);
          }
        }
      });
    },
    menu: {
      init: function() {
        var conf, createSubEntry, el, name, subEntries, _ref;
        if (g.VIEW === 'catalog' || !Conf['Image Expansion']) {
          return;
        }
        el = $.el('span', {
          textContent: 'Image Expansion',
          className: 'image-expansion-link'
        });
        createSubEntry = ImageExpand.menu.createSubEntry;
        subEntries = [];
        _ref = Config.imageExpansion;
        for (name in _ref) {
          conf = _ref[name];
          subEntries.push(createSubEntry(name, conf[1]));
        }
        return $.event('AddMenuEntry', {
          type: 'header',
          el: el,
          order: 80,
          subEntries: subEntries
        });
      },
      createSubEntry: function(name, desc) {
        var input, label;
        label = $.el('label', {
          innerHTML: "<input type=checkbox name='" + name + "'> " + name,
          title: desc
        });
        input = label.firstElementChild;
        if (name === 'Fit width' || name === 'Fit height') {
          $.on(input, 'change', ImageExpand.cb.setFitness);
        }
        input.checked = Conf[name];
        $.event('change', null, input);
        $.on(input, 'change', $.cb.checked);
        return {
          el: label
        };
      }
    }
  };

  ImageHover = {
    init: function() {
      if (g.VIEW === 'catalog' || !Conf['Image Hover']) {
        return;
      }
      return Post.callbacks.push({
        name: 'Image Hover',
        cb: this.node
      });
    },
    node: function() {
      var _ref;
      if (!((_ref = this.file) != null ? _ref.isImage : void 0)) {
        return;
      }
      return $.on(this.file.thumb, 'mouseover', ImageHover.mouseover);
    },
    mouseover: function(e) {
      var el, post;
      post = Get.postFromNode(this);
      el = $.el('img', {
        id: 'ihover',
        src: post.file.URL
      });
      el.dataset.fullID = post.fullID;
      $.add(d.body, el);
      UI.hover({
        root: this,
        el: el,
        latestEvent: e,
        endEvents: 'mouseout click',
        asapTest: function() {
          return el.naturalHeight;
        }
      });
      return $.on(el, 'error', ImageHover.error);
    },
    error: function() {
      var URL, post, src, timeoutID,
        _this = this;
      if (!doc.contains(this)) {
        return;
      }
      post = g.posts[this.dataset.fullID];
      src = this.src.split('/');
      if (src[2] === 'images.4chan.org') {
        URL = Redirect.to('file', {
          boardID: src[3],
          filename: src[5].replace(/\?.+$/, '')
        });
        if (URL) {
          this.src = URL;
          return;
        }
        if (g.DEAD || post.isDead || post.file.isDead) {
          return;
        }
      }
      timeoutID = setTimeout((function() {
        return _this.src = post.file.URL + '?' + Date.now();
      }), 3000);
      return $.ajax("//api.4chan.org/" + post.board + "/res/" + post.thread + ".json", {
        onload: function() {
          var postObj, _i, _len, _ref;
          if (this.status !== 200) {
            return;
          }
          _ref = JSON.parse(this.response).posts;
          for (_i = 0, _len = _ref.length; _i < _len; _i++) {
            postObj = _ref[_i];
            if (postObj.no === post.ID) {
              break;
            }
          }
          if (postObj.no !== post.ID) {
            clearTimeout(timeoutID);
            return post.kill();
          } else if (postObj.filedeleted) {
            clearTimeout(timeoutID);
            return post.kill(true);
          }
        }
      });
    }
  };

  RevealSpoilers = {
    init: function() {
      if (g.VIEW === 'catalog' || !Conf['Reveal Spoilers']) {
        return;
      }
      return Post.callbacks.push({
        name: 'Reveal Spoilers',
        cb: this.node
      });
    },
    node: function() {
      var thumb, _ref;
      if (this.isClone || !((_ref = this.file) != null ? _ref.isSpoiler : void 0)) {
        return;
      }
      thumb = this.file.thumb;
      thumb.removeAttribute('style');
      return thumb.src = this.file.thumbURL;
    }
  };

  Sauce = {
    init: function() {
      var err, link, links, _i, _len, _ref;
      if (g.VIEW === 'catalog' || !Conf['Sauce']) {
        return;
      }
      links = [];
      _ref = Conf['sauces'].split('\n');
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        link = _ref[_i];
        try {
          if (link[0] !== '#') {
            links.push(this.createSauceLink(link.trim()));
          }
        } catch (_error) {
          err = _error;
        }
      }
      if (!links.length) {
        return;
      }
      this.links = links;
      this.link = $.el('a', {
        target: '_blank'
      });
      return Post.callbacks.push({
        name: 'Sauce',
        cb: this.node
      });
    },
    createSauceLink: function(link) {
      var m, text;
      link = link.replace(/%(T?URL|MD5|board)/g, function(parameter) {
        switch (parameter) {
          case '%TURL':
            return "' + encodeURIComponent(post.file.thumbURL) + '";
          case '%URL':
            return "' + encodeURIComponent(post.file.URL) + '";
          case '%MD5':
            return "' + encodeURIComponent(post.file.MD5) + '";
          case '%board':
            return "' + encodeURIComponent(post.board) + '";
          default:
            return parameter;
        }
      });
      text = (m = link.match(/;text:(.+)$/)) ? m[1] : link.match(/(\w+)\.\w+\//)[1];
      link = link.replace(/;text:.+$/, '');
      return Function('post', 'a', "a.href = '" + link + "';\na.textContent = '" + text + "';\nreturn a;");
    },
    node: function() {
      var link, nodes, _i, _len, _ref;
      if (this.isClone || !this.file) {
        return;
      }
      nodes = [];
      _ref = Sauce.links;
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        link = _ref[_i];
        nodes.push($.tn('\u00A0'), link(this, Sauce.link.cloneNode(true)));
      }
      return $.add(this.file.info, nodes);
    }
  };

  Linkify = {
    init: function() {
      if (g.VIEW === 'catalog' || !Conf['Linkify']) {
        return;
      }
      this.catchAll = /\b((https?|ftps?|about|bitcoin|git|irc[s6]?):(\/{1,3}|[a-z0-9%])|www\d{0,3}[.]|[a-z0-9.\-]+[.][a-z]{2,4}\/|magnet:\?(dn|x[lts]|as|kt|mt|tr)=)([^\s()<>]+|\(([^\s()<>]+|(\([^\s()<>]+\)))*\))+(\(([^\s()<>]+|(\([^\s()<>]+\)))*\)|[^\s`!()\[\]{};:'".,<>?«»“”‘’])/g;
      return Post.callbacks.push({
        name: 'Linkify',
        cb: this.node
      });
    },
    node: function() {
      var anchor, boundaries, link, links, parent, range, walker, _i, _len;
      if (this.isClone || !(links = this.info.comment.match(Linkify.catchAll))) {
        return;
      }
      walker = d.createTreeWalker(this.nodes.comment, 1 | 4, {
        acceptNode: function(node) {
          var _ref;
          if ((_ref = node.nodeName) === '#text' || _ref === 'BR') {
            return 1;
          } else {
            return 3;
          }
        }
      });
      range = d.createRange();
      for (_i = 0, _len = links.length; _i < _len; _i++) {
        link = links[_i];
        boundaries = Linkify.find(link, walker);
        if (!boundaries) {
          continue;
        }
        anchor = Linkify.createLink(link);
        if (Linkify.surround(anchor, range, boundaries)) {
          if ((parent = anchor.parentNode).href === anchor.href) {
            $.replace(parent, anchor);
          }
          if (Conf['Clean Links']) {
            Linkify.cleanLink(anchor, link);
          }
          walker.currentNode = anchor.lastChild;
        } else {
          walker.previousNode();
        }
      }
      return range.detach();
    },
    find: function(link, walker) {
      var endNode, index, node, startNode, text;
      text = '';
      while (node = walker.nextNode()) {
        if (node.nodeName === 'BR') {
          return Linkify.find(link, walker);
        }
        text += node.data;
        if (text.indexOf(link) > -1) {
          break;
        }
      }
      if (!node) {
        return;
      }
      startNode = endNode = node;
      text = node.data;
      while (!((index = text.indexOf(link)) > -1)) {
        startNode = walker.previousNode();
        text = "" + startNode.data + text;
      }
      return {
        startNode: startNode,
        endNode: endNode,
        startOffset: index,
        endOffset: endNode.length - (text.length - index - link.length)
      };
    },
    createLink: function(link) {
      if (!/^[a-z][\w-]+:/.test(link)) {
        link = "http://" + link;
      }
      return $.el('a', {
        href: link,
        className: 'linkified',
        target: '_blank'
      });
    },
    surround: function(anchor, range, boundaries) {
      var endNode, endOffset, startNode, startOffset;
      startOffset = boundaries.startOffset, endOffset = boundaries.endOffset, startNode = boundaries.startNode, endNode = boundaries.endNode;
      range.setStart(startNode, startOffset);
      range.setEnd(endNode, endOffset);
      try {
        range.surroundContents(anchor);
        return true;
      } catch (_error) {
        if (boundaries.areRelocated) {
          return false;
        }
        Linkify.relocate(boundaries);
        return Linkify.surround(anchor, range, boundaries);
      }
    },
    relocate: function(boundaries) {
      var parent, parentNode;
      boundaries.areRelocated = true;
      if (boundaries.startOffset === 0) {
        parentNode = boundaries.startNode;
        while (!parentNode.previousSibling) {
          parentNode = parentNode.parentNode;
        }
        parent = parentNode.parentNode;
        boundaries.startNode = parent;
        boundaries.startOffset = __slice.call(parent.childNodes).indexOf(parentNode);
      }
      if (boundaries.endOffset === boundaries.endNode.length) {
        parentNode = boundaries.endNode;
        while (!parentNode.nextSibling) {
          parentNode = parentNode.parentNode;
        }
        parent = parentNode.parentNode;
        boundaries.endNode = parent;
        return boundaries.endOffset = __slice.call(parent.childNodes).indexOf(parentNode) + 1;
      }
    },
    cleanLink: function(anchor, link) {
      var length, node, _i, _len, _ref;
      length = link.length;
      _ref = $$('s, .prettyprint', anchor);
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        node = _ref[_i];
        if (length > node.textContent.length) {
          $.replace(node, __slice.call(node.childNodes));
        }
      }
    }
  };

  ArchiveLink = {
    init: function() {
      var div, entry, type, _i, _len, _ref;
      if (g.VIEW === 'catalog' || !Conf['Menu'] || !Conf['Archive Link']) {
        return;
      }
      div = $.el('div', {
        textContent: 'Archive'
      });
      entry = {
        type: 'post',
        el: div,
        order: 90,
        open: function(_arg) {
          var ID, board, thread;
          ID = _arg.ID, thread = _arg.thread, board = _arg.board;
          return !!Redirect.to('thread', {
            postID: ID,
            threadID: thread.ID,
            boardID: board.ID
          });
        },
        subEntries: []
      };
      _ref = [['Post', 'post'], ['Name', 'name'], ['Tripcode', 'tripcode'], ['E-mail', 'email'], ['Subject', 'subject'], ['Filename', 'filename'], ['Image MD5', 'MD5']];
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        type = _ref[_i];
        entry.subEntries.push(this.createSubEntry(type[0], type[1]));
      }
      return $.event('AddMenuEntry', entry);
    },
    createSubEntry: function(text, type) {
      var el, open;
      el = $.el('a', {
        textContent: text,
        target: '_blank'
      });
      open = type === 'post' ? function(_arg) {
        var ID, board, thread;
        ID = _arg.ID, thread = _arg.thread, board = _arg.board;
        el.href = Redirect.to('thread', {
          postID: ID,
          threadID: thread.ID,
          boardID: board.ID
        });
        return true;
      } : function(post) {
        var value;
        value = Filter[type](post);
        if (!value) {
          return false;
        }
        el.href = Redirect.to('search', {
          boardID: post.board.ID,
          type: type,
          value: value
        });
        return true;
      };
      return {
        el: el,
        open: open
      };
    }
  };

  DeleteLink = {
    init: function() {
      var div, fileEl, fileEntry, postEl, postEntry;
      if (g.VIEW === 'catalog' || !Conf['Menu'] || !Conf['Delete Link']) {
        return;
      }
      div = $.el('div', {
        className: 'delete-link',
        textContent: 'Delete'
      });
      postEl = $.el('a', {
        className: 'delete-post',
        href: 'javascript:;'
      });
      fileEl = $.el('a', {
        className: 'delete-file',
        href: 'javascript:;'
      });
      postEntry = {
        el: postEl,
        open: function() {
          postEl.textContent = 'Post';
          $.on(postEl, 'click', DeleteLink["delete"]);
          return true;
        }
      };
      fileEntry = {
        el: fileEl,
        open: function(_arg) {
          var file;
          file = _arg.file;
          if (!file || file.isDead) {
            return false;
          }
          fileEl.textContent = 'File';
          $.on(fileEl, 'click', DeleteLink["delete"]);
          return true;
        }
      };
      return $.event('AddMenuEntry', {
        type: 'post',
        el: div,
        order: 40,
        open: function(post) {
          var node;
          if (post.isDead) {
            return false;
          }
          DeleteLink.post = post;
          node = div.firstChild;
          node.textContent = 'Delete';
          DeleteLink.cooldown.start(post, node);
          return true;
        },
        subEntries: [postEntry, fileEntry]
      });
    },
    "delete": function() {
      var fileOnly, form, link, post;
      post = DeleteLink.post;
      if (DeleteLink.cooldown.counting === post) {
        return;
      }
      $.off(this, 'click', DeleteLink["delete"]);
      fileOnly = $.hasClass(this, 'delete-file');
      this.textContent = "Deleting " + (fileOnly ? 'file' : 'post') + "...";
      form = {
        mode: 'usrdel',
        onlyimgdel: fileOnly,
        pwd: QR.persona.getPassword()
      };
      form[post.ID] = 'delete';
      link = this;
      return $.ajax($.id('delform').action.replace("/" + g.BOARD + "/", "/" + post.board + "/"), {
        responseType: 'document',
        withCredentials: true,
        onload: function() {
          return DeleteLink.load(link, post, fileOnly, this.response);
        },
        onerror: function() {
          return DeleteLink.error(link);
        }
      }, {
        form: $.formData(form)
      });
    },
    load: function(link, post, fileOnly, resDoc) {
      var msg, s;
      if (resDoc.title === '4chan - Banned') {
        s = 'Banned!';
      } else if (msg = resDoc.getElementById('errmsg')) {
        s = msg.textContent;
        $.on(link, 'click', DeleteLink["delete"]);
      } else {
        if (resDoc.title === 'Updating index...') {
          (post.origin || post).kill(fileOnly);
        }
        s = 'Deleted';
      }
      return link.textContent = s;
    },
    error: function(link) {
      link.textContent = 'Connection error, please retry.';
      return $.on(link, 'click', DeleteLink["delete"]);
    },
    cooldown: {
      start: function(post, node) {
        var length, seconds, _ref;
        if (!((_ref = QR.db) != null ? _ref.get({
          boardID: post.board.ID,
          threadID: post.thread.ID,
          postID: post.ID
        }) : void 0)) {
          delete DeleteLink.cooldown.counting;
          return;
        }
        DeleteLink.cooldown.counting = post;
        length = 60;
        seconds = Math.ceil((length * $.SECOND - (Date.now() - post.info.date)) / $.SECOND);
        return DeleteLink.cooldown.count(post, seconds, length, node);
      },
      count: function(post, seconds, length, node) {
        if (DeleteLink.cooldown.counting !== post) {
          return;
        }
        if (!((0 <= seconds && seconds <= length))) {
          if (DeleteLink.cooldown.counting === post) {
            node.textContent = 'Delete';
            delete DeleteLink.cooldown.counting;
          }
          return;
        }
        setTimeout(DeleteLink.cooldown.count, 1000, post, seconds - 1, length, node);
        return node.textContent = "Delete (" + seconds + ")";
      }
    }
  };

  DownloadLink = {
    init: function() {
      var a;
      if (g.VIEW === 'catalog' || !Conf['Menu'] || !Conf['Download Link']) {
        return;
      }
      a = $.el('a', {
        className: 'download-link',
        textContent: 'Download file'
      });
      return $.event('AddMenuEntry', {
        type: 'post',
        el: a,
        order: 70,
        open: function(_arg) {
          var file;
          file = _arg.file;
          if (!file) {
            return false;
          }
          a.href = file.URL;
          a.download = file.name;
          return true;
        }
      });
    }
  };

  Menu = {
    init: function() {
      if (g.VIEW === 'catalog' || !Conf['Menu']) {
        return;
      }
      this.menu = new UI.Menu('post');
      return Post.callbacks.push({
        name: 'Menu',
        cb: this.node
      });
    },
    node: function() {
      var button;
      if (this.isClone) {
        button = $('.menu-button', this.nodes.info);
        $.on(button, 'click', Menu.toggle);
        return;
      }
      button = Menu.makeButton();
      return $.add(this.nodes.info, [$.tn('\u00A0'), button]);
    },
    makeButton: (function() {
      var a;
      a = null;
      return function() {
        var button;
        a || (a = $.el('a', {
          className: 'menu-button',
          innerHTML: '[<i></i>]',
          href: 'javascript:;'
        }));
        button = a.cloneNode(true);
        $.on(button, 'click', Menu.toggle);
        return button;
      };
    })(),
    toggle: function(e) {
      var post;
      try {
        post = Get.postFromNode(this);
      } catch (_error) {
        post = Get.threadFromNode(this).OP;
      }
      return Menu.menu.toggle(e, this, post);
    }
  };

  ReportLink = {
    init: function() {
      var a;
      if (g.VIEW === 'catalog' || !Conf['Menu'] || !Conf['Report Link']) {
        return;
      }
      a = $.el('a', {
        className: 'report-link',
        href: 'javascript:;',
        textContent: 'Report this post'
      });
      $.on(a, 'click', ReportLink.report);
      return $.event('AddMenuEntry', {
        type: 'post',
        el: a,
        order: 10,
        open: function(post) {
          ReportLink.post = post;
          return !post.isDead;
        }
      });
    },
    report: function() {
      var id, post, set, url;
      post = ReportLink.post;
      url = "//sys.4chan.org/" + post.board + "/imgboard.php?mode=report&no=" + post;
      id = Date.now();
      set = "toolbar=0,scrollbars=0,location=0,status=1,menubar=0,resizable=1,width=685,height=200";
      return window.open(url, id, set);
    }
  };

  Favicon = {
    init: function() {
      return $.ready(function() {
        var href;
        Favicon.el = $('link[rel="shortcut icon"]', d.head);
        Favicon.el.type = 'image/x-icon';
        href = Favicon.el.href;
        Favicon.SFW = /ws\.ico$/.test(href);
        Favicon["default"] = href;
        return Favicon["switch"]();
      });
    },
    "switch": function() {
      switch (Conf['favicon']) {
        case 'ferongr':
          Favicon.unreadDead = 'data:image/gif;base64,R0lGODlhEAAQAPIGAOgLAnMFAL8AAOkMA/+AgP+rqwAAAAAAACH5BAUKAAYALAAAAAAQABAAQANKaLrcDYDBF8YgAQZiswJVp1mDZ4CB+aUmmkYnq4IFphGFGoMwr0MwySSGs62KGZBAIAJZli2gcLhA9V6STTNkjAkCX803LDmVgwkAOw==';
          Favicon.unreadDeadY = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAxUlEQVR42q1TOwrCQBB9s0FRtJI0WoqFtSLYegoP4gVSeJsUHsHSI3iFeIqRXXgwrhlXwYHHhLwPTB7B36abBCV+0pA4DUBQUNZYQptGtW3jtoKyxgoe0yrBCoyZfL/5ioQ3URZOXW9I341l3oo+NXEZiW4CEuIzvPECopED4OaZ3RNmeAm4u+a8Jr5f17VyVoL8fr8qcltzwlyyj2iqcgPOQ9ExkHAITgD75bYBe0A5S4H/P9htuWMF3QXoQpwaKeT+lnsC6JE5I6aq6fEAAAAASUVORK5CYII=';
          Favicon.unreadSFW = 'data:image/gif;base64,R0lGODlhEAAQAPIGAADX8QBwfgC2zADY8nnl8qLp8gAAAAAAACH5BAUKAAYALAAAAAAQABAAQANKaLrcDYDBF8YgAQZiswJVp1mDZ4CB+aUmmkYnq4IFphGFGoMwr0MwySSGs62KGZBAIAJZli2gcLhA9V6STTNkjAkCX803LDmVgwkAOw==';
          Favicon.unreadSFWY = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAxElEQVQ4y2NgoBq4/vE/HJOsBiRQUIfA2AzBqQYqUfn00/9FLz+BaQxDCKqBmX7jExijKEDSDJPHrnnbGQhGV4RmOFwdVkNwhQMheYwQxhaIi7b9Z9A3gWAQm2BUoQOgRhgA8o7j1ozLC4LCyAZcx6kZI5qg4kLKqggDFFWxJySsUQVzlb4pwgAJaTRvokcVNgOqOv8zcHBCsL07DgNg8YsczzA5MxtUL+DMD8g0slxI/H8GQ/P/DJKyeKIRpglXZsIiBwBhP5O+VbI/JgAAAABJRU5ErkJggg==';
          Favicon.unreadNSFW = 'data:image/gif;base64,R0lGODlhEAAQAPIGAFT+ACh5AEncAFX/Acz/su7/5gAAAAAAACH5BAUKAAYALAAAAAAQABAAQANKaLrcDYDBF8YgAQZiswJVp1mDZ4CB+aUmmkYnq4IFphGFGoMwr0MwySSGs62KGZBAIAJZli2gcLhA9V6STTNkjAkCX803LDmVgwkAOw==';
          Favicon.unreadNSFWY = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAx0lEQVQ4y2NgoBYI+cfwH4ZJVgMS0KhEYGyG4FQDkzjzf9P/d/+fgWl0QwiqgSkI/c8IxsgKkDXD5LFq9rwDweiK0A2HqcNqCK5wICSPEcLYAtH+AMN/IXMIBrEJRie6OEgjDAC5x3FqxuUFNiEUA67j1IweTTBxBQ1puAG86jgSEraogskJWSBcwCGF5k30qMJmgMFEhv/MXBAs5oLDAFj8IsczTE7UEeECbhU8+QGZRpaTi2b4L2zF8J9TGk80wjThykzY5AAW/2O1C2mIbgAAAABJRU5ErkJggg==';
          break;
        case 'xat-':
          Favicon.unreadDead = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAxUlEQVR42s1SQQrCMBDsQ8WDIEV6LTT2A4og2Hi0veo7fIAH04e06L1jphIJRtoVLw4MmWZnh2aT6K8Ax82uyFOV6SSJK5Kae74naIZHfhfx5HxaTC8kdeCRhNzms8ayFTaHJuTLzvKpJSE+sVoDhxIoK2qv5vgGDutoz8vfhlJd33w1gDGg5h5r9NArCzA1UNevgPtQQJplmtMeOwI99AYBW73PI8EQqXsvIbjGduAaxwPcQ/oqwF/dUw5r5GfArcLa73gAGxUeHPIycrIAAAAASUVORK5CYII=';
          Favicon.unreadDeadY = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAA80lEQVQ4y2NgGEzgPwx3TZhYVVJRPik7O30uCIPYIDFkNRia/yNhEH9ieszB5ZlxZ0EYxMamhqAhn1KT3gPxB5I1wxT9r6r8B8T/ccnjDAOwhvaO//9nz/n/f85cMBtdHiMEQYG1DOhfkLP/V1T8A2u+eOn//0uXwAYdiIr6ZyQi8ltOWPCLm5vzVuIMuHT5///Ll8EGWEhJ/YcBfn7+lxgGlJSXT4KFNi4vwAA3N/c7DAN6J02uYsATiBqoBrzCmpCwROMHWDRaIBmANRCxGQBLSOCEBcRcUIzXAGQalpRBbB8iXMCAJPEfh9wRKG1HtRwIAJCmKOzjRex1AAAAAElFTkSuQmCC';
          Favicon.unreadSFW = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAzUlEQVQ4y2NgGEzgPwxP7OuqqigtmZSZnj0XhEFskBiyGgzNetMPwzGIH5M/8WBczfKzIAxiY1ND0JCkDZ/eA/EHkjXDFFUe+f8PiP/jkscZBiDcfvr//9lX/v+fA8QgNro8RgiCAiuuetlZkLMrgDaDNF988///pdcQg0BiIDmQGpBaogy4BDTg8htkAz7jNqAcGFWw0MbnBZAakFoMAyZP6K1CDixcgQhiQ9UyEBONH4iNRqwGwBISSQYg07CkjE0OX35gwKEAnxz5AADUHTv3RAHZ7QAAAABJRU5ErkJggg==';
          Favicon.unreadSFWY = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAA/klEQVQ4y2NgGEzgPwxP7OuqqigtmZSZnj0XhEFskBiyGgzNetMPwzGIH5M/8WBczfKzIAxiY1ND0JCkDZ/eA/EHkjXDFFUe+f8PiP/jkscZBiDcfvr//9lX/v+fA8QgNro8RgiCAiuuetlZkLMrgDaDNF988///pdcQg6KmHPgnomL0W1BM7ours9tWogy4BDTg8huIAVLaFv9hgJ+f/yWGAeXAqIKFNi4vwAA3N/c7DAMmT+itQg4s9EBkF5dDNuAV1oSEJRo/wKKRS1ELbgDWQMRmACwhgdhSoXn/GVnZwRivAcg0LCmD2Ly6VgRdwIAk8R+H3BEobUe1HAgA7g5BI+Q2tn8AAAAASUVORK5CYII=';
          Favicon.unreadNSFW = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAzklEQVQ4y2NgGEzgPwxP7JpYVVZSMSk9O2suCIPYIDFkNRia084YwzGIHzMx/mDc8vizIAxiY1ND0JCST0nvgfgDyZphinr+V/4D4v+45HGGAQjP+t/+f/X/2UA8B8xGl8cIQVBgxS2LPwtyds//in8gzTf/XwTiS2CDQGIgOZAakFqiDLgF1Hzr/2WEAZ/xGFBWUj4JFtr4vABSA1KLYcDk3slVyIGFKxBBbKhaBmKi8QOx0YjVAFhCIskAZBqWlLHJ4csPDDgU4JMjHwAAAfUvfvahyqUAAAAASUVORK5CYII=';
          Favicon.unreadNSFWY = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAA/klEQVQ4y2NgGEzgPwxP7JpYVVZSMSk9O2suCIPYIDFkNRia084YwzGIHzMx/mDc8vizIAxiY1ND0JCST0nvgfgDyZphinr+V/4D4v+45HGGAQjP+t/+f/X/2UA8B8xGl8cIQVBgxS2LPwtyds//in8gzTf/XwTiS2CDMg9E/ZMwEvktKCf4xdnNdStRBtwCar71/zLYADkLqf8wwM/P/xLDgLKS8kmw0MblBRjg5uZ+h2HA5N7JVciBhR6IAgocyAa8wpqQsETjB1g0iulwww3AGojYDIAlJBDbqkT2PwsHExjjNQCZhiVlEFvOhp+gCxiQJP7jkDsCpe2olgMB9UE2wvseYUsAAAAASUVORK5CYII=';
          break;
        case 'Mayhem':
          Favicon.unreadDead = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABFklEQVR4AZ2R4WqEMBCEFy1yiJQQ14gcIhIuFBFR+qPQ93+v66QMksrlTwMfkZ2ZZbMKTgVqYIDl3YAbeCM31lJP/Zul4MAEPJjBQGNDLGsz8PQ6aqLAP5PTdd1WlmU09mSKtdTDRgrkzspJPKq6RxMahfj9yhOzQEZwZAwfzrk1ox3MXibIN8hO4MAjeV72CemJGWblnRsOYOdoGw0jebB20BPAwKzUQPlrFhrXFw1Wagu9yuzZwINzVAZCURRL+gRr7Wd8Vtqg4Th/lsUmewyk9WQ/A7NiwJz5VV/GmO+MNjMrFvh/NPDMigHTaeJN09a27ZHRJmalBg54CgfvAGYSLpoHjlmpuAwFdzDy7oGS/qIpM9UPFGg1b1kUlssAAAAASUVORK5CYII=';
          Favicon.unreadDeadY = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABR0lEQVR4AYWSQWq0QBCFCw0SRIK0PQ4hiIhEZBhEySLyewUPEMgqR/JIXiDhzz7kKKYePIZajEzDRxfV9dWU3SO6IiVWUsVxT5R75Y4gTmwNnUh4kCulUiuV8sjChDjmKtaUcHgmHsnNrMPh0IVhiMIjKZGzNXDoyhMzF7C89z2KtFGD+FoNXEUKZdgpaPM8P++cDXTtBDca7EyQK8+bXTufYBccuvLAG26UnqN1LCgI4g/lm7zTgSux4vk0J8rnKw3+m1//pBPbBrVyGZVNmiAITviEtm3t+D+2QcJx7GUxlN4594K4ZY75Xzh0JVWqnad6TdP0H+LRNBjHcYNDV5xS32qwaC4my7Lwn6guu5QoomgbdFmWDYhnM8E8zxscuhLzPWtKA/dGqUizrityX9M0YX+DQ1ciXobnP6vgfmTOM7Znnk70B58pPaEvx+epAAAAAElFTkSuQmCC';
          Favicon.unreadSFW = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAA/ElEQVR4AZ3RUWqEMBSF4ftQZAhSREQJIiIXpQwi+tSldkFdWPsLhyEE0ocKH2Fyzg1mNJ4KAQ1arTUeeJMH6qwTUJmCHjMcC6KKtbSIylzdXpl18J/k4fdTpUFmPLOOa9bGe+P4+n5RYYfLXuiMsAlXofBxK2QXpvwN/jqg+AY91vR+pStk+apZe0fEhhMXDhUmWXEoO9WNmrWAzvRPq7jnB2jvUGfWTEgPcJzZFTbZk/0Tnh5QI+af6lVGvq/Do2atwVL4VJ+3QrZo1lr4Pw5wzVqDWaV7SUvHrZDNmrWAHq7g0rphkS3LXDMBVqFGhxGT1gGdDFnWaab6BRmXRvbxDmYiAAAAAElFTkSuQmCC';
          Favicon.unreadSFWY = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABQElEQVR4AY2SQUrEQBBFS9CMNFEkhAQdYmiCIUgcZlYGc4VsBcGVF/AuWXme4F7RtXiVWF9+Y9MYtOHRTdX/NZWaEj2RYpQTJeEdK4fKPuA7DjSGXiQkU0qlUqxySmFMEsYsNSU8zEmK4OwdEbmkKCclYoGmolfWCGyenh1O0EJE2gXNWpFC2S0IGrCQ29EbdPCPAmEHmXIxByf8hDAPD71yzAnXypatbSgoAN8Pyju5h4deMUrqJk1z+0uBN+/XX+gxfoFK2QafUJO2aRq//Q+/QIx2wr+Kwq0rusrP/QKf9MTCtbQLf9U1wNvYnz3qug45S68kSvVXgbPbx3nvYPXNOI7cRPWySukK+DcGCvA+urqZ3RmGAbmSXjFK5rpwW8nhWVJP04TYa9/3uO/goVciDiPlZhW8c8ZAHuRSeqIv32FK/GYGL8YAAAAASUVORK5CYII=';
          Favicon.unreadNSFW = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAA/ElEQVR4AZ3RUWqEMBSF4ftQZAihDCKKiAQJShERQx+6o662e2p/4TCEQF468BEm95yLovFr4PBEq9PjgTd5wBcZp6559AiIWDAq6KXV3aJMUMfDOsTf7Mf/XaFBAvYiE9W16b74/vl8UeBAlKOSmWAzUiXwcavMkrrFE9QXVJ+gx5q9XvUVivmqrr1jxIYLCacCs6y6S8psGNU1hw4Bu4JHuUB3pzJBHZcviLiKV9jkyO4vxHyBx1h+qlcY5b2Wj+raE0vlU33dKrNFXWsR/7EgqmtPBIXuIw+dt8osqGsOPaIGSeeGRbZiFtVxsAYeHSbMOgd0MhSzTp3mD4RaQX4aW3NMAAAAAElFTkSuQmCC';
          Favicon.unreadNSFWY = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABP0lEQVR4AYWS0UqFQBCGhziImNRBRImDmUgiIaF0kWSP4AMEXXXTE/QiPpL3UdR19Crb/PAvLEtyFj5mmfn/cdxd0RUokbJXEsZYCZUd4D72NBG8wkKmlEqtVMoFhTFJmKuoKelBTVIkjbNE5IainJTIeZqaXjkg8fp+Z7GCjiLQbWgOihTKsCFowUZtoNef4HgDf4JMuTbe8n/Br8NDr5zxhBul52i3FBQE+xflmzzTA69ESmpPmubunwZfztc/6IncBrXSe7/QkK5tW3f8H7dBjHH8q6Kwt033V6Hb4JeeWPgsq42rugfYZ92psWscRwMPvZIo9bEGD2+F2YUnBizLwpeoXnYpbQM34kAB9peP58aueZ4NPPRKxPusaRoYG6UizbquyH1O04T4RA+8EvAwUr6sgjFnDuReLaUn+ANygUa7+9SCWgAAAABJRU5ErkJggg==';
          break;
        case 'Original':
          Favicon.unreadDead = 'data:image/gif;base64,R0lGODlhEAAQAKECAAAAAP8AAP///////yH5BAEKAAMALAAAAAAQABAAAAI/nI95wsqygIRxDgGCBhTrwF3Zxowg5H1cSopS6FrGQ82PU1951ckRmYKJVCXizLRC9kAnT0aIiR6lCFT1cigAADs=';
          Favicon.unreadDeadY = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAhElEQVR42q1RwQnAMAjMu5M4guAKXa4j5dUROo5tipSDcrFChUONd0di2m/hEGVOHDyIPufgwAFASDkpoSzmBrkJ2UMyR9LsJ3rvrqo3Rt1YMIMhhNnOxLMnoMFBxHyJAr2IOBFzA8U+6pLBdmEJTA0aMVjpDd6Loks0s5HZNwYx8tfZCZ0kll7ORffZAAAAAElFTkSuQmCC';
          Favicon.unreadSFW = 'data:image/gif;base64,R0lGODlhEAAQAKECAAAAAC6Xw////////yH5BAEKAAMALAAAAAAQABAAAAI/nI95wsqygIRxDgGCBhTrwF3Zxowg5H1cSopS6FrGQ82PU1951ckRmYKJVCXizLRC9kAnT0aIiR6lCFT1cigAADs=';
          Favicon.unreadSFWY = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAMAAAAoLQ9TAAAALVBMVEUAAAAAAAAAAAAAAAABBQcHFx4KISoNLToaVW4oKCgul8M4ODg7OzvBwcH///8uS/CdAAAAA3RSTlMAx9dmesIgAAAAV0lEQVR42m2NWw6AIBAD1eILZO5/XI0UAgm7H9tOsu0yGWAQSOoFijHOxOANGqm/LczpOaXs4gISrPZ+gc2+hO5w2xdwgOjBFUIF+sEJrhUl9JFr+badFwR+BfqlmGUJAAAAAElFTkSuQmCC';
          Favicon.unreadNSFW = 'data:image/gif;base64,R0lGODlhEAAQAKECAAAAAGbMM////////yH5BAEKAAMALAAAAAAQABAAAAI/nI95wsqygIRxDgGCBhTrwF3Zxowg5H1cSopS6FrGQ82PU1951ckRmYKJVCXizLRC9kAnT0aIiR6lCFT1cigAADs=';
          Favicon.unreadNSFWY = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAMAAAAoLQ9TAAAALVBMVEUAAAAAAAAAAAAAAAAECAIQIAgWLAsePA8oKCg4ODg6dB07OztmzDPBwcH///+rsf3XAAAAA3RSTlMAx9dmesIgAAAAV0lEQVR42m2NWw6AIBAD1eIDhbn/cTVSCCTsfmw7ybbLZIBBIKkXKKU0E4M3aKT+tjCn5xiziwuIsNr7BTb7ErrDZV/AAaIHdwgV6AcnuFaU0Eeu5dt2XiUyBjCQ2bIrAAAAAElFTkSuQmCC';
      }
      if (Favicon.SFW) {
        Favicon.unread = Favicon.unreadSFW;
        return Favicon.unreadY = Favicon.unreadSFWY;
      } else {
        Favicon.unread = Favicon.unreadNSFW;
        return Favicon.unreadY = Favicon.unreadNSFWY;
      }
    },
    dead: 'data:image/gif;base64,R0lGODlhEAAQAKECAAAAAP8AAP///////yH5BAEKAAIALAAAAAAQABAAAAIvlI+pq+D9DAgUoFkPDlbs7lFZKIJOJJ3MyraoB14jFpOcVMpzrnF3OKlZYsMWowAAOw==',
    logo: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIAAAACAAgMAAAC+UIlYAAAACVBMVEUAAGcAAABmzDNZt9VtAAAAAXRSTlMAQObYZgAAAFtJREFUeF7t0rENACAMA0GW9JJMiRSKF2GEjyUKO1eyeiaTVP7FBZJdgbBoQCO9eQDDJW8TgSIgaJ0lgLyEowlAODvB/Id+hvhAwgxNrCChWcF9NBeA1GtNByYHKg38Lq6jLMwAAAAASUVORK5CYII='
  };

  ThreadExcerpt = {
    init: function() {
      if (g.VIEW !== 'thread' || !Conf['Thread Excerpt']) {
        return;
      }
      return Thread.callbacks.push({
        name: 'Thread Excerpt',
        cb: this.node
      });
    },
    node: function() {
      return d.title = Get.threadExcerpt(this);
    }
  };

  ThreadStats = {
    init: function() {
      if (g.VIEW !== 'thread' || !Conf['Thread Stats']) {
        return;
      }
      this.dialog = UI.dialog('thread-stats', 'bottom: 0; left: 0;', "<div class=\"move\" title=\"Post count / File count / Page count\"><span id=\"post-count\">...</span> / <span id=\"file-count\">...</span> / <span id=\"page-count\">...</span></div>");
      this.postCountEl = $('#post-count', this.dialog);
      this.fileCountEl = $('#file-count', this.dialog);
      this.pageCountEl = $('#page-count', this.dialog);
      return Thread.callbacks.push({
        name: 'Thread Stats',
        cb: this.node
      });
    },
    node: function() {
      var ID, fileCount, post, postCount, _ref;
      postCount = 0;
      fileCount = 0;
      _ref = this.posts;
      for (ID in _ref) {
        post = _ref[ID];
        postCount++;
        if (post.file) {
          fileCount++;
        }
      }
      ThreadStats.thread = this;
      ThreadStats.fetchPage();
      ThreadStats.update(postCount, fileCount);
      $.on(d, 'ThreadUpdate', ThreadStats.onUpdate);
      return $.add(d.body, ThreadStats.dialog);
    },
    onUpdate: function(e) {
      var fileCount, postCount, _ref;
      if (e.detail[404]) {
        return;
      }
      _ref = e.detail, postCount = _ref.postCount, fileCount = _ref.fileCount;
      return ThreadStats.update(postCount, fileCount);
    },
    update: function(postCount, fileCount) {
      var fileCountEl, postCountEl, thread;
      thread = ThreadStats.thread, postCountEl = ThreadStats.postCountEl, fileCountEl = ThreadStats.fileCountEl;
      postCountEl.textContent = postCount;
      fileCountEl.textContent = fileCount;
      (thread.postLimit && !thread.isSticky ? $.addClass : $.rmClass)(postCountEl, 'warning');
      return (thread.fileLimit && !thread.isSticky ? $.addClass : $.rmClass)(fileCountEl, 'warning');
    },
    fetchPage: function() {
      if (ThreadStats.thread.isDead) {
        ThreadStats.pageCountEl.textContent = 'Dead';
        $.addClass(ThreadStats.pageCountEl, 'warning');
        return;
      }
      setTimeout(ThreadStats.fetchPage, 2 * $.MINUTE);
      return $.ajax("//api.4chan.org/" + ThreadStats.thread.board + "/threads.json", {
        onload: ThreadStats.onThreadsLoad
      }, {
        whenModified: true
      });
    },
    onThreadsLoad: function() {
      var page, pages, thread, _i, _j, _len, _len1, _ref;
      if (this.status !== 200) {
        return;
      }
      pages = JSON.parse(this.response);
      for (_i = 0, _len = pages.length; _i < _len; _i++) {
        page = pages[_i];
        _ref = page.threads;
        for (_j = 0, _len1 = _ref.length; _j < _len1; _j++) {
          thread = _ref[_j];
          if (thread.no === ThreadStats.thread.ID) {
            ThreadStats.pageCountEl.textContent = page.page;
            (page.page === pages.length - 1 ? $.addClass : $.rmClass)(ThreadStats.pageCountEl, 'warning');
            return;
          }
        }
      }
    }
  };

  ThreadUpdater = {
    init: function() {
      var checked, conf, html, name, _ref;
      if (g.VIEW !== 'thread' || !Conf['Thread Updater']) {
        return;
      }
      this.button = $.el('a', {
        className: 'thread-refresh-shortcut fa fa-refresh',
        title: 'Refresh Thread',
        href: 'javascript:;'
      });
      $.on(this.button, 'click', this.update);
      Header.addShortcut(this.button, 1);
      html = '';
      _ref = Config.updater.checkbox;
      for (name in _ref) {
        conf = _ref[name];
        checked = Conf[name] ? 'checked' : '';
        html += "<div><label title='" + conf[1] + "'><input name='" + name + "' type=checkbox " + checked + "> " + name + "</label></div>";
      }
      html = "<div class=\"move\"><span id=\"update-status\"></span>&nbsp;<span id=\"update-timer\"></span></div>" + html + "<div><label title=\"Controls whether *this* thread automatically updates or not\"><input type=\"checkbox\" name=\"Auto Update This\" " + (Conf['Auto Update'] ? "checked" : "") + "> Auto Update This</label></div><div><label><input type=\"number\" name=\"Interval\" class=\"field\" min=\"5\" value=\"" + Conf['Interval'] + "\"> Refresh rate (s)</label></div><div><input value=\"Refresh thread\" type=\"button\" name=\"Update\"></div>";
      this.dialog = UI.dialog('updater', 'bottom: 0; right: 0;', html);
      this.timer = $('#update-timer', this.dialog);
      this.status = $('#update-status', this.dialog);
      this.isUpdating = Conf['Auto Update'];
      return Thread.callbacks.push({
        name: 'Thread Updater',
        cb: this.node
      });
    },
    node: function() {
      var input, _i, _len, _ref;
      ThreadUpdater.thread = this;
      ThreadUpdater.root = this.OP.nodes.root.parentNode;
      ThreadUpdater.lastPost = +ThreadUpdater.root.lastElementChild.id.match(/\d+/)[0];
      _ref = $$('input', ThreadUpdater.dialog);
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        input = _ref[_i];
        if (input.type === 'checkbox') {
          $.on(input, 'change', $.cb.checked);
        }
        switch (input.name) {
          case 'Scroll BG':
            $.on(input, 'change', ThreadUpdater.cb.scrollBG);
            ThreadUpdater.cb.scrollBG();
            break;
          case 'Auto Update This':
            $.off(input, 'change', $.cb.checked);
            $.on(input, 'change', ThreadUpdater.cb.autoUpdate);
            break;
          case 'Interval':
            $.on(input, 'change', ThreadUpdater.cb.interval);
            ThreadUpdater.cb.interval.call(input);
            break;
          case 'Update':
            $.on(input, 'click', ThreadUpdater.update);
        }
      }
      $.on(window, 'online offline', ThreadUpdater.cb.online);
      $.on(d, 'QRPostSuccessful', ThreadUpdater.cb.post);
      $.on(d, 'visibilitychange', ThreadUpdater.cb.visibility);
      ThreadUpdater.cb.online();
      return $.add(d.body, ThreadUpdater.dialog);
    },
    beep: 'data:audio/wav;base64,UklGRjQDAABXQVZFZm10IBAAAAABAAEAgD4AAIA+AAABAAgAc21wbDwAAABBAAADAAAAAAAAAAA8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABkYXRhzAIAAGMms8em0tleMV4zIpLVo8nhfSlcPR102Ki+5JspVEkdVtKzs+K1NEhUIT7DwKrcy0g6WygsrM2k1NpiLl0zIY/WpMrjgCdbPhxw2Kq+5Z4qUkkdU9K1s+K5NkVTITzBwqnczko3WikrqM+l1NxlLF0zIIvXpsnjgydZPhxs2ay95aIrUEkdUdC3suK8N0NUIjq+xKrcz002WioppdGm091pK1w0IIjYp8jkhydXPxxq2K295aUrTkoeTs65suK+OUFUIzi7xqrb0VA0WSoootKm0t5tKlo1H4TYqMfkiydWQBxm16+85actTEseS8y7seHAPD9TIza5yKra01QyWSson9On0d5wKVk2H4DYqcfkjidUQB1j1rG75KsvSkseScu8seDCPz1TJDW2yara1FYxWSwnm9Sn0N9zKVg2H33ZqsXkkihSQR1g1bK65K0wSEsfR8i+seDEQTxUJTOzy6rY1VowWC0mmNWoz993KVc3H3rYq8TklSlRQh1d1LS647AyR0wgRMbAsN/GRDpTJTKwzKrX1l4vVy4lldWpzt97KVY4IXbUr8LZljVPRCxhw7W3z6ZISkw1VK+4sMWvXEhSPk6buay9sm5JVkZNiLWqtrJ+TldNTnquqbCwilZXU1BwpKirrpNgWFhTaZmnpquZbFlbVmWOpaOonHZcXlljhaGhpZ1+YWBdYn2cn6GdhmdhYGN3lp2enIttY2Jjco+bnJuOdGZlZXCImJqakHpoZ2Zug5WYmZJ/bGlobX6RlpeSg3BqaW16jZSVkoZ0bGtteImSk5KIeG5tbnaFkJKRinxxbm91gY2QkIt/c3BwdH6Kj4+LgnZxcXR8iI2OjIR5c3J0e4WLjYuFe3VzdHmCioyLhn52dHR5gIiKioeAeHV1eH+GiYqHgXp2dnh9hIiJh4J8eHd4fIKHiIeDfXl4eHyBhoeHhH96eHmA',
    cb: {
      online: function() {
        if (navigator.onLine) {
          ThreadUpdater.outdateCount = 0;
          ThreadUpdater.setInterval();
          ThreadUpdater.set('status', null, null);
        } else {
          ThreadUpdater.set('timer', null);
          ThreadUpdater.set('status', 'Offline', 'warning');
        }
        return ThreadUpdater.count(true);
      },
      post: function(e) {
        if (!(ThreadUpdater.isUpdating && e.detail.threadID === ThreadUpdater.thread.ID)) {
          return;
        }
        ThreadUpdater.outdateCount = 0;
        if (ThreadUpdater.seconds > 2) {
          return setTimeout(ThreadUpdater.update, 1000);
        }
      },
      visibility: function() {
        if (d.hidden) {
          return;
        }
        ThreadUpdater.outdateCount = 0;
        return ThreadUpdater.seconds = Math.min(ThreadUpdater.seconds, ThreadUpdater.interval);
      },
      scrollBG: function() {
        return ThreadUpdater.scrollBG = Conf['Scroll BG'] ? function() {
          return true;
        } : function() {
          return !d.hidden;
        };
      },
      autoUpdate: function(e) {
        return ThreadUpdater.count(ThreadUpdater.isUpdating = this.checked);
      },
      interval: function(e) {
        var val;
        val = Math.max(5, parseInt(this.value, 10));
        ThreadUpdater.interval = this.value = val;
        if (e) {
          return $.cb.value.call(this);
        }
      },
      load: function(e) {
        var klass, req, text, _ref;
        $.rmClass(ThreadUpdater.button, 'fa-spin');
        req = ThreadUpdater.req;
        delete ThreadUpdater.req;
        if (e.type !== 'loadend') {
          req.onloadend = null;
          if (e.type === 'timeout') {
            ThreadUpdater.set('status', 'Retrying', null);
            ThreadUpdater.update();
          }
          return;
        }
        switch (req.status) {
          case 200:
            g.DEAD = false;
            ThreadUpdater.parse(JSON.parse(req.response).posts);
            return ThreadUpdater.setInterval();
          case 404:
            g.DEAD = true;
            ThreadUpdater.set('timer', null);
            ThreadUpdater.set('status', '404', 'warning');
            ThreadUpdater.thread.kill();
            return $.event('ThreadUpdate', {
              404: true,
              thread: ThreadUpdater.thread
            });
          default:
            ThreadUpdater.outdateCount++;
            ThreadUpdater.setInterval();
            _ref = req.status === 304 ? [null, null] : ["" + req.statusText + " (" + req.status + ")", 'warning'], text = _ref[0], klass = _ref[1];
            return ThreadUpdater.set('status', text, klass);
        }
      }
    },
    setInterval: function() {
      var i, j;
      i = ThreadUpdater.interval;
      j = Math.min(ThreadUpdater.outdateCount, 10);
      if (!d.hidden) {
        j = Math.min(j, 7);
      }
      ThreadUpdater.seconds = Math.max(i, [0, 5, 10, 15, 20, 30, 60, 90, 120, 240, 300][j]);
      ThreadUpdater.set('timer', ThreadUpdater.seconds);
      return ThreadUpdater.count(true);
    },
    set: function(name, text, klass) {
      var el, node;
      el = ThreadUpdater[name];
      if (node = el.firstChild) {
        node.data = text;
      } else {
        el.textContent = text;
      }
      if (klass !== void 0) {
        return el.className = klass;
      }
    },
    count: function(start) {
      clearTimeout(ThreadUpdater.timeoutID);
      if (start && ThreadUpdater.isUpdating && navigator.onLine) {
        return ThreadUpdater.timeout();
      }
    },
    timeout: function() {
      var sec;
      ThreadUpdater.timeoutID = setTimeout(ThreadUpdater.timeout, 1000);
      sec = ThreadUpdater.seconds--;
      ThreadUpdater.set('timer', sec);
      if (sec <= 0) {
        return ThreadUpdater.update();
      }
    },
    update: function() {
      var url, _ref;
      if (!navigator.onLine) {
        return;
      }
      $.addClass(ThreadUpdater.button, 'fa-spin');
      ThreadUpdater.count();
      ThreadUpdater.set('timer', '...');
      if ((_ref = ThreadUpdater.req) != null) {
        _ref.abort();
      }
      url = "//api.4chan.org/" + ThreadUpdater.thread.board + "/res/" + ThreadUpdater.thread + ".json";
      return ThreadUpdater.req = $.ajax(url, {
        onabort: ThreadUpdater.cb.load,
        onloadend: ThreadUpdater.cb.load,
        ontimeout: ThreadUpdater.cb.load,
        timeout: $.MINUTE
      }, {
        whenModified: true
      });
    },
    updateThreadStatus: function(type, status) {
      var change, hasChanged;
      if (!(hasChanged = ThreadUpdater.thread["is" + type] !== status)) {
        return;
      }
      ThreadUpdater.thread.setStatus(type, status);
      change = type === 'Sticky' ? status ? 'now a sticky' : 'not a sticky anymore' : status ? 'now closed' : 'not closed anymore';
      return new Notice('info', "The thread is " + change + ".", 30);
    },
    parse: function(postObjects) {
      var ID, OP, count, deletedFiles, deletedPosts, files, index, length, node, nodes, num, post, postObject, posts, scroll, sendEvent, threadID, _i, _len, _ref;
      OP = postObjects[0];
      Build.spoilerRange[ThreadUpdater.thread.board] = OP.custom_spoiler;
      ThreadUpdater.updateThreadStatus('Sticky', !!OP.sticky);
      ThreadUpdater.updateThreadStatus('Closed', !!OP.closed);
      ThreadUpdater.thread.postLimit = !!OP.bumplimit;
      ThreadUpdater.thread.fileLimit = !!OP.imagelimit;
      nodes = [];
      posts = [];
      index = [];
      files = [];
      count = 0;
      for (_i = 0, _len = postObjects.length; _i < _len; _i++) {
        postObject = postObjects[_i];
        num = postObject.no;
        index.push(num);
        if (postObject.fsize) {
          files.push(num);
        }
        if (num <= ThreadUpdater.lastPost) {
          continue;
        }
        count++;
        node = Build.postFromObject(postObject, ThreadUpdater.thread.board.ID);
        nodes.push(node);
        posts.push(new Post(node, ThreadUpdater.thread, ThreadUpdater.thread.board));
      }
      deletedPosts = [];
      deletedFiles = [];
      _ref = ThreadUpdater.thread.posts;
      for (ID in _ref) {
        post = _ref[ID];
        ID = +ID;
        if (post.isDead && __indexOf.call(index, ID) >= 0) {
          post.resurrect();
        } else if (__indexOf.call(index, ID) < 0) {
          post.kill();
          deletedPosts.push(post);
        } else if (post.file && !post.file.isDead && __indexOf.call(files, ID) < 0) {
          post.kill(true);
          deletedFiles.push(post);
        }
      }
      sendEvent = function() {
        return $.event('ThreadUpdate', {
          404: false,
          thread: ThreadUpdater.thread,
          newPosts: posts,
          deletedPosts: deletedPosts,
          deletedFiles: deletedFiles,
          postCount: OP.replies + 1,
          fileCount: OP.images + (!!ThreadUpdater.thread.OP.file && !ThreadUpdater.thread.OP.file.isDead)
        });
      };
      if (!count) {
        ThreadUpdater.set('status', null, null);
        ThreadUpdater.outdateCount++;
        sendEvent();
        return;
      }
      ThreadUpdater.set('status', "+" + count, 'new');
      ThreadUpdater.outdateCount = 0;
      if (Conf['Beep'] && d.hidden && Unread.posts && !Unread.posts.length) {
        if (!ThreadUpdater.audio) {
          ThreadUpdater.audio = $.el('audio', {
            src: ThreadUpdater.beep
          });
        }
        ThreadUpdater.audio.play();
      }
      ThreadUpdater.lastPost = posts[count - 1].ID;
      Main.callbackNodes(Post, posts);
      scroll = Conf['Auto Scroll'] && ThreadUpdater.scrollBG() && Header.getBottomOf(ThreadUpdater.root) > -25;
      $.add(ThreadUpdater.root, nodes);
      sendEvent();
      if (scroll) {
        if (Conf['Bottom Scroll']) {
          window.scrollTo(0, d.body.clientHeight);
        } else {
          Header.scrollTo(nodes[0]);
        }
      }
      threadID = ThreadUpdater.thread.ID;
      length = $$('.thread > .postContainer', ThreadUpdater.root).length;
      if (Conf['Enable 4chan\'s Extension']) {
        return $.globalEval("Parser.parseThread(" + threadID + ", " + (-count) + ")");
      } else {
        return Fourchan.parseThread(threadID, length - count, length);
      }
    }
  };

  ThreadWatcher = {
    init: function() {
      var now;
      if (!Conf['Thread Watcher']) {
        return;
      }
      this.db = new DataBoard('watchedThreads', this.refresh, true);
      this.dialog = UI.dialog('thread-watcher', 'top: 50px; left: 0px;', "<div><span class=\"move\">Thread Watcher <span id=\"watcher-status\"></span></span><a class=\"menu-button\" href=\"javascript:;\"><i class=\"fa fa-reorder\"></i></a></div><div id=\"watched-threads\"></div>");
      this.status = $('#watcher-status', this.dialog);
      this.list = this.dialog.lastElementChild;
      $.on(d, 'QRPostSuccessful', this.cb.post);
      $.on(d, '4chanXInitFinished', this.ready);
      switch (g.VIEW) {
        case 'index':
          $.on(d, 'IndexRefresh', this.cb.onIndexRefresh);
          break;
        case 'thread':
          $.on(d, 'ThreadUpdate', this.cb.onThreadRefresh);
      }
      now = Date.now();
      if ((this.db.data.lastChecked || 0) < now - 2 * $.HOUR) {
        this.db.data.lastChecked = now;
        ThreadWatcher.fetchAllStatus();
        this.db.save();
      }
      return Thread.callbacks.push({
        name: 'Thread Watcher',
        cb: this.node
      });
    },
    node: function() {
      var toggler;
      toggler = $.el('a', {
        className: 'watcher-toggler',
        href: 'javascript:;'
      });
      $.on(toggler, 'click', ThreadWatcher.cb.toggle);
      return $.after($('input', this.OP.nodes.post), [toggler, $.tn(' ')]);
    },
    ready: function() {
      $.off(d, '4chanXInitFinished', ThreadWatcher.ready);
      if (!Main.isThisPageLegit()) {
        return;
      }
      ThreadWatcher.refresh();
      $.add(d.body, ThreadWatcher.dialog);
      if (!Conf['Auto Watch']) {
        return;
      }
      return $.get('AutoWatch', 0, function(_arg) {
        var AutoWatch, thread;
        AutoWatch = _arg.AutoWatch;
        if (!(thread = g.BOARD.threads[AutoWatch])) {
          return;
        }
        ThreadWatcher.add(thread);
        return $["delete"]('AutoWatch');
      });
    },
    cb: {
      openAll: function() {
        var a, _i, _len, _ref;
        if ($.hasClass(this, 'disabled')) {
          return;
        }
        _ref = $$('a[title]', ThreadWatcher.list);
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
          a = _ref[_i];
          $.open(a.href);
        }
        return $.event('CloseMenu');
      },
      checkThreads: function() {
        if ($.hasClass(this, 'disabled')) {
          return;
        }
        return ThreadWatcher.fetchAllStatus();
      },
      pruneDeads: function() {
        var boardID, data, threadID, _i, _len, _ref, _ref1;
        if ($.hasClass(this, 'disabled')) {
          return;
        }
        _ref = ThreadWatcher.getAll();
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
          _ref1 = _ref[_i], boardID = _ref1.boardID, threadID = _ref1.threadID, data = _ref1.data;
          if (!data.isDead) {
            continue;
          }
          delete ThreadWatcher.db.data.boards[boardID][threadID];
          ThreadWatcher.db.deleteIfEmpty({
            boardID: boardID
          });
        }
        ThreadWatcher.db.save();
        ThreadWatcher.refresh();
        return $.event('CloseMenu');
      },
      toggle: function() {
        return ThreadWatcher.toggle(Get.postFromNode(this).thread);
      },
      rm: function() {
        var boardID, threadID, _ref;
        _ref = this.parentNode.dataset.fullID.split('.'), boardID = _ref[0], threadID = _ref[1];
        return ThreadWatcher.rm(boardID, +threadID);
      },
      post: function(e) {
        var board, postID, threadID, _ref;
        _ref = e.detail, board = _ref.board, postID = _ref.postID, threadID = _ref.threadID;
        if (postID === threadID) {
          if (Conf['Auto Watch']) {
            return $.set('AutoWatch', threadID);
          }
        } else if (Conf['Auto Watch Reply']) {
          return ThreadWatcher.add(board.threads[threadID]);
        }
      },
      onIndexRefresh: function() {
        var boardID, data, db, threadID, _ref;
        db = ThreadWatcher.db;
        boardID = g.BOARD.ID;
        _ref = db.data.boards[boardID];
        for (threadID in _ref) {
          data = _ref[threadID];
          if (!data.isDead && !(threadID in g.BOARD.threads)) {
            if (Conf['Auto Prune']) {
              ThreadWatcher.db["delete"]({
                boardID: boardID,
                threadID: threadID
              });
            } else {
              data.isDead = true;
              ThreadWatcher.db.set({
                boardID: boardID,
                threadID: threadID,
                val: data
              });
            }
          }
        }
        return ThreadWatcher.refresh();
      },
      onThreadRefresh: function(e) {
        var thread;
        thread = e.detail.thread;
        if (!(e.detail[404] && ThreadWatcher.db.get({
          boardID: thread.board.ID,
          threadID: thread.ID
        }))) {
          return;
        }
        return ThreadWatcher.add(thread);
      }
    },
    fetchCount: {
      fetched: 0,
      fetching: 0
    },
    fetchAllStatus: function() {
      var thread, threads, _i, _len;
      if (!(threads = ThreadWatcher.getAll()).length) {
        return;
      }
      ThreadWatcher.status.textContent = '...';
      for (_i = 0, _len = threads.length; _i < _len; _i++) {
        thread = threads[_i];
        ThreadWatcher.fetchStatus(thread);
      }
    },
    fetchStatus: function(_arg) {
      var boardID, data, fetchCount, threadID;
      boardID = _arg.boardID, threadID = _arg.threadID, data = _arg.data;
      if (data.isDead) {
        return;
      }
      fetchCount = ThreadWatcher.fetchCount;
      fetchCount.fetching++;
      return $.ajax("//api.4chan.org/" + boardID + "/res/" + threadID + ".json", {
        onloadend: function() {
          var status;
          fetchCount.fetched++;
          if (fetchCount.fetched === fetchCount.fetching) {
            fetchCount.fetched = 0;
            fetchCount.fetching = 0;
            status = '';
          } else {
            status = "" + (Math.round(fetchCount.fetched / fetchCount.fetching * 100)) + "%";
          }
          ThreadWatcher.status.textContent = status;
          if (this.status !== 404) {
            return;
          }
          if (Conf['Auto Prune']) {
            ThreadWatcher.db["delete"]({
              boardID: boardID,
              threadID: threadID
            });
          } else {
            data.isDead = true;
            ThreadWatcher.db.set({
              boardID: boardID,
              threadID: threadID,
              val: data
            });
          }
          return ThreadWatcher.refresh();
        }
      }, {
        type: 'head'
      });
    },
    getAll: function() {
      var all, boardID, data, threadID, threads, _ref;
      all = [];
      _ref = ThreadWatcher.db.data.boards;
      for (boardID in _ref) {
        threads = _ref[boardID];
        if (Conf['Current Board'] && boardID !== g.BOARD.ID) {
          continue;
        }
        for (threadID in threads) {
          data = threads[threadID];
          all.push({
            boardID: boardID,
            threadID: threadID,
            data: data
          });
        }
      }
      return all;
    },
    makeLine: function(boardID, threadID, data) {
      var div, fullID, href, link, x;
      x = $.el('a', {
        textContent: '×',
        href: 'javascript:;'
      });
      $.on(x, 'click', ThreadWatcher.cb.rm);
      if (data.isDead) {
        href = Redirect.to('thread', {
          boardID: boardID,
          threadID: threadID
        });
      }
      link = $.el('a', {
        href: href || ("/" + boardID + "/res/" + threadID),
        textContent: data.excerpt,
        title: data.excerpt
      });
      div = $.el('div');
      fullID = "" + boardID + "." + threadID;
      div.dataset.fullID = fullID;
      if (g.VIEW === 'thread' && fullID === ("" + g.BOARD + "." + g.THREADID)) {
        $.addClass(div, 'current');
      }
      if (data.isDead) {
        $.addClass(div, 'dead-thread');
      }
      $.add(div, [x, $.tn(' '), link]);
      return div;
    },
    refresh: function() {
      var boardID, data, list, nodes, refresher, thread, threadID, _i, _j, _len, _len1, _ref, _ref1, _ref2, _ref3;
      nodes = [];
      _ref = ThreadWatcher.getAll();
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        _ref1 = _ref[_i], boardID = _ref1.boardID, threadID = _ref1.threadID, data = _ref1.data;
        nodes.push(ThreadWatcher.makeLine(boardID, threadID, data));
      }
      list = ThreadWatcher.list;
      $.rmAll(list);
      $.add(list, nodes);
      _ref2 = g.BOARD.threads;
      for (threadID in _ref2) {
        thread = _ref2[threadID];
        $.extend($('.watcher-toggler', thread.OP.nodes.post), ThreadWatcher.db.get({
          boardID: thread.board.ID,
          threadID: threadID
        }) ? {
          className: 'watcher-toggler fa fa-bookmark',
          title: 'Unwatch thread'
        } : {
          className: 'watcher-toggler fa fa-bookmark-o',
          title: 'Watch thread'
        });
      }
      _ref3 = ThreadWatcher.menu.refreshers;
      for (_j = 0, _len1 = _ref3.length; _j < _len1; _j++) {
        refresher = _ref3[_j];
        refresher();
      }
    },
    toggle: function(thread) {
      var boardID, threadID;
      boardID = thread.board.ID;
      threadID = thread.ID;
      if (ThreadWatcher.db.get({
        boardID: boardID,
        threadID: threadID
      })) {
        return ThreadWatcher.rm(boardID, threadID);
      } else {
        return ThreadWatcher.add(thread);
      }
    },
    add: function(thread) {
      var boardID, data, threadID;
      data = {};
      boardID = thread.board.ID;
      threadID = thread.ID;
      if (thread.isDead) {
        if (Conf['Auto Prune'] && ThreadWatcher.db.get({
          boardID: boardID,
          threadID: threadID
        })) {
          ThreadWatcher.rm(boardID, threadID);
          return;
        }
        data.isDead = true;
      }
      data.excerpt = Get.threadExcerpt(thread);
      ThreadWatcher.db.set({
        boardID: boardID,
        threadID: threadID,
        val: data
      });
      return ThreadWatcher.refresh();
    },
    rm: function(boardID, threadID) {
      ThreadWatcher.db["delete"]({
        boardID: boardID,
        threadID: threadID
      });
      return ThreadWatcher.refresh();
    },
    convert: function(oldFormat) {
      var boardID, data, newFormat, threadID, threads;
      newFormat = {};
      for (boardID in oldFormat) {
        threads = oldFormat[boardID];
        for (threadID in threads) {
          data = threads[threadID];
          (newFormat[boardID] || (newFormat[boardID] = {}))[threadID] = {
            excerpt: data.textContent
          };
        }
      }
      return newFormat;
    },
    menu: {
      refreshers: [],
      init: function() {
        var menu;
        if (!Conf['Thread Watcher']) {
          return;
        }
        menu = new UI.Menu('thread watcher');
        $.on($('.menu-button', ThreadWatcher.dialog), 'click', function(e) {
          return menu.toggle(e, this, ThreadWatcher);
        });
        this.addHeaderMenuEntry();
        return this.addMenuEntries();
      },
      addHeaderMenuEntry: function() {
        var entryEl;
        if (g.VIEW !== 'thread') {
          return;
        }
        entryEl = $.el('a', {
          href: 'javascript:;'
        });
        $.event('AddMenuEntry', {
          type: 'header',
          el: entryEl,
          order: 60
        });
        $.on(entryEl, 'click', function() {
          return ThreadWatcher.toggle(g.threads["" + g.BOARD + "." + g.THREADID]);
        });
        return this.refreshers.push(function() {
          var addClass, rmClass, text, _ref;
          _ref = $('.current', ThreadWatcher.list) ? ['unwatch-thread', 'watch-thread', 'Unwatch thread'] : ['watch-thread', 'unwatch-thread', 'Watch thread'], addClass = _ref[0], rmClass = _ref[1], text = _ref[2];
          $.addClass(entryEl, addClass);
          $.rmClass(entryEl, rmClass);
          return entryEl.textContent = text;
        });
      },
      addMenuEntries: function() {
        var cb, conf, entries, entry, name, refresh, subEntries, _i, _len, _ref, _ref1, _results;
        entries = [];
        entries.push({
          cb: ThreadWatcher.cb.openAll,
          entry: {
            type: 'thread watcher',
            el: $.el('a', {
              textContent: 'Open all threads'
            })
          },
          refresh: function() {
            return (ThreadWatcher.list.firstElementChild ? $.rmClass : $.addClass)(this.el, 'disabled');
          }
        });
        entries.push({
          cb: ThreadWatcher.cb.checkThreads,
          entry: {
            type: 'thread watcher',
            el: $.el('a', {
              textContent: 'Check 404\'d threads'
            })
          },
          refresh: function() {
            return ($('div:not(.dead-thread)', ThreadWatcher.list) ? $.rmClass : $.addClass)(this.el, 'disabled');
          }
        });
        entries.push({
          cb: ThreadWatcher.cb.pruneDeads,
          entry: {
            type: 'thread watcher',
            el: $.el('a', {
              textContent: 'Prune 404\'d threads'
            })
          },
          refresh: function() {
            return ($('.dead-thread', ThreadWatcher.list) ? $.rmClass : $.addClass)(this.el, 'disabled');
          }
        });
        subEntries = [];
        _ref = Config.threadWatcher;
        for (name in _ref) {
          conf = _ref[name];
          subEntries.push(this.createSubEntry(name, conf[1]));
        }
        entries.push({
          entry: {
            type: 'thread watcher',
            el: $.el('span', {
              textContent: 'Settings'
            }),
            subEntries: subEntries
          }
        });
        _results = [];
        for (_i = 0, _len = entries.length; _i < _len; _i++) {
          _ref1 = entries[_i], entry = _ref1.entry, cb = _ref1.cb, refresh = _ref1.refresh;
          if (entry.el.nodeName === 'A') {
            entry.el.href = 'javascript:;';
          }
          if (cb) {
            $.on(entry.el, 'click', cb);
          }
          if (refresh) {
            this.refreshers.push(refresh.bind(entry));
          }
          _results.push($.event('AddMenuEntry', entry));
        }
        return _results;
      },
      createSubEntry: function(name, desc) {
        var entry, input;
        entry = {
          type: 'thread watcher',
          el: $.el('label', {
            innerHTML: "<input type=checkbox name='" + name + "'> " + name,
            title: desc
          })
        };
        input = entry.el.firstElementChild;
        input.checked = Conf[name];
        $.on(input, 'change', $.cb.checked);
        if (name === 'Current Board') {
          $.on(input, 'change', ThreadWatcher.refresh);
        }
        return entry;
      }
    }
  };

  Unread = {
    init: function() {
      if (g.VIEW !== 'thread' || !Conf['Unread Count'] && !Conf['Unread Tab Icon'] && !Conf['Desktop Notifications']) {
        return;
      }
      this.db = new DataBoard('lastReadPosts', this.sync);
      this.hr = $.el('hr', {
        id: 'unread-line'
      });
      this.posts = [];
      this.postsQuotingYou = [];
      return Thread.callbacks.push({
        name: 'Unread',
        cb: this.node
      });
    },
    node: function() {
      Unread.thread = this;
      Unread.title = d.title;
      Unread.lastReadPost = Unread.db.get({
        boardID: this.board.ID,
        threadID: this.ID,
        defaultValue: 0
      });
      $.on(d, '4chanXInitFinished', Unread.ready);
      $.on(d, 'ThreadUpdate', Unread.onUpdate);
      $.on(d, 'scroll visibilitychange', Unread.read);
      if (Conf['Unread Line']) {
        return $.on(d, 'visibilitychange', Unread.setLine);
      }
    },
    ready: function() {
      var ID, post, posts, _ref;
      $.off(d, '4chanXInitFinished', Unread.ready);
      posts = [];
      _ref = Unread.thread.posts;
      for (ID in _ref) {
        post = _ref[ID];
        if (post.isReply) {
          posts.push(post);
        }
      }
      Unread.addPosts(posts);
      return Unread.scroll();
    },
    scroll: function() {
      var down, hash, post, posts, root;
      if (!Conf['Scroll to Last Read Post']) {
        return;
      }
      if ((hash = location.hash.match(/\d+/)) && hash[0] in Unread.thread.posts) {
        return;
      }
      if (Unread.posts.length) {
        post = Unread.posts[0];
        while (root = $.x('preceding-sibling::div[contains(@class,"replyContainer")][1]', post.nodes.root)) {
          if (!(post = Get.postFromRoot(root)).isHidden) {
            break;
          }
        }
        if (!root) {
          return;
        }
        down = true;
      } else {
        posts = Object.keys(Unread.thread.posts);
        root = Unread.thread.posts[posts[posts.length - 1]].nodes.root;
      }
      return $.on(window, 'load', function() {
        if (Header.getBottomOf(root) < 0) {
          return Header.scrollTo(root, down);
        }
      });
    },
    sync: function() {
      var lastReadPost;
      lastReadPost = Unread.db.get({
        boardID: Unread.thread.board.ID,
        threadID: Unread.thread.ID,
        defaultValue: 0
      });
      if (!(Unread.lastReadPost < lastReadPost)) {
        return;
      }
      Unread.lastReadPost = lastReadPost;
      Unread.readArray(Unread.posts);
      Unread.readArray(Unread.postsQuotingYou);
      if (Conf['Unread Line']) {
        Unread.setLine();
      }
      return Unread.update();
    },
    addPosts: function(posts) {
      var ID, data, post, _i, _len, _ref;
      for (_i = 0, _len = posts.length; _i < _len; _i++) {
        post = posts[_i];
        ID = post.ID;
        if (ID <= Unread.lastReadPost || post.isHidden) {
          continue;
        }
        if (QR.db) {
          data = {
            boardID: post.board.ID,
            threadID: post.thread.ID,
            postID: post.ID
          };
          if (QR.db.get(data)) {
            continue;
          }
        }
        Unread.posts.push(post);
        Unread.addPostQuotingYou(post);
      }
      if (Conf['Unread Line']) {
        Unread.setLine((_ref = Unread.posts[0], __indexOf.call(posts, _ref) >= 0));
      }
      Unread.read();
      return Unread.update();
    },
    addPostQuotingYou: function(post) {
      var quotelink, _i, _len, _ref;
      if (!QR.db) {
        return;
      }
      _ref = post.nodes.quotelinks;
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        quotelink = _ref[_i];
        if (!(QR.db.get(Get.postDataFromLink(quotelink)))) {
          continue;
        }
        Unread.postsQuotingYou.push(post);
        Unread.openNotification(post);
        return;
      }
    },
    openNotification: function(post) {
      var name, notif;
      if (!Header.areNotificationsEnabled) {
        return;
      }
      name = Conf['Anonymize'] ? 'Anonymous' : $('.nameBlock', post.nodes.info).textContent.trim();
      notif = new Notification("" + name + " replied to you", {
        body: post.info.comment,
        icon: Favicon.logo
      });
      notif.onclick = function() {
        Header.scrollToIfNeeded(post.nodes.root, true);
        return window.focus();
      };
      return notif.onshow = function() {
        return setTimeout(function() {
          return notif.close();
        }, 7 * $.SECOND);
      };
    },
    onUpdate: function(e) {
      if (e.detail[404]) {
        return Unread.update();
      } else {
        return Unread.addPosts(e.detail.newPosts);
      }
    },
    readSinglePost: function(post) {
      var i;
      if ((i = Unread.posts.indexOf(post)) === -1) {
        return;
      }
      Unread.posts.splice(i, 1);
      if (i === 0) {
        Unread.lastReadPost = post.ID;
        Unread.saveLastReadPost();
      }
      if ((i = Unread.postsQuotingYou.indexOf(post)) !== -1) {
        Unread.postsQuotingYou.splice(i, 1);
      }
      return Unread.update();
    },
    readArray: function(arr) {
      var i, post, _i, _len;
      for (i = _i = 0, _len = arr.length; _i < _len; i = ++_i) {
        post = arr[i];
        if (post.ID > Unread.lastReadPost) {
          break;
        }
      }
      return arr.splice(0, i);
    },
    read: function(e) {
      var i, post, _i, _len, _ref;
      if (d.hidden || !Unread.posts.length) {
        return;
      }
      _ref = Unread.posts;
      for (i = _i = 0, _len = _ref.length; _i < _len; i = ++_i) {
        post = _ref[i];
        if (Header.getBottomOf(post.nodes.root) < -1) {
          break;
        }
      }
      if (!i) {
        return;
      }
      Unread.lastReadPost = Unread.posts.splice(0, i)[i - 1].ID;
      Unread.saveLastReadPost();
      Unread.readArray(Unread.postsQuotingYou);
      if (e) {
        return Unread.update();
      }
    },
    saveLastReadPost: function() {
      if (Unread.thread.isDead) {
        return;
      }
      return Unread.db.set({
        boardID: Unread.thread.board.ID,
        threadID: Unread.thread.ID,
        val: Unread.lastReadPost
      });
    },
    setLine: function(force) {
      var post;
      if (!(d.hidden || force === true)) {
        return;
      }
      if (!(post = Unread.posts[0])) {
        $.rm(Unread.hr);
        return;
      }
      if ($.x('preceding-sibling::div[contains(@class,"replyContainer")]', post.nodes.root)) {
        return $.before(post.nodes.root, Unread.hr);
      }
    },
    update: function() {
      var count;
      count = Unread.posts.length;
      if (Conf['Unread Count']) {
        d.title = "" + (count || !Conf['Hide Unread Count at (0)'] ? "(" + count + ") " : '') + (g.DEAD ? "/" + g.BOARD + "/ - 404" : "" + Unread.title);
      }
      if (!Conf['Unread Tab Icon']) {
        return;
      }
      Favicon.el.href = g.DEAD ? Unread.postsQuotingYou[0] ? Favicon.unreadDeadY : count ? Favicon.unreadDead : Favicon.dead : count ? Unread.postsQuotingYou[0] ? Favicon.unreadY : Favicon.unread : Favicon["default"];
      return $.add(d.head, Favicon.el);
    }
  };

  Redirect = {
    archives: [{"uid":0,"name":"Foolz","domain":"archive.foolz.us","http":true,"https":true,"software":"foolfuuka","boards":["a","co","gd","jp","m","sp","tg","tv","v","vg","vp","vr","wsg"],"files":["a","gd","jp","m","tg","vg","vp","vr","wsg"]},{"uid":1,"name":"NSFW Foolz","domain":"nsfw.foolz.us","http":true,"https":true,"software":"foolfuuka","boards":["u"],"files":["u"]},{"uid":2,"name":"The Dark Cave","domain":"archive.thedarkcave.org","http":true,"https":true,"software":"foolfuuka","boards":["c","int","out","po"],"files":["c","po"]},{"uid":3,"name":"4plebs","domain":"archive.4plebs.org","http":true,"https":true,"software":"foolfuuka","boards":["hr","tg","tv","x"],"files":["hr","tg","tv","x"]},{"uid":4,"name":"Nyafuu","domain":"archive.nyafuu.org","http":true,"https":true,"software":"foolfuuka","boards":["c","w","wg"],"files":["c","w","wg"]},{"uid":7,"name":"Install Gentoo","domain":"archive.installgentoo.net","http":false,"https":true,"software":"fuuka","boards":["diy","g","sci"],"files":[]},{"uid":8,"name":"Rebecca Black Tech","domain":"rbt.asia","http":true,"https":true,"software":"fuuka","boards":["cgl","g","mu","w"],"files":["cgl","g","mu","w"]},{"uid":9,"name":"Heinessen","domain":"archive.heinessen.com","http":true,"https":false,"software":"fuuka","boards":["an","fit","k","mlp","r9k","toy"],"files":["an","fit","k","r9k","toy"]},{"uid":10,"name":"warosu","domain":"fuuka.warosu.org","http":true,"https":true,"software":"fuuka","boards":["3","cgl","ck","fa","ic","jp","lit","tg","vr"],"files":["3","cgl","ck","fa","ic","jp","lit","tg","vr"]},{"uid":13,"name":"Foolz Beta","domain":"beta.foolz.us","http":true,"https":true,"withCredentials":true,"software":"foolfuuka","boards":["a","co","gd","jp","m","sp","tg","tv","u","v","vg","vp","vr","wsg"],"files":["a","gd","jp","m","tg","u","vg","vp","vr","wsg"]}],
    data: {
      thread: {},
      post: {},
      file: {}
    },
    init: function() {
      var archive, arr, boardID, data, type, uid, _i, _j, _k, _len, _len1, _len2, _ref, _ref1, _ref2, _ref3;
      _ref = Conf['selectedArchives'];
      for (boardID in _ref) {
        data = _ref[boardID];
        for (type in data) {
          uid = data[type];
          _ref1 = Conf['archives'];
          for (_i = 0, _len = _ref1.length; _i < _len; _i++) {
            archive = _ref1[_i];
            if (archive.uid !== uid || type === 'post' && archive.software !== 'foolfuuka') {
              continue;
            }
            arr = type === 'file' ? archive.files : archive.boards;
            if (__indexOf.call(arr, boardID) >= 0) {
              Redirect.data[type][boardID] = archive;
            }
          }
        }
      }
      _ref2 = Conf['archives'];
      for (_j = 0, _len1 = _ref2.length; _j < _len1; _j++) {
        archive = _ref2[_j];
        _ref3 = archive.boards;
        for (_k = 0, _len2 = _ref3.length; _k < _len2; _k++) {
          boardID = _ref3[_k];
          if (!(boardID in Redirect.data.thread)) {
            Redirect.data.thread[boardID] = archive;
          }
          if (!(boardID in Redirect.data.post || archive.software !== 'foolfuuka')) {
            Redirect.data.post[boardID] = archive;
          }
          if (!(boardID in Redirect.data.file || __indexOf.call(archive.files, boardID) < 0)) {
            Redirect.data.file[boardID] = archive;
          }
        }
      }
      return Redirect.update();
    },
    update: function(cb) {
      return $.get('lastarchivecheck', 0, function(_arg) {
        var lastarchivecheck, now;
        lastarchivecheck = _arg.lastarchivecheck;
        now = Date.now();
        if (lastarchivecheck > now - 2 * $.DAY) {
          return;
        }
        return $.ajax('https://4chan-x.just-believe.in/json/archives.json', {
          onload: function() {
            if (this.status !== 200) {
              return;
            }
            Conf['archives'] = JSON.parse(this.response);
            $.set({
              lastarchivecheck: now,
              archives: Conf['archives']
            });
            return typeof cb === "function" ? cb(now) : void 0;
          }
        });
      });
    },
    to: function(dest, data) {
      var archive;
      archive = (dest === 'search' ? Redirect.data.thread : Redirect.data[dest])[data.boardID];
      if (!archive) {
        return '';
      }
      return Redirect[dest](archive, data);
    },
    protocol: function(archive) {
      var protocol;
      protocol = location.protocol;
      if (!archive[protocol.slice(0, -1)]) {
        protocol = protocol === 'https:' ? 'http:' : 'https:';
      }
      return "" + protocol + "//";
    },
    thread: function(archive, _arg) {
      var boardID, path, postID, threadID;
      boardID = _arg.boardID, threadID = _arg.threadID, postID = _arg.postID;
      path = threadID ? "" + boardID + "/thread/" + threadID : "" + boardID + "/post/" + postID;
      if (archive.software === 'foolfuuka') {
        path += '/';
      }
      if (threadID && postID) {
        path += archive.software === 'foolfuuka' ? "#" + postID : "#p" + postID;
      }
      return "" + (Redirect.protocol(archive)) + archive.domain + "/" + path;
    },
    post: function(archive, _arg) {
      var URL, boardID, postID;
      boardID = _arg.boardID, postID = _arg.postID;
      URL = new String("" + (Redirect.protocol(archive)) + archive.domain + "/_/api/chan/post/?board=" + boardID + "&num=" + postID);
      URL.archive = archive;
      return URL;
    },
    file: function(archive, _arg) {
      var boardID, filename;
      boardID = _arg.boardID, filename = _arg.filename;
      return "" + (Redirect.protocol(archive)) + archive.domain + "/" + boardID + "/full_image/" + filename;
    },
    search: function(archive, _arg) {
      var boardID, path, type, value;
      boardID = _arg.boardID, type = _arg.type, value = _arg.value;
      type = type === 'name' ? 'username' : type === 'MD5' ? 'image' : type;
      value = encodeURIComponent(value);
      path = archive.software === 'foolfuuka' ? "" + boardID + "/search/" + type + "/" + value : "" + boardID + "/?task=search2&search_" + (type === 'image' ? 'media_hash' : type) + "=" + value;
      return "" + (Redirect.protocol(archive)) + archive.domain + "/" + path;
    }
  };

  Anonymize = {
    init: function() {
      if (g.VIEW === 'catalog' || !Conf['Anonymize']) {
        return;
      }
      return Post.callbacks.push({
        name: 'Anonymize',
        cb: this.node
      });
    },
    node: function() {
      var email, name, tripcode, _ref;
      if (this.info.capcode || this.isClone) {
        return;
      }
      _ref = this.nodes, name = _ref.name, tripcode = _ref.tripcode, email = _ref.email;
      if (this.info.name !== 'Anonymous') {
        name.textContent = 'Anonymous';
      }
      if (tripcode) {
        $.rm(tripcode);
        delete this.nodes.tripcode;
      }
      if (this.info.email) {
        $.replace(email, name);
        return delete this.nodes.email;
      }
    }
  };

  CustomCSS = {
    init: function() {
      if (!Conf['Custom CSS']) {
        return;
      }
      return this.addStyle();
    },
    addStyle: function() {
      return this.style = $.addStyle(Conf['usercss']);
    },
    rmStyle: function() {
      if (this.style) {
        $.rm(this.style);
        return delete this.style;
      }
    },
    update: function() {
      if (!this.style) {
        this.addStyle();
      }
      return this.style.textContent = Conf['usercss'];
    }
  };

  Dice = {
    init: function() {
      if (g.BOARD.ID !== 'tg' || g.VIEW === 'catalog' || !Conf['Show Dice Roll']) {
        return;
      }
      return Post.callbacks.push({
        name: 'Show Dice Roll',
        cb: this.node
      });
    },
    node: function() {
      var dicestats, roll, _ref;
      if (this.isClone || !(dicestats = (_ref = this.info.email) != null ? _ref.match(/dice[+\s](\d+)d(\d+)/) : void 0)) {
        return;
      }
      roll = $('b', this.nodes.comment).firstChild;
      return roll.data = "Rolled " + dicestats[1] + "d" + dicestats[2] + ": " + (roll.data.slice(7));
    }
  };

  ExpandThread = {
    init: function() {
      if (g.VIEW !== 'index' || !Conf['Thread Expansion']) {
        return;
      }
      this.statuses = {};
      $.on(d, 'IndexRefresh', this.onIndexRefresh);
      return Thread.callbacks.push({
        name: 'Thread Expansion',
        cb: this.node
      });
    },
    node: function() {
      return ExpandThread.setButton(this);
    },
    setButton: function(thread) {
      var a;
      if (!(a = $.x('following-sibling::a[contains(@class,"summary")][1]', thread.OP.nodes.root))) {
        return;
      }
      a.textContent = ExpandThread.text.apply(ExpandThread, ['+'].concat(__slice.call(a.textContent.match(/\d+/g))));
      return $.on(a, 'click', ExpandThread.cbToggle);
    },
    onIndexRefresh: function() {
      var status, thread, threadID, _ref, _ref1, _ref2;
      _ref = ExpandThread.statuses;
      for (threadID in _ref) {
        status = _ref[threadID];
        if ((_ref1 = status.req) != null) {
          _ref1.abort();
        }
        delete ExpandThread.statuses[threadID];
      }
      _ref2 = g.BOARD.threads;
      for (threadID in _ref2) {
        thread = _ref2[threadID];
        ExpandThread.setButton(thread);
      }
    },
    text: function(status, posts, files) {
      var text;
      text = [status];
      text.push("" + posts + " post" + (posts > 1 ? 's' : ''));
      if (+files) {
        text.push("and " + files + " image repl" + (files > 1 ? 'ies' : 'y'));
      }
      text.push(status === '-' ? 'shown' : 'omitted');
      return text.join(' ') + '.';
    },
    cbToggle: function(e) {
      if (e.shiftKey || e.altKey || e.ctrlKey || e.metaKey || e.button !== 0) {
        return;
      }
      e.preventDefault();
      return ExpandThread.toggle(Get.threadFromNode(this));
    },
    toggle: function(thread) {
      var a, threadRoot;
      threadRoot = thread.OP.nodes.root.parentNode;
      if (!(a = $('.summary', threadRoot))) {
        return;
      }
      if (thread.ID in ExpandThread.statuses) {
        return ExpandThread.contract(thread, a, threadRoot);
      } else {
        return ExpandThread.expand(thread, a, threadRoot);
      }
    },
    expand: function(thread, a, threadRoot) {
      var status;
      ExpandThread.statuses[thread] = status = {};
      a.textContent = ExpandThread.text.apply(ExpandThread, ['...'].concat(__slice.call(a.textContent.match(/\d+/g))));
      return status.req = $.cache("//api.4chan.org/" + thread.board + "/res/" + thread + ".json", function() {
        delete status.req;
        return ExpandThread.parse(this, thread, a);
      });
    },
    contract: function(thread, a, threadRoot) {
      var filesCount, inlined, num, postsCount, reply, status, _i, _len, _ref;
      status = ExpandThread.statuses[thread];
      delete ExpandThread.statuses[thread];
      if (status.req) {
        status.req.abort();
        if (a) {
          a.textContent = ExpandThread.text.apply(ExpandThread, ['+'].concat(__slice.call(a.textContent.match(/\d+/g))));
        }
        return;
      }
      num = (function() {
        if (thread.isSticky) {
          return 1;
        } else {
          switch (g.BOARD.ID) {
            case 'b':
            case 'vg':
              return 3;
            case 't':
              return 1;
            default:
              return 5;
          }
        }
      })();
      postsCount = 0;
      filesCount = 0;
      _ref = $$('.thread > .replyContainer', threadRoot).slice(0, -num);
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        reply = _ref[_i];
        if (Conf['Quote Inlining']) {
          while (inlined = $('.inlined', reply)) {
            inlined.click();
          }
        }
        postsCount++;
        if ('file' in Get.postFromRoot(reply)) {
          filesCount++;
        }
        $.rm(reply);
      }
      return a.textContent = ExpandThread.text('+', postsCount, filesCount);
    },
    parse: function(req, thread, a) {
      var data, filesCount, post, postData, posts, postsCount, postsRoot, root, _i, _len, _ref;
      if ((_ref = req.status) !== 200 && _ref !== 304) {
        a.textContent = "Error " + req.statusText + " (" + req.status + ")";
        return;
      }
      data = JSON.parse(req.response).posts;
      Build.spoilerRange[thread.board] = data.shift().custom_spoiler;
      posts = [];
      postsRoot = [];
      filesCount = 0;
      for (_i = 0, _len = data.length; _i < _len; _i++) {
        postData = data[_i];
        if (post = thread.posts[postData.no]) {
          if ('file' in post) {
            filesCount++;
          }
          postsRoot.push(post.nodes.root);
          continue;
        }
        root = Build.postFromObject(postData, thread.board.ID);
        post = new Post(root, thread, thread.board);
        if ('file' in post) {
          filesCount++;
        }
        posts.push(post);
        postsRoot.push(root);
      }
      Main.callbackNodes(Post, posts);
      $.after(a, postsRoot);
      postsCount = postsRoot.length;
      a.textContent = ExpandThread.text('-', postsCount, filesCount);
      if (Conf['Enable 4chan\'s Extension']) {
        return $.globalEval("Parser.parseThread(" + thread + ", 1, " + postsCount + ")");
      } else {
        return Fourchan.parseThread(thread.ID, 1, postsCount);
      }
    }
  };

  FileInfo = {
    init: function() {
      if (g.VIEW === 'catalog' || !Conf['File Info Formatting']) {
        return;
      }
      this.funk = this.createFunc(Conf['fileInfo']);
      return Post.callbacks.push({
        name: 'File Info Formatting',
        cb: this.node
      });
    },
    node: function() {
      if (!this.file || this.isClone) {
        return;
      }
      return this.file.text.innerHTML = FileInfo.funk(FileInfo, this);
    },
    createFunc: function(format) {
      var code;
      code = format.replace(/%(.)/g, function(s, c) {
        if (c in FileInfo.formatters) {
          return "' + FileInfo.formatters." + c + ".call(post) + '";
        } else {
          return s;
        }
      });
      return Function('FileInfo', 'post', "return '" + code + "'");
    },
    convertUnit: function(size, unit) {
      var i;
      if (unit === 'B') {
        return "" + (size.toFixed()) + " Bytes";
      }
      i = 1 + ['KB', 'MB'].indexOf(unit);
      while (i--) {
        size /= 1024;
      }
      size = unit === 'MB' ? Math.round(size * 100) / 100 : size.toFixed();
      return "" + size + " " + unit;
    },
    escape: function(name) {
      return name.replace(/<|>/g, function(c) {
        return c === '<' && '&lt;' || '&gt;';
      });
    },
    formatters: {
      t: function() {
        return this.file.URL.match(/\d+\..+$/)[0];
      },
      T: function() {
        return "<a href=" + this.file.URL + " target=_blank>" + (FileInfo.formatters.t.call(this)) + "</a>";
      },
      l: function() {
        return "<a href=" + this.file.URL + " target=_blank>" + (FileInfo.formatters.n.call(this)) + "</a>";
      },
      L: function() {
        return "<a href=" + this.file.URL + " target=_blank>" + (FileInfo.formatters.N.call(this)) + "</a>";
      },
      n: function() {
        var fullname, shortname;
        fullname = this.file.name;
        shortname = Build.shortFilename(this.file.name, this.isReply);
        if (fullname === shortname) {
          return FileInfo.escape(fullname);
        } else {
          return "<span class=fntrunc>" + (FileInfo.escape(shortname)) + "</span><span class=fnfull>" + (FileInfo.escape(fullname)) + "</span>";
        }
      },
      N: function() {
        return FileInfo.escape(this.file.name);
      },
      p: function() {
        if (this.file.isSpoiler) {
          return 'Spoiler, ';
        } else {
          return '';
        }
      },
      s: function() {
        return this.file.size;
      },
      B: function() {
        return FileInfo.convertUnit(this.file.sizeInBytes, 'B');
      },
      K: function() {
        return FileInfo.convertUnit(this.file.sizeInBytes, 'KB');
      },
      M: function() {
        return FileInfo.convertUnit(this.file.sizeInBytes, 'MB');
      },
      r: function() {
        if (this.file.isImage) {
          return this.file.dimensions;
        } else {
          return 'PDF';
        }
      }
    }
  };

  Fourchan = {
    init: function() {
      var board;
      if (g.VIEW === 'catalog') {
        return;
      }
      board = g.BOARD.ID;
      if (board === 'g') {
        $.globalEval("window.addEventListener('prettyprint', function(e) {\n  window.dispatchEvent(new CustomEvent('prettyprint:cb', {\n    detail: prettyPrintOne(e.detail)\n  }));\n}, false);");
        Post.callbacks.push({
          name: 'Parse /g/ code',
          cb: this.code
        });
      }
      if (board === 'sci') {
        $.globalEval("window.addEventListener('jsmath', function(e) {\n  if (jsMath.loaded) {\n    // process one post\n    jsMath.ProcessBeforeShowing(e.detail);\n  } else {\n    // load jsMath and process whole document\n    jsMath.Autoload.Script.Push('ProcessBeforeShowing', [null]);\n    jsMath.Autoload.LoadJsMath();\n  }\n}, false);");
        return Post.callbacks.push({
          name: 'Parse /sci/ math',
          cb: this.math
        });
      }
    },
    code: function() {
      var apply, pre, _i, _len, _ref;
      if (this.isClone) {
        return;
      }
      apply = function(e) {
        return pre.innerHTML = e.detail;
      };
      $.on(window, 'prettyprint:cb', apply);
      _ref = $$('.prettyprint:not(.prettyprinted)', this.nodes.comment);
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        pre = _ref[_i];
        $.event('prettyprint', pre.innerHTML, window);
      }
      $.off(window, 'prettyprint:cb', apply);
    },
    math: function() {
      if (this.isClone || !$('.math', this.nodes.comment)) {
        return;
      }
      return $.event('jsmath', this.nodes.post, window);
    },
    parseThread: function(threadID, offset, limit) {
      return $.event('4chanParsingDone', {
        threadId: threadID,
        offset: offset,
        limit: limit
      });
    }
  };

  IDColor = {
    init: function() {
      if (g.VIEW === 'catalog' || !Conf['Color User IDs']) {
        return;
      }
      this.ids = {};
      return Post.callbacks.push({
        name: 'Color User IDs',
        cb: this.node
      });
    },
    node: function() {
      var rgb, span, style, uid;
      if (this.isClone || !(uid = this.info.uniqueID)) {
        return;
      }
      rgb = IDColor.compute(uid);
      span = this.nodes.uniqueID;
      style = span.style;
      style.color = rgb[3];
      style.backgroundColor = "rgb(" + rgb[0] + "," + rgb[1] + "," + rgb[2] + ")";
      $.addClass(span, 'painted');
      span.textContent = uid;
      return span.title = 'Highlight posts by this ID';
    },
    compute: function(uniqueID) {
      var hash, rgb;
      if (uniqueID in IDColor.ids) {
        return IDColor.ids[uniqueID];
      }
      hash = this.hash(uniqueID);
      rgb = [(hash >> 24) & 0xFF, (hash >> 16) & 0xFF, (hash >> 8) & 0xFF];
      rgb.push((rgb[0] * 0.299 + rgb[1] * 0.587 + rgb[2] * 0.114) > 170 ? 'black' : 'white');
      return this.ids[uniqueID] = rgb;
    },
    hash: function(uniqueID) {
      var i, msg, _i, _ref;
      msg = 0;
      for (i = _i = 0, _ref = uniqueID.length; _i < _ref; i = _i += 1) {
        msg = (msg << 5) - msg + uniqueID.charCodeAt(i);
      }
      return msg;
    }
  };

  Keybinds = {
    init: function() {
      var hotkey, init;
      if (g.VIEW === 'catalog' || !Conf['Keybinds']) {
        return;
      }
      for (hotkey in Conf.hotkeys) {
        $.sync(hotkey, Keybinds.sync);
      }
      init = function() {
        var node, _i, _len, _ref;
        $.off(d, '4chanXInitFinished', init);
        $.on(d, 'keydown', Keybinds.keydown);
        _ref = $$('[accesskey]');
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
          node = _ref[_i];
          node.removeAttribute('accesskey');
        }
      };
      return $.on(d, '4chanXInitFinished', init);
    },
    sync: function(key, hotkey) {
      return Conf[hotkey] = key;
    },
    keydown: function(e) {
      var key, notification, notifications, op, target, thread, threadRoot, _i, _len, _ref;
      if (!(key = Keybinds.keyCode(e))) {
        return;
      }
      target = e.target;
      if ((_ref = target.nodeName) === 'INPUT' || _ref === 'TEXTAREA') {
        if (!/(Esc|Alt|Ctrl|Meta)/.test(key)) {
          return;
        }
      }
      threadRoot = Nav.getThread();
      if (op = $('.op', threadRoot)) {
        thread = Get.postFromNode(op).thread;
      }
      switch (key) {
        case Conf['Toggle board list']:
          if (Conf['Custom Board Navigation']) {
            Header.toggleBoardList();
          }
          break;
        case Conf['Open empty QR']:
          Keybinds.qr(threadRoot);
          break;
        case Conf['Open QR']:
          Keybinds.qr(threadRoot, true);
          break;
        case Conf['Open settings']:
          Settings.open();
          break;
        case Conf['Close']:
          if (Settings.dialog) {
            Settings.close();
          } else if ((notifications = $$('.notification')).length) {
            for (_i = 0, _len = notifications.length; _i < _len; _i++) {
              notification = notifications[_i];
              $('.close', notification).click();
            }
          } else if (QR.nodes) {
            QR.close();
          }
          break;
        case Conf['Spoiler tags']:
          if (target.nodeName !== 'TEXTAREA') {
            return;
          }
          Keybinds.tags('spoiler', target);
          break;
        case Conf['Code tags']:
          if (target.nodeName !== 'TEXTAREA') {
            return;
          }
          Keybinds.tags('code', target);
          break;
        case Conf['Eqn tags']:
          if (target.nodeName !== 'TEXTAREA') {
            return;
          }
          Keybinds.tags('eqn', target);
          break;
        case Conf['Math tags']:
          if (target.nodeName !== 'TEXTAREA') {
            return;
          }
          Keybinds.tags('math', target);
          break;
        case Conf['Submit QR']:
          if (QR.nodes && !QR.status()) {
            QR.submit();
          }
          break;
        case Conf['Update']:
          switch (g.VIEW) {
            case 'thread':
              ThreadUpdater.update();
              break;
            case 'index':
              Index.update();
          }
          break;
        case Conf['Watch']:
          ThreadWatcher.toggle(thread);
          break;
        case Conf['Expand image']:
          Keybinds.img(threadRoot);
          break;
        case Conf['Expand images']:
          Keybinds.img(threadRoot, true);
          break;
        case Conf['Front page']:
          if (g.VIEW === 'index') {
            Index.pageNav(0);
          } else {
            window.location = "/" + g.BOARD + "/";
          }
          break;
        case Conf['Open front page']:
          $.open("/" + g.BOARD + "/");
          break;
        case Conf['Next page']:
          if (!(g.VIEW === 'index' && Conf['Index Mode'] === 'paged')) {
            return;
          }
          $('.next button', Index.pagelist).click();
          break;
        case Conf['Previous page']:
          if (!(g.VIEW === 'index' && Conf['Index Mode'] === 'paged')) {
            return;
          }
          $('.prev button', Index.pagelist).click();
          break;
        case Conf['Search form']:
          $.id('search-btn').click();
          break;
        case Conf['Next thread']:
          if (g.VIEW !== 'index') {
            return;
          }
          Nav.scroll(+1);
          break;
        case Conf['Previous thread']:
          if (g.VIEW !== 'index') {
            return;
          }
          Nav.scroll(-1);
          break;
        case Conf['Expand thread']:
          ExpandThread.toggle(thread);
          break;
        case Conf['Open thread']:
          Keybinds.open(thread);
          break;
        case Conf['Open thread tab']:
          Keybinds.open(thread, true);
          break;
        case Conf['Next reply']:
          Keybinds.hl(+1, threadRoot);
          break;
        case Conf['Previous reply']:
          Keybinds.hl(-1, threadRoot);
          break;
        case Conf['Deselect reply']:
          Keybinds.hl(0, threadRoot);
          break;
        case Conf['Hide']:
          if (g.VIEW === 'index') {
            ThreadHiding.toggle(thread);
          }
          break;
        default:
          return;
      }
      e.preventDefault();
      return e.stopPropagation();
    },
    keyCode: function(e) {
      var kc, key;
      key = (function() {
        switch (kc = e.keyCode) {
          case 8:
            return '';
          case 13:
            return 'Enter';
          case 27:
            return 'Esc';
          case 37:
            return 'Left';
          case 38:
            return 'Up';
          case 39:
            return 'Right';
          case 40:
            return 'Down';
          default:
            if ((48 <= kc && kc <= 57) || (65 <= kc && kc <= 90)) {
              return String.fromCharCode(kc).toLowerCase();
            } else {
              return null;
            }
        }
      })();
      if (key) {
        if (e.altKey) {
          key = 'Alt+' + key;
        }
        if (e.ctrlKey) {
          key = 'Ctrl+' + key;
        }
        if (e.metaKey) {
          key = 'Meta+' + key;
        }
        if (e.shiftKey) {
          key = 'Shift+' + key;
        }
      }
      return key;
    },
    qr: function(thread, quote) {
      if (!(Conf['Quick Reply'] && QR.postingIsEnabled)) {
        return;
      }
      QR.open();
      if (quote) {
        QR.quote.call($('input', $('.post.highlight', thread) || thread));
      }
      return QR.nodes.com.focus();
    },
    tags: function(tag, ta) {
      var range, selEnd, selStart, value;
      value = ta.value;
      selStart = ta.selectionStart;
      selEnd = ta.selectionEnd;
      ta.value = value.slice(0, selStart) + ("[" + tag + "]") + value.slice(selStart, selEnd) + ("[/" + tag + "]") + value.slice(selEnd);
      range = ("[" + tag + "]").length + selEnd;
      ta.setSelectionRange(range, range);
      return $.event('input', null, ta);
    },
    img: function(thread, all) {
      var post;
      if (all) {
        return ImageExpand.cb.toggleAll();
      } else {
        post = Get.postFromNode($('.post.highlight', thread) || $('.op', thread));
        return ImageExpand.toggle(post);
      }
    },
    open: function(thread, tab) {
      var url;
      if (g.VIEW !== 'index') {
        return;
      }
      url = "/" + thread.board + "/res/" + thread;
      if (tab) {
        return $.open(url);
      } else {
        return location.href = url;
      }
    },
    hl: function(delta, thread) {
      var axe, height, next, postEl, replies, reply, root, _i, _len;
      postEl = $('.reply.highlight', thread);
      if (!delta) {
        if (postEl) {
          $.rmClass(postEl, 'highlight');
        }
        return;
      }
      if (postEl) {
        height = postEl.getBoundingClientRect().height;
        if (Header.getTopOf(postEl) >= -height && Header.getBottomOf(postEl) >= -height) {
          root = postEl.parentNode;
          axe = delta === +1 ? 'following' : 'preceding';
          if (!(next = $.x("" + axe + "-sibling::div[contains(@class,'replyContainer')][1]/child::div[contains(@class,'reply')]", root))) {
            return;
          }
          Header.scrollToIfNeeded(next, delta === +1);
          this.focus(next);
          $.rmClass(postEl, 'highlight');
          return;
        }
        $.rmClass(postEl, 'highlight');
      }
      replies = $$('.reply', thread);
      if (delta === -1) {
        replies.reverse();
      }
      for (_i = 0, _len = replies.length; _i < _len; _i++) {
        reply = replies[_i];
        if (delta === +1 && Header.getTopOf(reply) > 0 || delta === -1 && Header.getBottomOf(reply) > 0) {
          this.focus(reply);
          return;
        }
      }
    },
    focus: function(post) {
      return $.addClass(post, 'highlight');
    }
  };

  Nav = {
    init: function() {
      var append, next, prev, span;
      switch (g.VIEW) {
        case 'index':
          if (!Conf['Index Navigation']) {
            return;
          }
          break;
        case 'thread':
          if (!Conf['Reply Navigation']) {
            return;
          }
          break;
        default:
          return;
      }
      span = $.el('span', {
        id: 'navlinks'
      });
      prev = $.el('a', {
        textContent: '▲',
        href: 'javascript:;'
      });
      next = $.el('a', {
        textContent: '▼',
        href: 'javascript:;'
      });
      $.on(prev, 'click', this.prev);
      $.on(next, 'click', this.next);
      $.add(span, [prev, $.tn(' '), next]);
      append = function() {
        $.off(d, '4chanXInitFinished', append);
        return $.add(d.body, span);
      };
      return $.on(d, '4chanXInitFinished', append);
    },
    prev: function() {
      if (g.VIEW === 'thread') {
        return window.scrollTo(0, 0);
      } else {
        return Nav.scroll(-1);
      }
    },
    next: function() {
      if (g.VIEW === 'thread') {
        return window.scrollTo(0, d.body.scrollHeight);
      } else {
        return Nav.scroll(+1);
      }
    },
    getThread: function() {
      var thread, threadRoot, _i, _len, _ref;
      _ref = $$('.thread');
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        threadRoot = _ref[_i];
        thread = Get.threadFromRoot(threadRoot);
        if (thread.isHidden && !thread.stub) {
          continue;
        }
        if (Header.getTopOf(threadRoot) >= -threadRoot.getBoundingClientRect().height) {
          return threadRoot;
        }
      }
      return $('.board');
    },
    scroll: function(delta) {
      var axe, next, thread, top;
      thread = Nav.getThread();
      axe = delta === +1 ? 'following' : 'preceding';
      if (next = $.x("" + axe + "-sibling::div[contains(@class,'thread')][1]", thread)) {
        top = Header.getTopOf(thread);
        if (delta === +1 && top < 5 || delta === -1 && top > -5) {
          thread = next;
        }
      }
      return Header.scrollTo(thread);
    }
  };

  PSAHiding = {
    init: function() {
      if (!Conf['Announcement Hiding']) {
        return;
      }
      $.addClass(doc, 'hide-announcement');
      $.addClass(doc, 'hide-announcement-enabled');
      return $.on(d, '4chanXInitFinished', this.setup);
    },
    setup: function() {
      var btn, entry, psa;
      $.off(d, '4chanXInitFinished', PSAHiding.setup);
      if (!(psa = $.id('globalMessage'))) {
        $.rmClass(doc, 'hide-announcement');
        $.rmClass(doc, 'hide-announcement-enabled');
        return;
      }
      entry = {
        type: 'header',
        el: $.el('a', {
          textContent: 'Show announcement',
          className: 'show-announcement',
          href: 'javascript:;'
        }),
        order: 50,
        open: function() {
          return psa.hidden;
        }
      };
      $.event('AddMenuEntry', entry);
      $.on(entry.el, 'click', PSAHiding.toggle);
      PSAHiding.btn = btn = $.el('a', {
        innerHTML: '<span>[&nbsp;-&nbsp;]</span>',
        title: 'Hide announcement.',
        className: 'hide-announcement',
        href: 'javascript:;'
      });
      $.on(btn, 'click', PSAHiding.toggle);
      $.get('hiddenPSA', 0, function(_arg) {
        var hiddenPSA;
        hiddenPSA = _arg.hiddenPSA;
        PSAHiding.sync(hiddenPSA);
        $.before(psa, btn);
        return $.rmClass(doc, 'hide-announcement');
      });
      return $.sync('hiddenPSA', PSAHiding.sync);
    },
    toggle: function(e) {
      var UTC;
      if ($.hasClass(this, 'hide-announcement')) {
        UTC = +$.id('globalMessage').dataset.utc;
        $.set('hiddenPSA', UTC);
      } else {
        $.event('CloseMenu');
        $["delete"]('hiddenPSA');
      }
      return PSAHiding.sync(UTC);
    },
    sync: function(UTC) {
      var hr, psa;
      psa = $.id('globalMessage');
      psa.hidden = PSAHiding.btn.hidden = UTC && UTC >= +psa.dataset.utc ? true : false;
      if ((hr = psa.nextElementSibling) && hr.nodeName === 'HR') {
        return hr.hidden = psa.hidden;
      }
    }
  };

  RelativeDates = {
    INTERVAL: $.MINUTE / 2,
    init: function() {
      if (g.VIEW === 'catalog' || !Conf['Relative Post Dates']) {
        return;
      }
      $.on(d, 'visibilitychange ThreadUpdate', this.flush);
      this.flush();
      return Post.callbacks.push({
        name: 'Relative Post Dates',
        cb: this.node
      });
    },
    node: function() {
      var dateEl;
      if (this.isClone) {
        return;
      }
      dateEl = this.nodes.date;
      dateEl.title = dateEl.textContent;
      return RelativeDates.setUpdate(this);
    },
    relative: function(diff, now, date) {
      var days, months, number, rounded, unit, years;
      unit = (number = diff / $.DAY) >= 1 ? (years = now.getYear() - date.getYear(), months = now.getMonth() - date.getMonth(), days = now.getDate() - date.getDate(), years > 1 ? (number = years - (months < 0 || months === 0 && days < 0), 'year') : years === 1 && (months > 0 || months === 0 && days >= 0) ? (number = years, 'year') : (months = (months + 12) % 12) > 1 ? (number = months - (days < 0), 'month') : months === 1 && days >= 0 ? (number = months, 'month') : 'day') : (number = diff / $.HOUR) >= 1 ? 'hour' : (number = diff / $.MINUTE) >= 1 ? 'minute' : (number = Math.max(0, diff) / $.SECOND, 'second');
      rounded = Math.round(number);
      if (rounded !== 1) {
        unit += 's';
      }
      return "" + rounded + " " + unit + " ago";
    },
    stale: [],
    flush: function() {
      var now, update, _i, _len, _ref;
      if (d.hidden) {
        return;
      }
      now = new Date();
      _ref = RelativeDates.stale;
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        update = _ref[_i];
        update(now);
      }
      RelativeDates.stale = [];
      clearTimeout(RelativeDates.timeout);
      return RelativeDates.timeout = setTimeout(RelativeDates.flush, RelativeDates.INTERVAL);
    },
    setUpdate: function(post) {
      var markStale, setOwnTimeout, update;
      setOwnTimeout = function(diff) {
        var delay;
        delay = diff < $.MINUTE ? $.SECOND - (diff + $.SECOND / 2) % $.SECOND : diff < $.HOUR ? $.MINUTE - (diff + $.MINUTE / 2) % $.MINUTE : diff < $.DAY ? $.HOUR - (diff + $.HOUR / 2) % $.HOUR : $.DAY - (diff + $.DAY / 2) % $.DAY;
        return setTimeout(markStale, delay);
      };
      update = function(now) {
        var date, diff, relative, singlePost, _i, _len, _ref;
        date = post.info.date;
        diff = now - date;
        relative = RelativeDates.relative(diff, now, date);
        _ref = [post].concat(post.clones);
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
          singlePost = _ref[_i];
          singlePost.nodes.date.firstChild.textContent = relative;
        }
        return setOwnTimeout(diff);
      };
      markStale = function() {
        return RelativeDates.stale.push(update);
      };
      return update(new Date());
    }
  };

  Report = {
    init: function() {
      if (!(/report/.test(location.search) && d.cookie.indexOf('pass_enabled=1') === -1)) {
        return;
      }
      return $.asap((function() {
        return $.id('recaptcha_response_field');
      }), Report.ready);
    },
    ready: function() {
      var field;
      field = $.id('recaptcha_response_field');
      $.on(field, 'keydown', function(e) {
        if (e.keyCode === 8 && !field.value) {
          return $.globalEval('Recaptcha.reload("t")');
        }
      });
      return $.on($('form'), 'submit', function(e) {
        var response;
        e.preventDefault();
        response = field.value.trim();
        if (!/\s/.test(response)) {
          field.value = "" + response + " " + response;
        }
        return this.submit();
      });
    }
  };

  Time = {
    init: function() {
      if (g.VIEW === 'catalog' || !Conf['Time Formatting']) {
        return;
      }
      this.funk = this.createFunc(Conf['time']);
      return Post.callbacks.push({
        name: 'Time Formatting',
        cb: this.node
      });
    },
    node: function() {
      if (this.isClone) {
        return;
      }
      return this.nodes.date.textContent = Time.funk(Time, this.info.date);
    },
    createFunc: function(format) {
      var code;
      code = format.replace(/%([A-Za-z])/g, function(s, c) {
        if (c in Time.formatters) {
          return "' + Time.formatters." + c + ".call(date) + '";
        } else {
          return s;
        }
      });
      return Function('Time', 'date', "return '" + code + "'");
    },
    day: ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'],
    month: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
    zeroPad: function(n) {
      if (n < 10) {
        return "0" + n;
      } else {
        return n;
      }
    },
    formatters: {
      a: function() {
        return Time.day[this.getDay()].slice(0, 3);
      },
      A: function() {
        return Time.day[this.getDay()];
      },
      b: function() {
        return Time.month[this.getMonth()].slice(0, 3);
      },
      B: function() {
        return Time.month[this.getMonth()];
      },
      d: function() {
        return Time.zeroPad(this.getDate());
      },
      e: function() {
        return this.getDate();
      },
      H: function() {
        return Time.zeroPad(this.getHours());
      },
      I: function() {
        return Time.zeroPad(this.getHours() % 12 || 12);
      },
      k: function() {
        return this.getHours();
      },
      l: function() {
        return this.getHours() % 12 || 12;
      },
      m: function() {
        return Time.zeroPad(this.getMonth() + 1);
      },
      M: function() {
        return Time.zeroPad(this.getMinutes());
      },
      p: function() {
        if (this.getHours() < 12) {
          return 'AM';
        } else {
          return 'PM';
        }
      },
      P: function() {
        if (this.getHours() < 12) {
          return 'am';
        } else {
          return 'pm';
        }
      },
      S: function() {
        return Time.zeroPad(this.getSeconds());
      },
      y: function() {
        return this.getFullYear().toString().slice(2);
      }
    }
  };

  Board = (function() {
    Board.prototype.toString = function() {
      return this.ID;
    };

    function Board(ID) {
      this.ID = ID;
      this.threads = {};
      this.posts = {};
      g.boards[this] = this;
    }

    return Board;

  })();

  Thread = (function() {
    Thread.callbacks = [];

    Thread.prototype.toString = function() {
      return this.ID;
    };

    function Thread(ID, board) {
      this.ID = ID;
      this.board = board;
      this.fullID = "" + this.board + "." + this.ID;
      this.posts = {};
      this.isSticky = false;
      this.isClosed = false;
      this.postLimit = false;
      this.fileLimit = false;
      g.threads[this.fullID] = board.threads[this] = this;
    }

    Thread.prototype.setStatus = function(type, status) {
      var icon, name, root, typeLC;
      name = "is" + type;
      if (this[name] === status) {
        return;
      }
      this[name] = status;
      if (!this.OP) {
        return;
      }
      typeLC = type.toLowerCase();
      if (!status) {
        $.rm($("." + typeLC + "Icon", this.OP.nodes.info));
        return;
      }
      icon = $.el('img', {
        src: "//static.4chan.org/image/" + typeLC + (window.devicePixelRatio >= 2 ? '@2x' : '') + ".gif",
        alt: type,
        title: type,
        className: "" + typeLC + "Icon"
      });
      root = type === 'Closed' && this.isSticky ? $('.stickyIcon', this.OP.nodes.info) : $('[title="Quote this post"]', this.OP.nodes.info);
      return $.after(root, [$.tn(' '), icon]);
    };

    Thread.prototype.kill = function() {
      this.isDead = true;
      return this.timeOfDeath = Date.now();
    };

    Thread.prototype.collect = function() {
      var post, postID, _i, _len, _ref;
      _ref = this.posts;
      for (post = _i = 0, _len = _ref.length; _i < _len; post = ++_i) {
        postID = _ref[post];
        post.collect();
      }
      delete g.threads[this.fullID];
      return delete this.board.threads[this];
    };

    return Thread;

  })();

  Post = (function() {
    Post.callbacks = [];

    Post.prototype.toString = function() {
      return this.ID;
    };

    function Post(root, thread, board, that) {
      var capcode, date, email, flag, info, name, post, subject, tripcode, uniqueID;
      this.thread = thread;
      this.board = board;
      if (that == null) {
        that = {};
      }
      this.ID = +root.id.slice(2);
      this.fullID = "" + this.board + "." + this.ID;
      post = $('.post', root);
      info = $('.postInfo', post);
      this.nodes = {
        root: root,
        post: post,
        info: info,
        comment: $('.postMessage', post),
        quotelinks: [],
        backlinks: info.getElementsByClassName('backlink')
      };
      if (!(this.isReply = $.hasClass(post, 'reply'))) {
        this.thread.OP = this;
        this.thread.isSticky = !!$('.stickyIcon', info);
        this.thread.isClosed = !!$('.closedIcon', info);
      }
      this.info = {};
      if (subject = $('.subject', info)) {
        this.nodes.subject = subject;
        this.info.subject = subject.textContent;
      }
      if (name = $('.name', info)) {
        this.nodes.name = name;
        this.info.name = name.textContent;
      }
      if (email = $('.useremail', info)) {
        this.nodes.email = email;
        this.info.email = decodeURIComponent(email.href.slice(7));
      }
      if (tripcode = $('.postertrip', info)) {
        this.nodes.tripcode = tripcode;
        this.info.tripcode = tripcode.textContent;
      }
      if (uniqueID = $('.posteruid', info)) {
        this.nodes.uniqueID = uniqueID;
        this.info.uniqueID = uniqueID.firstElementChild.textContent;
      }
      if (capcode = $('.capcode.hand', info)) {
        this.nodes.capcode = capcode;
        this.info.capcode = capcode.textContent.replace('## ', '');
      }
      if (flag = $('.flag, .countryFlag', info)) {
        this.nodes.flag = flag;
        this.info.flag = flag.title;
      }
      if (date = $('.dateTime', info)) {
        this.nodes.date = date;
        this.info.date = new Date(date.dataset.utc * 1000);
      }
      this.parseComment();
      this.parseQuotes();
      this.parseFile(that);
      this.clones = [];
      g.posts[this.fullID] = thread.posts[this] = board.posts[this] = this;
      if (that.isArchived) {
        this.kill();
      }
    }

    Post.prototype.parseComment = function() {
      var bq, i, node, nodes, text, _i, _j, _len, _ref, _ref1;
      this.nodes.comment.normalize();
      bq = this.nodes.comment.cloneNode(true);
      _ref = $$('.abbr, .exif, b', bq);
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        node = _ref[_i];
        $.rm(node);
      }
      text = [];
      nodes = d.evaluate('.//br|.//text()', bq, null, 7, null);
      for (i = _j = 0, _ref1 = nodes.snapshotLength; _j < _ref1; i = _j += 1) {
        text.push(nodes.snapshotItem(i).data || '\n');
      }
      return this.info.comment = text.join('').trim().replace(/\s+$/gm, '');
    };

    Post.prototype.parseQuotes = function() {
      var quotelink, _i, _len, _ref;
      this.quotes = [];
      _ref = $$('.quotelink', this.nodes.comment);
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        quotelink = _ref[_i];
        this.parseQuote(quotelink);
      }
    };

    Post.prototype.parseQuote = function(quotelink) {
      var fullID, match;
      if (!(match = quotelink.href.match(/boards\.4chan\.org\/([^\/]+)\/res\/\d+#p(\d+)$/))) {
        return;
      }
      this.nodes.quotelinks.push(quotelink);
      if (this.isClone) {
        return;
      }
      fullID = "" + match[1] + "." + match[2];
      if (this.quotes.indexOf(fullID) === -1) {
        return this.quotes.push(fullID);
      }
    };

    Post.prototype.parseFile = function(that) {
      var alt, anchor, fileEl, fileInfo, size, thumb, unit;
      if (!((fileEl = $('.file', this.nodes.post)) && (thumb = $('img[data-md5]', fileEl)))) {
        return;
      }
      alt = thumb.alt;
      anchor = thumb.parentNode;
      fileInfo = fileEl.firstElementChild;
      this.file = {
        info: fileInfo,
        text: fileInfo.firstElementChild,
        thumb: thumb,
        URL: anchor.href,
        size: alt.match(/[\d.]+\s\w+/)[0],
        MD5: thumb.dataset.md5,
        isSpoiler: $.hasClass(anchor, 'imgspoiler')
      };
      size = +this.file.size.match(/[\d.]+/)[0];
      unit = ['B', 'KB', 'MB', 'GB'].indexOf(this.file.size.match(/\w+$/)[0]);
      while (unit-- > 0) {
        size *= 1024;
      }
      this.file.sizeInBytes = size;
      this.file.thumbURL = that.isArchived ? thumb.src : "" + location.protocol + "//thumbs.4chan.org/" + this.board + "/thumb/" + (this.file.URL.match(/(\d+)\./)[1]) + "s.jpg";
      this.file.name = $('span[title]', fileInfo).title;
      if (this.file.isImage = /(jpg|png|gif)$/i.test(this.file.name)) {
        return this.file.dimensions = this.file.text.textContent.match(/\d+x\d+/)[0];
      }
    };

    Post.prototype.kill = function(file, now) {
      var clone, quotelink, strong, _i, _j, _len, _len1, _ref, _ref1;
      now || (now = new Date());
      if (file) {
        if (this.file.isDead) {
          return;
        }
        this.file.isDead = true;
        this.file.timeOfDeath = now;
        $.addClass(this.nodes.root, 'deleted-file');
      } else {
        if (this.isDead) {
          return;
        }
        this.isDead = true;
        this.timeOfDeath = now;
        $.addClass(this.nodes.root, 'deleted-post');
      }
      if (!(strong = $('strong.warning', this.nodes.info))) {
        strong = $.el('strong', {
          className: 'warning',
          textContent: this.isReply ? '[Deleted]' : '[Dead]'
        });
        $.after($('input', this.nodes.info), strong);
      }
      strong.textContent = file ? '[File deleted]' : this.isReply ? '[Deleted]' : '[Dead]';
      if (this.isClone) {
        return;
      }
      _ref = this.clones;
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        clone = _ref[_i];
        clone.kill(file, now);
      }
      if (file) {
        return;
      }
      _ref1 = Get.allQuotelinksLinkingTo(this);
      for (_j = 0, _len1 = _ref1.length; _j < _len1; _j++) {
        quotelink = _ref1[_j];
        if (!(!$.hasClass(quotelink, 'deadlink'))) {
          continue;
        }
        $.add(quotelink, $.tn('\u00A0(Dead)'));
        $.addClass(quotelink, 'deadlink');
      }
    };

    Post.prototype.resurrect = function() {
      var clone, quotelink, strong, _i, _j, _len, _len1, _ref, _ref1;
      delete this.isDead;
      delete this.timeOfDeath;
      $.rmClass(this.nodes.root, 'deleted-post');
      strong = $('strong.warning', this.nodes.info);
      if (this.file && this.file.isDead) {
        strong.textContent = '[File deleted]';
      } else {
        $.rm(strong);
      }
      if (this.isClone) {
        return;
      }
      _ref = this.clones;
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        clone = _ref[_i];
        clone.resurrect();
      }
      _ref1 = Get.allQuotelinksLinkingTo(this);
      for (_j = 0, _len1 = _ref1.length; _j < _len1; _j++) {
        quotelink = _ref1[_j];
        if ($.hasClass(quotelink, 'deadlink')) {
          quotelink.textContent = quotelink.textContent.replace('\u00A0(Dead)', '');
          $.rmClass(quotelink, 'deadlink');
        }
      }
    };

    Post.prototype.collect = function() {
      this.kill();
      delete g.posts[this.fullID];
      delete this.thread.posts[this];
      return delete this.board.posts[this];
    };

    Post.prototype.addClone = function(context) {
      return new Clone(this, context);
    };

    Post.prototype.rmClone = function(index) {
      var clone, _i, _len, _ref;
      this.clones.splice(index, 1);
      _ref = this.clones.slice(index);
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        clone = _ref[_i];
        clone.nodes.root.dataset.clone = index++;
      }
    };

    return Post;

  })();

  Clone = (function(_super) {
    __extends(Clone, _super);

    function Clone(origin, context) {
      var file, index, info, inline, inlined, key, nodes, post, root, val, _i, _j, _k, _len, _len1, _len2, _ref, _ref1, _ref2, _ref3;
      this.origin = origin;
      this.context = context;
      _ref = ['ID', 'fullID', 'board', 'thread', 'info', 'quotes', 'isReply'];
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        key = _ref[_i];
        this[key] = origin[key];
      }
      nodes = origin.nodes;
      root = nodes.root.cloneNode(true);
      post = $('.post', root);
      info = $('.postInfo', post);
      this.nodes = {
        root: root,
        post: post,
        info: info,
        comment: $('.postMessage', post),
        quotelinks: [],
        backlinks: info.getElementsByClassName('backlink')
      };
      _ref1 = $$('.inline', post);
      for (_j = 0, _len1 = _ref1.length; _j < _len1; _j++) {
        inline = _ref1[_j];
        $.rm(inline);
      }
      _ref2 = $$('.inlined', post);
      for (_k = 0, _len2 = _ref2.length; _k < _len2; _k++) {
        inlined = _ref2[_k];
        $.rmClass(inlined, 'inlined');
      }
      root.hidden = false;
      $.rmClass(root, 'forwarded');
      $.rmClass(post, 'highlight');
      if (nodes.subject) {
        this.nodes.subject = $('.subject', info);
      }
      if (nodes.name) {
        this.nodes.name = $('.name', info);
      }
      if (nodes.email) {
        this.nodes.email = $('.useremail', info);
      }
      if (nodes.tripcode) {
        this.nodes.tripcode = $('.postertrip', info);
      }
      if (nodes.uniqueID) {
        this.nodes.uniqueID = $('.posteruid', info);
      }
      if (nodes.capcode) {
        this.nodes.capcode = $('.capcode', info);
      }
      if (nodes.flag) {
        this.nodes.flag = $('.countryFlag', info);
      }
      if (nodes.date) {
        this.nodes.date = $('.dateTime', info);
      }
      this.parseQuotes();
      if (origin.file) {
        this.file = {};
        _ref3 = origin.file;
        for (key in _ref3) {
          val = _ref3[key];
          this.file[key] = val;
        }
        file = $('.file', post);
        this.file.info = file.firstElementChild;
        this.file.text = this.file.info.firstElementChild;
        this.file.thumb = $('img[data-md5]', file);
        this.file.fullImage = $('.full-image', file);
      }
      if (origin.isDead) {
        this.isDead = true;
      }
      this.isClone = true;
      index = origin.clones.push(this) - 1;
      root.dataset.clone = index;
    }

    return Clone;

  })(Post);

  DataBoard = (function() {
    DataBoard.keys = ['hiddenThreads', 'hiddenPosts', 'lastReadPosts', 'yourPosts', 'watchedThreads'];

    function DataBoard(key, sync, dontClean) {
      var init,
        _this = this;
      this.key = key;
      this.onSync = __bind(this.onSync, this);
      this.data = Conf[key];
      $.sync(key, this.onSync);
      if (!dontClean) {
        this.clean();
      }
      if (!sync) {
        return;
      }
      init = function() {
        $.off(d, '4chanXInitFinished', init);
        return _this.sync = sync;
      };
      $.on(d, '4chanXInitFinished', init);
    }

    DataBoard.prototype.save = function() {
      return $.set(this.key, this.data);
    };

    DataBoard.prototype["delete"] = function(_arg) {
      var boardID, postID, threadID;
      boardID = _arg.boardID, threadID = _arg.threadID, postID = _arg.postID;
      if (postID) {
        delete this.data.boards[boardID][threadID][postID];
        this.deleteIfEmpty({
          boardID: boardID,
          threadID: threadID
        });
      } else if (threadID) {
        delete this.data.boards[boardID][threadID];
        this.deleteIfEmpty({
          boardID: boardID
        });
      } else {
        delete this.data.boards[boardID];
      }
      return this.save();
    };

    DataBoard.prototype.deleteIfEmpty = function(_arg) {
      var boardID, threadID;
      boardID = _arg.boardID, threadID = _arg.threadID;
      if (threadID) {
        if (!Object.keys(this.data.boards[boardID][threadID]).length) {
          delete this.data.boards[boardID][threadID];
          return this.deleteIfEmpty({
            boardID: boardID
          });
        }
      } else if (!Object.keys(this.data.boards[boardID]).length) {
        return delete this.data.boards[boardID];
      }
    };

    DataBoard.prototype.set = function(_arg) {
      var boardID, postID, threadID, val, _base, _base1, _base2;
      boardID = _arg.boardID, threadID = _arg.threadID, postID = _arg.postID, val = _arg.val;
      if (postID !== void 0) {
        ((_base = ((_base1 = this.data.boards)[boardID] || (_base1[boardID] = {})))[threadID] || (_base[threadID] = {}))[postID] = val;
      } else if (threadID !== void 0) {
        ((_base2 = this.data.boards)[boardID] || (_base2[boardID] = {}))[threadID] = val;
      } else {
        this.data.boards[boardID] = val;
      }
      return this.save();
    };

    DataBoard.prototype.get = function(_arg) {
      var ID, board, boardID, defaultValue, postID, thread, threadID, val, _i, _len;
      boardID = _arg.boardID, threadID = _arg.threadID, postID = _arg.postID, defaultValue = _arg.defaultValue;
      if (board = this.data.boards[boardID]) {
        if (!threadID) {
          if (postID) {
            for (thread = _i = 0, _len = board.length; _i < _len; thread = ++_i) {
              ID = board[thread];
              if (postID in thread) {
                val = thread[postID];
                break;
              }
            }
          } else {
            val = board;
          }
        } else if (thread = board[threadID]) {
          val = postID ? thread[postID] : thread;
        }
      }
      return val || defaultValue;
    };

    DataBoard.prototype.clean = function() {
      var boardID, now, val, _ref;
      _ref = this.data.boards;
      for (boardID in _ref) {
        val = _ref[boardID];
        this.deleteIfEmpty({
          boardID: boardID
        });
      }
      now = Date.now();
      if ((this.data.lastChecked || 0) < now - 2 * $.HOUR) {
        this.data.lastChecked = now;
        for (boardID in this.data.boards) {
          this.ajaxClean(boardID);
        }
      }
      return this.save();
    };

    DataBoard.prototype.ajaxClean = function(boardID) {
      var _this = this;
      return $.cache("//api.4chan.org/" + boardID + "/threads.json", function(e) {
        var board, page, thread, threads, _i, _j, _len, _len1, _ref, _ref1;
        if (e.target.status !== 200) {
          if (e.target.status === 404) {
            _this["delete"](boardID);
          }
          return;
        }
        board = _this.data.boards[boardID];
        threads = {};
        _ref = JSON.parse(e.target.response);
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
          page = _ref[_i];
          _ref1 = page.threads;
          for (_j = 0, _len1 = _ref1.length; _j < _len1; _j++) {
            thread = _ref1[_j];
            if (thread.no in board) {
              threads[thread.no] = board[thread.no];
            }
          }
        }
        _this.data.boards[boardID] = threads;
        _this.deleteIfEmpty({
          boardID: boardID
        });
        return _this.save();
      });
    };

    DataBoard.prototype.onSync = function(data) {
      this.data = data || {
        boards: {}
      };
      return typeof this.sync === "function" ? this.sync() : void 0;
    };

    return DataBoard;

  })();

  Main = {
    init: function() {
      var db, flatten, pathname, _i, _len, _ref, _ref1;
      pathname = location.pathname.split('/');
      g.BOARD = new Board(pathname[1]);
      if ((_ref = g.BOARD.ID) === 'z' || _ref === 'fk') {
        return;
      }
      g.VIEW = (function() {
        switch (pathname[2]) {
          case 'res':
            return 'thread';
          case 'catalog':
            return 'catalog';
          default:
            return 'index';
        }
      })();
      if (g.VIEW === 'thread') {
        g.THREADID = +pathname[3];
      }
      flatten = function(parent, obj) {
        var key, val;
        if (obj instanceof Array) {
          Conf[parent] = obj[0];
        } else if (typeof obj === 'object') {
          for (key in obj) {
            val = obj[key];
            flatten(key, val);
          }
        } else {
          Conf[parent] = obj;
        }
      };
      flatten(null, Config);
      _ref1 = DataBoard.keys;
      for (_i = 0, _len = _ref1.length; _i < _len; _i++) {
        db = _ref1[_i];
        Conf[db] = {
          boards: {}
        };
      }
      Conf['selectedArchives'] = {};
      Conf['archives'] = Redirect.archives;
      $.get(Conf, function(items) {
        $.extend(Conf, items);
        return Main.initFeatures();
      });
      return $.on(d, '4chanMainInit', Main.initStyle);
    },
    initFeatures: function() {
      var initFeature;
      switch (location.hostname) {
        case 'api.4chan.org':
          return;
        case 'sys.4chan.org':
          Report.init();
          return;
        case 'images.4chan.org':
          $.ready(function() {
            var URL, pathname;
            if (Conf['404 Redirect'] && d.title === '4chan - 404 Not Found') {
              Redirect.init();
              pathname = location.pathname.split('/');
              URL = Redirect.to('file', {
                boardID: g.BOARD.ID,
                filename: pathname[pathname.length - 1]
              });
              if (URL) {
                return location.replace(URL);
              }
            }
          });
          return;
      }
      initFeature = function(name, module) {
        var err;
        try {
          return module.init();
        } catch (_error) {
          err = _error;
          return Main.handleErrors({
            message: "\"" + name + "\" initialization crashed.",
            error: err
          });
        }
      };
      initFeature('Polyfill', Polyfill);
      initFeature('Header', Header);
      initFeature('Settings', Settings);
      initFeature('Index Generator', Index);
      initFeature('Announcement Hiding', PSAHiding);
      initFeature('Fourchan thingies', Fourchan);
      initFeature('Custom CSS', CustomCSS);
      initFeature('Redirect', Redirect);
      initFeature('Resurrect Quotes', Quotify);
      initFeature('Filter', Filter);
      initFeature('Thread Hiding', ThreadHiding);
      initFeature('Reply Hiding', PostHiding);
      initFeature('Recursive', Recursive);
      initFeature('Strike-through Quotes', QuoteStrikeThrough);
      initFeature('Quick Reply', QR);
      initFeature('Menu', Menu);
      initFeature('Report Link', ReportLink);
      initFeature('Thread Hiding (Menu)', ThreadHiding.menu);
      initFeature('Reply Hiding (Menu)', PostHiding.menu);
      initFeature('Delete Link', DeleteLink);
      initFeature('Filter (Menu)', Filter.menu);
      initFeature('Download Link', DownloadLink);
      initFeature('Archive Link', ArchiveLink);
      initFeature('Quote Inlining', QuoteInline);
      initFeature('Quote Previewing', QuotePreview);
      initFeature('Quote Backlinks', QuoteBacklink);
      initFeature('Mark Quotes of You', QuoteYou);
      initFeature('Mark OP Quotes', QuoteOP);
      initFeature('Mark Cross-thread Quotes', QuoteCT);
      initFeature('Anonymize', Anonymize);
      initFeature('Color User IDs', IDColor);
      initFeature('Time Formatting', Time);
      initFeature('Relative Post Dates', RelativeDates);
      initFeature('File Info Formatting', FileInfo);
      initFeature('Sauce', Sauce);
      initFeature('Image Expansion', ImageExpand);
      initFeature('Image Expansion (Menu)', ImageExpand.menu);
      initFeature('Reveal Spoilers', RevealSpoilers);
      initFeature('Auto-GIF', AutoGIF);
      initFeature('Image Hover', ImageHover);
      initFeature('Thread Expansion', ExpandThread);
      initFeature('Thread Excerpt', ThreadExcerpt);
      initFeature('Favicon', Favicon);
      initFeature('Unread', Unread);
      initFeature('Thread Stats', ThreadStats);
      initFeature('Thread Updater', ThreadUpdater);
      initFeature('Thread Watcher', ThreadWatcher);
      initFeature('Thread Watcher (Menu)', ThreadWatcher.menu);
      initFeature('Index Navigation', Nav);
      initFeature('Keybinds', Keybinds);
      initFeature('Show Dice Roll', Dice);
      initFeature('Linkify', Linkify);
      $.on(d, 'AddCallback', Main.addCallback);
      return $.ready(Main.initReady);
    },
    initStyle: function() {
      var mainStyleSheet, setStyle, style, styleSheets, _ref;
      $.off(d, '4chanMainInit', Main.initStyle);
      if (!Main.isThisPageLegit() || $.hasClass(doc, 'fourchan-x')) {
        return;
      }
      if ((_ref = $('link[href*=mobile]', d.head)) != null) {
        _ref.disabled = true;
      }
      $.addClass(doc, 'gecko');
      $.addClass(doc, 'fourchan-x');
      $.addStyle(Main.css);
      if (g.VIEW === 'catalog') {
        $.addClass(doc, $.id('base-css').href.match(/catalog_(\w+)/)[1].replace('_new', '').replace(/_+/g, '-'));
        return;
      }
      style = 'yotsuba-b';
      mainStyleSheet = $('link[title=switch]', d.head);
      styleSheets = $$('link[rel="alternate stylesheet"]', d.head);
      setStyle = function() {
        var styleSheet, _i, _len;
        $.rmClass(doc, style);
        for (_i = 0, _len = styleSheets.length; _i < _len; _i++) {
          styleSheet = styleSheets[_i];
          if (styleSheet.href === mainStyleSheet.href) {
            style = styleSheet.title.toLowerCase().replace('new', '').trim().replace(/\s+/g, '-');
            break;
          }
        }
        return $.addClass(doc, style);
      };
      setStyle();
      if (!mainStyleSheet) {
        return;
      }
      return new MutationObserver(setStyle).observe(mainStyleSheet, {
        attributes: true,
        attributeFilter: ['href']
      });
    },
    initReady: function() {
      var GMver, err, errors, href, i, postRoot, posts, thread, threadRoot, v, _i, _j, _len, _len1, _ref, _ref1;
      if (d.title === '4chan - 404 Not Found') {
        if (Conf['404 Redirect'] && g.VIEW === 'thread') {
          href = Redirect.to('thread', {
            boardID: g.BOARD.ID,
            threadID: g.THREADID,
            postID: +location.hash.match(/\d+/)
          });
          location.replace(href || ("/" + g.BOARD + "/"));
        }
        return;
      }
      Main.initStyle();
      if (g.VIEW === 'thread' && (threadRoot = $('.thread'))) {
        thread = new Thread(+threadRoot.id.slice(1), g.BOARD);
        posts = [];
        _ref = $$('.thread > .postContainer', threadRoot);
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
          postRoot = _ref[_i];
          try {
            posts.push(new Post(postRoot, thread, g.BOARD));
          } catch (_error) {
            err = _error;
            if (!errors) {
              errors = [];
            }
            errors.push({
              message: "Parsing of Post No." + (postRoot.id.match(/\d+/)) + " failed. Post will be skipped.",
              error: err
            });
          }
        }
        if (errors) {
          Main.handleErrors(errors);
        }
        Main.callbackNodes(Thread, [thread]);
        Main.callbackNodes(Post, posts);
      }
      if ($.hasClass(d.body, 'fourchan_x')) {
        Main.disableReports = true;
        alert('4chan X v2 detected: Disable it or v3 will break.');
      }
      GMver = GM_info.version.split('.');
      _ref1 = "1.12".split('.');
      for (i = _j = 0, _len1 = _ref1.length; _j < _len1; i = ++_j) {
        v = _ref1[i];
        if (v < GMver[i]) {
          break;
        }
        if (v === GMver[i]) {
          continue;
        }
        new Notice('warning', "Your version of Greasemonkey is outdated (v" + GM_info.version + " instead of v1.12 minimum) and 4chan X may not operate correctly.", 30);
        break;
      }
      try {
        localStorage.getItem('4chan-settings');
      } catch (_error) {
        err = _error;
        new Notice('warning', 'Cookies need to be enabled on 4chan for 4chan X to operate properly.', 30);
        Main.disableReports = true;
      }
      return $.event('4chanXInitFinished');
    },
    callbackNodes: function(klass, nodes) {
      var callback, err, errors, i, len, node, _i, _j, _len, _ref;
      len = nodes.length;
      _ref = klass.callbacks;
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        callback = _ref[_i];
        for (i = _j = 0; _j < len; i = _j += 1) {
          node = nodes[i];
          try {
            callback.cb.call(node);
          } catch (_error) {
            err = _error;
            if (!errors) {
              errors = [];
            }
            errors.push({
              message: "\"" + callback.name + "\" crashed on " + klass.name + " No." + node + " (/" + node.board + "/).",
              error: err
            });
          }
        }
      }
      if (errors) {
        return Main.handleErrors(errors);
      }
    },
    addCallback: function(e) {
      var Klass, obj;
      obj = e.detail;
      if (typeof obj.callback.name !== 'string') {
        throw new Error("Invalid callback name: " + obj.callback.name);
      }
      switch (obj.type) {
        case 'Post':
          Klass = Post;
          break;
        case 'Thread':
          Klass = Thread;
          break;
        default:
          return;
      }
      obj.callback.isAddon = true;
      return Klass.callbacks.push(obj.callback);
    },
    handleErrors: function(errors) {
      var div, error, logs, _i, _len;
      if (!(errors instanceof Array)) {
        error = errors;
      } else if (errors.length === 1) {
        error = errors[0];
      }
      if (error) {
        new Notice('error', Main.parseError(error), 15);
        return;
      }
      div = $.el('div', {
        innerHTML: "" + errors.length + " errors occurred. [<a href=javascript:;>show</a>]"
      });
      $.on(div.lastElementChild, 'click', function() {
        var _ref;
        return _ref = this.textContent === 'show' ? ['hide', false] : ['show', true], this.textContent = _ref[0], logs.hidden = _ref[1], _ref;
      });
      logs = $.el('div', {
        hidden: true
      });
      for (_i = 0, _len = errors.length; _i < _len; _i++) {
        error = errors[_i];
        $.add(logs, Main.parseError(error));
      }
      return new Notice('error', [div, logs], 30);
    },
    parseError: function(data) {
      var error, message;
      Main.logError(data);
      message = $.el('div', {
        textContent: data.message
      });
      error = $.el('div', {
        textContent: data.error
      });
      return [message, error];
    },
    errors: [],
    logError: function(data) {
      if (!Main.errors.length) {
        $.on(window, 'unload', Main.postErrors);
      }
      c.error(data.message, data.error.stack);
      return Main.errors.push(data);
    },
    postErrors: function() {
      var errors;
      if (Main.disableReports) {
        return;
      }
      errors = Main.errors.filter(function(d) {
        return !!d.error.stack;
      }).map(function(d) {
        var message, name, stack, _ref;
        _ref = d.error, name = _ref.name, message = _ref.message, stack = _ref.stack;
        stack = stack.replace(/file:\/{3}.+\//g, '');
        return "" + d.message + " " + name + ": " + message + " " + stack;
      }).join('\n');
      if (!errors) {
        return;
      }
      return $.ajax('https://4chan-x.just-believe.in/errors', null, {
        sync: true,
        form: $.formData({
          n: "4chan X v" + g.VERSION,
          t: 'userscript',
          ua: window.navigator.userAgent,
          url: window.location.href,
          e: errors
        })
      });
    },
    isThisPageLegit: function() {
      var _ref;
      if (!('thisPageIsLegit' in Main)) {
        Main.thisPageIsLegit = location.hostname === 'boards.4chan.org' && !$('link[href*="favicon-status.ico"]', d.head) && ((_ref = d.title) !== '4chan - Temporarily Offline' && _ref !== '4chan - Error' && _ref !== '504 Gateway Time-out');
      }
      return Main.thisPageIsLegit;
    },
    css: "@font-face {\n  font-family: 'FontAwesome';\n  src: url('data:application/font-woff;base64,d09GRgABAAAAAK28AA4AAAABO4gAAQAAAAAAAAAAAAAAAAAAAAAAAAAAAABGRlRNAAABRAAAABwAAAAcZh8W20dERUYAAAFgAAAAHwAAACABwwAET1MvMgAAAYAAAAA+AAAAYIsCehNjbWFwAAABwAAAASQAAAJy0Wu8A2dhc3AAAALkAAAACAAAAAgAAAAQZ2x5ZgAAAuwAAJnQAAEZfHOOtQloZWFkAACcvAAAADEAAAA2A7otNmhoZWEAAJzwAAAAHwAAACQNggfqaG10eAAAnRAAAAHIAAAGRgpsDgdsb2NhAACe2AAAAwcAAAMu0XSKdG1heHAAAKHgAAAAHwAAACAB7AIcbmFtZQAAogAAAAFlAAACuDwMZZVwb3N0AACjaAAACkoAABFUCXKd3XdlYmYAAK20AAAABgAAAAZBXFJnAAAAAQAAAADMPaLPAAAAAMtUgjAAAAAAzozx23jaY2BkYGDgA2IJBhBgYmBkYGScCiRZwDwGAAq9AMkAeNpjYGZ9xDiBgZWBhaWHxZiBgaENQjMVM0SB+ThBQWVRMYMDg8JXBjaG/0A+GwOjMpBiRFKiwMAIAAKvCRMAAHjazZG7SgNhEIXn31zUIPnHa2KUZbMPoD5BWLAPW9hYGLewlJAnCHmCkMY2pNQmiAiSzspSfIFcQLCUM0W8RM3vxhVBwUYsPDBnOHD4ihkiilE0a6RCJ3UQJvWe48oPt08eJYjJoRYdU5vO6NJJORvOXt51bTcYENKwUUARJZRRRR1NtHGKK/Rwh7GkxZZ1KUhRSlKWqtSlOSRjQvKEePRBpC9EAiMPDz4CVFBDAy2c4ALXGABCwuLIpnjiSyAVqUljQjQ3Zt/smh2zbbYGqf5t/7w37I66HSfHq5zjLGd4mZd4kRd4nueYOcYWKyZt9Fi/6hf9rEf6ST/qB30/exhd42+lkvSJVVZo1vdC9Ir/oKlkZjqxMkPZHxvxX3HfAOwveKYAAQAB//8AD3javL0JfFTl1TB+z3O3mTv73FmSyWQy+2SBJGS2AFmGsJME2QQExIiiCC6gIIrbKIjiLiqltmrUqqWrXezXavGd2mpXfW1rV/33i221/V6ttbY/WyFz+c7z3JnJJCSiff/vB5l7n309z/Occ55zzuUIt53jeLuED07muGzIEeIdIccIFLTcdjK8XQwe2y5xxzj6D7iqfzOo/8wTnPSomOfq0eOSwRHq8LikSCgcT2WSIQfE06keSIY6AiA92lK8GXK+eNw3mqdPyBVvbok2esW8tzEqLohgdJGLp+L4x3PkipaIt85gqGN1Yh0c1tGCHofLSsKtJNVDkh1ehzjem8pkIZPs8EjcvC3nrzt/yzx8Tb9gTXG8Nx7gc2Z7Y6cYPD7csbTF7W5Zei6+EqT2L8XZ1QH89xuSCnBCJ0dYG/LYBpkLYdftXJD+ALsaTgA+onFid2aiQcHjdOMweIS89oF2h/YByHApLw+kMlHtyNdeu1M7fvSSS46CCAEQj15yNayJEUwAsp5Yy6cG4rD66rEUlxzVjt/52te0IzE6O9yJvMyJHOfjurlBjos5JFmQraQFRwAS8Vg84XB5cKwzji7SyuMcSG6X1+MNCLNJRw+fzWR7IOvQJyftoNODA5UPxrR/PJjMXdQO0H5RLvmg9o9YULWIBYsKomQ2HMtZ1Hu+87I0K5xtdQG4WrPhWdLL38mcmd/YdyzXt3Fjn1jo2xjkuWjgpX0t7TNmtLfseykQLXIWVRUaidPoMCiiavnszsOPiTN8Macz5pshPna45e6h4wWaW6Bl6HNM+5bn/Bwn4JC2CmlsYUeAeHt4nFA6pvz9KWfxLiUy0NWmjfRcf8myaHTZJdf3jGhvFO/OO8k6Q/Ssc++Y+9q/WhbnotHc4pZ/vfb/vVH8rF72F3HuRriwDqMqFkfnLSbiEwE0q1IwzcbUTIdXFXFMfNp9q8HtUt1ar9aLE+omq7V7azvh/dfVLvV1eL+Tv9Lj0x7UzLLFXW9++21zvVuywj9gU507ZlwM329q0mYuNtIlQip1Gyn0GiFmwqnlY2K5HVM3Q9gBHdq6o0e1ddCxGPbAlfB91q6mqZtFXNDUA9dqN/Rov9LWf//7vFJuZseHtJK2ESEbx76BSyBUlSAk1SPS8e+gKysgcnYpGLdngmL+4JWjh648KLuDmQWbu419K2/cf+PKPmP35gWZoFvWCq9rz73+OvTsveLWW69Ib9517llzG1vSLfjXOPesc3dt5v+kx7/OcSa6pmRarw1rbuN6udO4s7hLuOu4O7mHuC9znJhOxVsgLNWDyzMbEKxP4QdHKs6gvrQMYGL8x0x/qvomLibIx31sZ5viIXBxX5GjHh6fo9xYjFiVU8tXpzpVmbgMP2ALScKFlKtEwf2TOYs+wgrW6FMYCz8+5uSrk2j3n6LAp4+xukW2iAUK8FL1fNLdetwI1cKEETtFPM8NpDQuNTCQIuw55ubzU8UQjm6jAymgT/KTKs/oT6aK4dhiZfvPybDIgVtvVTforXJM8MP/sH9ifYTrbNQKjZ2djZCjzzE3yVf7ivmp4z56ymo3BJmTPuDXFWdxzMlPGnrKBFWFIQhNOhf/v8/CRx9VEWNGWRiPYce5qeOq3f/mWI0bCjy7ruSs0k3CVzkP+vDMkKVwG0A81Qt4Shjx0QDSTf7itJv9y/w3a4f8fuqAOLmL+vn3lrEo/82wlfr9fu035G70YrkXn3hHdAoHuQjHRV02kMIJI9Cy46mscXz5HpdsBNHJStZ+q/1WLwni6CrVBvFS6b/F0A+N9VdK0fEZfb+I4Nk4T5/hFv1BpyWiz81s3Jbx0eGpBzyIoIR5cVNhXgKnWkZUC6ItI4iCjDnH4WP96anwMTJyUk7qfLMKSXtqa/8USFp1n2ycl2s/CWo/WvuLBVoryX28VrP2fuSWls59iS63Ji5NMSMiCUFsTTrlzGY8Xo8kW7H1DAPAgy/RCog/ej1OumfrOzTFs/e8pP1R+4H2x5f2PHyw5YKGoK1507bltxx9+egty7dtarYFG7Y2H3y4mB/YMoB/JP9pmnLPS+D/9Deg76KgtaX5guCSX165BZNjri1X/nJJ8ILmFmvwIu1ZsqTINmjCNmj8J1ZwxLF9gYtVwEUHkphD99P2TeaHU/k5p7VgdbIH5D+ee6jscF6qMSfk8PlekbkJdcOlo/Rl5WkQvHeMeUTqGZsPRmNcjP5UPCy5PB0UgnB9yjgjLpyRCK5RScb/tNW4XBMyBaR4gqKOiN9jUCvQwcAFnC2HJnEVZxD/Zz3EBe3NImqNlAFFq60gY1AA4e7QK4cOvUIO2c3fUl2RRYqx7i6P2XrLtDa7Ra7/ndUN/hlNtys2q+nahGywLXLWWf+XxW43PW2tbZyrGH13eyyW8YnvMNos5uujLLHPhomJh9ZwCC76ndlD/JlYx1qzT4ndYbzQa7u5w++wfNPu3mY0XZpRLGaTe31tx4w64rawtK2tM5ebzYoleqeyrTqxsjtpsOqJ2/3Ezc6OEi6rw8hsbi53vo6HVM+yeAq/ivSvK0Dp1h6AEI5uSJJFBmkVhCVSXtNZRt/iGLIzxGllc4oPmMKdH81bbTyf423W4hAU2mVF+64i85c4rUMb+0YRnxpioJNeYFtKT5iltgWQtjr5YBUYWadwFwP810cHEOYdMWHJLiMhxvsweHRg5ZW7VvLfZLU/EUulYk849fXvwwE7T+Q5la1/1jXseAn/ShvBk0VAi7CTIUFhjVLXCEql/djt8OKmgviplj+BOBfiqqSX9ML/7jFYeIuhOFAcMJsthh4DUch/BtcG/8GWxqsKIUEtSBFbiujCCAgAv9XiZM4SI5HJnOJ/GIAYlyh1BrLG5/vJd2nftC3foHwARv+KdH0YcHbx3EqW2hqCUDyBK2EiNulFsM4LnDbU2KlxJA9XKGbt++Z6etAfZ4csyTcO+aHT38hjEAxr+XozzDQr2pbRPI0W8Sxv9Gs/8A/pdbO16cK9e9oYZqvvOyWw4eytELSCPQDBDGcnuJvi1o47O4+bhL4jjOjgMHzwmPbrYwcPHoPGY3DpS9pD2kbtoZdegrPhETibH9EqcENhoahhqoOlHOSs6qQvvcTmsQPxpaW4PToQ2jlI861AyRSZd0s6neOKIHWTwGBK7ci8hNROGDcGoJAbppsLS0Z3jggbNo+wpgZclqctLqgBp/kfZid5v7WYszjBhcHauxjuAqelmGv1wcOGqAtWYogNQx7HJDZMAitdUQM87CN+AdjJpBUEs92O9KZqAcoasJzA5+JMj+BS/apF3zct6Dz+dk+ZjpAoEmjjYlw3YiilvbD8VsfNtNfT0UtXH3hkisdAPEtZTzoMuB2hDlHnKkGcvR7vbDzGkDT46U2zPj3rZngZweMZR4OWc2acWq7B4WgGJM2AEl9c8+MpPRf9g+AY3ndzJ/4RR1O9llNVKNQ3JaDA6KZcFazUcFGGA7gq7aqAituRxO2iBCw9YI8LVfAiDCnad0w1Jq1gMxg8BbZw8O8nFZg5ePAkqCFDZrP2HaMRcnbVxeDGqg07SUcVpB09CXQmaau+x+mboc6WoJggTN3WjVUtVKAXmw05288+tKn3urRhtskNOa1mM/QajVrBDh98SFMJgwmKAljoyodQKyT4EI+HV8gbio1BQ1bVd2Ov6uFPQBfw8Eqx6xV8QddZkCNDcd8x3Khq31HSPj7nSyvv1JIcr/DwrmYnZlJ4RAswzuTve7pJY204XFv8dXfVGNm4OsohoXg4O3RphRkcmymWvTAcLI7YHHZ7MBhqIMEPXfTksSVOrWA0qDGSj6lOVSv8+MNWPVTalKzsRYl4L8QjYStBnC3ZQc/7Dnqwy5JQQTKTHQKe/YjacRRSmx2Ohlvu/WEZ+dr9ymLZbjUdMILhIu1HXxhD1e4BddsNCOEip+V88cZE4JYDJRRvy1kKMd5iqFX23U1TQif4X9pzyebrcRFV4zNRbiFbBYQLhaOIsIxt0Uh54DHbUUFOygs7xbpSwXO6wcmFWNvxrP4T5LQt2jsHtb9uu15N0enClaceWPj1s/f/eYGpGcHRotbS/mEodq8U+H2LOgfuB/UguLbdgNlgRCTaP7SvXXze9apeRDylHuhbdMOljnO9Kq/S7BhyywE9wCKDGQaxa2qcgqJhSn4BBxMw0vQp/BPp0vQp/OoErpV6EhdKJwSmeggYP8o8PCMzJndjomPMQznB43i9MFQp7u+TuIp///DoJM/co7Q+YbCaCczWuKDz2BvoGk/T3d5dfapTWhDPX0oJenA5U5SVz5WJ7iAYYDsYgo2dPFfYeujQVm2kqB/zGF34Jhi0f32z0EnhMleiIxxclsFlZcPD7S6jY4MUq24lkbDOm6XrnOLMiDInGUMUa8+xDuX6t/aLhdq6X93ffc2GWxcXtHcddl+8wT3r7W9te/raeEfmujNXWnxxkVsUP26lHRfeiy9K9/fvKoq1ddad01LTDhrjPvJm0Gutv2LWbLU51Rwv37MwOrKfttBGEPF3j8dR6/GHCyKdIrjVELergR7nFdSsDGNIIxDsVYnAlPJdrm95Dqwdw0YXXh6eV/+k9ivtq9qvnqyfF7584Vjc2gOeb7m69o9ACgYgNbKf3PL4vTNCK7cFx5DP4MIu81mb7gXp05/Wjt276Sxz18LgGFIa3LYyNOPexz8BNS/u2fOi9me9X0GeE0YQ52T7Fh6LFdjFg8bD41mtal/TjrF9WIJBXKrC8HG6wmEQQyiaOaivQQovQSHPypo+eWmcPpGUmc2nWnl2peCdtA7In75AsfniNeFwDf3FfTZlwSQVa87De/1irM5d765tnddai++6mFjHQBf3u2/hnC1k7VnMbfs4bcIjtRTK7mmQ3mbE28Q4pPAQEJ12kqAEeSXFR+4RnP652yzmunii07Vk5colrs5E3Gex3Aaf035uQTBNyA1ya/SqW265KtqKThb5848+ClntJa04W4z7Eq56W+bRbz6asdW7Egj9s7+hpbTr1mNM1CtYhFrferBDB9jX+2rR641ikvWcmRH5FObpPaQJd1aV8+KJ24Anfhuec3StuiNpFX8h/AGjVSjyjz90QCkcabd0xBFxhNzJNOhJHJDHf3weSS9KdtAfz9HnCa6YF/J5Gq3l2buI/0X80SCeo9lGAa6DUj4aS/IaC6d8ZgwkLCENpj+O3Q2Wz4aT+5HgWkp9mU3548mII6n+N369+C8Y3NjQ8AD+9fRc29DQy/4e6O3Fv2vZ38be3qMbN9Jkvb1i/tj14nX/1o/Oi36m3ye+xfbo+ioeRQkjQgpijPjyQAE3x/6twuUxzZ1IpWPFdDw9kILhdD5OfhwTTDSyX8ulY5orFiM/ieXTMJwaSMeLmUQZN71P3laqK32q2kQ9FMlA3BNpXCT5EVoBeRYcbQ3AL2M0Lp8e+QjtS7FAfwNmwsrIj+Jpvdk8pyDOcyW2eRV3LrcTIRZpEiulu3A5Z1O4duPZHsKWcZw+JzowSvLKrEulfLLkZcc8ouEJjygxdy9k4mOkXJVfOr/Ro/1FvWzO6ObBO/01HgnwTCRmt+SdZuBFwvt5d7MAsiBEBbVNAAMhVo9kcFhUVyjhh7iFfLBkmUd7J7rwzNFP1ZlMSs0V/KfqMwaYJpP48b8IZisZstQKbnQUh9Gx5aQQITxz0ejluTXbls7tElqthjrJ5KpT4tviSqPBFJai28PGVtESEX274oaI0eDyGcyxUKLWAxJv3L5k9PLd8232ugUNPv5VT8QWqKAtWqHi1O9zPyGW7oqhwxsAd+nApowKfOpggee1cJvXHUokQmpte0RbqC2Mtul+t1fMGy2d4WP/DHdaDEH4rLY2RP2iEf3G8l6el/S9yIw0fxfHNembCeP7hMqgmHWUWNY6hhYpH88lsKT4N6P0AHefUfwN07sWIa8ToqplRMddRizqjtMQjyHDjUP+w/7G3Gk7gKN7TmfjcFGnPXPakEUdptjMMJLQw6ftIEHKnDjsH2o8we0oyQboNHOIa8IeUDEMxKNLyMAYAlVhV5X40HZ+8TtHjrxzhB+hKNOxPH2OJNXNacKlN6vJ4vlj/GR+6AhNShYf2jrK0vH4vHnGwoUzbj6eh4ocwxhvmY6fkVuOs8QjSULZkRDJMlw/q8JsQCLNO+6/2yUjFRMJ2wi9f8hmUm0khhPLpBg6AqAniMkS+cXnFj88aK0PdqWLtW7imXWG2+X/C9T0p00vnu9OzPQlapo8Fk9bW6cEy7afOXRa5w9mCXs7zWapbYPWU9/vdfgGeXfCDaRPe6N9Of9TrQcIkLMO7juunSab7DZ7imwhr7i14HvZ826ctXXuihkGVZDcCSRXDQZiItNDfotiDjgu/SOZ89Ocq8HsNgkS7w84FJfBWqGr2VmmcjFuM8fFPIzKwc0iTnsou2XVxdMwGoJ+7KINXFAanUyapyc8+98G0yHbQZFPxOzYOAUowMmMI4wDIun/2yCOA9MykNv7uVi8adbKZU8sqQOetPR9+aunr/p8aimRAYp/JNO9g60OwUhEARQwOZOBVQIIcPV00SlCk2vJ2vNb0jOnT2vO9fqu/sq6DfXujr4li29YsfP5Vb8M2YIrFi245NK+jcGgctcXtfds5EX5hoe29/dbpoX2PLCpZXTzRiNvVutq+vLwN+DuXeMRDGaemFZKFgCos/jq29ounbNgZ5trZtMFW28YOL0nuygarbEJArHwXEkGRKDregnHufVx6AU12yOke/k0HYusiCNGAViSq//zNiKxgaGesM4PZ8PhEBYPNNab6zrNBqfVJNfbPYam8/2KCt2JcN+9oQVABFnKZnIxs9koTPN0x1vMhM9lowGQZOIUa5xGVb32haYbl1x7OqiqK5a7Fcy5acvTjW54oPemjrhHIuR8K8HRVNV6j2q22xpmTot/f5v2wwffmi67bJIo1tc3KEB4wULALJfXxTHs4wXcjRynenFSe8Eb8noyvaTD64cGkOmUInbHACCMnZGlBnCo9BzQF4E+6xG2UvAdTyfSJahJ8PGMjjlS2tlK6HVBPOFI4yZg0y/36NaEm1SvfrFHeT3LM67/uEG97FcQC1v9stxso5MlJOpr6w02A7FY5YU3t4RDCqE8FPOMRhLstAVcIm91XTy46PG1d7d7CLhn3eoy8gYiYkZBMsQvbrnUbnH6TFKdPM0svRxyuq51zcKfM7RsWbVHvK5OxFIJ2A0GAN8D6XNnBVUb33J6zWCG2ARCxLMTh7Wva5+8pzVRKyt2waBMF3H2FINV8Ag+Y8geN7sjhb/BF3p2bPAIIkh1JmP7eSHVV+eAptXHScz3iA//YgJXdo2W9qb9jC+xhdtXmQPxlHOQ/khz4EjYE5NNAWV1SPTowLXbpl+wIgmHNG7GW5oCeBZ6ppiD87/UtHGqOVjxiem15TmQRELYDHT8xVqZAfH1Uterh+MjjX1qkqG3iGNDfwAap4HRYqJD71eqhv7Y3yedcOrhdP7uON4Ju8X779zly1zc9wGTSpFKEjTHmRSLyJi4cd8x5pO4UhxLKeCzMCbUct4pnP+P2q2LRvL5UruZTyxLBTEf+R9ot+Nj+se3u3q0q8f63x7p/ydtPrX7Y7b5Q3iCE2+pHafwTwY3HxZ/qr4Dp1oYCj3FQ8L4Y8wjIpJ9jJsqZir3SKU0uGwy5+jfK05h0tDJs7E7/5PGVOd30/uUlC7tC/9NyKAcU6tRGzEaIWi0WlQR/R+wHkqsKcfZU8hNdI+l4UeoyAbN7qePU/esupOT9rF0x6Dzjagkzn+3j0O0g4wDacUmikc+bhfJS35agi6Wgi7N+DG6qPMzmexyA5s/dnaW+1Qm4usBGDdL5hIWc51Tu+jI7mJu95Eju0lh9xG4x1lntiQoM6rZIapwz+PlmCO7H4ODquio0G2yzkO2cgGulY4kpYMyHUi+pQEHsop1jhXHvRPY5yS/fXj79mFh+7E85IYJYgsfsH5IdCTuqZa2FOw04fZiQcsVWFII4uCxARMwS/A4Y5kLhZJsJNIDb4nbOAmpyVqkCbhQNiG7k25IIQYBSOMgZoskPrbPAYgqAGU3I4oG29a/tT5PLvcocvH3Mj5JQM7A8GhBGxLfij2uDT0ezaTjb8Uw1bY8P+yhqRQPTfUjbWi0AMNkJB17HIYfi8f/K1HCPwVdxsQ7nqNiBco7iTMGO68Lg8ChkHavvXdRr027JwTT4LMwjS/JcHAXLRg9ForHQ7y04KJXYJr2yjj5FZVKp4fZndi4i3DuXnqnxd874fZrSMjpN13kbyffUer3ApxY4NyMz5tKtPKIlclWnpKGno5MjN7JU+Yoj7RBB9JHvNdDOBfUe/yyEBZkP0KWq39rP+G0V7S12ivLpR1nXOw3dqSSBv/FZ+yQlkM+GoKWUNZrt3uzoRYIRdP9/U+9omG/Xrn7BuOjt/7mzEA4HDjzN7c+arxOX6/Sv7CfEsLYTK6HW4St0meTi+NcerKgjgdtijDaqBBF9cUNLk2KBPJsymWccCZrx1+x8/DOIcIFHdojjqADNi4/snuUQTmf683YeN48w+r0ekYZGPIIYsacrXEIgsUhbUTYsEEb2eBf5j/shyEspnOIFCrlFP/zWb2U3UdqZbsDi5EkXeBkY9+1FizFTl7URopYFPFvgOAGP5ayrDL+7D6+hVs3UaZ3RkeJBqUSB1U9o6iw16Pq96TdEAnKkuphq55K9vfI9CaHSSFhl8V8uYvcCYvqG+xUpO3l3jn9Prvq/ouWZ6t/WDt62e7pvNcg2BXFM7M5Irsjs5decstTW4dxy/CpuJOTiFYs91O11Im+sFDu5S9VxVJjNxjhNS2P+0VzYe8B7UmvCRHs8PlD+zpnrBpatnLOrISHbTCYJFXu+3U4121MQtEx2bQyMuCkiaU7marf+4+XphzrbmVGFQsvjZ9TxSJJrj+N/qymr0ZbUVNzMb5BJjfh6+IaslF7bvxUKqQylRpOpcIb4BXMWwNfYhlqtA8wKy2kpMNzApcozufc0jnDWFqUTVUWVGAsLEqplE9Zujkz3ldZHIDJQlHqBT243E9wagpxVESFmJM+EDFCpJU5VeDYhQSNpk76wGig0cCR+z9eenV8bVV8A6qDlGH8LioONEGmy+3IZPmfqz6fWpxlFKqk8o3iZarZdyznM6vkBaNSXFfGuRHjXmcyVOmmtEwsf4pqWKJMVqrUdnKd5AXVd1LNs6ZoAiY2+4qzWFv2lPhqtZO0JVWpeaVes0GkT1WltYmGSWvD0R0rnvX1avE6cR/VzjAiwUq7xdbolmMve0Mhr9juJWcXAxaXTyz4XBZ0Rblxsou20gk/7lAVJ6odcSUNA6ZtMJqv9om5MaqnmgKKl2G3XM9JtUjjKKZx5YzllfSx81a65i31UGa9leTpdOjIC3TocGDo8OHQ0ZHDDpvpmJrIC8yBI4cPs4+8oBgr5Zfh5KTyvY7xV7O0qslqNIhVaiGGqWtHx+NjA0QeV4zVjRm3Jsa3ZWIjKrVX1zu+xgkVsfnGGiQQJYSTOo5T9U2BzQZUzQitxzoGYxT2xBeqpoVMLw+xr/imLnMV953Apz6e6RPfEEzijxFj4sTSPlQSR2fclDB5s7gzFiO3xbbF+mMxzQdvxtCxLUZu1R/Mo/m0uvhWdOpl7jrxlNCPZXrLclltTOvKSJ+9ejeE/hiWuzU2EIvBm5ovFhuIXhjFWkghlSjuwlLp9Q+8CW/Qd388jmHj1wC9r+SoNlEk5NBVhtyOkK43lAw5dOWhtANPinHSQgXadTb+J9g4gO5hgmi5ieJCuVLMyXmg5WTZqSr5pVK7yq05uQ1VukeT1lqifVsmkUMs19PC7mpTbUAZ3m1UkscGjK9P8dwGkNnTk+zoBS97jm/DNepTT6nqOrXORx2+OnSeHAJ7J7QNHvyw5KUQeGnKsfEyuTLaWkTHEb1kbfXSVlI5yar2iYJzPQK0tuX3+FzvdMJWWgVpcI5OlOMM+51Ys3bB77Fqpx8JmkMqTbdkQhuq5ctmcQsQc56op5ZqBZ0FSVujC6LQGwArlFPgOZztEaITRE4rMurcJY+v+Xve5t0nm+3GdCicau9vbO+9gEW2hILhWQ21kJ/Q+uGKMDv50tpDK35R4zxXMs+rqUmF4q0e/665URqtdqtO94y2Jd0TgWGsT5QGm1Xuk2MM9BgjugKE/IQuj5Mw5JzWobIA7ZAuHY3uqg6Skxo/jIEcjUWHVmAvJ3/HcCnE+Z2J7R2Dgzama1TRPWmFRIX3YgU5SREh6g+At6KW0gM6fwbjK2kxX6WMHshW0mI+LEP44kV0IV0UfPhCtpwufDg4MQAui/vujL31MPM+/FbsTho/IYBwU+WuBMC0qbOXAsbLgUaY1DynKxrJukZQLy6IlH5nUKJSbLhyXZmp5B4XH1R8yoED+Dio0Lcywf/ih0lCwg8nz1Tx1364OPTJstrGKiy8AUqHyJRCm/do6+jy/q2qnofve0DF53nqng8V3vw55lEhzlKyLDTvsY/Yzhs5I+dkusmpBDD5MpHJcSJQYcN0nZyShpAYXFO0Hhi+5IcHV4/Wkr/f9BiS02Jwz4vaH7QfaH+gQla4JXRC/Ytk78P7i7Yz1hz88bfJe+sPjt77CPRqL2i/ZxKdAZgF9dRFz8PciTS2oR9HqqSrpJ+tjL+W1hls7MwFhmDNSRVzEIvH+6nYAz0Lya2YKo6HoPYqHo9zSD6f7td+i+flADuYqXDELfH4kvhWTNCv4yVpsVCqT+dxMZ0zqPCq9IlilKFYiBV3JVLJBJYPsWIuNWdOihS0V7H+eCodxxOe5DIxdiRjBRDrT2PtEMfa2akc0XGIvNQv5qjWPpQ7VsF+Ksd/qUKpH0uiWla/ZUWxjmBR5NZINkMRCpL+kLbQtpbxlqewznxZ37w8mKVuVXCw0thipbSXaXziSDHcIj6QyqcGIE7Hrx/xjnRMR3Qor4diOr/FsR4YoHMRp+jIGO5ZoPBeoumo/LZVxEPMVdJ3byUJgn11hpKOMt2mUUpqZNOnPrljU09EFB02u1k22/jr0o+QH44gtUU4HqkzjZJfwJkbMqfvGt6cnSdFjDaXw+jDk7L+8e/vh3soJoKpuHHnaZveEq9nDEsvLz8mF9cGZYVDur3ozLe/K9rX71J14V2s/i4YwEV/Ke+lbu3r1K0oMHBXSWIX/uJj6StCwDQ9Jl/MZH9pBkzvYxlS8ZLMoOXE7eLfxcv09k3VjqnazeTsJmnIFO0muUkbQu6ZtNkV+xqirhNZWo8VYK2skAoAUcqK6uoOMX1RqudC1ViYB4YbO/ngZKEsfakugnXxOi01gV9Myz3OxGqFQqmkslKqToNSOopqDQXAhgdfYkzPTpR0VaHpeAb24tnnFSvtjWWo9pkki+KPWut8Od/5rdr7DNK191vPR39dKyjo1KNA0ReBUorS3oc3MfhijP6k9iJT3U5+EsMvxvj77y/HQJJpg79Yiak+DyitMp1JjTrLO/5EnX9eTSVoAOKoqh5C2vQtu7zlk686LQWLy4UPJ3EqivU1q6I4XNZvWVVxIh5y/K/PWlWX5VmLS4XzyEVmyWCQzMV7FJutfMeF7cpxFs6DVPMSiiU50iG3w13C+5LsFtnliaYY8pzs0PXUqnXQdEqLWVthp3NSN7/S4eGHtULUX/BHtc7vXutrwZkjv+5sbPFd81wjPIl4lK4DpWNT3zxz794zt3Xn893bqAu+aXV+vRNeKRS0aZ21dXX85ocbOpd14l/Dw8MUDSvDlK7huPfpvYOPPTaIL6fOL2M0sJvdYtCGC0yER5fcyFIdBNwTJU7nBQLVV4hQ8yVUElZXNKWSsQTRWfyjBihERHK+oL32hz24vGrcdRtdB0D+ho/EXa3aW6/+cuTeW2wHvfa2lp76QLPLQQw837Okx0+Maz7x7EXZr3/tq/cllIQrnKhJ9AbtfDwVP+fITe4aXHM1G9Wrt4B01qYR7bmLLmwTl+QGch5fvWCVLHJkMDNLFeYpyfRlP3tod9Rp442JmJJweI0b9u3UbcGIlB9qo9oX4sSbFhfbdBNexugUcQf3JgICvVMauy87wc04bWjotBlzBVh324F1Wd3Xx+u+4Yq0vKAu3XfmykWL1ieH8gBNq3Ze/4VN5ZCNN5RCSrgEHXeByrSHmGGeeAJ3fZ0vLskeBHY2FzrDnEkNc3QWghxuedkMvr1S/vAb3bqgV/cbh2+Eu+EVuLv4lN91zdf8jf49q138ha7btETxPS1xm8t1G/yGWOE3t5Hc27u2XPktqqL8rSu37Hr7xb//ncxs9H/tGpff71q9R/vZvMib2lvgeSMyL/IGeLT/eoPp8Q7LVAbcyNVy3dxc7nSE/GwrsKY6J7YzRttZ4rJiCir1TFsc6mBarJTXryIthFQ7Y0MLeFJHs/FEFhFt0rJ0zSbsyxPkwFgv4Ca4QNuwdYbiNO+xT7vzb2tdrk/CC2A5Y31GcYq+aCDE22MP3gA1Bii4EgsOabt+t+QVuODKy57oPevLM394e29hG+2nppGLx7r5V5l8u2g+eoZ9ARbbP+fXBxoGGt4Cu+Nsu1l1qkTR2m99owPen75vQTi3/AvP7nO+8+2vXbY999Wz9Lmz4/70LoOnEIWo2Cn3JB48ckWqFOilh1B1Z4u7ksX0mslS2pUsPBeJOWaHj3Hh2Y5YhOccXQu6HsaNSbXSB+yDH5tlk0m2alnFYuGfPJbv7a0Ph+upuHBDNFo6ky4UL6R6hrh920Atc70TRmAc8BagagZUAYltQ0bQ3V4Rl7w4NC038PCw6MjLZoG3Sdr/0Ypp0TJktBKb8eioiYCCbok8B7wmWHliylvt5JPDAwVxKFUYeLi4SLUOScBbYFQrPuewDhmJafSobLeYzzZCGnjwGux2U94iPjQ8kKMn2Qn9ruJk2euy1PVp3GUc5y1JjscmvKHaX2HelPbjqnTZCXGxCVoqJXIvVGW7wJOHoDYCQ5DTCtrwRDcZYe48ffIcDdHd2vCY6g6mqYQDKy04Fgn5gdQxpjGf39iX69sI+gtD9HqDOZYtl4PgKJYPBf2NoSQIQSZlS40djH6RJaEZClXBg8eZgRURn0P0mmFIfw6U6Bhcz+IIUjFZbgfVIZRbhSpxhfJddjcgUdMqJTLZgJAM6aoL4KxEhvAowCVsrZZ0wIMgk+2RKqnJ/V2DnkAy2T9thKnTHhMlo1ag99rBrZ3rUgMdfalZdbNLSajWdVm9kCY5wbUv7WquCbbWN83tXnPmFfP0MiYElnMJDRuemp5d1FTPWAyjVj8tBdcXAC9bveHW7sSZX2fxVO9R+w6/u5wg0NXb2nNR37orlq1OhljmcSF68rF7GNwOKWqKCAmuKEnEPSyeSMczcXoGillqjqEHqOKezL2rnfuPBf0vaMdmzHHUCbwICjETud3dVBMwPfDUHe/CwDf+AZ/mW7XPaL/5vOHLc60G4nGCYBdsvJUY0t7O1kWNZ4B06Ia/fGHz58fT/EmmOex2MayofJLh/hPgO3r4ysl2Sq7+89rD2iLt4ed1TZG2rhWtza0rutp0LzV4pOmW30rGkMZ8pJD/ofbsU09B3w91FmNqIO4RBA8lhCif+LyxpNXZynxibpfUL/SzG+0SjWecQNpVKDuf5isTdTp1Cru2UiYuvKHVIS1HKSydlmPlprFcE7trOYmOM1aTb7ToMfLtTXhTp9z4nE7VkVvLlJvOo05U7lXlPJ7ZMarlpdvdauW7oBvGad7qdgdCTBqgfEGX1m/o9C4ywyK8H0l/eocnclSPz2CSBYK0hQ1yapcKOVvcVyA5u3FYJnmbVnDNcmkFGlYs0DCq81fOgWteUCSH5IJhGEaUywF5j0fLO2qoEJqpYIJDNQ4t7/UCC4K8uWA0jWXRhqr4SXlR1+GeTW3M6FIYQulNtRZlMUDcLrliMZBCepaP6KYDOwKCl4ljMO0N/sfs9eNg7fG3QRZr+MPMjiCSnZ4Y+cpPdNa3vdZmEmQQvuKLp5nuhv7HFzSuJsbvketrFHs7Faf3WZPzBD6LTouzwROT41X6dq6T75XqGe8jv2/D8dyGffs2AD7J8IZ9/HCR+fkCfQb3jc3pGib33axDjX5Ol9XtqbQS1dQAOTW+fHlNg/Zky6f6jhfC6QZYhi4hF05rR0YLG1/q1r4sQqniIP4WNUS0ncmFvkBDBG7DN8waPmuRtlMSHEJVYyhvhyMFicnccAyIJl7xjl3okgIuugnXt1XXr8JvU3T9Vt3OClzlMlbfyyAnc3xBr6t8pzzxBnn8ffGkBY7dBk+4/S3d9pZsfMjVemINOi0fcoz/fbieLMHdl+THflQKbEzUZ5xdNarqBXnCUQ2OE0zXC/KjSL8dHxZ06SmGq79YLfAzjhfSzuXYqZhB/FZHbr2IxtJXPIGIrpduLIiv0hfl/yEllMictNlynXPm1c6Z371hzVXi9b8/rX59W/rcxfUei8+9bd7Ou301935p+/du2zwDae7mI7tHmdwUX9h9hH+w1tg4GLf0XbWmXpV3nt3ReWk31JL+XVaD0LsC1vEbF+7+1JFVTuN0IGO5joxrv8q4EdkI23XSjG0fcSfZGE9oZJ33hYbOi798eN/evSDBvdUNIVtfvWhG4tW7bt/7avFGchW8X12bXGXbh+JrVFOulespUZBV1ECmLM0VSoc4ezwo2T1B6uZDCG5ytSkA3YAc0qhUiMTZ2Wh9j8k0jR6itkyFfMlO4ejrVLMPuzn6vVzxainfnz7Gpfv70xI+yVf9zo19FEdo7DQwsafR5/LQAD2v08wCyWuFy2/J54+zDCJ9sjFbJN3D6N+FJfkqOs2MjGfKHh5K/zLl4oTsLTHmdEm/lDNasnSZLauFBgQ+t314u9rUvGx76c1/d5PDmAi38EOv+Zc2N/qLZz959NEXn4WO4Udf3AvnDPGt4eAmh0WRlq06Yyb/5PD27cuam9TtpbfGOTYF8ZDBzI3NS/3kob0vPjoMHc+++OjRJ7UHhvgWPDkdmxRpcMW6vvJas+FaexdnyIHzch13lDteJTem9w975qi4quwVuT/EYNHHN1dUZawImMxRSQY/nsBNhtbDssepuBEVP2JlUVVtL+45WL5eArbK8d/JzOcZFPGHBrYM4NmiP7W81fgpkzvcKcve3apJuSzWaDLL3qdNTvCGmy6XLSblLlnpsXvNhxVrJannCpo03FKd1GCmSc1dNq8Jk5L8fWZnUthDDANWl8tlHTCQPULSab7vPosjKQg9naWIZJMkXCEkHZb7Pm76kkmmEwy5RwAW0iWHdte3FBVqIk3tcxXFLAd2y+tU84VtNTblk4r7DNlwY51RsS71TIvXgMNUSWoymg2By+V1TuuFreOS2gc87WEvcRRHbrPb6mp31Ar8wo1uQtwbF/ICeutsdoyo99IIEg2eiVELm8gCGuett/Hv/ju5Knsww7FjjB9llxiWzcwa4WSnEMPuERjrgd6v4MKUEe0ISBTSmBaHFAnSFRtFqMS1S80dfVt75j9Wr7v6/mgHb1IJEgNE5CUQo/Z6t3L1Hd+G+XAtzCddd1ytuOvtUREkqneJyVzmjuj9V69brf3th7MCD0Pjzmv2e68/xN+q/dfbB+xrG41I0fKyJAkyT8VC3LHGmkU/233r2wcOFA9c8dNFNY0xd1wCjBQkSeatdpCNjWvt+4R1qza8u3+wf+EvK/g80wHs4i4as5oD9CBMZei9f4XCQlQAe0pJV+xXD+CBQ1lvuCJdbGWwn9RC6CCN7ad0UVKKjNrhwwRU9Fo3rSM8vrRDGx7KDflqYk2erBCvnRZtStiDQUusvs3bLv5875UFMRBxpl22YEt+hjGOWO4Xbo+eOfTMVTs92gjdP8EZ3TxrRo033pJIrtq/oP3JLYd12zsknxyc9ePZmzb6Lr+xxTtP7AimI1FnMS/JNoODLH7CF7AvXhLsmF/b7YAN0TOWhKKDc92ezYO3Pjy9pbE/TfLp/pq9/enaK/c1x+bcsuvMcw5zZTt9uqxqN7V/XbWjJdhcU0WqjM6Ika2iPmAiVb4jXrqPx9NUJ5ciVeVdjkmlUlNhldMHgYbuYLK7MqKV4WoJ2lxpZyQgblqT3/tzsd3bVh+zBIP2RFN0Wm1cyHqaYjU+HE8Y6liaP7zlyfZIZP+qZCLcaKpR22dvjmrvsDELenbmn71g+21fhC4+bpwh6LqiGhfZAI7u2vkdwSWL7QHf6SsWE4fBJkvFvDMaSQc7xHnelhsv923cNPvHswY7zj98zpmXz5s/JxbatHK1u2Nwb40+ao3Tpj14QBzc7HHPHYyGluh2lPkco/MR2zrJajGfm2iVWBw59vzJZoer1yXV4Z1Jbz5beUb901EMW3k8MpG8ypZw1Qm2oKV8y9yhDVuu2LyoxtnjrFm0+YotG4bmtjxD5pN5386/UbzbOYWdaP4Ly69e3GpPDs71ezz+uYNJe+viq5d/9pniy6Tt25+lxqKdk5mRHpOBDeI+0khxuZjLYyXVeIa7FFDCNmeRAF+5hask02/dSJ6AaLNoTAKUWscqeSnuCNSiuENSBH5YLY5QIUdmLx0K9CouOCQE3T6LLvSuWphnY1+R4qeCbCIdKZoFEwdLdhqCiDAXR8q6zLo9AmpPK81x2aQjgrseO7H1mwaKLIflZDri4dHNDmH3ROzuL3/60/uwYOfihTNh9iKy+E8Hr7h5MfkTz/9JtnVN2wmvVGN7e8g3fpmaNy+VnD9/9Am44/4Hd23uK94G++LOyIwHyGXVuB/jpzO7LyYqrw86KuFguARtAG+l5swSPKOiMklHiT6kRKMDd6ARxO7wjyCtqq1MEocZ6sQB7UJf/IFzKuYjU+c8QIaBiYsw22ral5CYrTc76uCNuO+q5wmn26fUuOcrNBzdRxOT2TCu120UVysRVCxJTnaXOCbuS1gDirmKbDMz8UaFWp8s7tSvFMltT6q6GCQZ0gplIV+WsCzgy4xK8iuowCTLx24WaT56xTlmJ8tL4XQKGDRW+pSKI5ZT0oSIhJoJRTz1II8wGQjCZUd2U2V8BrPAeqKNlGC2FIaY/v2TgyIki7kqyCUFHXINOlhXeAZUrt7CLaZ3GGmkID2xdMgl48nkdumnF7DLpPI86PcubEumNlFKzKp0FWUNP1pygjsqfPcEt+T2o/mVt7+4ozkdr++e27/LaR3FKdnVP7e7Pp5u3vHi7Ss7GyGILaPs02BjJ7n9kZ8OLf3s+0M/faT+sy/lF9618zQx0xQeTGaWrJ+vW8mZv35JJjkYbsqIp+28a2G+sVPni3bq+mAVfQgr5+ECuPamc0nudtw/pEScyscnPLKEjkTJ661605crIqVTSWqvC4NLUgutlD2X6BHoEZWII6YdD8utOC5eXMsB3kvHJ0xtw7Th6g4ARZbxx/cCFQJgC2nMUNQtBrPFaDCbO4xGg9NoTIsGhecVxS8pRhl/ewQbnhr2LrvD7phFgoLdzr9wZPeI3eFRUjPXnz2n6bTYdP/WRPzMF860py+tnxY7rSl39vqZjUZ3e98crzrb5XLbJTPiuS2KYulZNJca4/B4RsoL/5tGs8mAv7RZlnyi3CaLoizyYpOsmETJaNpllgSPINpNxGIivGKo4Qn/FbptEIPb8dezp0s1mdP2nn7l6et3GBtranw+U3C6ccd6DLhhWaZGiiLW2tIUbBR4o9Uqikqn1xtvs4AgxG/gPV6+QuSW7TwUGJ+KrfcPtzXIjIJmsnT49Lt5PYpaXyux66ayNvh59zndwHWf44bPM4ODzVRwk0ptnuCoITYoODNOKDTEpzA42LloUWcnGWosL8dGxEYLqqrlApXzVDyBsDaDO5eep1R5OZ0qWRegJzUVKKKrh7iZOAhdQvSymN3KOrMuohsl0tOyjPo1rC5tqKfFbAzGEA2+zmoyKwZFEYzqUlfXn2e3nD+388Ccoetm1HpqPDVn1858feZT51//i93520Y/dfWPZv6+E8MWb/bURhfnVy+9/7k9XX+apQ64li9RiCAYid1JXph2a13AP93nXe+JOcHY7q3xZGYs/j9/vb5xuMm7Zlq9pyE6/VfguvUx7Znj2Wn19ZcsrlnrbXy46ZJfvPSNObO7l7Yrm1d513kVh0PxSI0PjJeloDqDzGICpbsZlsbRvUIoGVtitnARu6DcSDo+1AneAK+b+qJOQjU9xLzL5N68cUNdMtewzLhpMK/99bT2CB8wOeVkZ0ftmjqr7IyY4kEbX2+dOXemIrth4HsHSNhaZ3R2dnS5rPXNQu3MBeoCiYfGujW1HZ1J2WkK8JH208CRH9xkXNaQS9Zt2LjZbXLxEqabWSs011tdXR2dTmOdNUwOfG8A3LKCZVvreVswboo45fJ5VbF5y51KwU0YGtMz2fi7MVWU3UdErnzG0LjhsQj9jNZtrki6/UUHZL2gfqjxlfwoB0+ccQY8YZ7SCgt3PA6HTj9d2yqu+3B7LGO8qPn0tpjqgOEORw20UdS7lSSqJAioU0RkhlmvRhqH8wZEZnzjZJYa4Rem06Lfozhnddpl1ezhz7kzSyyS3DSjSXHxfI2vzquY2tOt80TRIjtJF8z8jNTubKqN2mfe40Z0vhrlgTUm0dDir+ddypw+WbKQ7J3n8B6zKluaoi02xeMXpemtM4KCx33PTHu0tsnZLn1G+34XccoWUZzXmuZnjue/AZXlklaJ+od2GMeCWEHQKVJ6D66/usHjZXfnHsFbsgdG+YwZadXcs2DgU7/UfvYF7W+vR1pef/KCxxtC/pbm7ffMW9q3dNqVsP4Fw9H9tw1dNBS74Exhy6b5Vv8NWvGd/3XR3cIt5JqzRZP3K7uEOD/tjpVr++/7mhKP7j96nnvmZb1KWb+A58R3OTeFBj5CLeM4dPNzVOyMuOEEQOAP2uc/97kvPP+HOxJtbol/Rfvj6A/4TvB//hvPaH+0hCNBVs4TbF5X0DXKydREwr97vOGpyVEuWlBwSpz26p/rav/dc8pX92ft1bKwJskf1y4ZFGucP//3T53zeGeNOAh3H/+pLkrEjbPxFxtnoTpO+avJ8XZ7KEbDBTF13If7/6FXDg2NfWwAMVtme4cvlHUAqG2eQmdjlfXDb1HTPBUb6vpeUVuyR8ZNsBGUdNMGUOuLiF9TPQW6mWTT1J5ayC2N0P1BZ41bVCmvWizqB/gcAi4PWO7GvjEuOAaPjFjU45xqIUPFYYtKzabldVkVsfwtpe4qDV2PziSk2zPjClIOIEK1xylWYggXHYszM3v0usIu2U5UZ/1NtVHi0Z55ozbkdvjEYYjuuPQmYiEup/9OXwzMX9X+oF3zq9qIy+njQYL//fQzvwRdi1d73u9yh2rfgPkeEq29qd7psNx06Q7ttUfqXK5I7a9gL9R/1QKx2juR2LD88pmntVBJD5Qr3XE1cE0Ue+Am3HN5J35jJlQ2wwyTWq0V7G19bW190MZeD1UrFB/vED79gFBjHX3PWiMIX9FH2v68Y0OWt2Y3OJ63w9l9ejb69y6MWc6Cv8DvLQ6HpXhticzM1XaQjem+vnTxkQ62t+5ltHgrl2LQQCkr/HExK+4tVsDjzwr0vMxkK1bPgxRoeAcnOgQpT3kRAyntLG3XrD4h7pKcM9ri9Y99oVWertbximMPq3MEvgYvpgby2uXaLXAln2d809QArA+pG7clQnOSs5sCszrqmr3Xd12+akdmYx+1NZofSI1G+ae1nzVp7zVX+DZUvsOEO08WEbgUw1fClCaAUJyk7UGHi4h0aNlnCugAZ5z0RNJte7pdMh5Zdie/zD779OD5i4u7RednHivmHxNj2IMcriwtlxp49rnCZw2dyzoNny0892Tw9Nl2++Lzof0peFEDrF/Tkk+lBugiG0g9DL8B6bHnXE6VrjXV6XruMe2YVpIDJoiTaeI1jJbxIgjb2JOK1ctM7llmZtXps5exG+mT2jyizwZmC4o+vR79aWN2Z2Rx6JaAYm78dtrcUN/0dLvSZJYbXDfd5G9uUtqfbqpvMKe/3WhWArdMSNVUf9NN9U3j05D8hGzEQ7OZmsayNfvHF92kmBtuvTVgUsalqXybjK7pNLdlIu+RCfZRFRa5dBNAuWm4x1XzHsv8NKnEfCxp1ZZP6x6R4mqCfkMSqnAexceXdhQLgWjgjIU1fTWWxkULA/MXBoOLnn1++dESxxH6Eeo+ccERIcS4jjce/czsEssxqHhr3HXWGjInYmkMt/XGr3nUA5dXMx5dM9MrW+Z23zrNnVu+vHZmMZ/LVTMcB9IXHO6ZqXMb58/W2WZG1eG3+fklWfeKnlxk/555XYe5qvHJ4i5xGVKASYeOt+h2vloFdsuFGCj7ahtuFRR78Ur0cpCRuz0wgR5h6Hov6LcWOs6eDOnnP5WJ8wYkhuIMBYOROV2N9YQXyaJGaw2oTo/bsPAMHLFioWPpQAr6dU6kcPaaVS88C1t0qqU/rY3M/syz193xJEA3HxKOXPCJw1vgcs+j18R728KNlsgcUmOtc9d4FQim+/MkX9PRHOJ5iSzPeRDDDsW93XNbVqZnugaTqYEKK7ImePqKXC5eGt0iDtbs+Z+6TRw4z+Oe2XP4gvMPd83bsz+S61nhzi7hcRAdqrG/wqtlfG0GW0J5JKjphFYIsztV2aHfFDqY2V7dZhz7z44yxtiWqk65VrFEAWT07wYyhoLHy7CnMgn3+LipTp0EDQShIU+nvTj5tJ8/Bhzr9M8adlXD5FSAu/ojQGgFnLnq8UlxXZSXTfXF2OU745fQ/1ZBByKSdDDCL+2guKMHVxz1kYoVCNybJHY/wu4lqXpoyVAtk8Kq0uqk1FAuV2TTbhQnm/TWmWNzni9RxKW5zsyhc51ZcuVNE+aarZ/Z80kOIXFRCXANwhRgG9Ghlu9mQ1ucp4NqQP5wUC0B9niaooFhQUwvkhodZCqAsuRqAKBfFqAhE/QkqUyyphxV1fX0mwGH1jud62ErOtFxFN6nmpmTaU4e1RUGaXpMqh3CXOg4+uG6lKxtXIp+9InqJGKjKrqbrImejixkqzX/RJGVrTdGUxhG+H6pqbB1PVgmNm1zhrW+1BjfWEMxtTalvmSpXQldVxL0pvRCRbuVfZQhOl5v8qSeVyoD68RWncda65yiL8VTtauNDVdFSFNX6HR5gTVrnE0Sqs85Sc+dbFRObte5Y7M8CQxwJz5MH80EvyY1E/QPrCB39JTsPnrjlB3RC1I84ZJTcSlRJmwplRtnxuRkpIrkTDyRZFEy0kBuDz0haJSEu52VUDNz9EyR6Y+m7oE0vbaLeJj8PR67nkzCw1JI3rgVaA1hWmSGFsiwPQ81XCd5ZEpjUkIrztiSVGRF1gvxZj3eOL1ER9osEWamKAMk65EzDEOh7fJkcUuRvfiWSswVQI8cliKMn5LN6AasOwJYEYuNUMMtlOtCn3Rnop+gyupxlKD1ZDNpKZFqJZTZy/LSUZLcYXpp2cPHGW+Lyk5SWrgHWCh4mFBBxIPtyqbiWU+WVY67Hm1nDyDylUpjBv1WM9GRDSNunqFZsTb2yqTYhGQiNICOEX3H+QwTQ09k6CTwUkS28l7KcKPUqRzHBFaBurAlAYbx4UC78G+iJgk/j9gkIoog2a3xsIN4eb6GJ2YTSEYrURQJiI0Az4uSQQZewsOVN/E2uyIZeVkEm4s3pPAtg8Uv8D5elGUCkijwJlWQjV5JjNaGJEk284Q3glnmIzbRIhgVVbTyRrNR5M02gwIOuwGMosHA+xW1Tq6TRDApFmKViEXBGkXRwMtBRahxiIIAvGDlW9slSbSTsEG0SjJ2SCaCzWqwSwfPkEWB8IpRghaV8BawAy/L2DrCOyyWELbcaRYEs4F4AXjga3kggkR8NoqVEAPm4hWri0h2g9EjiRIhFrOLF+sMitkh2vxyVCWiSSaiT8SELoO1wSnyhAhGIgEgru8ReQuOEwGjRExmVQZ6RR6WLSq9fDcLhDYehxHkFskmi0Ss4WtFHnsmKsRkkA1A/9lkRQGrQ3BLsgA43EZZFEWjWZbEBl4mvOAhDp53WhQ7bzbyDmLzOI6+dDev8k4JZKOdJ4pgkmQ6VQTcNtFsNEkiwcUk8jajVbAQnDuiEoGX1Toi2O1wkqKQ9jw4QDGDbJAkg0o8gGDhAbsFQYrg0BtreNEkIniLikIAcFwJiJIAgl0SjAYiGgXJqPKSVZQdFoNdMLglItAxEj22WtFgtFiMIlhtvOSlE2szCzaxBsdSoUoOTqzAiCPkRbirBZvBCmYbjplslDFQEQDnVXAJYq1g5EEgsgEHFIfb5sMmGMEqi3ajwEuSWeKtOJLL7pAB7NgFE/gdAs6ZFacRggkBzNN5vtEAxGSUxIgk+Y24mdE8xNVcK4hugcfaZLfdQ6Q6l2KISrJFUggOuoB9DQuqASxOEy85JUE01BC+3hYCI8KN7BQMNbyRIBQjBCCuYLeYsQUqbzPwPBEMzXYl5LATG0/taQoIjbxRMlnAIdY5eYFH8OVFq9KILodJNhiNBt6pGkE0CKrdiDWZeDsxKwaDLEsER1U0gEkgFuwBrjQgiiSO3hD9JNaDyIKZttaA00whjccKcFkRSUQorpVw5ZqIkRfs2Ble6bA0OGptHkGuMzAtBfcJt3Qto5vcVIqxjOUbS5qxVG40gGDORAw4O8e+QeGSRbdX/wyFjlqRzxVXU4njrfE4OZL4BHnN2/bW7bpSzqzrptnt2qvfEe+9ymhzlO4V/ojJYxcyWecjmz4BtyXm7n9CZyKFAqaw6cjINn79QhdX/S1OXdayDk/X2Ui9hNIhKP9O8Q3XiX6Bo6i/lhe4UfpRLmpC/yNZZmTm+fFvNFdmc1EzFG9O5aH0t4j091Uix3iUHrlido4q/rJvRHWIVzkaNJVZmzvBqZpKP/4kcs3Cb5rqNbXoY4bmONUHb8Jf6psSY3Yp2cxROcU29p2SqjEIucs2oCLuEPv+wMTrSEK/HMAJpW+q0Gtr+lH0oRNY9gfcxj4Y0ll2MNS3UeTyRU4L6uyTYdq1YRwCqgCS79uoGwPfWG0TZyHHGQllFbjHvghkZCdQmdGUoco5cvnjRboNKxsGJfTxoBlZrMhrD8A5d2Gnyx8Kukt7QHvgLjpApY8A3QXnYIDqM5sb6X0USwPnYCb2Ba2CL84scvF/mDxfIDEhFzXIRXPRFKxuloLWLaq6HCLH7Js7uBncTG4Ot5Jbz7jilECx69yELCJCMPnXq0vcuPJXrJkJBybfwwRuMS8ppSDLHzl//4rtV0v9V8ye2ycK4z93rfQt23/z/mV9Sulz16O6nTx+dUmalA9tX7H//EeWi31zZ1/RL12tCwsShMLlS+Hs5hZvrP7WonWKT2OLHUwWT2sofSG7+NDS5VeJu26tj3lbmmEriyzri90lbRff5ULcXO6CktUSJIUDAiPbkBQbM7CSgbIBlnJYtixOw3szJW0JfZ9JlHTxS0pclM/iZS7xSf/L/sbmAB80qXJno63WZ27gQ/6X6poa/ff4i3P8L/kbE/X3+P0v1zVNTMVfd/o9K6+4cuVLK9euXb3nilUvr5rgh1wjlh7kG8y+Wltjp6ya0N3c6P/POt9BP/kzOvx1B/0JTFTXMD5R8fV3Vx5cefp/rrziqtVr12LJ470lm5OUz1zH7v8QLri0zgjk6Iew9CtNOQBy/vWHjhdwu7xjJ4FprzwIMHvB0NZDTdd/FvIPvY576L5XM37bKzDtyTt6Dm3t7w38FOmNy3DNWZiee4had2dQl9Ul6kvSKS30GAhBIu2IONziPzvnbz2e3zq/E/6ZK5u6ivty2tvau+QH2ruu/NozrrvuDL4W7iwJce2Yp62ELzbE4E5tR0zfdqAkyyhzS7kN3FbuCm4/d9uYrX8RGI+R7XEMObeWljrD2ZNMkJXJNIbZt2PY1S7DtqlQbWnSKYMxo5uol9jXjvgeZroHy6I+avUEC6El4x/mSoBbZibp0Z2ltfI68wwuhON+XgzkLXaHtbj0YoOAOPGmlfvuvnn1OpO8acW+gyvnGS179liM81Ye3Ldikyw2tZx+4O59KzfJmNJwMfmq1WG35AMi7z++obVj+YZzlyT0V+vyjtbEknM36C+wDoWsp/l4q4h40q+GyAjumMPUvL1V8PFD+eK/vkJMRD8kfdqlrmjEnkOUb2+fADPaB29Pr1q66qqBO9KrGizGxYuNloZV6TsGZl+YOG1V6o7B9hkg9MFeg5yzR6KuW5r3JWdH6aM4O7mvOcoeZHiWKeoytPl4O6JF8H+CJJfTlu8YMhBBsAs+rZCDw7fwgn7vop8bDVyYi3FJ+kWJcfcupROyrCXidmSSMoSMEFLpIVL6NGcqU/FIw+UboOII/RIE0E9BUP3+eV157RfQUmTP70GXxiwEEK6R/7XuFCqq/RAsfXMCM2MZ2jcbf6H9gnxe+4X2Geiiujz0axXANQ6N/kvI6z7G1xZO7BOvFq9mVpldZa0o3YJGSaC9pOUApS8cjPndE9KLVz+466ZzR/+547WHHryMnKl02y1K8ZHTztt6cIA39K7IreotPuML18dr4X6lx25WtPN6L12xtpvMP/cTux48lzdc9umHfrej+Ihitncr5KzBQ1svGBj9Z++q3IpeMr8mXh+s087DuB4F7u9eu+JSLGzTOHk4qis9X/+2B5OBY9+NGdOvdyTLLK+JuqDiJN8aBfohII7P512K9iel3abfwOVxuHkcbi1fpaGLYcfY7RzPhr/G38g+SpS3zlCgVnGVFeyPc7rFBsJV3eaoo0NVXyf9s3/o1Hbxqq+phaHx18z6fRy7xypp0nxcS9vj7e5N5a6ypv3mZE52xhdkag9Bv09LcFmK0ZQ1x5zlW8IJtXNThJ9s5ZndE+p/4rvVN2vH8pMEVrtfZtngLt3g73DFsDH/h4kh8Pcqq8d0WG1Mx/OfXABX/ADu2hdyV+N2wFZBVl8dcqKHZNNhKcI+YIXnkeoOMaarfmeS6GGXwZSZm0yfbBg8lE6mKLYpyYls0nHKQbjmouVb+2bOmFnfcoHPMCOq2ufYt8LgWckuoh2S2vr62uprWyOn15w1a/G581bMh+vE/9LHwWnVB0r7yjYghuaFN28V366OqR6tVcs29K2dXu/PGTqVuU1OIOnDay8zLyG5h6LO5KpUyzRvbd2s2cmZKxd2rGzN1nZp39HHzOpU+cvPOafpkUazIzZwnXahdk0lYsK48lX6Qmn6fRWYIBAY05VJMrqRVqq9YQOqfMIOtsrlAB+q+oIKw36YAgjFm9NZXerHW7K5RiWpJKYw/AGT/IPv+r3t+28EoWNX3yWKySqaV1k70mv3XDpvbl/fL+ZvmRV7Gz4lN3nbY4uWLV521aXLb5tpM1C68TxbwCZGprf0zF6c6x+c3rY8TPJj39zLRaafvf6p/HWqORpfdlWXsw5pyvs618+etXbx3Lk9rlZ/zQkukb5kc3ZGpLXd6fY22s0Gq+XC9kA8No2El8QNM2NRt6fO19U9b9Xi+iq+6Dn01kmNt+mGaVmfOrKy1y3pA+Jxl77Go/dW73GrPmQ2QNDyerJjn5uh6T2qZ2zk9E9e4IaTiE+0L9geM/CWuq7UvvDqFTsDnQEgXbku1QJglaZHuteesWVNZ0u7I+pwyzakudVwy7lWsurFgSuQ1p+eWCzZeINVctt88SX92y46+MSu3V3dHrujVlzttI59Pl0MEbIWBJlHGt+aMxprrZdbEtIb2p+vXjo71OZ3hqL+zlmLP33apntWz57rjgDhVyu8hcQtco0ZTJLNJzeaVO2m71400Dpn1sxgqLWtf2D3sgdg8Nu10WM3lOfGyXFKRWZjoo3/O7mHdMsN1X13TPDD/7B/Yn0n6zyW7egxKeIq9/gYjZs67qOnrHZTcpfJHohUtKxiWxDuqDi1MSdvnSz0lAmqCoOl1V8Wpftw3YlPlexCqEy/sIVa2kDCF6JlK6Al+6IxL90pegGmeAuPQ2yb9ippdh475sw4X3A6RYm+j/1406ZAAH9w9fe+19WFP/53pZDigyUH/wzL+4sMzYtZMzSv84W7WWRgkzbK8nV9r7ixFEICJQfjPeQq+L+dq+EGq27ZqW1mSuJV1FgcLqsQj+giEOyJCJhO+CEi08NTw0zMTIVu8p6Jv2s/gfyDFuN3jaIuDg8DBtUaMid4SpRSSjbHJ8whq2pAwh0Eo2p5Qe2tG477BKRkdPl5gsgi5rcUv8S8QmGUs3mtRh6ACkvQHwBvtHpt1LaoIeNpqwtjIb6Crsg/hsMsqdhDoBsRJe2pSg4zFsRTfVn9EqL09UsdpaTfzkuXdEW9vOSldhPo7Y5Eb+J+esm22apxumtr95XPbtn9+zsu+MZ161uWDQYMxEwkR/KnRz5x5MC27iVWQ8yb6ehZXXuOQ3hJK1vxXM74tMEzF4a/kph54N1Dl/7g2llD1+yfu+XBoDkot0teV/e6T/zmM3s/986a7siuMxr+b3PvAR9HcfeN78y262X3mu6k0/VTPVk63Z26TsWW5Sp3GxtZ2MaWC7hjG7fDNhibZgwY02JBIFQDAUx5YpJLAgkJnQdCCiTiCeEFQgt5CMW69X9m9pqKbfL83/f9vGDd9t2Z2dmZX/1+a9o3zOmulnonrF4Idnz0uuwFytVtSp7cn6mdgCoHs5WTqZzOVrkM7hNDgqlT5XOsygrzyqYTf5uw/clV/Sd2nlc+farWzKhYzljz6r033bt/VROunCVS3TzHtsRmfApfRvISkCR46XzPI8E6EPyv2Xds667v2355+/LbXaxKV2G0ii3zj7x112X3fTKvybN5XnF12/pZE6ulJctulaVHKj+3xIr0mjC1Ass2PMalwak6VqcScDzxNaOSi7JxIp3LI2fq4kmGIUyoOHY/GI7hyKissYyweLqREA5/pJL2Lb7FYTvv4v6Yy8hVGytFn7FApZT+9PHNKx5wBcyfzLsgvCg+zrri/PZVLS76jfu3SBXBio6pHRWh9rZQpJozKNXs/v3S5INvTblrFy79aQonfVOJ+oB7/cLKNo8OMh6Nx+gp8LmDYBI/Dfz4Era5oAMcX7atdlZ/f1XfwZm7rnCmXFvu757dvr57fE2j0xdvnLDp8PUhTsXqVPGpi3c9fP+WPhwhRO6c4aOQ54tCqhLNwWvRHFEbUmAxBL9tTNTdAunadMgCDjmO0m7ei9PryHEWz7hu0mqY3kCOyU6HaMeiSPrDTRTBTUSvUaoKjD6x0ljNGd3R/nWzo10bd8UbV/fGtrqbV7efv9w6Lr4ofMG8veDE/v1d00Lt7SF3JB6PuK8+Kv1347btvU1+V+KuG6fEGU4FS5w3vfXTq3qZNZlW6jIH3b4C1DwaDwN1vrreps7Vzc6CSC99R9P+XTMP9lX198+q3QZbp1feNX/SxgmNrT57TaimuWtp/cCle01V4y/qaJ4UP7/aCme1a6wGh4J97sar3y02Ze6PWy1rX8W4ERrUYhVESyZ0rrw1ipOWfEgM9oQgaoUapG8Z5dENpwGZ3bWjZjvM8ZrYcOPUMi22uZZN3XVk19QyeQHLVh05lcBjEpM48nHA/i2xxvAY9DjRB5IHevwmafCDqw/tmDZtxyF5IZVBCl8gkV86nuM2CqTxDxik+1HaTNYNwVhAxWAoUYpjRNG4SIid6PPIugiSYm0Glxyp1BQdl6+V+c/lZAIMkjJEcBCSGAchCUgciSgH/svXxqkExBgi2izbcBpuAVs9cg9i4vn3ETMc0wkYZ/LKLIMdWgGMkzLLz8LpPJnCB9LPHZ6rU0RRYTeJE/VjjszRcscA7EslRPaiVAL2yXTiWVmASZwa0Ioupu9UYgSEApZdk4yMj+cY2arCiHYa3saBEc32x7yWGKMNyXPQ48757obdKH0tTaXLeI53R498bi432Y4RknNau8XKWJ2wEZK4AH8Uw5tRvI4phyR5wUc83GlKK9mJUFEPVu2v77okDED4kq76B8DE+vIl3dKVi1Rt5c1RKxJdos3lbaqF0gOelotnTWWTbYvphqEPSKS/vTrwr6VlVdXVVWXb/xwEc6cfCkun4nxVkU8QfEVVfPxTW9kNrdP6e8k7fxSN9WtIHmF5GnHDIoct49hK4u0gsVgY71wwVAG32UtSNsH50pNgMVgxG85ctuJHy5jrpKdmzG2dY1ZLTyGVCHRDU1nXitYHX6OvG3LTfwE13UuWdE+64IKhd1MvQGHllvFhZzj1NrgOfDFu3CHXuLrivw7nAqgl8gJO7fYFAxhKIIwtknholUdWfoQLBIMQMhaOuvQV6f3bH5J+exEPFAdUegPf/eaW/mcPzphx8Nn+JU9MOJDntdizGog33A4KX6ELpRek91+59Pp9qgLFQSVULe5Hp7+GrupqP5jn1bjswrWXvoLKWHLazP2d/T3GxXIPA9bFya5ODicPs+l9LQxJrWat6S4U4vCoz6btajqGpK+wQYzPm8Gr/bt/NeEivilQfprapSvRQTNjYBR0Ie1Q2wW7tqRQ6i9UKi1qJ+0MqAxGlZEzQZ0OLBrrVHB0jFN3AaocW/BW+yP+NX4/wF7DcoCepYMmDp1kUAXQBWqLUkmsiFp0K7UD3VSBbm6G6DHoWaNPRaUa49Rdp6lyVJcglcUDkWOrMaMt9vpMzuVpZwVdMRoCOCOexMJgfmXfiDMy0YLAwMs+MWOtDKYsABlbCKjlxdcXco6aEH9hU6/B1HPrQZOhAi4hR1Iy+BBMn3fVNaL3uyu84jUYcQusAVO+uBbISEMwTRl9DOyyV+ocdmkPO7Vp6sGSnqlNG3TyGS+SxWb5vKR06s9FRe8C7kl8k2u/kJ7IjAsyLpgFz38UEmKRXIhh8/mojJjvixoCTA4wDEMZDEcMI0DWU6R+6Y7Xr9szz2ELHd1eXj+++SWw9PXXwYw8HDFWbxsFJPYFuB18CG5nEld9cmDdi5Nq+hbOaF0T4BRXfQKET36dAxczG8fAFnsYBB98MGefQWMiGtuW5dciW4faAH4LZ0FkAGfHYkCiMb1AekX61x2r+i7wegorItMm3wJUd9yR+gHGYDh5DqQGtuF7ITRcyyT6H1s+82hd3WyTWKzS9T/20mMfHvjkHLANp745N2LD9q2vo/EBnKboHWgMc8s+atk5ExNZ2XGTTgxAowTtR588vVlQp97XFjEqo5F5XlrFKAStwP6GsRnARNHOPgiuVjAi/VuT7dT2AsgWGuiSC4Fab6PrdUKBUaGSqpfAfJ6SOcNtxUghdJvDIwmjx9xHnNre4UQf5UgcTSMd8lRdiZy62CdRxFo95taUWihvkwTHkj6I0QVL6vLTHpPJzNljbNVOSWauxXuTU2qTdTnZJIk0/enUgrRclAn/xyg8xmiNbIHFKiWXcZPhSDswYpNMZjLOQZTCswZ2lwFzwMOT29GJo68dDdQGpi2b5m6h3aJWrame39C5tZw3M2qjoGbMfPmWK7eQTcFINrd2Nsyv1qi1IqikToM5P70aaAfvcYMUVVZRhsOin0ud7D96tB+LMDXTptXATnVAK6pCoUlNKh9nNHI+VdOk/PVQSCVqWfgUMF7Zc8NfD0L4xhIIl2ChlMn6nBSUDUlQMaSbuWU/k3uUIcmdzQlvHk7gQjwbNJJssU9CSmAmyRSx9MIkqgMFyqW3IJVzRNWVMGjNrMdzQQK7OcAAcGXxbFMXofPnpMg7H5BdGNjtpNWj+aAvK1cS3hoDVUItIn5bkqYu+1VxMiaa0nF+mYl4Lq3hKLEFxEw4+i+Ls4G1ZpIoRt5c5g/H2keI6gDhoGgX19TictUsnTwwfu3+g/vXju9UlaoS2ve1CbTsTKyobGxiqgoKKrWtIVNPb48p1KqtLCioYpoaK1csuP6pnz51/QKaWKVDNehurim1E3dMr6ycvmPihdPVFepbrr/+FrSYfuFt66unbKwpjPodDn9tkdUWqqmora2oCdmsRbV4X7SwZuOU6vW3LX1wfVvb+gfJ+C/j49pJPg4x4ef8ZjLnJQklMeRhZwZyie8yxJr2uwFRq9FIP1cqQZzQWvZh4kaChPndAEEi7pORLkEfqgX6p0LnYXbIOEaxFKE7A2hJzO5Z2MoMbiHhUYqQXOPynHcs4+fDZIbsWXzuLCXoBsmNBzFxZh8mzlysghlP/NWXYE/87YBu7Fq86kjpnnthn04AfcQHNkDYOgdQtRZr3iT++T3vxJzaN0HFw4dajqya0lL8+ugyBklQt4x3kY1RDqcRJs5YRvwY1Ap3qvIKe5YyDuhwTdD5Go1OkEgbgz5R+vQMhRzGE89T86m+nLeLzcax0DH0lRLQAxnkAGd6umKoA+CvN4OMFiTD0bDtQLAWx6c6mWzQi+wGZwJySIva39DfaWmYsG5gXVd9wT4wYV/BqiOuup4615T+KWQ5vhEARqXo7G/wq6VkOsTlj8S9v3PbwYPbOncd2bhQX9v5omlZc8+6dT3Ny0wvthT39xe3xI+sWlBUhj/usqIFGH8jt9W5xaNqK64tE/ULNx7ZRf8+HeySzWGX22JqTtKLIfXHaGJcPkyqkqZDJS4x8kWgd+mKynkKxF4mvz2ckmyWjxBJoiab0tFVL0Nc3/1OwM6pjE1enBLgLj4JFCeL3Xjd22RUcfbAO3fjXfVdqHVoOSAj3rLELG0+9t57x/aZfn+IQHQ4fUiKE6SLiWXzsIA2fE6IucwO/d60j+y8yrykBTVNmpdU9jljbdYvx42xOQh3pDuFs2FiaZz3cCZaTBogSJPMwBCVkMPDILVvURztZBIYgG7fIhqtn0LylhwVNjiUXLSPpfbJsmQ6X65yRLbc986Ro+PnTI07VzqcLA/G0/K8h7xdUkHgljs66qTlI/hgucS6nni859sveOrIqlPUqiN8/L1j8X2LMOomNrwco8cNrJMSqSR6NKNE/ciF2wgOYvawHEZ7JdUuSwB8NsNW7kakm1hk/Jbh62z2TG8+L2xXPSEkqO/Kh2jAWT0U2Y+GhpP7cOghm0wl0Kcw9BXu+LQafRxQhq3tI8GJAyPXvyVcHhCN2LRr38mAnKPLZrj0SqiqNDbvMEduZKRjNzuzuzGLaxCMEZ+AiZRSxC+TQaLJrA+hOZfM7zgic6tKI/1aU4Rn9VNyBnECxyjUFZaAvqwr7MvsWuowTeFpXkoUaUC9RiX1DyXy4hNeIBO7KQ8zf7Tf7BHqZ9Sr1F+oz5FEpAfFoBI0j+bMjozYZkds+8fgyD7bcf//Y9ef6/yR9cVv3JiJLB2F1YTRTLNiVw4jnMqtn85bp8+w//T/xfPhGfYPLzNInErguhHgLCqfeX4wW9N/jq543r7UP8fY+c//gydK/zxryb67AQOSDsoCWV7oM7YonuWbeYr6E/XV//2v5H/SS7NDVV5/LQAZjgNvZHhkVTMIm0dj6ofdWY3k/0jv/r697zTWbNHIJuMFg8yhvPIk0vfL9E0QR6Mj5t6J/2/ro+foUUM3MAkXHoJdpxKkX9FJuaB9fdkgMnm9Mvf5AHKFNIjnn3iWQx37mZuopcM9zQTeNSOeieT1ZRkrvBnaCnP2bdakoauGOaMDxBMdlf3Q2SmWmNGkF0HiBzrFL3nIUjLqN5K+iVtfxuvPrGJ+zGTGN02+G7v4KzFuH5C9MmmTHNJYIf9LjSp1XAYDd426D16FIezOyXipcfzqgD2O7kZi8wMZ3AwZKz9IVaNvsVvOGD1n1b+XlEe0oTGqmJKlvwSRbJjkqeRATvpzoZ1gYOzafHZWoTCDPUKw57HngdMC3iuL1OV0JGz08t5gJIz+D0ZiES/6Pxa2or2RRijHNYOwlWWsFj4BpPekgcG49KfxuPn7BuLxgWSfy5VIJhMuV18SbxNBZzzwxzHbBWsHMO5C/yO9Sqd0gYFBV9KlsCVsCrQcBAMuJdbs4q4GL41luHg61oZDvZB4G7DYanZHYqQ9gzF3zG3ljRi8e3KEQRNDInHsvbgLDLropCuOc0tOU5HJUjyZTL53DMTjiUTSNTQ4jKcVs63kKFpHxHjK0CcEH3EUchCJWZSoHGcuzLC15sdaJmVfFKbdyPik8IAg4YgH+j9GxGGOKNf34Y8dq1xSUi5bUn6WXKr4yJLJBLJxuXTDL4ANI3HAK6mp9D+ZMJLiSrGGqgc5N2Oaj5cZayfcqKpR2VVSSKUCb6CVGpVK2gIOgINj7j5O1sge9COfskXaohp7t8zlhsr1n5lyUbk4HmWGch2Va4ydcBZ+uHzfA+gJ5KbgDVSusXbDqXJZydYBcCBd4pBq7N24XFOpa5gwM2tYeynzgoww68kYO5nwuWo9bPeno4qKnw8uGXM3JZfrOCrXxvz2GsFOL4y1E5XrjNUdYzc8PvrlojNwwcbYjcci1L/gRvIecaky7M653oQ6UvrsYf2G/nTsxiLjG+obcFb2nt+7E5zpbZN7TgVaJkzPku/5b7xAcNGZ3gm+ZyW658ZcOb9n49OVZ2jOtF9ZlhurZDzV0TwIJmdW265tAZG8MQSbDofxIKQGXS6ZoN3lSg1mORFcNJEphkgc9FQcbuef0azFY4iuqSeQC73Li+nQk+x8PLYN9xx4QR5eHS4rFgHTMmOYralFI6ApDAayAX3tpwZELUMefyqJDZsDMiTVAL3OYBgwGAAlo4vK6Lh0X85gLQ7NIsbnPjRLZWPfGVnWsaKZPSvn+M/FHpHGtPhRugW0tNxYOfy85cQ4MChbiIdwCeiXhwUlMnIBiE3EKkfen+npkDRBIxhJbwBeI2BP1Gkk1VGkjug3iZtgAIybUitRsmWhdspiGROKNIFsv6enulyuIXICg3/z5x81YdJJs+O2ANnZmGWWvjFLhHv48CgqXGYgjyj32bFwLdJzuptYNXL1aYGNIEOunKU+y6cYGvsEmlrXIyV61mGXPZnN4quO1JUM9qyjE2c4AON497oemMSufjL1HVmFhF/59DH2U2OWWwfz1Bwk65F5Op8a6ewn0NSogq3rAQlc7jMcYJKp+MgSA1LiM+yn8jBok5SSMlIFWU96G7E0p42m2ezIMyx9I7YzuZLgizmNTbNnNzVCVvaT/713X2/vPubi9kXt7YtScMXhFSsOw5gMz3aIMEgeJf11aOvs2VtnS3+TJfR2fFFv6hV8UTu9AF+0oo/kVQx9SFgnwWWk9w6P+VDLMaKZXsmNRFxJx23mdb5hHLnARGO4iDTYKQ5DZMuHx0+ZjWGXiEbD3bRew2sMeiPLeluWrL/ltiWYGFeiRKwzog8c/uauCBj4kfRX3mNXGk16pZfrjF04sHlOtFiD85HJafgHo71KF12RxZylyHdWTc3HI78OeEKgljD55a1bZdQsTzCQjg/ElGi0aOJ1jNcTYoIZz5Zs98ZmceL+gomC5tnNBfgH3pJdfebgJaW3dj3UdbT8koPxJYevmHnfzCsOL4kPNgf23/DzI4umJe49eOUqd8uVjvCau1ffcNeN+1bevTrsuBL098zu7Jw9/GfH1vvMarX5vq3z906u1OkqJ+8Fild3TF3X5FVyYmnLsrbtr316bOb8Tcunz/a6Zk5bvmnejIHh35EVv4X0OIe/krOOtjIDE1K9U/Gc+xgT044iZRqE5Fg8C48IPxzJ0iRzZW5mMVdmEGd/gVoZkA+1MAH3BVG/OzKyYEhRZXNsTvnlIh5vq4X7KnIqWbLQLv1RiDDxkkUFICCcugrbRmUcRlxoQFUcYutD0h/KD3ecSmbLjTS5ZPQ8ix6e7y0vlm6yGbwVxWC15YmBXFUeBI2R8T9saZBuiozPVWbRQHUofx5j8rncZIO6PAhluejz+QjFc8xxdILolfGciimvJjLvAbd1Yu/5qThmjYXJ8/fCvlEn41Vpg/xWUvkvBJ+/d3jZM0xOI/AoR0ow9IhcH3oAP18uBe3KL9wAMXMTYzdDCpp58FBfXonAC7n8mwxGZpo/vpDyUbWEvYm4kwMElsaJxvNYC3CCkWCPlCEEXTpocEI0awr5pPIX+38r/davsNkLqhQF++/bX6AYV2OTVHJckcxTBiYvf/BTaejTB5ejJWA+ffCDESMaeHnbjTduQzdAt+lZurTHbjNUgVfziM5SEr5see42aKobMQaOXTcLgXeUYx9w9Akeev6NuilsNePStaoqsNsUuK5S7N+rW7igypCplgLdBlUVKv+ndVOTHI9yHPGQicnEHfr7VykRsKeIrg4Tdinw79VEdpCCE/9W4dMyMlrImSkd38+6xIyIdfMZKK8n6OVkqBB3DR0XdElBl9AJcmZMZhXG05VJL6Q330wefudw8k3pTVDxJp14EyRHXYNXV5DqpKPd3pTmS28mEqAC3Acw67w+a1PC85oXfcmNVBc1i1pMraa2UHuJ1fqH1ONkbEJ1Qp8uqkcsbz2Yt47OQe8NraNa+M98zjn3n2mdzV83ZtcjeFscg9kMJAx9BvQvYRg0oH/pLYYyDCFhm+4zpLLHyQKMvZlZSlR6O7dEt12HL/gWiSiTI98STFWMrArWkTO+yPtNfTFqlzTGRnoB5EX6nzRAzjPgPGVMZzdEHkTjX3ksjqftnBaqjJqLJd1MnBRvJHkbBEMCjHCnpr2mmSxKHHTLZFFGYiQEOJNZiCbKxP0HZrYuu6/3+AdfnYxdsDQWK6yo33rqIm8RGYGLvKhvsUmviv/jzfMnFMYnrGtYLn21WC8YDK5i77xr7u5e94t1gfClJy3K4uJi8HfYv9BVHdudun+93l/g0Fno9d4G4ykdGfO/NDZgB//mFBsUWGaTV+d2Fs5vUCpEP/zAazKXNwdaYuI6DWsQTDhHLFN3FvXgMqqGmkBtwN8hx5ujIvlF68EIGiqVqDnMpFJWM6oXOojqarb8/2oWOn7ixZcff+jNt+mP/n6TSWTrtDViyF7hrbBY7eLyE6tFU1n11uP3H6h033jqof9RW0Fb0nDhM33g0ecVlzy7Vqp7elPlIKekCzkbL3JqhqH/3BBRcieNkH92oeK5MvDZ/6whsV0OyXjE9uLDOXajbC8W00h3NuweyxijYioF3RCZmmk8apWObYGSKvMyNHEfrjx9Az+L+ZQ8vx5Jmfwo06TFpEQzOiafw4ALGEZ8zGLClWNZKlVwvHQVY9W0aLUM2CyvwGvGrMCBsa14jPu7r9DFRsaqZdXySqp/7Mrl8gR+Qpkx9hIwZ2CMcIUwvikBNjTp6DTYh4BDF0ecZEZPQGoBQTqSVZei0qC0URQ5rac8UsgpTBxdAMtvir915/BzwG0n7we/moBReNJ6DA6KHy9twFkRUxtu3rmzTmMECjs4dE/XdO2pEedJ3xX+/Lgs98PTx7ld7CClokpQHSpR29NGK0sHlUAkWL9+wieF2aSimEwKaTMi6wTMXQBIt7c7jzWCliYN+Eq6aS5rsRqtUqvUihYWdq50o0uoBP96z1RUaH4P/KtSgB3f1aqaQPtQc/F9YGk7iEg/kDRuv+aTTzR+N+ahcsV4TENVKtV387EsTnOCxFtTORIDtwcDAwIZI4XdlkoYS1iVxZFKWrwqwcRSWoND0PPMD09RXsh6LTDuqChRwQQv6kqzONVoLIFoNKkjjAlK4B4hlbvTMSYjRHN3DPU+bAMlAUCVcFYqgf6OM4mMm2doYJjXh571L9RflMqviVMMnfpH9NeX5xmi+/K8Q18rlejsfw0dFzL5e4yM24yZNyfnbFDCiPh5HIcl8+8UAWI8lJPQMuOcGY99SCxmI+kdwEU0fvkffFpW/2sr4GDn/sSsilqk3tdWpBfRZbG2KWVBI9m0kUuYp8liIvntq11QIL23O1Be0jLeXrCgFhs90C66Nrcu6e3FxgJ/WdP09M6M7QPbQXWUnQpQbdT51EpqM5JE0m85bba1mKxygDAJ/gnkCYxsNmEjiFHY0KiAMyFimMgG8FlUJSvgA3m80ABp8LlbgLxbs9i6m30oeOo0pdJq1EoloPDrG5B5rAbzsqZZKIMlSY+ZzZ8Do2Om44bCQukzwWsGPbNTN38ufZ4GXgIC2ic9msZWAtPM8Nq826S+lG8Nbj5NqW3ZBwLlaYr0BUAW4bzE7UFy/sBWDKgEppu9gvSZA8gwTED83IweNRf2CkBIQzNJn31hRkWaewm5QPqxeaVMyUXl3fKeYQ+Tx4Q+9KEMEbtwU0ZjzYscwCOaLn8vCfxPw4iT+RTEBDcO5k3akERjIz+gqTjYVREsjaEtvXnPtMaa85vbyr0TtYJGe7eWVQyAcT137ZkJbJkLbHBitLexyWGxzi4wFvvFylk3eB0NVWXxooLzDIqdKqcWqFr6b87YLiD+pp2YpywfJUWmUs5MZmb83dIjZ7iEbEMP2OPxDH05WknIHEAyaF0WIgUk0o7FVDLALCeOOTlJGlJL0I8zy/Uy4iEiLdsngnQgne+ff3tnIQjizSAoBH7s2fYD1yA+iH8YLkVOpAl4Hh61KFZiV1MejMHiN3sxEoQXw0u5I2GR9kbcBBwjHG2FbrOXFoHZTYKrmcwbCsrMQSRvKRyhL/vmmE1B00qV/jZJSjz/zAFgugqa0R5aUXA1ADuffgl+nJJopnbaedNqG0vDIZ1lpd0/a+XFV1ZPnj8lRn94771DZUqN2WT77l7gBYb73mcCSo1SU/b+fdJX0u/hva84CoX4qo7WUIs7UB1UOxb5i9q2LK3rbWwob3L3yP2NxTjZ9B5Upwnfp07smetEf886fZKSGHp4nXrWXHzl+KXLJjHnqNIfXnFUgtE1al/Z0dAZ7CH1AUjf2s3KWISUH8fwW7AZi/SAAJm7iBUH9KQo6VHuK726YCgRaEhRgVYDWqfROo3WCQYjE/FMLhyiKko9aMmgpWwvfYeMn/0yfhpBVsdYw2YPr4MyMng2L51QuctzZxXwBD0RI8ZPwcItTm7PJLMT+ilMxmPG1leMwyKTLCGlYMH00u7KTv/FLmBRe3b3h5pne0u9a2bMvsTpd4b8PYuPKP1KLYAQFvvpI4t7/CG0/5I5PWvQWbOb4x9WAZYFNm9FpaW+uqd85kLw5Ax8aEfwaJBFooYqUu/vrOwunb5g4czynup6S2WF1wYZCAFgqBGXpktSH3GOeFpaFmMShBswTL4/ijdnCexJAH6Awl8jMVFRrvQ6ngVcZBZwWZiE9Ic/EIjKtI0BUH+Q/oBNBgSAE62cpk5K35zE8cd0PPGu9Ixtnxxous8Gut6VhwgZ25MgKC2XqH0nT+6D+BdHFyNZZiOJ9+3Aszm6YbY4SiBnEfCo0fMKOaoCQT4fD8Ni8gMZIB/oL8QpKsxK6eaT+2LRvgvWPEPKO6o+Oy+W0Eg/U6Vi3iBL6dLUDSf3Lb8HTr9wxVq5AhHolG5O7Dsp9oXTFbEPq6q2U1KhK+34FniJ7oBreEk2TlzmovLIcSHouxSNJh1LCESzOPVMXIpv6f17smPpgU27IgZNocYQ2bXpwNIOOUAIxmHi1HWtk56mH01Rc+/fu2Nmt53nON7ePXPH3vvnygNhLoc/jR3ixeOh1W10+0dEi4zeHpExlRbu8i2waDL5Dn1+OfMjTeXWMfnJd4S9MtHbjhkJ5QXagyS2JHARgBUi0+WtTztFXHoszqQiTITy7xTSbjSRcweRrDclF1/vT0N++kM41Uvm88qUOxbBOAXpzxrn5WUDxN3ooJ9ACUE5sp7+kUahYWgprtadptZeL092O5e6Gtd1NZsYY4lBazVqWLGubWVdQe++Xh0I6dQgSTPoKlZ+531S0qDkQR8U1Mutj24cIlMT7Vp1v3NtVeMkt8LLa2psKtfktvFCWQWulbtYLcA+wCtx3XynXZzsx63MY/M1YYmV5nhWRndC6kV2DZc/FvVhZ90AEU6B7ebHZmwwQZ2U4JUadVzLzpH+l/QJzemUcaNmUGUA2/t6ToLZgNWZGFlKBYlvpZse7+mTLjeoBhklfmkmUDAHKOOiCSR00LRhxk+uFbNcT6/LOgag3WI0iDoLWrrRH+0m2Ofc63dLjz2mLXTU3f+S9NhL0n/h31uYoQt/3NhUBk+lWDpe53IPddHP4D/QNaO7+2fD44bwgEP5Y9FapFVleAw4ko2T7yqjr1kuitLLICyKy7EW1yCK4FdiLbxihCXzGnwUhNF5tSK+okE+Gb59Rux9+fno0cE0WYFVmSYGyH8+fBk9Tr4dui0ISy+TgtATRj4flwoXTS7my+g8fMW5ng9i0Uy2j0yPoBzxfOaavNqIuUqCkQ0A5BYYWVjw9miM/zHagDS/MtMQI99B5ah6yS9hpDn5U9III18Y3DJGG8RJ7oyR9LAY6lkYrsgrsuGIX3QHgZtm/cwqw9DVVXCZ5fnntI9YwCoGrKhJ7dBLdWwikfpp6hf0g4+kPn4/Erla+ngZWApdJ8Bb3y256y7SfzWn49x/p/EF3UoounkW3Vd0x9xAZN+T/jX0TmpCFygtAj8CH3SemtjAPBM4NRENby9KXwE1WHbDnXeCWaD0Z+m2MvAyn8ucvG9VHoeqAIdaKTgKo9gJrHmqcp7SaQ5nogKMLSCWATKmk/KotNykYLSq87dI66Vaaf2W85U6RmFCI2afRaHQL+v46iZZuG6YcOTNIxMa5I2bvupYplcoLKBPJzAfkLFpaEAasCig8vzr7r33uvOVUD5oEg3LFu40wf1EWv+hZ/MEHD06YbPnh2RHaptp58JlBtEkyN8/kRu8o/xbOP4VdZq0ohOR2Y8ZV47gzZWWDNLUbzmXIsGJjhNut2dwyfHTpSQ1zIcl6/VEWsmhIAdcnMHiogxU+u9MfhAZ3hhY0rlf4AKJGP2Jyf+hczhC4McynPEFoOU9fD2cnb20IrXrnN4cYkNBInuCzvkFzxFrPir2PFFXQtopiSNmx16nXZm1MX+y/kmQw3wbVQ7jObbzyzHWD8iVAfx+rNV8nCWeclAR7LXOxgphMlPiGyL8GYDIHgEYAj7M8EH2WxhBPjCaAxPKIdbgAa309Ic6k1F76ztqIGgTWhPYzS7/8UfSe7fqlCpB+xJY9DpPDqjUoDg/klRGNPB8CCZqgQkdF4D6nVu1RpP2VlD80Y+Xs0ClInv516W7X9IKKiX98sj40pzfzjGCHYUM5YS4iegSoxg0HsMhacVul8tgMOpHsSqkbhYmCSAuCqI/lfCLCiV6l9HTEe5F9rdElkPvUsnmZgs8SMtm4ChqWz6YkYCJ9ctqMSFFoSn1nPQcWAlXoQEZ89KkjqBxe5UQpa8a2uxf7d9Vt26gbqffT1+FNnbijV1+pkl6LoVxePFVtfhsfFUtvh5eN7TJjy4aWIfOW+2nD/rRRWhjp3/1sHaRdf2RKdtjxP7KAcZ0YsxoX9mkMDy6lx7GHVs1hkXhHDEC2Cg5RKw8tIzylwuAS+Rzy8LBrI1eqiHUs/KZ9J58mlk0TqIS0d+xu6lCHJdeDnJA9jh63pujWaa/E0qSOIfNrFBoBpQGEE+WCEY7iAst6JU76Hv82EoqmPRJNUz4/cUgYbFICReZy5AcjJ5B4d4mZuw16dBLTCtpdBMJMerC4XLJEp9DSqKbSkm7ET1SSurUA1qlkqVE3dCdk1wSui9IFAf8MKFO6kzicFnAlycLgGBOFhj1GR6Hy9Oze+V/psUBLBMtz3+Ln8LlaVkAnSOffKtIX5H/PnPjPodGdnP6nVp5HMRP4CVI+ylpI0gTXelHxxTeeLS2rg+8oTNK7xq1OiPwGqVT0CUNpgbpxKLCwqOFPYWL4MAw5tuHjtb21YH/0OJLdFp8SSoOXQB9m9Ig7FuErjhaWLio70zffQGOR05HefBccYZNKgZkA8KYUe4uQh2Q+lhuCGg5JDg02uCIbt8HkBIRLC3C55GWQ+eJrBGW5ZckVw4/zhVXZgadIuDRsbKJIhYNQkwULW+NLA/9KehDL2XAGSzZ8ou9F9S5Vfeq9DxnoStWhe67ukSjscPAsOZ6HJ2PRoI+7CIZCLYu7rv0wqYTf9HQShtYsqW2aqDMyMLksMbKjf8QvVmBchIfCjACI5q8QTpScxhFGSSQaiAhUbQrLyRzVMAmSCYSYHrqv05TSCN/lwR1ymfDxSOm5ByPFUb8qkzjlcgfDWqGkSPFyFZitglWKSm2i1LSKhhxUunRdFysliZQEvlNRJ/vLZbiDgdIFnu9KdewINoR49eIMsnDRXqQOHeZjCWpRIlRsKJZol0EcevmM5cJ/NDr9RaDpMMhxYulP37/MpG4btnnG7WCc5Ypju/vlZ/1p3z/54jOfWdeUxpx26Y+oclITK6gX8kvE5E/6X+iMvWhEclq4fRAx3s9VDArUgdi2dUoRRjRkdBNXKQsBkGRhXBUUM4qr2JDM8HYYlqxg5F+zUtr1CyjFW0O9ALEj6W7WhfjBmqHdAcu1JIOcMHg8kVqJUeX0xYtw+hNBY5i3a4XasCbBqWKtrEOyUbT4EU9khBsUFBLO8e9uFvwFReaDQyr1Wr+dkxjxhQ+HMuyDATsu6J2vVasHyfoNuiENwBlRc/XHsMuWUAzNA0T6zQa3Qa7v1Oj0a9T6zcfoBl0IYAsz6f1cXoItUdrLgp5uCVfRrnBzj+c7oa52EgIjjsTfGbMWHLoIdTknTpB1F6wGNd08dc/e+YIUhFWKLVaFVvWVzmnH1STxLtXwQ8E3V3oRV4nXY/PPIK62G5Ru1cn/OXBP+9UFKh2qwFUsoW+3ilvC7q9WlG6/IQMeA2o2tMU/QbSH5bIXPZZERNHgbZi4CvrOBm+Gdtb6WBIgZ1zWVsT5kFPVyPNNIqhleg3fn1M0O3Xiu3bezoLWKN+BW/QK+H6PX7/jO1Of09tNFg5raq9NFRgfO4OUbtfJ9Sv7mgSOKNmhkKv09LWWMu8ssVbjWX+yaGqSF1fbLzfDhbf8q79EdwajygrKsM29Kz9KgjVcKldMXd6YY2n1Go2CF5HRWl946TSg685n8AQ4o9yHneZgRNMh/WAVtGCt8g6t9NeEXR4RcFkrQq0tM1Pv7M96J21ZGRwHeAtadboIBXMBlvHsgJMICOHh90ZLdFixd6ZPYLuPuubD9wLfDqVwvxLg1J6BWOdrNt3p0WaQ2xqd9T/5/W4aDT5/j6qMj6ItMGy5Trh0BOmx6RbDYKgAWtfUmp3a8W5MwUdOrBe1F6Oz0WrzbMEAuqIRA3CWU+5vWmShzRMS7a7ySJHDUbfRuqrSNbRuBrOdDNzrsOZOLjwIdQpSE4ncMnL30k/UyhUwi9E1duiX1XK/0xh/plRpVRIv36b9Lk/A4+8RFUBkwTdCq04R9D1a0XYbjAYBGleYJ5tvhHcLRp0xtSzorZfJ8wRtSt0gvSkVpR9Xqysd9QRXR13fMyjk1+yMcJMs2vyqMaIe1bhLLhVYG3qBekh8C0xWPKi9t6MWzrjq4aOF+gVL+yQ4uBOadd/XzIyeA3tuAmVfbNOyOOmUlAaJO0UoNH2YtQzRK9oMVlro2LMbXWHg168AylB8g5ZR6RJj6G9tMwqTmdLmxsP6cx7cYvDlhaezhoceOyfhzOOTQYAbPJK77jAnVd5J4Bj0+6agfasdUtvE2z3t+7mbcds/I9evxct1UY48BquzyPua/HiogWsSmU4YGfPAysu4G27bPwScNH5rP2AQaViF67Fp1zveRyNGXNAOVKfGcz+9lAikUghVVp6C22gXccTCRfqpamjNhvsR786FewnsrZsWQbz9VqNTToK+m3yr0arl+5Ln4D127rTFPMhascwNZFgLlkwKY6O4c3eiCdo9ho96DOKISnIGA54jTgo0VoTi4TN0TD6cdJ0bYjxEBDWmhYOb6CpAW20cMx1wk2bN2n58LRNu2fe2lN2qzBRfKF4bY3CwKm0U9a+GXffOrPk1umX9je/7qzoappfM12haAh0VreFqp1iV4Gvqaa7vI1nGz3tFY0Bn0AnnpxSeOSqrjUTqizM6VNgiDoNngqDwwAUd94NwNDX8KshvrjxgtQdvjpfgYaD0sOAZjUGuycEvnGH3VYVB4D0MpoeFDprcSjPj2CU80uHZZujydnvxuhXSNzMAmFgvpQiiZ5ZA+pqaGpoMINzgZGvEiClr5FeqJFltSx+RzX+bs6JC3+mZ58ZsgOMXaqBrJoFdLnVi89QVFRWz2mK28lhZHsf1U7NRO0QxnRQXh5NMkDGl8qoRfJkQrQlFpObRVsBpq3AES2YuQIgocKMT4yImJUi6OXDeCmGRebeH0/UYPpDJvWFSvo5jnqQktjCliRxKTiEpTP1NFivUWKiPI3w4VYYk67j9Gqd0vzNG9Lg5Kp/Vk2W3pvwwV0fMP1/rDIwJuDRnHJmwK0Moontw3X9bkC4/KPzoFFQKmlAb/zbgtRnCkENIdxCX7Zq1aFDq1bBI6lVsk8nv961uN7+XL3ZM9YbjKgZfdZ2+B71vmNY7cQztkK22v81Vq2loVz1mN2jmkCF5KotaBj3pPHgsL5VT3VjbDz/WV7xcEsA/W9uw8Gxq8y48i0GWIVPkD6dIBuSTK6RJBunKbKBfvvGqnUe3P+X51iVp7FM/fW5+o+spf8sr36EZeQc28ywCkiusVsDDoyo87DWyLWTK1uVDWM1Bdhw7gYgfZ59Jd3nO3B0r58474lH/sx93m/S0YThIybLlzEv5qJM49PjDwADOaC5HwdSYA4atn1+Y21Ld2fNhNQPzlDpz+x1PZvHt4RsQlBv8AdmX2iA5hkVq644dNH2u51S+b0A8gqhZWZy+19aV03aMCU6d6w6x1q2XDSz2qDg1/OMdvM8a+F1F648/Cys2rABPMrbWINGKzTMfSa1gRpV9xiJbM7V/ezj3IjqiWdrju9R99fy6/fLszQEk678qQfGqv3QyGqy4THbI4OHGU/bVxdl3rociDHSnsdi9EQLbyH8cRyPMacBoWom7mACtYhhZqGMVGw2YSI4yGOzERWwO/x+hz0wELBLxHcLXPYAMxDT0yGjUR9UNsQv900xtt8+b+p2rz3gK7D1V3e6BbtSyasLTaI91F3l1iuBKAq0TsEA87QNxBuD7gkd2WQM9Du3tcI1pbmuud6/bvwUWOywlwPgt8PLCvwQbojPcwtN/rJgRZNJNBfXlDQ5bYEpFR7OZtJtyODtUVyc5N450viS2Zc3UjP3W8xEy4VWHNxCYJox4TOUqavTTYLbo5HGvHnkjzedqSFWxsD6adLfGYWOFgQTUOrdVd0hu2gqVPNKpV1wd1b32wp8Abt3+9R5t7cbp/gujzcog3qjMUTTmZZI/U1uA9IejzTPn7ZBZ7Jx/pKpAZuzqaSm2CyamiqCZf4mwT0vvgFCfwG8zO4HoNzuKIZTxq/z16OGm+LC6PoZG4WS+IfKqWbUGsuo3dTV1A+ox6hfEJkFR7lj61cYQ8b5kSCI/o+w6C/tnAunzfJGNh37g07BYiG2HphNGWYgNCCSgNYi4DWb0Nm10VrMY4UTLmpALaEidLsI6moa1NNF+hkS2/mgl4B8msOY3JbEYCGJSTbIYUASY7oc3nQ5Rhnmbi4yGgzGoqfb21PP90yaBn7cEfS7lVw7ADqTBbTymlKvu6PD5SvV8KcgrXFEaovMpqLlDvPlHhsHpMvicWgWVe3lV0qfSJ9eWdGmMplUbeUHYOBAOVpPac+bHI5MU7h4r3oScJuLqsMOs9kRri4yn+joIDDdHZwa3R18nW+4+eiOGsOg4UFPOPzhBGkBuHfCLun6kspCQwB4pC9tUF8MbGsP15rLSn3g0ztLysxPKot0FqEk4Gi8rNERCBQ2TGkL24HGrKbrbg+Hb69N0T+eVdHI6vVsY8W844/OLm/C603ls+lGUPLLX1oXWVfEfrN1T0NRIFDUQBaOJrBB+luxAdqAQfqTX3BUAsVw2yz6OtB4+Tc0Xub6x0JqKbWTOkDdRj1C9G+MvojeNYuEntoafxjjBBvD7jFeS+blRVDviJCX5494SYdpBuFRLzaGWY08aLOGsB7znIt0EQyFjnqFi/QQEKbR3TEodFjM9D25n+G+5x+jh9IvBq0WizUIZp533lDDaumFlcuAa8ECp0OgwQKFJjQuCo4rjdGa8gULKsdFjUowcyEa1kKPO4IdncHCouD4iUgBgamBuXPhq3bd/IanU/anGxZo7Wi98Sn4AVkfsi/ftkxX5S9c1QWeLPSP7wgUFgY6xvsLwfSFkZqQVrEQ0ILDCXz/2WEBlZbOUKjzSG9v6tfgM+mKMjPtAmukbdU2f3Pv8932uugfUivHxWKOWdqwyjd+3orp/nDYP/04WkQcDiX9izfGj39jQmrex5saezizmetpXPcZXudNJh6tMzppvfQPoJ90cMVs6dsJj8xAVwd6HunBN5kpaWMtflsYHJSud0NLOdgpx0ZiruR/USJGQQCcrBnHxJpgRhHG1l5zxtgCogDvhHNUXzsCn5tNqhQAd2rUSuvnJXb6t2p16gvQo1apLJ+X2aTjAgQFwX9Y6AsFaVLIg/kY0CvU6yvBMoN56DyQusVk1FfCi130tZU5bgk8NomEAxCjt2I7DvYMmGnOiqOrYoDsARZAtqJBgMRw6yinyi5z8dOCglfsfE6pVBieKRbpGG/8iVOULkRqtMn1tMArlNIQuEXxp2HGZxq861FrjL8H0o90Oq2PnqHxpoJQcnuR4gzeAfA/DVeOxu6hZMx2grlBDXdbAp9JJkyV+zLmLwG53oyBZp0SVexxuw16kw5S0An1esOqrj8P7fpz12qDTg/T2/Se9PbCSUYQNwlCIJUICAoViB9JrrmnrXuZoqBAsay77Z41wzcpGdOLS7IHid8TM1cXo0+bMQNzkI8gfR79i5mVGqRMfyY9IFnYCsmCdGXrDWAuAGBeagaYKwnSw2wIzJSs0v1gHvhIelgS6GbpVemvoFV6f430J4D/86/pA4WY5U56n/m99FfpNaCTvpT+If0cFNG7pJ9LX4JxBNufYneReDp9tjReHJHLRoCRN7uDmHjTbdQC3i+y6A/wSsgDv8jT9ECqmX4CnLrRC7bRA0O/h0ltqnUmfDCYmvNbeMHU1DFwAly7Q1oP27fdtG3/zeBmsDDV4UXlGUwdgavmtx1pA288dfgp8Ll0dA/oBy+nnpoDJ36U6rLDZ/J8LOY03h6FRhIctIqp19H4403LBVRWcsylZ8oxf7GR0tTM/abEO3uelz40XeO1M5UFPun9E4ndJ07sToDXS4oeLiohPw9vmnHq4IxNm2Ywl8zYdDG8srVz11s7gD7Z2Zraavd6wePfPPLIN4/AG+4pLC0tvAdd9Gnu9E1534ue4IiMyp/OZodmMjoIbZTcg8GtOx7dseNR+ChZZDih5J49dB/el/6X/11CNCtgTnXRzYaVIBxzDwtton4jXQyjvVJEivT2QxU4NRKZ4bD0yiB8PDV1AFSPlcPbw17G/hDJ6TgDsQMzSgELF8S5OFH0sYTwd4Q+GfT5iEgS87FI+sSBxkgKE0m+AOGRCgRbARIjnIATOYLz4Ee7GXwEc2zEfCyOlaCrFJsjwaLCgK87tlb3qyWtk2nmhkULL33fNLGiWnpX+rQ8FBeci2JN77/TGlk0V6HXVvjmvvr8ilDXzLipwMUJf4GxQTNnOGGfw1aUu4ekW785rDdrWR4qvWa7ki7y1PmcO0+C7aD0tiYDgPe0TnEZZ840CppG4+oNFYXbxi9MKBRH4aUOr1JRVc2rPPZCr5IvKlQovEOC/cKObtO4KtqoMHki3r7nDMobb+Q8dfTT90o2Z22hcVfAsU5TVOqoVda8sP2hifZKp1OvDgn+eaEpphbCpSq/KwUZRRuQrkuYwgOE1jkaIynfJJ1dxO2DpVos1CNJV6yNBoJooNIDwgeJGzaK+RdYjpfb2kmj/QzWAYRRAlfPTF85KA/OnqSYv28VDWOVE6570tQRrLjt/opAh1kb8jh/9YbbV1OnZvV3Sv13aVi7vuqObx/3OPX7lcbydb+X/rGvN1AeZhQWHwcUnKBd+TigT9iKi5lxoGSY9+vW8pDFtFKwRpvbL9Ys6qiebyqeCRrMdo41mTi+wCTaeCSws3xBiuaDBcyqVZzm1roZjtBSsW0V/HXEEnO3OjQevWmcs/Pq3/rYWpNH3WMqXKg1BcxADWpGjO+AmpbOk8LxW2FCS2ZK02WahzFgeiOEvw1c8Sc0nd+o0wZrOjZvXD5lat/KGdOaGsyWBxbG48Egm5B2/EO65Eqf31o85ctxRtFRUBOORldDxx+c0djUaWPgwfkj6adECOOXV+beNIeNphxN6Shf4tL5c8P+IpUaCNJnd6uLiqrHde42GsvKm1umdbTUg4/zm3T3iTqrsdC5FARPAO959fVlJdYfSmumlZf5/CaTTsuwo9qEPh2HKdTVMFqQZWwVGn5l0Ep3qbQ6lXSHVqE0pXEKkZJkkBIqFUgYRJEhtv9TmdgMCqbYJL5nOv4jC8UcS+dXwVT2PmaDFvTiu4OlWkYUT5FAaGYwYADo5lLCkObIAjRP0SlyzwyafgZL3yqDePAULsGIQsHB4c9YoiM1SHN20VwyfU/ZvzschR+z8yRxEUaUCu5FTfEDnSK/CqiBsnHt61F7BkjGYLpbYeHY66FhJC3d4vdMNCyZhxVk+GtlMkGrycKuD86+LFG9cG5b84wZ4aM33bB+3UMTV/Z7Kpcs79rSW1s73dt2UHqvyNkajfo76MmTHgU0mrnbdu58zuVye9AG++X7hw85nR5Pmy/eEe5dv+NXzKXNkye3RgU1d9Oa1aW0gWY0+dx2NJrT1TLztN9IWKHSS/hAai7+4xJDm3GIFBRSm3thJfxfqYtgJLVl6LOd8Cb64qEP4B2Eo5Pg3bK7SMxiIZL8piKdg6JqomTeYtJLVp7d5E4uQ2iSZMRmrF4SZT5IfG04SRFHqeOI0GIcDoCTrHnyhaQ/kBoLeNdltbos4KTLYnFZh06VNTXObWpipscrJzfNbTrYVF7WBCaF4vDh1YmhpYk1XbxGy09c/ObiibxWw4Mj+HhTWXkTU2TF95H/vdpUJs0sb2oqBw+XNYmp5aH4X/HWX+XfeAjeCm6KPb958/OxvVqe0+wrK9un4Xht6qbMVeWNjWh+VaO2+JZwd+gpD+CBCfhANegCnxMsEi+mhqqxcgEeVQoEsGjJc3hcb6GbQAAJzC10oBYbK0AQmyXQQSx3khkwEE2bL/Dgj2aDGFLH0W7OavKGUDdGRwMc5lLCoxhPEomsNRaOJH6SqZfGcwKNpwogc6Wg2SMgzxRoWsVJGDpsIcHiLxJmrfgMCz4Fvwc94GSJl1zshOYomnjQVI4uJjnw+GbEDxrFU1O4Bcn3uDxmi7WG55CqiWvEyDNYsBaJAhwZ+kytoBariF4dUk/QIy34BjVR4IS4MICAmtAEQonX0UG5IfD9cRMQQTxCCoju5qR5E74nLiC2chHbVwAfJFYvVOuYPGuGCQwMnz7Xgh9Ck9uiFsKNmr5xup2dLLxZrWRYkV3E6FU2BS3dxjAsTfM8xxgZACGA9JwYg8RbJOYqgWqS1+ae51YHi/VArTQLWi3QeQosDGNSB/WNnIKzFPgLVWoByRrGAothtQCUpQU08BQ6iiBQGnkVx6h5IwAmm9EEgEWpCAItq9JZVA5LVQyWOVysUs3SSo2pW1lhL4iqADAUlBkDHrfDooWQ49S8li6cHrWYyyw0cBZpBet0BQScwuxiIMewjC/EljCm+5QGutipKNOFgoyWA7RJFdp6eYVVrYHokZyZtkJohBa9D3RMS91JqzklpFU0rabBD6HSyLFKloO0rkxQqp9QaWgdD6GOUdSxWlqvVLI0BCrIMAqdAhh0MGayQN5m9dsDisDiQuPygGBVeZwVc8UppoouX7iw6O64GPeV21iVBwA0fKt0c41OmzniCnuUWgFqWAZ4aNpjusxrW9ZmLS+nBZNq27jOSjWDBj7BySv8loDpYp2GgbU9wbbIKl/9eBbJDktjC/RIBFGrHI6oR3AISh20BASDSVTVnVfS2NwdGacOutxuWgd0ervBwVwIRMChqgA9rdZy0kygMLKsQgWBQUUr8OuG0q2CTV/gMBSpPHw5O+5ik6n1rk0lkKm8NBRsKhY0oGWm02cxt3kUtBOAmlpAtxeIep6Js84Ss5JW7NIraYavbwegvlhfUQxptRIUiRYnKPMxep3GCnR2VmHVqwE0Ao3SqNRxqCQ0V8yIDJJKGUZvBUBjEPVKRglZluFoHuia7Bp1S7GS5gtax3UWcffVC8sVNnNxa2GhCNi2CzUuxrpfqQ+V0PrG6pCtU2FQQFbJ1xr0EwMKLlTQYS0C4iaXeeUCu+B3qekyox1CJQv0pl8oeJqhVRwPoCHGAGFQbVQAwAHAOGj2E8gpoB5otRyjZTkaNRtgvntBU2C1WIwmrcCIkxwGXlAWWVA3Ri+p0FUAQJMWdWuNUW2dpzaM8/uUGkYleDzdbhNLa/VlnE1jUes7dUYlV6DgXDqaq6htCxp/WjvJo7QZLEWYLX15tNN0Xe26X523vdwMihxlxzoXb1m/svG1edVdJRB6/KjRFaKmiPXrZscm7GzrYt3V3gJUrQK1elKXpjjsdKj1OUy9BKWjXEi2DlE1VAs1F0fm+AO0FzvOMU8ZHQgybjxDW2W6ZTSSoGHCxQZ4PMIBDx9l8dyONhgxEMRXkbGkBdQ4GWt0WBR+2RIIDdEbd13p1T/98b5ms0v6jXQEzO+pueHg9oCfEVZs3XEw6QIh+p03fj2vdO2NQ/9AEzqc/sw3U6bv3Tj+0q4m/fv0YaA0dUzeOb5AhEraN3VCZ1Ok3Km6dIRu5sNXcuap866dqj4Cb6huOZ/X7XhvwYLbejt1WsD+7q172r68+fOm4s8/mPw3+iIArr9bfOBN+/hok1nyfPgY0BTE67sLI2WcFXUvGmkMLHxhLAzIdPu1UL1YJwnRVQCzAIdrCAFumukZ4pzSYqyEmGXi4IzPogXK5F3oz4fJ7mKy+R4j4DEWAePfMTcFG+ZPre53FpYJ+kPlnSW+CntV/bqH+joTazsCk+Y2HT7P4uppC8+oLqspqgn/9/3dV6xtB6vfO7anf2r3ddKpZ9caetIbgMUb4A81s6IVNrWN5w0Gu3Gqze2xxStjC0LFrWu7mxc2+XU+i85UEgy7KitdTZWL9vonbD507L0ew9pnAXtd99T+PfKGdApvZNuAIbJ9k4yRkkEosSrxVJaBFrAUgzAJhDWheZe4qaqxEpDGKCkG6Id9MXWi9uZI6kQkAidHjkSAUtpwsqy+sXRrWRk44vRzhU1BuBpG9mzV6VIBo4mBgkZaqdNt01bohmCLvgxS2WvRT0T6Wlp/sqx0W0ljQxlmj2RK4Wo6cgQ2b9WX6VMBPYAacFRfptuq1w/BVv0w+wOJN/aPinA8B5Yq9vsSGRkMpj2i2a2BgP074vJg0W+SIf7UIYqQvUPiKSX+UlqXOymQ5VBkKXaQoF3KXp4g7TWLFhL/M4zgpTYmRrx0muWN5EyjZs/kyuDgj3DRXyu/VgbsyfbQQKg9aQ8ov678a1G4pM4AqO4VILGiG1AGqW/vf+zd+x9gsKSuHMzZJ12oF+wB6QtMswwMAbugB7ftkx4sryspsoHE6tVSwkb34Qv2ymVlcFn9JII1Ldx6z7CU2yyLZUbV9dTF2xe1kz+0vq4HJnrWSYOkNHRckvn1+obWkZK8Jo3DS/qQRLAFwUDPunXg5Vw5MvYvN45bb8EmggwbHkRilMXqyzf0sKDXYCyqKpnbbPM1NfpszXNLQ0VGAzN/xKDyMfiDZVJfsR1JKCUlhR5gL+6bZLl2jHGhAukTb7KnUT/qxjZUQvSGBoGaFuBHQwnODwv6SW4yS8Jp/QEc+ojlypifxNayMfxJ+AneDUsCWK0WNrnwtrc+fuu2hfICrGUM0jtavU565wmVS/WE9I5Or5XeMTCs8oknlCxjAD50EPieUHqUTwAfOgh86YNQnbsNWkT0bJ/0ikGl4nq/0Wq/6eVUKgOo6WP1Rs0332gN6CiokY9qNPJR6RV01KD95htNWtf7KbubElAPpfx4LMNDGQdlUnCfn7DZEi7PqI+IxhgKAwfcEsmb+Sxa96T02yf6f3N6+YOf7jmEJslAr3T54O2Ymnbj80C4pcIouOcuPPzdjZdcXFqs4z9CtYk+mbynSXr47T2fPrh8+y9f/Oelr4DC228B1pd2crC0tHjaqxtv/O5wWCjWlcg4YFwy7TMuT0f+EaOne1T8+6ickHge6gRcnv8FoyPfkSMc5s/6kQyVRw0RRAzi5QQ/yuFVELwL1+kBro9NUu3URJLBVo4UXjwS4m6gY7BL0hOCVRnKxlaQJpdoBMYg/j6KCZpOGkwHuHECPtcXsA92vCaKQlT4FWuKty8Zlwhf2N2o0z9lKrSJIm38bYMMi3FcDNSKx+kpx8XagHh80C5NSCV+AlQ/gefVBh7c8rpYK4ri86yh1GXHQGqOYFCre9VsECKmv24YwBULyBfKt5H+CKnLf/IT9IGfPk0BfifTRV1BYu04WXezhoshkgAgUu5YLoBmRBrJBFYTIdvwcriOIRopVgRlBkkqeGbEv066JtbCENQFomLhvoL0GBNBTsHWOoDtekgXQTIItPqR3sLvtB63lU7TGIuNcSwnXFODFBFFWeA0ZYubTM6e+jYbrbKJesAzjODd2HVk/fm2ApV3Tf81TRzN6MuAoLGwrEFhqtUbiqLlJYVayAlKFQt1PFfQpBWM5sh/zIyYHEimR3I8Z9QpBE9Zi7+pikGSOORMKuAK1nD0N/EPXJFlxaUl5mZUiL3nsfqAs4BhTRqNee74KgVgbd7x5foCjhVpprStw2ZTlVw7ALhrDBaWE5GMydBqc83qwqKm+dWFLFD4Gvq7S9q1Go8SWkS1HQINayx2N9QuCKhbPFXFSsjYyxe29G9T6WkaoH+Q1StlnuEHuK/ZyZSKjHpV1BxqJbUbfZFZPRhzCpNVpHRaM/iYqFn9IeBD+hv+GGNRnx/pumhkxHmpAtrESqATB4Zhxzf6dIlCCZ0gDbAZRTqlrEj6yT6yK4gVWlkthz/E7tVpZovQOWOTQqnVFfFGp855ovK/1q6eUVX1+qq1i5FmOCCdPvwX6U865QAAh/8C/CAw6dDPpZT0gfTfb+25KnE/WDCprZLhdHqOu+p3ocpKyOpUmvpFnZtmF4iKcisqmGl+q62MYe22JjBnXjiorInaFYW+lpaH5hWO0xQXbv9yyDNBr7O7PeNdjtu0DpZVa4t1rLp3eZ/P88zi8xc5ik409d04QWf99LC8uLbzur39LR1bnlqzETCJ+6+YFL9ep0HdADY2t27U6tSoRzWshIt7t9ehp6MytPZp0dNtpax2el9qo8Mu1DhmPtE5PiJwxXVVnH1yvj9wA6WkRMw1T7hxkX6NxHWjCfKY0NkHDDwaLI0WRmAuevD55x48+EuP95fSbamXTtwLfEzkxEupx4HvXk9v77xvDh36hm2WHEPSBUvfBrafgPG/S5VJH769FBwbAn9z/k76SRpnmmIv5ShqNba30FhE5SieIGSg8VgHsSsBoM8ritdZvM4Wg2gkxCJtn9Eh1QYNUdgeosOfModX2Utd83v7l/ZObzIY10vHXhPtdvE4KF/um9g7f8ncWe4NL+zf0FoQsfOWrs7FM+fGK7kJu5fMbQ67LSyjUTi66mp1gXD3RU0+ljMJCh7pRbqq6PzFl3XCYPO0ObOnNBqN1hrONrlny6ZrwY97NjW7aJ2zQKV6X/oW2AMF4K2TOkGhrZi0a1aVyTttSsXeAUBD2lhUN2njhEKjWNrY2lqtN1zazZnGT1q3/prOgu6e8+bPmhDV69mFdt7aGmkohtZpu2c2OwX0/dA3XMlbG0MBWI1EFzOSX/7OUiQK20Ryk4iUBeR4d2B2G/Gf35xhf2L+vnFGvTSU+nzGRuZ3p8oyfxtn0NNmbASO9jlbpH8C7ZY57WDCaeo0mIh+ru7omL1lS56sWYCkpep0bs2YFKiWMyRGMYk0CWqGoFMmQb3/bElS8NoxuFAfPFuy1DA/drqsw5lc84lchTOWFfOd4gLmaFwxC+rAWQs7mC4iaMWsqjKbq3T6rKUdJcPLdtJcMcG5spaogN1klFO2jCacf3qW5K4kjl/SpFOuNIGhX32PXCoeffvFuRx24QzsBOmU9bKzcRSkM9OB66xUBWnuzPORXG6mojiLkohkWCKLWfHsSoWxYGoloxEtg27FCDEp9i6IbrMbZ0qJ9OnlDdJrP7ld+vq21x8wXnoY8M/semszdDScprSGEuPnUonNT/dBhW5utL23v9MP7pVWGsCvS4zvg/NfevzPtwHl7SdAWcve6F8uf0b6ds+79g0J3gveddtotcEebu1tH38BL/0lkfBK9cN060aC/hMNBrBlIZqWMmTfKLZqYruoGZsusSETyh4kbD9G/4/y6jEfGPy1+43jOlrqDT0dPFtVXlhRbi1SqmmrWlPlqJ8QvafEIGrMHdVGNRotjD6fuay5fLZn75b+A8O9dvTeOVVtJlosi82p5kNTahdOM7tCthltK0yXef1xBZKkbizkFT5IW2GRUBrThq45HF5q15jNUweuAmEQGOaBAlnujk6s4UFGVumywUCtIJK/nkmFQiJQEZIhWXNmxRil0geZxLPSyZ8N6IS3aU6l1Fo/yiwFHdoJtujtVmlLenESMGQvTP5MOvmsoINL2wGnMiSsiq7zs2vfYQ31xCbWhLe2nZ9ZkQq0wPRTdOz/A4d9Jkx42mNgZGBgYGZovrupNCWe3+YrAzc7Awic6/l4G0b///+fgb2BDcTlYGACUQCcOA6BAAAAeNpjYGRgYGP4d5eBgb3hPxCwNzAARZABYy8AqVwHfAB42oVUvU4DMQz2XZKLBFSwsNClQgwM7cKfGO8FYOuIhHgAhIQY6BTxZDwUe/ncsy9O2qonffLFsR37cxyX6IPwtUui5ndApG04oDOyBZoE0ABi31vI5SB5z/o3fxv5wr6d2eN/RmBdKveAb+jf1aZTHxrsobsIsobvjdrsAvv5IXbPdk7W49k5DuuOguZpbLw5Px6C5OIpS1tvLHWrYHM3+11LDfK8B7721bYDvcS5Z8Ssn4Q0crey3ISS97NQ9aKIl0wdA5e9rbmreDDnk0uGh9yTsW4rNdeQ9+fOcqS1UBGzLXLeh0TnUneoen8m8hP6Rmx8fZeA0yLvRNOovFc5eullm6hvhTfV+SpX/F8Xc5eGHOo7E039wXAaDWchz1XuT8o5ItYCmKAHTwzktgDGtZ7hU577cRZnFe/G1ldzqHHicE94/iZYL8S3RY6tciLylXkC3uAfGLBvOokN+4nOJ/4d5NWufgsvJzr7qmvKe/qgepzrGIfmy/Tuzv1AOvJB54joMZDc2USXDOg36BTmPQOmfAfNvepD2q5Dzn2WN9gxhE+dZ7U77qp5CuYdgv1cJPdhFtN6bd9U2J7vmJViVre/f5P2Zi942mNgYBAjA8oxBDBMYrjC6MRYwLiOiYHJhlmFuYnFg+Ucyy9WG9ZlrH/YQtiOsKex/+EI4ZjE8YDTg3MF5z+uIK4JXLe4dbhn8bjwVPCc4jXjjeFdwufCt4JfjN+Hf5lAhECXwCNBLcFVQi5C24QrRCxEpoh8EPUSXSLmJ3ZA3Eg8TXyT+DcJFYkAiRmSApJ9kj+kEqQmSF2TZpNWkg6TLpFeIv1KRkrGR6ZMZonMB1kV2TrZA3Jack3yTPIZ8nvk/ymYKeQprFB4pKimWKZ4TPGPkoRSgdIeZTXlGcqPVCxUDqgKqKapKajtUfujnqDeo75HQ0ujSWOZxhtNJc0IzTVaPFpOWsu0+XTydJ7oVumJ6FnpTdL7oO+g36H/zMDFYI1hneEjoxyjB8ZCxkHGZ0ykTOxM9ph8M7UwnWPGYBZmtsZcyXyTRYDFA8say1NWYlZJVhts99nx2eXYTbN7YB9kf8Mhx2GWwxXHTU4SThVOj5wZnF2c17lEubxzneZW5HbF3cV9k4ecxzpPN89Fngc873n+8ZLy8vFq8JrntcdbxDvGe52Pl88JXy3ffb4//DL83vi7+Z8IkAjICtgX6BK4KfBdkFFQVtCJYKOQM2EcYZPCfoX7hFeEH4kQiIiJWBHxJdIhMitySuS+yBdRBlF1UXuiWaLNoidFv4sJiymJ2RFrFlsR+yjOIa4j7lt8SvyhBJVEtsSWJI2kjKQLyUbJDcmrkv+l5KTapE5JvZbGlVaSdiedJ31a+rOMiIwJGV8ykzI3ZT7IUshKyFqU9ShbKNsie1P2qxyfnAk5l3Idcstyz+T+y3PLq8hbkfcs3ya/If9SgUhBXMGKgk+FVYWXikKK1hTzFceUMJTUlKwo+VdaVfqlrKTsS3lR+YeKsopfVVzVTNUe1YuqP9VY1LTUPKgVqXWrfVU3qe5MvUy9W31B/az6Iw0yDS4Np5pntFi07GpVaM1qPdDG0ubStqudq92vfUdHXWdAZ1eXQldY15pupm6/7p7uZz0xPfuwQQCZWCxcAHjaY2BkYGCcxiTJIMIAAkxAzAiEDAwOYD4DABaYAQwAeNptkc1OwkAUhc8I/hCjK0NcNsa4cIG0uGKHKGpChGCDbgtSaZRC2vqDj+EDuHDhg7jSnUufwOfw9HaKYMhk7nz3zMy5t1MAa3hFBiqbA2BxJqyQZ5bwAtZxojmDLbQ1Z7GDR82L2MaL5iXefde8TPdvzSvYVE+ac9hQz5pXsaveNH8gr740f6KofnCOU9RhoIERevBJNQy5RqQ6PHRFDRkNmChwpvsVPFANmQ24tjivcYdbOAh4poEz2HSo4ABlZja1I1yiSW5JNs/F+OfTZhZw15PTBkrswOIski1Gk8p8pyYdevRIOg8YXfEyeHIosS87VWYjjLnjsW5fvju+0yWlVV2uwdQdd/JCEXUHV1QH0u8NNYdqJH4dfsefi8810i8a8htq4hq7zHZ+SId7qXNM8uk+lr4i9lnGHkda35m5V5BKF6TOVIemvJbNTnryl7uiWhxxLNHRwj6jyTF5019rT2jbAAAAeNptVwV0JMcR3V87yyDpzszMIDjBmc/MjGcYD/TuzO3QDYjMFDDHcRxmZmZmJoeZGRzmxKnumRW8RE/qruppqKr+9atVopL6eXy5tLX0f37wiGxKVCqDSveX7indXbqv9CDK0FBBFTXU0UATLbTRQRcjpXtLD5UewCjGsAEbsQN2xE7YGbtgV+yG3bEH9sRe2Bv7YF/sh/1xAA7EQTgYh+BQHIbDcQSOxFE4GuOYwCSmsAnTmMEs5rAZx+BYHIfjcQJOxEnYgpNxCk7FaTgdZ+BMnIWzcQ7OxXk4HxfgQlyEi3EJLsVluBxX4Epcha24GtfgWlwHHdfDgAkLNgR66MOBi20YwIOPACEibC91S4+VOoiRIEWGeSxgEUtYxg24ETfhZtyCW3EbbscduBN34Ql4Ip6EJ+Nu3IN7cR/uxwN4EE/BQ3gqHsbT8AiejmfgmXgWno3n4Ll4Hp6PF+CFeBFejJfgpXgZXo5X4JV4FV6N1+C1eB1ejzfgjXgT3oy34K14G96Od+CdeBfejffgvXgf3o8P4IP4ED6Mj+Cj+Bg+jk/gk/gUPo3P4LP4HD6PL+CLeBRfwpfxFXwVX8PX8Q18E9/Ct/EdfBffw/fxA/wQP8KP8RP8FD/Dz/EL/BK/wq/xG/wWj+F3+D3+gD/iT/gz/oK/4m/4O/6Bf+Jf+Df+g8epRCCiMmlUoSrVqE4NalKL2tShLo3QKI3RBtpIO9COtBPtXNqPdqFdaTfanfagPWkv2pv2oX1pP9qfDqAD6SA6mA6hQ+kwOpyOoCPpKDqaxmmCJmmKNtE0zdAszdFmOoaOpePoeDqBTqSTaAudTKfQqXQanU5n0Jl0Fp1N59C5dB6dTxfQhXQRXUyX0KV0GV1OV9CVdBVtpavpGrqWriOdrieDzNKjZJFNgnrUJ4dc2kYD8singEKKaDvFlFBKGc3TAi3SEi3TDXQj3UQ30y10K91Gt9MddCfdVXq4lgXu+PiWcdlPjo8P+4minyz6qaLfVPTTRT9T9LNFP1f0m4t+S95Pnp7306o/jc+p9D0jSSp+lrhWNRFGbDl1EcwLL4xExWE91ZLUiJuy0YUfpUtalohY67meX08d3TPivqDUqUnZTVIKB9VY+OG8qC2Hoa+7QV31YZaWw16vmrj9wPDKVtivpLGROJoT+qLOuwnd8FItdX2hxaFht+1wIfBYkMP1oVLNItlV3MAMF1uRZyzplhtbnuAzI2GktVj0YpE4dWmK2tALrYHW84x+k52xIycMRNKcD73MFzrb0ypEeUCjkLOouj22QlvUTEP15dToa/yXaGYYDuqy8Y14UIliN0irluGL2NB6YZDyd8+uuqnhuVYrFYup7gi376RNJS+4duo0+Vs/0D3RS9u5aIkgFXErV2I5vZPL27IkdXtLmvSl5QY2z8vXFbKa2+0ZlpBR0+ddW4S1yLXSLBbVSASW6zV9I9KlrSKuGrbckCPMdgrbTSuJY8SiYjmCIyQvrJOkItJNwxosGLHd6RkcwqFWHwqaDHolMhgEDIwwqvXCWI631fShonYqlIrYJqy0zefMx2HueWeoKBcakZclugRG03eDQmzlIFJyLRyovrM9ExwSXie1hhv0wnxZYsVCBIkTpp1iWY6KBi/MpaZpBEPRiONwQdnRykVlRT2Xs6j4rhChQiRxxOYk7rLQe5nntQs58Q3PGxWLlmf4xopZWt/tMeyE0eMciUVdLDHQ+DYaUrC8MBFtjkrgBn01vcLxDETdMjwR2EZcjY3ADv2aFfo+33HVN/qBSJvDeGXRShylfQz3dEGItMOuR5Hc0uKEbfcYhSLOD2sVijRhpDB8XsSpyyeOFboTxu4yw9fwGox43XLkJumCmzIu88BLkEnYK62dI17nw+OwPBBLGmdzUi9MTjqpk/lmwrbKwI0UmjRX6g1FJI7h9VqKXXJOqcl9mSI6nhsMGJx5KGtRljjsVoezR8RMG7r8rCjEDap8eOQstfoun2DmOMjZQR5T8RgHHFyZ7y0F8fyg7jB5c7WpJuSHFQ7Xh75W852rWSA5pMUQ46SRAbbLcZKUHZuTgtHAwQs0U3hey5Jh7XFgU9F0+BoLdCtRoq2mpCzKR2RAxnJE6quI3LBuRG0wsm4oi9Yvktswh4emqC7EnPNOJTWSQVJlRmVnGmbsip5lJKIpkZvnSaUfh1mkyVhWGCOZXTWFwQxRtrKUrzLiqBiRwo8baYkxL5oyPrrJQB0w4sKY8USZR6HHjBG7A5E6vGHfaWTMSzFvK9gG0xMVBq9rMc1n1qDB18j2cPp2VyQV9tF+GPbZmxUOaK0ZqPAdiqUmx1ykytN6LnKS5oJK4lxUseK8YQoPEi0JY4YaN3meKImTZ1jZVFEZYk1ju0MGTJ/xb3NJMkO+41YBZzmzPYS2qijM8SnjNRXMrXXGdsx3bzAjMuc1PWmEzrAw68wLfM990VUh1ocVrJ2rOVJrspTqvt3itakTJhx8UU8yN5U3VpegkidWLS5UQnCFCZmVZaVU5US6YGauxx7067w4knWnYfh8uhFYouoLe+CmrZ40iU/ZJth0wXXAyWmqN94TY3aYmRJKgYy4wt+6kRx/64YYf+t06VdzdX1rzcL6cEVzdWrNFsmAy0bVMyLZKaCkbT80pV8qG9sFvhXemtuzMC22zsX8ntnbIGBn8rkVrv7eUrOgAg7M6FoKVDS0hgal3hSLkczC/Hb5AqN8XiXx2ZBKj1MrKPvCqfWZ6yLDrjPNKVzU5VtCzuwqQVELo9muc4y5ehmeJl8MDWUQT/NGVviuICAmk7xYqPzVLGaxhlwiy+VAkg2jUtMnZze31lSWVpJxRnL6uhHDOjNziafNTbWjbHlZxs4VluACKjeUYeyuirp6eDmu8OzusNDk1ozJEqUzmhhDmZs4HNGYyU7IwrNo2UxQRbVJho+WDetGCoJaOyQJaq2uCMpJfW9as5JkqsrYZMps5qxagJiZiavjRsa7GyVusqYgja2MDYuWpk+NTzXU00/uX+VBtre7+nJQ5TqnfDVY9wQnvYRhLijE5t/VM0LRukoJfWpispmXfFUROO05rWVlywGyihSGrpw9WxZZXO6bUTlL7LIbxOVt0VI5zszyIF4om6kln8misZKzo4qHTAmMyDFMzkh9anLzhpXRlOnUzFKR7PS/Q9KtznBYcfDYOk1xkz41tUk20+0lrqaZWThSKNoiX3Njcfj0WJkjg1mzGSz8qGZK55fekLz4jcV6Pzb8ao/ftIO4bNhMHROzE13TTc1Mhr64BmZCL27lnRoa8UI+aLVKddboWbT2q8TV6Bo9T/EFfuaGC0mN0zQOXbvCiZEtspmuKWtLMliKuKiFWZxsz/jG+DnAUAmrPaZlT2iykQU8daNyksmrnZmpyX9u3HlRNrM+zQ8qC8I1Q/7HIeBfnjA72VW+60Pn5dimHXOThjXXy2uO/DTTtcN0zQc5Ntee56c4v0qVTTwyNy6bCdlMymZKNvKK5qZlMyObWdnMyWbzfwEYotW2AAAAAVJnQVsAAA==') format('woff');\n  font-weight: normal;\n  font-style: normal;\n}\n.fa{display:inline-block;font-family:FontAwesome;font-style:normal;font-weight:normal;line-height:1;-webkit-font-smoothing:antialiased;-moz-osx-font-smoothing:grayscale}\n.fa-lg{font-size:1.3333333333333333em;line-height:.75em;vertical-align:-15%}\n.fa-2x{font-size:2em}\n.fa-3x{font-size:3em}\n.fa-4x{font-size:4em}\n.fa-5x{font-size:5em}\n.fa-fw{width:1.2857142857142858em;text-align:center}\n.fa-ul{padding-left:0;margin-left:2.142857142857143em;list-style-type:none}.fa-ul>li{position:relative}\n.fa-li{position:absolute;left:-2.142857142857143em;width:2.142857142857143em;top:.14285714285714285em;text-align:center}.fa-li.fa-lg{left:-1.8571428571428572em}\n.fa-border{padding:.2em .25em .15em;border:solid .08em #eee;border-radius:.1em}\n.pull-right{float:right}\n.pull-left{float:left}\n.fa.pull-left{margin-right:.3em}\n.fa.pull-right{margin-left:.3em}\n.fa-spin{-webkit-animation:spin 2s infinite linear;-moz-animation:spin 2s infinite linear;-o-animation:spin 2s infinite linear;animation:spin 2s infinite linear}\n@-moz-keyframes spin{0%{-moz-transform:rotate(0deg)} 100%{-moz-transform:rotate(359deg)}}@-webkit-keyframes spin{0%{-webkit-transform:rotate(0deg)} 100%{-webkit-transform:rotate(359deg)}}@-o-keyframes spin{0%{-o-transform:rotate(0deg)} 100%{-o-transform:rotate(359deg)}}@-ms-keyframes spin{0%{-ms-transform:rotate(0deg)} 100%{-ms-transform:rotate(359deg)}}@keyframes spin{0%{transform:rotate(0deg)} 100%{transform:rotate(359deg)}}.fa-rotate-90{filter:progid:DXImageTransform.Microsoft.BasicImage(rotation=1);-webkit-transform:rotate(90deg);-moz-transform:rotate(90deg);-ms-transform:rotate(90deg);-o-transform:rotate(90deg);transform:rotate(90deg)}\n.fa-rotate-180{filter:progid:DXImageTransform.Microsoft.BasicImage(rotation=2);-webkit-transform:rotate(180deg);-moz-transform:rotate(180deg);-ms-transform:rotate(180deg);-o-transform:rotate(180deg);transform:rotate(180deg)}\n.fa-rotate-270{filter:progid:DXImageTransform.Microsoft.BasicImage(rotation=3);-webkit-transform:rotate(270deg);-moz-transform:rotate(270deg);-ms-transform:rotate(270deg);-o-transform:rotate(270deg);transform:rotate(270deg)}\n.fa-flip-horizontal{filter:progid:DXImageTransform.Microsoft.BasicImage(rotation=0, mirror=1);-webkit-transform:scale(-1, 1);-moz-transform:scale(-1, 1);-ms-transform:scale(-1, 1);-o-transform:scale(-1, 1);transform:scale(-1, 1)}\n.fa-flip-vertical{filter:progid:DXImageTransform.Microsoft.BasicImage(rotation=2, mirror=1);-webkit-transform:scale(1, -1);-moz-transform:scale(1, -1);-ms-transform:scale(1, -1);-o-transform:scale(1, -1);transform:scale(1, -1)}\n.fa-stack{position:relative;display:inline-block;width:2em;height:2em;line-height:2em;vertical-align:middle}\n.fa-stack-1x,.fa-stack-2x{position:absolute;width:100%;text-align:center}\n.fa-stack-1x{line-height:inherit}\n.fa-stack-2x{font-size:2em}\n.fa-inverse{color:#fff}\n.fa-glass:before{content:\"\\f000\"}\n.fa-music:before{content:\"\\f001\"}\n.fa-search:before{content:\"\\f002\"}\n.fa-envelope-o:before{content:\"\\f003\"}\n.fa-heart:before{content:\"\\f004\"}\n.fa-star:before{content:\"\\f005\"}\n.fa-star-o:before{content:\"\\f006\"}\n.fa-user:before{content:\"\\f007\"}\n.fa-film:before{content:\"\\f008\"}\n.fa-th-large:before{content:\"\\f009\"}\n.fa-th:before{content:\"\\f00a\"}\n.fa-th-list:before{content:\"\\f00b\"}\n.fa-check:before{content:\"\\f00c\"}\n.fa-times:before{content:\"\\f00d\"}\n.fa-search-plus:before{content:\"\\f00e\"}\n.fa-search-minus:before{content:\"\\f010\"}\n.fa-power-off:before{content:\"\\f011\"}\n.fa-signal:before{content:\"\\f012\"}\n.fa-gear:before,.fa-cog:before{content:\"\\f013\"}\n.fa-trash-o:before{content:\"\\f014\"}\n.fa-home:before{content:\"\\f015\"}\n.fa-file-o:before{content:\"\\f016\"}\n.fa-clock-o:before{content:\"\\f017\"}\n.fa-road:before{content:\"\\f018\"}\n.fa-download:before{content:\"\\f019\"}\n.fa-arrow-circle-o-down:before{content:\"\\f01a\"}\n.fa-arrow-circle-o-up:before{content:\"\\f01b\"}\n.fa-inbox:before{content:\"\\f01c\"}\n.fa-play-circle-o:before{content:\"\\f01d\"}\n.fa-rotate-right:before,.fa-repeat:before{content:\"\\f01e\"}\n.fa-refresh:before{content:\"\\f021\"}\n.fa-list-alt:before{content:\"\\f022\"}\n.fa-lock:before{content:\"\\f023\"}\n.fa-flag:before{content:\"\\f024\"}\n.fa-headphones:before{content:\"\\f025\"}\n.fa-volume-off:before{content:\"\\f026\"}\n.fa-volume-down:before{content:\"\\f027\"}\n.fa-volume-up:before{content:\"\\f028\"}\n.fa-qrcode:before{content:\"\\f029\"}\n.fa-barcode:before{content:\"\\f02a\"}\n.fa-tag:before{content:\"\\f02b\"}\n.fa-tags:before{content:\"\\f02c\"}\n.fa-book:before{content:\"\\f02d\"}\n.fa-bookmark:before{content:\"\\f02e\"}\n.fa-print:before{content:\"\\f02f\"}\n.fa-camera:before{content:\"\\f030\"}\n.fa-font:before{content:\"\\f031\"}\n.fa-bold:before{content:\"\\f032\"}\n.fa-italic:before{content:\"\\f033\"}\n.fa-text-height:before{content:\"\\f034\"}\n.fa-text-width:before{content:\"\\f035\"}\n.fa-align-left:before{content:\"\\f036\"}\n.fa-align-center:before{content:\"\\f037\"}\n.fa-align-right:before{content:\"\\f038\"}\n.fa-align-justify:before{content:\"\\f039\"}\n.fa-list:before{content:\"\\f03a\"}\n.fa-dedent:before,.fa-outdent:before{content:\"\\f03b\"}\n.fa-indent:before{content:\"\\f03c\"}\n.fa-video-camera:before{content:\"\\f03d\"}\n.fa-picture-o:before{content:\"\\f03e\"}\n.fa-pencil:before{content:\"\\f040\"}\n.fa-map-marker:before{content:\"\\f041\"}\n.fa-adjust:before{content:\"\\f042\"}\n.fa-tint:before{content:\"\\f043\"}\n.fa-edit:before,.fa-pencil-square-o:before{content:\"\\f044\"}\n.fa-share-square-o:before{content:\"\\f045\"}\n.fa-check-square-o:before{content:\"\\f046\"}\n.fa-move:before{content:\"\\f047\"}\n.fa-step-backward:before{content:\"\\f048\"}\n.fa-fast-backward:before{content:\"\\f049\"}\n.fa-backward:before{content:\"\\f04a\"}\n.fa-play:before{content:\"\\f04b\"}\n.fa-pause:before{content:\"\\f04c\"}\n.fa-stop:before{content:\"\\f04d\"}\n.fa-forward:before{content:\"\\f04e\"}\n.fa-fast-forward:before{content:\"\\f050\"}\n.fa-step-forward:before{content:\"\\f051\"}\n.fa-eject:before{content:\"\\f052\"}\n.fa-chevron-left:before{content:\"\\f053\"}\n.fa-chevron-right:before{content:\"\\f054\"}\n.fa-plus-circle:before{content:\"\\f055\"}\n.fa-minus-circle:before{content:\"\\f056\"}\n.fa-times-circle:before{content:\"\\f057\"}\n.fa-check-circle:before{content:\"\\f058\"}\n.fa-question-circle:before{content:\"\\f059\"}\n.fa-info-circle:before{content:\"\\f05a\"}\n.fa-crosshairs:before{content:\"\\f05b\"}\n.fa-times-circle-o:before{content:\"\\f05c\"}\n.fa-check-circle-o:before{content:\"\\f05d\"}\n.fa-ban:before{content:\"\\f05e\"}\n.fa-arrow-left:before{content:\"\\f060\"}\n.fa-arrow-right:before{content:\"\\f061\"}\n.fa-arrow-up:before{content:\"\\f062\"}\n.fa-arrow-down:before{content:\"\\f063\"}\n.fa-mail-forward:before,.fa-share:before{content:\"\\f064\"}\n.fa-resize-full:before{content:\"\\f065\"}\n.fa-resize-small:before{content:\"\\f066\"}\n.fa-plus:before{content:\"\\f067\"}\n.fa-minus:before{content:\"\\f068\"}\n.fa-asterisk:before{content:\"\\f069\"}\n.fa-exclamation-circle:before{content:\"\\f06a\"}\n.fa-gift:before{content:\"\\f06b\"}\n.fa-leaf:before{content:\"\\f06c\"}\n.fa-fire:before{content:\"\\f06d\"}\n.fa-eye:before{content:\"\\f06e\"}\n.fa-eye-slash:before{content:\"\\f070\"}\n.fa-warning:before,.fa-exclamation-triangle:before{content:\"\\f071\"}\n.fa-plane:before{content:\"\\f072\"}\n.fa-calendar:before{content:\"\\f073\"}\n.fa-random:before{content:\"\\f074\"}\n.fa-comment:before{content:\"\\f075\"}\n.fa-magnet:before{content:\"\\f076\"}\n.fa-chevron-up:before{content:\"\\f077\"}\n.fa-chevron-down:before{content:\"\\f078\"}\n.fa-retweet:before{content:\"\\f079\"}\n.fa-shopping-cart:before{content:\"\\f07a\"}\n.fa-folder:before{content:\"\\f07b\"}\n.fa-folder-open:before{content:\"\\f07c\"}\n.fa-resize-vertical:before{content:\"\\f07d\"}\n.fa-resize-horizontal:before{content:\"\\f07e\"}\n.fa-bar-chart-o:before{content:\"\\f080\"}\n.fa-twitter-square:before{content:\"\\f081\"}\n.fa-facebook-square:before{content:\"\\f082\"}\n.fa-camera-retro:before{content:\"\\f083\"}\n.fa-key:before{content:\"\\f084\"}\n.fa-gears:before,.fa-cogs:before{content:\"\\f085\"}\n.fa-comments:before{content:\"\\f086\"}\n.fa-thumbs-o-up:before{content:\"\\f087\"}\n.fa-thumbs-o-down:before{content:\"\\f088\"}\n.fa-star-half:before{content:\"\\f089\"}\n.fa-heart-o:before{content:\"\\f08a\"}\n.fa-sign-out:before{content:\"\\f08b\"}\n.fa-linkedin-square:before{content:\"\\f08c\"}\n.fa-thumb-tack:before{content:\"\\f08d\"}\n.fa-external-link:before{content:\"\\f08e\"}\n.fa-sign-in:before{content:\"\\f090\"}\n.fa-trophy:before{content:\"\\f091\"}\n.fa-github-square:before{content:\"\\f092\"}\n.fa-upload:before{content:\"\\f093\"}\n.fa-lemon-o:before{content:\"\\f094\"}\n.fa-phone:before{content:\"\\f095\"}\n.fa-square-o:before{content:\"\\f096\"}\n.fa-bookmark-o:before{content:\"\\f097\"}\n.fa-phone-square:before{content:\"\\f098\"}\n.fa-twitter:before{content:\"\\f099\"}\n.fa-facebook:before{content:\"\\f09a\"}\n.fa-github:before{content:\"\\f09b\"}\n.fa-unlock:before{content:\"\\f09c\"}\n.fa-credit-card:before{content:\"\\f09d\"}\n.fa-rss:before{content:\"\\f09e\"}\n.fa-hdd:before{content:\"\\f0a0\"}\n.fa-bullhorn:before{content:\"\\f0a1\"}\n.fa-bell:before{content:\"\\f0f3\"}\n.fa-certificate:before{content:\"\\f0a3\"}\n.fa-hand-o-right:before{content:\"\\f0a4\"}\n.fa-hand-o-left:before{content:\"\\f0a5\"}\n.fa-hand-o-up:before{content:\"\\f0a6\"}\n.fa-hand-o-down:before{content:\"\\f0a7\"}\n.fa-arrow-circle-left:before{content:\"\\f0a8\"}\n.fa-arrow-circle-right:before{content:\"\\f0a9\"}\n.fa-arrow-circle-up:before{content:\"\\f0aa\"}\n.fa-arrow-circle-down:before{content:\"\\f0ab\"}\n.fa-globe:before{content:\"\\f0ac\"}\n.fa-wrench:before{content:\"\\f0ad\"}\n.fa-tasks:before{content:\"\\f0ae\"}\n.fa-filter:before{content:\"\\f0b0\"}\n.fa-briefcase:before{content:\"\\f0b1\"}\n.fa-fullscreen:before{content:\"\\f0b2\"}\n.fa-group:before{content:\"\\f0c0\"}\n.fa-chain:before,.fa-link:before{content:\"\\f0c1\"}\n.fa-cloud:before{content:\"\\f0c2\"}\n.fa-flask:before{content:\"\\f0c3\"}\n.fa-cut:before,.fa-scissors:before{content:\"\\f0c4\"}\n.fa-copy:before,.fa-files-o:before{content:\"\\f0c5\"}\n.fa-paperclip:before{content:\"\\f0c6\"}\n.fa-save:before,.fa-floppy-o:before{content:\"\\f0c7\"}\n.fa-square:before{content:\"\\f0c8\"}\n.fa-reorder:before{content:\"\\f0c9\"}\n.fa-list-ul:before{content:\"\\f0ca\"}\n.fa-list-ol:before{content:\"\\f0cb\"}\n.fa-strikethrough:before{content:\"\\f0cc\"}\n.fa-underline:before{content:\"\\f0cd\"}\n.fa-table:before{content:\"\\f0ce\"}\n.fa-magic:before{content:\"\\f0d0\"}\n.fa-truck:before{content:\"\\f0d1\"}\n.fa-pinterest:before{content:\"\\f0d2\"}\n.fa-pinterest-square:before{content:\"\\f0d3\"}\n.fa-google-plus-square:before{content:\"\\f0d4\"}\n.fa-google-plus:before{content:\"\\f0d5\"}\n.fa-money:before{content:\"\\f0d6\"}\n.fa-caret-down:before{content:\"\\f0d7\"}\n.fa-caret-up:before{content:\"\\f0d8\"}\n.fa-caret-left:before{content:\"\\f0d9\"}\n.fa-caret-right:before{content:\"\\f0da\"}\n.fa-columns:before{content:\"\\f0db\"}\n.fa-unsorted:before,.fa-sort:before{content:\"\\f0dc\"}\n.fa-sort-down:before,.fa-sort-asc:before{content:\"\\f0dd\"}\n.fa-sort-up:before,.fa-sort-desc:before{content:\"\\f0de\"}\n.fa-envelope:before{content:\"\\f0e0\"}\n.fa-linkedin:before{content:\"\\f0e1\"}\n.fa-rotate-left:before,.fa-undo:before{content:\"\\f0e2\"}\n.fa-legal:before,.fa-gavel:before{content:\"\\f0e3\"}\n.fa-dashboard:before,.fa-tachometer:before{content:\"\\f0e4\"}\n.fa-comment-o:before{content:\"\\f0e5\"}\n.fa-comments-o:before{content:\"\\f0e6\"}\n.fa-flash:before,.fa-bolt:before{content:\"\\f0e7\"}\n.fa-sitemap:before{content:\"\\f0e8\"}\n.fa-umbrella:before{content:\"\\f0e9\"}\n.fa-paste:before,.fa-clipboard:before{content:\"\\f0ea\"}\n.fa-lightbulb-o:before{content:\"\\f0eb\"}\n.fa-exchange:before{content:\"\\f0ec\"}\n.fa-cloud-download:before{content:\"\\f0ed\"}\n.fa-cloud-upload:before{content:\"\\f0ee\"}\n.fa-user-md:before{content:\"\\f0f0\"}\n.fa-stethoscope:before{content:\"\\f0f1\"}\n.fa-suitcase:before{content:\"\\f0f2\"}\n.fa-bell-o:before{content:\"\\f0a2\"}\n.fa-coffee:before{content:\"\\f0f4\"}\n.fa-cutlery:before{content:\"\\f0f5\"}\n.fa-file-text-o:before{content:\"\\f0f6\"}\n.fa-building:before{content:\"\\f0f7\"}\n.fa-hospital:before{content:\"\\f0f8\"}\n.fa-ambulance:before{content:\"\\f0f9\"}\n.fa-medkit:before{content:\"\\f0fa\"}\n.fa-fighter-jet:before{content:\"\\f0fb\"}\n.fa-beer:before{content:\"\\f0fc\"}\n.fa-h-square:before{content:\"\\f0fd\"}\n.fa-plus-square:before{content:\"\\f0fe\"}\n.fa-angle-double-left:before{content:\"\\f100\"}\n.fa-angle-double-right:before{content:\"\\f101\"}\n.fa-angle-double-up:before{content:\"\\f102\"}\n.fa-angle-double-down:before{content:\"\\f103\"}\n.fa-angle-left:before{content:\"\\f104\"}\n.fa-angle-right:before{content:\"\\f105\"}\n.fa-angle-up:before{content:\"\\f106\"}\n.fa-angle-down:before{content:\"\\f107\"}\n.fa-desktop:before{content:\"\\f108\"}\n.fa-laptop:before{content:\"\\f109\"}\n.fa-tablet:before{content:\"\\f10a\"}\n.fa-mobile-phone:before,.fa-mobile:before{content:\"\\f10b\"}\n.fa-circle-o:before{content:\"\\f10c\"}\n.fa-quote-left:before{content:\"\\f10d\"}\n.fa-quote-right:before{content:\"\\f10e\"}\n.fa-spinner:before{content:\"\\f110\"}\n.fa-circle:before{content:\"\\f111\"}\n.fa-mail-reply:before,.fa-reply:before{content:\"\\f112\"}\n.fa-github-alt:before{content:\"\\f113\"}\n.fa-folder-o:before{content:\"\\f114\"}\n.fa-folder-open-o:before{content:\"\\f115\"}\n.fa-expand-o:before{content:\"\\f116\"}\n.fa-collapse-o:before{content:\"\\f117\"}\n.fa-smile-o:before{content:\"\\f118\"}\n.fa-frown-o:before{content:\"\\f119\"}\n.fa-meh-o:before{content:\"\\f11a\"}\n.fa-gamepad:before{content:\"\\f11b\"}\n.fa-keyboard-o:before{content:\"\\f11c\"}\n.fa-flag-o:before{content:\"\\f11d\"}\n.fa-flag-checkered:before{content:\"\\f11e\"}\n.fa-terminal:before{content:\"\\f120\"}\n.fa-code:before{content:\"\\f121\"}\n.fa-reply-all:before{content:\"\\f122\"}\n.fa-mail-reply-all:before{content:\"\\f122\"}\n.fa-star-half-empty:before,.fa-star-half-full:before,.fa-star-half-o:before{content:\"\\f123\"}\n.fa-location-arrow:before{content:\"\\f124\"}\n.fa-crop:before{content:\"\\f125\"}\n.fa-code-fork:before{content:\"\\f126\"}\n.fa-unlink:before,.fa-chain-broken:before{content:\"\\f127\"}\n.fa-question:before{content:\"\\f128\"}\n.fa-info:before{content:\"\\f129\"}\n.fa-exclamation:before{content:\"\\f12a\"}\n.fa-superscript:before{content:\"\\f12b\"}\n.fa-subscript:before{content:\"\\f12c\"}\n.fa-eraser:before{content:\"\\f12d\"}\n.fa-puzzle-piece:before{content:\"\\f12e\"}\n.fa-microphone:before{content:\"\\f130\"}\n.fa-microphone-slash:before{content:\"\\f131\"}\n.fa-shield:before{content:\"\\f132\"}\n.fa-calendar-o:before{content:\"\\f133\"}\n.fa-fire-extinguisher:before{content:\"\\f134\"}\n.fa-rocket:before{content:\"\\f135\"}\n.fa-maxcdn:before{content:\"\\f136\"}\n.fa-chevron-circle-left:before{content:\"\\f137\"}\n.fa-chevron-circle-right:before{content:\"\\f138\"}\n.fa-chevron-circle-up:before{content:\"\\f139\"}\n.fa-chevron-circle-down:before{content:\"\\f13a\"}\n.fa-html5:before{content:\"\\f13b\"}\n.fa-css3:before{content:\"\\f13c\"}\n.fa-anchor:before{content:\"\\f13d\"}\n.fa-unlock-o:before{content:\"\\f13e\"}\n.fa-bullseye:before{content:\"\\f140\"}\n.fa-ellipsis-horizontal:before{content:\"\\f141\"}\n.fa-ellipsis-vertical:before{content:\"\\f142\"}\n.fa-rss-square:before{content:\"\\f143\"}\n.fa-play-circle:before{content:\"\\f144\"}\n.fa-ticket:before{content:\"\\f145\"}\n.fa-minus-square:before{content:\"\\f146\"}\n.fa-minus-square-o:before{content:\"\\f147\"}\n.fa-level-up:before{content:\"\\f148\"}\n.fa-level-down:before{content:\"\\f149\"}\n.fa-check-square:before{content:\"\\f14a\"}\n.fa-pencil-square:before{content:\"\\f14b\"}\n.fa-external-link-square:before{content:\"\\f14c\"}\n.fa-share-square:before{content:\"\\f14d\"}\n.fa-compass:before{content:\"\\f14e\"}\n.fa-toggle-down:before,.fa-caret-square-o-down:before{content:\"\\f150\"}\n.fa-toggle-up:before,.fa-caret-square-o-up:before{content:\"\\f151\"}\n.fa-toggle-right:before,.fa-caret-square-o-right:before{content:\"\\f152\"}\n.fa-euro:before,.fa-eur:before{content:\"\\f153\"}\n.fa-gbp:before{content:\"\\f154\"}\n.fa-dollar:before,.fa-usd:before{content:\"\\f155\"}\n.fa-rupee:before,.fa-inr:before{content:\"\\f156\"}\n.fa-cny:before,.fa-rmb:before,.fa-yen:before,.fa-jpy:before{content:\"\\f157\"}\n.fa-ruble:before,.fa-rouble:before,.fa-rub:before{content:\"\\f158\"}\n.fa-won:before,.fa-krw:before{content:\"\\f159\"}\n.fa-bitcoin:before,.fa-btc:before{content:\"\\f15a\"}\n.fa-file:before{content:\"\\f15b\"}\n.fa-file-text:before{content:\"\\f15c\"}\n.fa-sort-alpha-asc:before{content:\"\\f15d\"}\n.fa-sort-alpha-desc:before{content:\"\\f15e\"}\n.fa-sort-amount-asc:before{content:\"\\f160\"}\n.fa-sort-amount-desc:before{content:\"\\f161\"}\n.fa-sort-numeric-asc:before{content:\"\\f162\"}\n.fa-sort-numeric-desc:before{content:\"\\f163\"}\n.fa-thumbs-up:before{content:\"\\f164\"}\n.fa-thumbs-down:before{content:\"\\f165\"}\n.fa-youtube-square:before{content:\"\\f166\"}\n.fa-youtube:before{content:\"\\f167\"}\n.fa-xing:before{content:\"\\f168\"}\n.fa-xing-square:before{content:\"\\f169\"}\n.fa-youtube-play:before{content:\"\\f16a\"}\n.fa-dropbox:before{content:\"\\f16b\"}\n.fa-stack-overflow:before{content:\"\\f16c\"}\n.fa-instagram:before{content:\"\\f16d\"}\n.fa-flickr:before{content:\"\\f16e\"}\n.fa-adn:before{content:\"\\f170\"}\n.fa-bitbucket:before{content:\"\\f171\"}\n.fa-bitbucket-square:before{content:\"\\f172\"}\n.fa-tumblr:before{content:\"\\f173\"}\n.fa-tumblr-square:before{content:\"\\f174\"}\n.fa-long-arrow-down:before{content:\"\\f175\"}\n.fa-long-arrow-up:before{content:\"\\f176\"}\n.fa-long-arrow-left:before{content:\"\\f177\"}\n.fa-long-arrow-right:before{content:\"\\f178\"}\n.fa-apple:before{content:\"\\f179\"}\n.fa-windows:before{content:\"\\f17a\"}\n.fa-android:before{content:\"\\f17b\"}\n.fa-linux:before{content:\"\\f17c\"}\n.fa-dribbble:before{content:\"\\f17d\"}\n.fa-skype:before{content:\"\\f17e\"}\n.fa-foursquare:before{content:\"\\f180\"}\n.fa-trello:before{content:\"\\f181\"}\n.fa-female:before{content:\"\\f182\"}\n.fa-male:before{content:\"\\f183\"}\n.fa-gittip:before{content:\"\\f184\"}\n.fa-sun-o:before{content:\"\\f185\"}\n.fa-moon-o:before{content:\"\\f186\"}\n.fa-archive:before{content:\"\\f187\"}\n.fa-bug:before{content:\"\\f188\"}\n.fa-vk:before{content:\"\\f189\"}\n.fa-weibo:before{content:\"\\f18a\"}\n.fa-renren:before{content:\"\\f18b\"}\n.fa-pagelines:before{content:\"\\f18c\"}\n.fa-stack-exchange:before{content:\"\\f18d\"}\n.fa-arrow-circle-o-right:before{content:\"\\f18e\"}\n.fa-arrow-circle-o-left:before{content:\"\\f190\"}\n.fa-toggle-left:before,.fa-caret-square-o-left:before{content:\"\\f191\"}\n.fa-dot-circle-o:before{content:\"\\f192\"}\n.fa-wheelchair:before{content:\"\\f193\"}\n.fa-vimeo-square:before{content:\"\\f194\"}\n.fa-turkish-lira:before,.fa-try:before{content:\"\\f195\"}\n\n/* General */\n.dialog {\nbox-shadow: 0 1px 2px rgba(0, 0, 0, .15);\nborder: 1px solid;\ndisplay: block;\npadding: 0;\n}\n.field {\nbackground-color: #FFF;\nborder: 1px solid #CCC;\n-moz-box-sizing: border-box;\nbox-sizing: border-box;\ncolor: #333;\nfont-family: inherit;\nfont-size: 13px;\nmargin: 0;\npadding: 2px 4px 3px;\noutline: none;\ntransition: color .25s, border-color .25s, flex .25s;\n}\n.field::-moz-placeholder,\n.field:hover::-moz-placeholder {\ncolor: #AAA !important;\n}\n.field:hover {\nborder-color: #999;\n}\n.field:hover, .field:focus {\ncolor: #000;\n}\n.field[disabled] {\nbackground-color: #F2F2F2;\ncolor: #888;\n}\n.move {\ncursor: move;\n}\nlabel, .watcher-toggler {\ncursor: pointer;\n}\na[href=\"javascript:;\"] {\ntext-decoration: none;\n}\n.warning {\ncolor: red;\n}\n\n/* 4chan style fixes */\n.opContainer, .op {\ndisplay: block !important;\n}\n.post {\noverflow: visible !important;\n}\n[hidden] {\ndisplay: none !important;\n}\n\n/* fixed, z-index */\n#overlay,\n#qp, #ihover,\n#updater, #thread-stats,\n#navlinks, #header,\n#qr {\nposition: fixed;\n}\n#overlay {\nz-index: 999;\n}\n#notifications {\nz-index: 70;\n}\n#qp, #ihover {\nz-index: 60;\n}\n#menu {\nz-index: 50;\n}\n#navlinks, #updater, #thread-stats {\nz-index: 40;\n}\n#qr {\nz-index: 30;\n}\n#thread-watcher:hover {\nz-index: 20;\n}\n#header {\nz-index: 10;\n}\n#thread-watcher {\nz-index: 5;\n}\n\n/* Header */\n:root.top-header body {\nmargin-top: 2em;\n}\n:root.bottom-header body {\nmargin-bottom: 2em;\n}\n:root.fourchan-x #navtopright,\n:root.fourchan-x #navbotright,\n:root.fourchan-x:not(.show-original-top-board-list) #boardNavDesktop,\n:root.fourchan-x:not(.show-original-bot-board-list) #boardNavDesktopFoot {\ndisplay: none !important;\n}\n#header {\nright: 0;\nleft: 0;\n}\n#header.top {\ntop: 0;\n}\n#header.bottom {\nbottom: 0;\n}\n#header-bar {\nborder-width: 0;\ndisplay: flex;\npadding: 3px;\nposition: relative;\ntransition: all .1s .05s ease-in-out;\n}\n#header.top #header-bar {\nborder-bottom-width: 1px;\n}\n#header.bottom #header-bar {\nbox-shadow: 0 -1px 2px rgba(0, 0, 0, .15);\nborder-top-width: 1px;\n}\n#board-list {\nflex: 1;\nalign-self: center;\ntext-align: center;\n}\n#header-bar.autohide:not(:hover) {\nbox-shadow: none;\ntransition: all .8s .6s cubic-bezier(.55, .055, .675, .19);\n}\n#header.top #header-bar.autohide:not(:hover) {\nmargin-bottom: -1em;\n-webkit-transform: translateY(-100%);\ntransform: translateY(-100%);\n}\n#header.bottom #header-bar.autohide:not(:hover) {\n-webkit-transform: translateY(100%);\ntransform: translateY(100%);\n}\n#toggle-header-bar {\nleft: 0;\nright: 0;\nheight: 10px;\nposition: absolute;\n}\n#header.top #toggle-header-bar {\ncursor: n-resize;\nbottom: -10px;\n}\n#header.bottom #toggle-header-bar {\ncursor: s-resize;\ntop: -10px;\n}\n#header.top #header-bar.autohide #toggle-header-bar {\ncursor: s-resize;\n}\n#header.bottom #header-bar.autohide #toggle-header-bar {\ncursor: n-resize;\n}\n#header-bar a:not(.entry) {\ntext-decoration: none;\npadding: 1px;\n}\n.shortcut:not(:last-child)::after {\ncontent: \" / \";\n}\n.brackets-wrap::before {\ncontent: \" [ \";\n}\n.brackets-wrap::after {\ncontent: \" ] \";\n}\n\n/* Notifications */\n#notifications {\nheight: 0;\ntext-align: center;\n}\n#header.bottom #notifications {\nposition: fixed;\ntop: 0;\nleft: 0;\nwidth: 100%;\n}\n.notification {\ncolor: #FFF;\nfont-weight: 700;\ntext-shadow: 0 1px 2px rgba(0, 0, 0, .5);\nbox-shadow: 0 1px 2px rgba(0, 0, 0, .15);\nborder-radius: 2px;\nmargin: 1px auto;\nwidth: 500px;\nmax-width: 100%;\nposition: relative;\ntransition: all .25s ease-in-out;\n}\n.notification.error {\nbackground-color: hsla(0, 100%, 38%, .9);\n}\n.notification.warning {\nbackground-color: hsla(36, 100%, 38%, .9);\n}\n.notification.info {\nbackground-color: hsla(200, 100%, 38%, .9);\n}\n.notification.success {\nbackground-color: hsla(104, 100%, 38%, .9);\n}\n.notification a {\ncolor: white;\n}\n.notification > .close {\npadding: 6px;\ntop: 0;\nright: 0;\nposition: absolute;\n}\n.message {\n-moz-box-sizing: border-box;\nbox-sizing: border-box;\npadding: 6px 20px;\nmax-height: 200px;\nwidth: 100%;\noverflow: auto;\n}\n\n/* Settings */\n:root.fourchan-x body {\n-moz-box-sizing: border-box;\nbox-sizing: border-box;\n}\n#overlay {\nbackground-color: rgba(0, 0, 0, .5);\ndisplay: flex;\nposition: fixed;\ntop: 0;\nleft: 0;\nheight: 100%;\nwidth: 100%;\n}\n#fourchanx-settings {\n-moz-box-sizing: border-box;\nbox-sizing: border-box;\nbox-shadow: 0 0 15px rgba(0, 0, 0, .15);\nheight: 600px;\nmax-height: 100%;\nwidth: 900px;\nmax-width: 100%;\nmargin: auto;\npadding: 3px;\ndisplay: flex;\nflex-direction: column;\n}\n#fourchanx-settings > nav {\ndisplay: flex;\npadding: 2px 2px 0;\n}\n#fourchanx-settings > nav a {\ntext-decoration: underline;\n}\n#fourchanx-settings > nav a.close {\ntext-decoration: none;\npadding: 2px;\n}\n.sections-list {\nflex: 1;\n}\n.tab-selected {\nfont-weight: 700;\n}\n.section-container {\nflex: 1;\nposition: relative;\n}\n.section-container > section {\nposition: absolute;\ntop: 0;\nright: 0;\nbottom: 0;\nleft: 0;\noverflow: auto;\n}\n.section-sauce ul,\n.section-rice ul {\nlist-style: none;\nmargin: 0;\npadding: 8px;\n}\n.section-sauce li,\n.section-rice li {\npadding-left: 4px;\n}\n.section-main label {\ntext-decoration: underline;\n}\n.section-filter ul,\n.section-qr ul {\npadding: 0;\n}\n.section-filter li,\n.section-qr li {\nmargin: 10px 40px;\n}\n.section-filter textarea {\nheight: 500px;\n}\n.section-qr textarea {\nheight: 200px;\n}\n.section-sauce textarea {\nheight: 350px;\n}\n.section-rice .field[name=\"boardnav\"] {\nwidth: 100%;\n}\n.section-rice textarea {\nheight: 150px;\n}\n.section-archives table {\nwidth: 100%;\n}\n.section-archives th:not(:first-child) {\nwidth: 30%;\n}\n.section-archives td {\ntext-align: center;\n}\n.section-archives select {\nwidth: 90%;\n}\n.section-keybinds .field {\nfont-family: monospace;\n}\n#fourchanx-settings fieldset {\nborder: 1px solid;\nborder-radius: 3px;\n}\n#fourchanx-settings legend {\nfont-weight: 700;\n}\n#fourchanx-settings textarea {\nfont-family: monospace;\nmin-width: 100%;\nmax-width: 100%;\n}\n#fourchanx-settings code {\ncolor: #000;\nbackground-color: #FFF;\npadding: 0 2px;\n}\n.unscroll {\noverflow: hidden;\n}\n\n/* Index */\n:root.index-loading .board,\n:root.index-loading .pagelist {\ndisplay: none;\n}\n.summary {\ntext-decoration: none;\n}\n\n/* Announcement Hiding */\n:root.hide-announcement #globalMessage,\n:root.hide-announcement-enabled #toggleMsgBtn {\ndisplay: none;\n}\na.hide-announcement {\nfloat: left;\n}\n\n/* Unread */\n#unread-line {\nmargin: 0;\n}\n\n/* Thread Updater */\n#updater:not(:hover) {\nbackground: none;\nborder: none;\nbox-shadow: none;\n}\n#updater > .move {\npadding: 0 3px;\n}\n#updater > div:last-child {\ntext-align: center;\n}\n#updater input[type=number] {\nwidth: 4em;\n}\n#updater:not(:hover) > div:not(.move) {\ndisplay: none;\n}\n#updater input[type=\"button\"] {\nwidth: 100%;\n}\n.new {\ncolor: limegreen;\n}\n\n/* Thread Watcher */\n#thread-watcher {\nmax-width: 200px;\nmin-width: 150px;\npadding: 3px;\nposition: absolute;\n}\n#thread-watcher > div:first-child {\ndisplay: flex;\nalign-items: center;\n}\n#thread-watcher .move {\nflex: 1;\n}\n#watcher-status:not(:empty)::before {\ncontent: \"(\";\n}\n#watcher-status:not(:empty)::after {\ncontent: \")\";\n}\n#watched-threads:not(:hover) {\nmax-height: 150px;\noverflow: hidden;\n}\n#watched-threads div {\noverflow: hidden;\ntext-overflow: ellipsis;\nwhite-space: nowrap;\n}\n#watched-threads .current {\nfont-weight: 700;\n}\n#watched-threads a {\ntext-decoration: none;\n}\n#watched-threads .dead-thread a[title] {\ntext-decoration: line-through;\n}\n\n/* Thread Stats */\n#thread-stats {\nbackground: none;\nborder: none;\nbox-shadow: none;\n}\n\n/* Quote */\n.deadlink {\ntext-decoration: none !important;\n}\n.backlink.deadlink:not(.forwardlink),\n.quotelink.deadlink:not(.forwardlink) {\ntext-decoration: underline !important;\n}\n.inlined {\nopacity: .5;\n}\n#qp input, .forwarded {\ndisplay: none;\n}\n.quotelink.forwardlink,\n.backlink.forwardlink {\ntext-decoration: none;\nborder-bottom: 1px dashed;\n}\n.filtered {\ntext-decoration: underline line-through;\n}\n.inline {\nborder: 1px solid;\ndisplay: table;\nmargin: 2px 0;\n}\n.inline .post {\nborder: 0 !important;\nbackground-color: transparent !important;\ndisplay: table !important;\nmargin: 0 !important;\npadding: 1px 2px !important;\n}\n#qp > .opContainer::after {\ncontent: '';\nclear: both;\ndisplay: table;\n}\n#qp .post {\nborder: none;\nmargin: 0;\npadding: 2px 2px 5px;\n}\n#qp img {\nmax-height: 80vh;\nmax-width: 50vw;\n}\n.qphl {\noutline: 2px solid rgba(216, 94, 49, .7);\n}\n\n/* File */\n.fileText:hover .fntrunc,\n.fileText:not(:hover) .fnfull,\n.expanded-image > .post > .file > .fileThumb > img[data-md5],\n:not(.expanded-image) > .post > .file > .fileThumb > .full-image {\ndisplay: none;\n}\n.expanding {\nopacity: .5;\n}\n.expanded-image {\nclear: both;\n}\n.expanded-image > .op > .file::after {\ncontent: '';\nclear: both;\ndisplay: table;\n}\n:root.fit-height .full-image {\nmax-height: 100vh;\n}\n:root.fit-width .full-image {\nmax-width: 100%;\n}\n:root.gecko.fit-width .full-image {\nwidth: 100%;\n}\n#ihover {\n-moz-box-sizing: border-box;\nbox-sizing: border-box;\nmax-height: 100%;\nmax-width: 75%;\npadding-bottom: 16px;\n}\n\n/* Index/Reply Navigation */\n#navlinks {\nfont-size: 16px;\ntop: 25px;\nright: 10px;\n}\n\n/* Filter */\n.opContainer.filter-highlight {\nbox-shadow: inset 5px 0 rgba(255, 0, 0, .5);\n}\n.filter-highlight > .reply {\nbox-shadow: -5px 0 rgba(255, 0, 0, .5);\n}\n\n/* Thread & Reply Hiding */\n.hide-thread-button,\n.hide-reply-button {\nfloat: left;\nmargin-right: 2px;\n}\n.stub ~ * {\ndisplay: none !important;\n}\n.stub input {\ndisplay: inline-block;\n}\n\n/* QR */\n:root.hide-original-post-form #postForm,\n:root.hide-original-post-form .postingMode,\n:root.hide-original-post-form #togglePostForm,\n#qr.autohide:not(.has-focus):not(:hover) > form {\ndisplay: none;\n}\n#qr select, #dump-button, .remove, .captcha-img {\ncursor: pointer;\n}\n#qr > div {\nmin-width: 300px;\ndisplay: flex;\nalign-items: center;\n}\n#qr .move {\nalign-self: stretch;\nflex: 1;\n}\n#qr select[data-name=thread] {\nmargin: 0;\n-webkit-appearance: none;\n-moz-appearance: none;\nappearance: none;\nborder: none;\nbackground: none;\nfont: inherit;\n}\n#qr option {\ncolor: #000;\nbackground-color: #F7F7F7;\n}\n#qr .close {\npadding: 0 3px;\n}\n#qr > form {\ndisplay: flex;\nflex-direction: column;\n}\n.persona {\ndisplay: flex;\n}\n.persona .field {\nflex: 1;\n}\n.persona .field:hover,\n.persona .field:focus {\nflex: 3;\n}\n#dump-button {\nbackground: linear-gradient(#EEE, #CCC);\nborder: 1px solid #CCC;\nmargin: 0;\npadding: 2px 4px 3px;\noutline: none;\nwidth: 30px;\n}\n#dump-button:hover,\n#dump-button:focus {\nbackground: linear-gradient(#FFF, #DDD);\n}\n#dump-button:active,\n.dump #dump-button:not(:hover):not(:focus) {\nbackground: linear-gradient(#CCC, #DDD);\n}\n:root.gecko #dump-button {\npadding: 0;\n}\n#qr:not(.dump) #dump-list-container {\ndisplay: none;\n}\n#dump-list-container {\nheight: 100px;\nposition: relative;\n-webkit-user-select: none;\n-moz-user-select: none;\nuser-select: none;\n}\n#dump-list {\ncounter-reset: qrpreviews;\ntop: 0;\nright: 0;\nbottom: 0;\nleft: 0;\noverflow: hidden;\nposition: absolute;\nwhite-space: nowrap;\n}\n#dump-list:hover {\nbottom: -12px;\noverflow-x: auto;\nz-index: 1;\n}\n#dump-list::-webkit-scrollbar {\nheight: 12px;\n}\n#dump-list::-webkit-scrollbar-thumb {\nborder: 1px solid;\n}\n.qr-preview {\nbackground-position: 50% 20%;\nbackground-size: cover;\nborder: 1px solid #808080;\ncolor: #FFF !important;\nfont-size: 12px;\n-moz-box-sizing: border-box;\nbox-sizing: border-box;\ncursor: move;\ndisplay: inline-block;\nheight: 92px;\nwidth: 92px;\nmargin: 4px;\npadding: 2px;\nopacity: .6;\noutline: none;\noverflow: hidden;\nposition: relative;\ntext-shadow: 0 1px 1px #000;\ntransition: opacity .25s ease-in-out;\nvertical-align: top;\nwhite-space: pre;\n}\n.qr-preview:hover,\n.qr-preview:focus {\nopacity: .9;\ncolor: #FFF !important;\n}\n.qr-preview#selected {\nopacity: 1;\n}\n.qr-preview::before {\ncounter-increment: qrpreviews;\ncontent: counter(qrpreviews);\nfont-weight: 700;\ntext-shadow: 0 0 3px #000, 0 0 5px #000;\nposition: absolute;\ntop: 3px;\nright: 3px;\n}\n.qr-preview.drag {\nborder-color: red;\nborder-style: dashed;\nopacity: 1;\n}\n.qr-preview.over {\nborder-color: #FFF;\nborder-style: dashed;\nopacity: 1;\n}\n.remove {\ncolor: #E00 !important;\nfont-weight: 700;\npadding: 3px;\n}\n.remove:hover::after {\ncontent: ' Remove';\n}\n.qr-preview > label {\nbackground: rgba(0, 0, 0, .5);\nright: 0;\nbottom: 0;\nleft: 0;\nposition: absolute;\ntext-align: center;\n}\n.qr-preview > label > input {\nmargin: 1px 0;\nvertical-align: bottom;\n}\n#add-post {\ndisplay: inline-block;\nfont-size: 30px;\nheight: 30px;\nwidth: 30px;\nline-height: 1;\ntext-align: center;\nposition: absolute;\nright: 0;\nbottom: 0;\nz-index: 1;\n}\n#qr textarea {\nmin-height: 160px;\nmin-width: 100%;\ndisplay: block;\n}\n#qr.has-captcha textarea {\nmin-height: 120px;\n}\n.textarea {\nposition: relative;\n}\n#char-count {\ncolor: #000;\nbackground: hsla(0, 0%, 100%, .5);\nfont-size: 8pt;\nposition: absolute;\nbottom: 1px;\nright: 1px;\npointer-events: none;\n}\n#char-count.warning {\ncolor: red;\n}\n.captcha-img {\nbackground: #FFF;\noutline: 1px solid #CCC;\noutline-offset: -1px;\n}\n.captcha-img > img {\ndisplay: block;\nheight: 57px;\nwidth: 300px;\n}\n#file-n-submit-container {\nposition: relative;\n}\n#file-n-submit {\nposition: absolute;\ntop: 0;\nright: 0;\nbottom: 0;\nleft: 0;\ndisplay: flex;\nalign-items: center;\n}\n#file-n-submit-container input[type='file'] {\n/* Keep it to set an appropriate height to the container. */\nvisibility: hidden;\n}\n#file-n-submit-container input {\nmargin: 0;\n}\n#file-n-submit input[type='submit'] {\norder: 1;\n}\n#file-n-submit.has-file #qr-no-file,\n#file-n-submit:not(.has-file) #qr-filename,\n#file-n-submit:not(.has-file) #qr-filesize,\n#file-n-submit:not(.has-file) #qr-file-spoiler,\n#file-n-submit:not(.has-file) #qr-filerm,\n#qr-filename:focus ~ #qr-filesize {\ndisplay: none;\n}\n#qr-no-file,\n#qr-filename,\n#qr-filesize,\n#qr-filerm,\n#qr-file-spoiler {\nmargin: 0 2px !important;\n}\n#qr-no-file {\ncursor: default;\nflex: 1;\n}\n#qr-filename {\n-webkit-appearance: none;\n-moz-appearance: none;\nappearance: none;\nbackground: none;\nborder: none !important;\ncolor: inherit;\nfont: inherit;\nflex: 1;\ntext-overflow: ellipsis;\n}\n#qr-filesize {\nfont-size: .8em;\n}\n#qr-filesize::before {\ncontent: \" (\";\n}\n#qr-filesize::after {\ncontent: \")\";\n}\n\n/* Menu */\n.menu-button {\nposition: relative;\n}\n.menu-button i:not(.fa-reorder) {\nborder-top:   6px solid;\nborder-right: 4px solid transparent;\nborder-left:  4px solid transparent;\ndisplay: inline-block;\nmargin: 2px;\nvertical-align: middle;\n}\n@media screen and (resolution: 1dppx) {\n.fa-reorder {\n  font-size: 14px;\n}\n#shortcuts .fa-reorder {\n  vertical-align: -1px;\n}\n}\n#menu {\nborder-bottom: 0;\ndisplay: flex;\nmargin: 2px 0;\nflex-direction: column;\nposition: absolute;\noutline: none;\n}\n#menu.top {\ntop: 100%;\n}\n#menu.bottom {\nbottom: 100%;\n}\n#menu.left {\nleft: 0;\n}\n#menu.right {\nright: 0;\n}\n.entry {\ncursor: pointer;\noutline: none;\npadding: 3px 7px;\nposition: relative;\ntext-decoration: none;\nwhite-space: nowrap;\n}\n.entry.disabled {\ncolor: graytext !important;\n}\n.entry.has-submenu {\npadding-right: 20px;\n}\n.has-submenu::after {\ncontent: '';\nborder-left:   6px solid;\nborder-top:    4px solid transparent;\nborder-bottom: 4px solid transparent;\ndisplay: inline-block;\nmargin: 4px;\nposition: absolute;\nright: 3px;\n}\n.has-submenu:not(.focused) > .submenu {\ndisplay: none;\n}\n.submenu {\nborder-bottom: 0;\ndisplay: flex;\nflex-direction: column;\nposition: absolute;\nmargin: -1px 0;\n}\n.submenu.top {\ntop: 0;\n}\n.submenu.bottom {\nbottom: 0;\n}\n.submenu.left {\nleft: 100%;\n}\n.submenu.right {\nright: 100%;\n}\n.entry input {\nmargin: 0;\n}\n\n/* colored uid */\n\n.posteruid.painted  {\npadding: 0 5px;\nborder-radius: 1em;\nfont-size: 0.8em;\ncursor: pointer;\n}\n\n/* General */\n:root.yotsuba .dialog {\nbackground-color: #F0E0D6;\nborder-color: #D9BFB7;\n}\n:root.yotsuba .field:focus {\nborder-color: #EA8;\n}\n\n/* Header */\n:root.yotsuba #header-bar {\nfont-size: 9pt;\ncolor: #B86;\n}\n:root.yotsuba #header-bar a {\ncolor: #800000;\n}\n\n/* Settings */\n:root.yotsuba #fourchanx-settings fieldset {\nborder-color: #D9BFB7;\n}\n\n/* Quote */\n:root.yotsuba .backlink.deadlink {\ncolor: #00E !important;\n}\n:root.yotsuba .inline {\nborder-color: #D9BFB7;\nbackground-color: rgba(255, 255, 255, .14);\n}\n\n/* QR */\n:root.yotsuba #qr select {\ncolor: #00E;\n}\n:root.yotsuba #qr select:hover {\ncolor: red;\n}\n.yotsuba #dump-list::-webkit-scrollbar-thumb {\nbackground-color: #F0E0D6;\nborder-color: #D9BFB7;\n}\n:root.yotsuba .qr-preview {\nbackground-color: rgba(0, 0, 0, .15);\n}\n\n/* Menu */\n:root.yotsuba #menu {\ncolor: #800000;\n}\n:root.yotsuba .entry {\nborder-bottom: 1px solid #D9BFB7;\nfont-size: 10pt;\n}\n:root.yotsuba .focused.entry {\nbackground: rgba(255, 255, 255, .33);\n}\n\n/* General */\n:root.yotsuba-b .dialog {\nbackground-color: #D6DAF0;\nborder-color: #B7C5D9;\n}\n:root.yotsuba-b .field:focus {\nborder-color: #98E;\n}\n\n/* Header */\n:root.yotsuba-b #header-bar {\nfont-size: 9pt;\ncolor: #89A;\n}\n:root.yotsuba-b #header-bar a {\ncolor: #34345C;\n}\n\n/* Settings */\n:root.yotsuba-b #fourchanx-settings fieldset {\nborder-color: #B7C5D9;\n}\n\n/* Quote */\n:root.yotsuba-b .backlink.deadlink {\ncolor: #34345C !important;\n}\n:root.yotsuba-b .inline {\nborder-color: #B7C5D9;\nbackground-color: rgba(255, 255, 255, .14);\n}\n\n/* QR */\n:root.yotsuba-b #qr select {\ncolor: #34345C;\n}\n:root.yotsuba-b #qr select:hover {\ncolor: #DD0000;\n}\n.yotsuba-b #dump-list::-webkit-scrollbar-thumb {\nbackground-color: #D6DAF0;\nborder-color: #B7C5D9;\n}\n:root.yotsuba-b .qr-preview {\nbackground-color: rgba(0, 0, 0, .15);\n}\n\n/* Menu */\n:root.yotsuba-b #menu {\ncolor: #000;\n}\n:root.yotsuba-b .entry {\nborder-bottom: 1px solid #B7C5D9;\nfont-size: 10pt;\n}\n:root.yotsuba-b .focused.entry {\nbackground: rgba(255, 255, 255, .33);\n}\n\n/* General */\n:root.futaba .dialog {\nbackground-color: #F0E0D6;\nborder-color: #D9BFB7;\n}\n:root.futaba .field:focus {\nborder-color: #EA8;\n}\n\n/* Header */\n:root.futaba #header-bar {\nfont-size: 11pt;\ncolor: #B86;\n}\n:root.futaba #header-bar a {\ncolor: #800000;\n}\n\n/* Settings */\n:root.futaba #fourchanx-settings fieldset {\nborder-color: #D9BFB7;\n}\n\n/* Quote */\n:root.futaba .backlink.deadlink {\ncolor: #00E !important;\n}\n:root.futaba .inline {\nborder-color: #D9BFB7;\nbackground-color: rgba(255, 255, 255, .14);\n}\n\n/* QR */\n:root.futaba #qr select {\ncolor: #00E;\n}\n:root.futaba #qr select:hover {\ncolor: red;\n}\n.futaba #dump-list::-webkit-scrollbar-thumb {\nbackground-color: #F0E0D6;\nborder-color: #D9BFB7;\n}\n:root.futaba .qr-preview {\nbackground-color: rgba(0, 0, 0, .15);\n}\n\n/* Menu */\n:root.futaba #menu {\ncolor: #800000;\n}\n:root.futaba .entry {\nborder-bottom: 1px solid #D9BFB7;\nfont-size: 12pt;\n}\n:root.futaba .focused.entry {\nbackground: rgba(255, 255, 255, .33);\n}\n\n/* General */\n:root.burichan .dialog {\nbackground-color: #D6DAF0;\nborder-color: #B7C5D9;\n}\n:root.burichan .field:focus {\nborder-color: #98E;\n}\n\n/* Header */\n:root.burichan #header-bar {\nfont-size: 11pt;\ncolor: #89A;\n}\n:root.burichan #header-bar a {\ncolor: #34345C;\n}\n\n/* Settings */\n:root.burichan #fourchanx-settings fieldset {\nborder-color: #B7C5D9;\n}\n\n/* Quote */\n:root.burichan .backlink.deadlink {\ncolor: #34345C !important;\n}\n:root.burichan .inline {\nborder-color: #B7C5D9;\nbackground-color: rgba(255, 255, 255, .14);\n}\n\n/* QR */\n:root.burichan #qr select {\ncolor: #34345C;\n}\n:root.burichan #qr select:hover {\ncolor: #DD0000;\n}\n.burichan #dump-list::-webkit-scrollbar-thumb {\nbackground-color: #D6DAF0;\nborder-color: #B7C5D9;\n}\n:root.burichan .qr-preview {\nbackground-color: rgba(0, 0, 0, .15);\n}\n\n/* Menu */\n:root.burichan #menu {\ncolor: #000000;\n}\n:root.burichan .entry {\nborder-bottom: 1px solid #B7C5D9;\nfont-size: 12pt;\n}\n:root.burichan .focused.entry {\nbackground: rgba(255, 255, 255, .33);\n}\n\n/* General */\n:root.tomorrow .dialog {\nbackground-color: #282A2E;\nborder-color: #111;\n}\n:root.tomorrow .field:focus {\nborder-color: #000;\n}\n\n/* Header */\n:root.tomorrow #header-bar {\nfont-size: 9pt;\ncolor: #C5C8C6;\n}\n:root.tomorrow #header-bar a {\ncolor: #81A2BE;\n}\n\n/* Settings */\n:root.tomorrow #fourchanx-settings fieldset {\nborder-color: #111;\n}\n\n/* Quote */\n:root.tomorrow .backlink.deadlink {\ncolor: #81A2BE !important;\n}\n:root.tomorrow .inline {\nborder-color: #111;\nbackground-color: rgba(0, 0, 0, .14);\n}\n\n/* QR */\n:root.tomorrow #qr select {\ncolor: #81A2BE;\n}\n:root.tomorrow #qr select:hover {\ncolor: #5F89AC;\n}\n.tomorrow #dump-list::-webkit-scrollbar-thumb {\nbackground-color: #282A2E;\nborder-color: #111;\n}\n:root.tomorrow .qr-preview {\nbackground-color: rgba(255, 255, 255, .15);\n}\n\n/* Menu */\n:root.tomorrow #menu {\ncolor: #C5C8C6;\n}\n:root.tomorrow .entry {\nborder-bottom: 1px solid #111;\nfont-size: 10pt;\n}\n:root.tomorrow .focused.entry {\nbackground: rgba(0, 0, 0, .33);\n}\n\n/* General */\n:root.photon .dialog {\nbackground-color: #DDD;\nborder-color: #CCC;\n}\n:root.photon .field:focus {\nborder-color: #EA8;\n}\n\n/* Header */\n:root.photon #header-bar {\nfont-size: 9pt;\ncolor: #333;\n}\n:root.photon #header-bar a {\ncolor: #F60;\n}\n\n/* Settings */\n:root.photon #fourchanx-settings fieldset {\nborder-color: #CCC;\n}\n\n/* Quote */\n:root.photon .backlink.deadlink {\ncolor: #F60 !important;\n}\n:root.photon .inline {\nborder-color: #CCC;\nbackground-color: rgba(255, 255, 255, .14);\n}\n\n/* QR */\n:root.photon #qr select {\ncolor: #F60;\n}\n:root.photon #qr select:hover {\ncolor: #FF3300;\n}\n.photon #dump-list::-webkit-scrollbar-thumb {\nbackground-color: #DDD;\nborder-color: #CCC;\n}\n:root.photon .qr-preview {\nbackground-color: rgba(0, 0, 0, .15);\n}\n\n/* Menu */\n:root.photon #menu {\ncolor: #333;\n}\n:root.photon .entry {\nborder-bottom: 1px solid #CCC;\nfont-size: 10pt;\n}\n:root.photon .focused.entry {\nbackground: rgba(255, 255, 255, .33);\n}\n"
  };

  Main.init();

}).call(this);
